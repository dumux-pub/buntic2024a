// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_VE_TIMEMEASURER_HH
#define DUMUX_VE_TIMEMEASURER_HH

#include <chrono>
#include <iomanip>

namespace Dumux {

namespace TimeMeasuringDetail {
constexpr bool enableTimeMeasuringZp                   = true;
constexpr bool enableTimeMeasuringCoarseMobilities     = true;
constexpr bool enableTimeMeasuringCouplingFluxesVEToFD = true;
constexpr bool enableTimeMeasuringCouplingFluxesFDToVE = true;
constexpr bool enableTimeMeasuringAdaptivityCriteria   = true;
}; //end namespace TimeMeasuringDetail


class TimeMeasuring 
{
public:
    static constexpr auto enableTimeMeasuringZp = TimeMeasuringDetail::enableTimeMeasuringZp;
    static constexpr auto enableTimeMeasuringCoarseMobilities = TimeMeasuringDetail::enableTimeMeasuringCoarseMobilities;
    static constexpr auto enableTimeMeasuringCouplingFluxesVEToFD = TimeMeasuringDetail::enableTimeMeasuringCouplingFluxesVEToFD;
    static constexpr auto enableTimeMeasuringCouplingFluxesFDToVE = TimeMeasuringDetail::enableTimeMeasuringCouplingFluxesFDToVE;
    static constexpr auto enableTimeMeasuringAdaptivityCriteria = TimeMeasuringDetail::enableTimeMeasuringAdaptivityCriteria;

    TimeMeasuring()
    {
        totalTimeZp_                   = 0.0;
        totalTimeCoarseLambdas_        = 0.0;
        totalTimeCouplingFluxesVEtoFD_ = 0.0;
        totalTimeCouplingFluxesFDToVE_ = 0.0;
        totalTimeAdaptivtyCriteria_    = 0.0;

        //assign NaN to signal that this time was not measured instead of having the impression that it took 0 seconds
        if(!enableTimeMeasuringZp)
            totalTimeZp_ = std::numeric_limits<double>::quiet_NaN();
        if(!enableTimeMeasuringCoarseMobilities)
            totalTimeCoarseLambdas_ = std::numeric_limits<double>::quiet_NaN();
        if(!enableTimeMeasuringCouplingFluxesVEToFD && !enableTimeMeasuringCouplingFluxesFDToVE)
        {
            totalTimeCouplingFluxesVEtoFD_ = std::numeric_limits<double>::quiet_NaN();
            totalTimeCouplingFluxesFDToVE_ = std::numeric_limits<double>::quiet_NaN();
        }
        if(!enableTimeMeasuringAdaptivityCriteria)
            totalTimeAdaptivtyCriteria_ = std::numeric_limits<double>::quiet_NaN();
    }

    void addToTotalTimeZp(const double& singleTimeDiff)
    {
        totalTimeZp_ += singleTimeDiff;
    }

    void addToTotalTimeCoarseLambdas(const double& singleTimeDiff)
    {
        totalTimeCoarseLambdas_ += singleTimeDiff;
    }

    void addToTotalTimeCouplingFluxesVEToFD(const double& singleTimeDiff)
    {
        totalTimeCouplingFluxesVEtoFD_ += singleTimeDiff;
    }

    void addToTotalTimeCouplingFluxesFDToVE(const double& singleTimeDiff)
    {
        totalTimeCouplingFluxesFDToVE_ += singleTimeDiff;
    }

    void addToTotalTimeAdaptivityCriteria(const double& singleTimeDiff)
    {
        totalTimeAdaptivtyCriteria_ += singleTimeDiff;
    } 

    double getTotalVETime() const
    {
        return (totalTimeZp_ + totalTimeCoarseLambdas_ + totalTimeCouplingFluxesVEtoFD_ + totalTimeCouplingFluxesFDToVE_ + totalTimeAdaptivtyCriteria_)/1000000.0;
    }


    void printAllTimes(const double& averageIterationNumberZp) const
    {
        std::cout << std::fixed << std::setprecision(5);
        std::cout << "-------------SUMMARIZING ALL VE RELEVANT RUNTIMES-------------" << std::endl;

        std::cout << "\n- Gas plume distance zp was computed via        ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeZp_/1000000.0 << " seconds." << std::endl;
        std::cout << "- Gas plume distance zp needed                                                                     " << averageIterationNumberZp << " iterations on average for converging." << std::endl;


        std::cout << "- Coarse mobilities (lambdas) were computed via ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeCoarseLambdas_/1000000.0 << " seconds." << std::endl;


        std::cout << "- Coupling fluxes FD to VE were computed via    ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeCouplingFluxesFDToVE_/1000000.0 << " seconds." << std::endl;   
        std::cout << "- Coupling fluxes VE to FD were compute via     ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeCouplingFluxesVEtoFD_/1000000.0 << " seconds." << std::endl;
                     // "- Coupling fluxes FD to VE were computed via    >deterministic approach< and the computation took: "
        std::cout << "    -> In total, the coupling flux computation took:                                               " << (totalTimeCouplingFluxesFDToVE_+totalTimeCouplingFluxesVEtoFD_)/1000000.0 << " seconds." << std::endl;

        std::cout << "- Adaptivity criteria were computed via         ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeAdaptivtyCriteria_/1000000.0 << " seconds." << std::endl;

        std::cout << "- Secondaries everywhere were computed via      ";
        std::cout << ">deterministic approach<. ";
        std::cout << std::endl;
    }



    void printAllTimesForPureVE(const double& averageIterationNumberZp) const
    {
        std::cout << std::fixed << std::setprecision(5);
        std::cout << "-------------SUMMARIZING ALL VE RELEVANT RUNTIMES-------------" << std::endl;

        std::cout << "\n- Gas plume distance zp was computed via        ";

        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeZp_/1000000.0 << " seconds." << std::endl;
        std::cout << "- Gas plume distance zp needed                                                                     " << averageIterationNumberZp << " iterations on average for converging." << std::endl;


        std::cout << "- Coarse mobilities (lambdas) were computed via ";
        std::cout << ">deterministic approach< ";
        std::cout << "and the computation took: " << totalTimeCoarseLambdas_/1000000.0 << " seconds." << std::endl;

        std::cout << "- Secondaries everywhere were computed via      ";
        std::cout << ">deterministic approach<. ";
        std::cout << std::endl;
    }

private:
    double totalTimeZp_, totalTimeCoarseLambdas_, totalTimeCouplingFluxesVEtoFD_, totalTimeCouplingFluxesFDToVE_, totalTimeAdaptivtyCriteria_;
};

} // end namespace Dumux

#endif
