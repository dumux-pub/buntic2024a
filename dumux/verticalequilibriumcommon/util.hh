// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Reconstructs quantities from the coarse scale to the fine scale for the incompressible 2p test.
 */
#ifndef DUMUX_TWOPVE_UTIL_HH
#define DUMUX_TWOPVE_UTIL_HH

#include <string>
#include <cmath>
#include <stdexcept>
#include <iostream>

#include <cstdio>
#include <vector>
#include <fstream>
#include <iomanip>

#include <dumux/discretization/cellcentered/tpfa/fvelementgeometry.hh>
#include <dumux/discretization/localview.hh>
#include <dumux/porousmediumflow/velocityoutput.hh>

namespace Dumux {

namespace VerticalEquilibriumModelUtil {


    /*!
     * \brief Initializes the coarse grid
     *
     * Sets the number of vertical cells to 1 while keeping the coordinates of the top-right corner of the domain and the cells in horizontal direction
     *
     * \param gridManagerCoarse gridManager belonging to the coarse grid
     */
    template<class TypeTag>
    void initializeCoarseGrid(GridManager<GetPropType<TypeTag, Properties::Grid>>& gridManagerCoarse)
    {
        using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        constexpr int dim = GridView::dimension;

        const GlobalPosition lowerLeftCoarse = getParam<GlobalPosition>("Grid.LowerLeft");
        const GlobalPosition upperRightCoarse = getParam<GlobalPosition>("Grid.UpperRight");
        std::array<int, dim> cellsCoarse = getParam<std::array<int, dim>>("Grid.Cells");
        cellsCoarse[dim-1] = 1;
        gridManagerCoarse.init(lowerLeftCoarse, upperRightCoarse, cellsCoarse);
    }


    /*!
     * \brief Initializes the coarse grid
     *
     * Sets the number of vertical cells to 1 while keeping the coordinates of the top-right corner of the domain and the cells in horizontal direction
     *
     * \param gridManagerCoarse gridManager belonging to the coarse grid
     */
    template<class TypeTag>
    void initializeFineGrid(GridManager<GetPropType<TypeTag, Properties::Grid>>& gridManagerFine)
    {
        using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        constexpr int dim = GridView::dimension;

        const GlobalPosition lowerLeft = getParam<GlobalPosition>("Grid.LowerLeft");
        const GlobalPosition upperRight = getParam<GlobalPosition>("Grid.UpperRight");
        std::array<int, dim> cellsFine = getParam<std::array<int, dim>>("Grid.FineCells");
        gridManagerFine.init(lowerLeft, upperRight, cellsFine);
    }


    /*!
     * \brief Returns a multimap which maps coarse-scale element indices to the fine-grid elements
     *
     * Traverses all elements of the fine-scale grid and maps all elements belonging to one column of the domain to that column index. Key: coarse-scale element idx, Value: fine-scale element
     *
     * \param gridGeometryFine gridGeometry of the fine-scale grid
     * \param gridGeometryCoarse gridGeometry of the coarse-scale grid
     */
    template<typename Element, typename GridGeometry>
    const std::multimap<int, Element> createVEMultiMapCoarseToFine(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                                                   std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        int numberVEColumns = gridGeometryCoarse->numScv() - 1;
        int coarseElementCounter = 0;

        std::multimap<int, Element> mapColumns;
        for (const auto& element : Dune::elements(gridGeometryFine->gridView()))
        {
            //use fact that the (fine-scale) grid is traversed first in x-direction then in y direction
            if(coarseElementCounter>numberVEColumns)
                coarseElementCounter = 0; //once the x-direction has been traversed, start over again
            mapColumns.insert(std::make_pair(coarseElementCounter, element));
            coarseElementCounter++;
        }

        return mapColumns;
    }


    /*!
     * \brief Returns a multimap which maps fine-scale indices to the coarse-grid elements
     *
     * Traverses all elements of the fine-scale grid and maps the respective coare-scale element to this fine-scale element index
     * This is of type std::map as each key (elementIdx on fine scale) occurs exactly once. Key: fine-scale element idx, Value: coarse-scale element
     *
     * \param gridGeometryFine gridGeometry of the fine-scale grid
     * \param gridGeometryCoarse gridGeometry of the coarse-scale grid
     */
    template<typename Element, typename GridGeometry>
    const std::map<int, Element> createVEMultiMapFineToCoarse(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                                              std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        int numberVEElements = gridGeometryCoarse->numScv();
        int numberVEFineElements = gridGeometryFine->numScv();
        int fineElementsPerColumn = -1;
        int coarseElementCounter = 0;

        //check if sizes of fine and coarse scale can match
        if(numberVEFineElements%numberVEElements != 0)
        {
            throw std::invalid_argument("The number of fine VE elements must be an integral multiple of the number of coarse VE elements!");
        }
        else
        {
            fineElementsPerColumn = numberVEFineElements/numberVEElements;
        }

        std::map<int, Element> mapColumns{};
        for (const auto& elementCoarse : Dune::elements(gridGeometryCoarse->gridView()))
        {
            for(int i = 0; i < fineElementsPerColumn; i++)
            {
                int fineElementIdx = coarseElementCounter + i*numberVEElements;
                mapColumns.insert(std::make_pair(fineElementIdx, elementCoarse));
            }
            coarseElementCounter++;
        }

        return mapColumns;
    }


    /*!
     * \brief Returns a multimap which maps the virtual coarse-scale element indices to the fullD elements
     *
     * Traverses all elements of the fullD grid and maps all elements belonging to one column of the domain to that column index
     *
     * \param gridGeometryFullD gridGeometry of the fullD grid
     * \param numberOfVerticalCellsFullD number of vertical cells in the fullD grid
     */
    template<typename Element, typename GridGeometry, typename Scalar>
    const std::multimap<int, Element> createFullDMultiMapCoarseToFine(const GridGeometry& gridGeometryFullD,
                                                                      std::string groupName = "",
                                                                      bool verbose = false)
    {
        const auto upperRightCoords = getParamFromGroup<std::vector<Scalar>>(groupName, "Grid.UpperRight");
        const auto lowerLeftCoords = getParamFromGroup<std::vector<Scalar>>(groupName, "Grid.LowerLeft");
        auto numCells = getParamFromGroup<std::vector<Scalar>>(groupName, "Grid.Cells");

        std::multimap<int, Element> mapColumnsFullD{};
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;

        int xCells = numCells[0];
        const Scalar xWidth = upperRightCoords[0] - lowerLeftCoords[0];
        Scalar deltaX = xWidth/xCells;

        if constexpr(dim==2) // 2D
        {
            for (const auto& element : Dune::elements(gridGeometryFullD.gridView()))
            {
                int columnIdx = (int)std::round((element.geometry().center()[0] - 0.5*deltaX)/deltaX);
                mapColumnsFullD.insert(std::make_pair(columnIdx, element));
            }

            if(verbose)
            {
                //iterate over fullD elements via
                for(int columnIdx = 0; columnIdx<xCells; columnIdx++)
                {
                    std::cout << "For columnidx: " << columnIdx << " we have element: " << std::endl;
                    const auto range = mapColumnsFullD.equal_range(columnIdx);

                    int counterColumnElements = 0;
                    for(auto iterator = range.first; iterator!=range.second; iterator++)
                    {
                        counterColumnElements++;
                        const auto eIdx = gridGeometryFullD.elementMapper().index(iterator->second);
                        std::cout << "    " << eIdx << " at pos: " << (iterator->second).geometry().center() << std::endl;
                    }

                    //sanity check
                    if(counterColumnElements != numCells[dim-1])
                    {
                        std::ostringstream errMsg;
                        errMsg << "A column must always contain as many elements as there are cells in vertical direction. We counted: " << counterColumnElements << " but expected: " << numCells[dim-1] << std::endl;
                        throw std::length_error(errMsg.str());
                    }
                }
            }

            return mapColumnsFullD;
        }
        else if constexpr(dim==3) // 3D
        {
            const Scalar yWidth = upperRightCoords[1] - lowerLeftCoords[1];
            int yCells = numCells[1];
            Scalar deltaY = yWidth/yCells;

            for (const auto& element : Dune::elements(gridGeometryFullD.gridView()))
            {
                int idxX = (int)std::round((element.geometry().center()[0] - 0.5*deltaX)/deltaX);
                int idxY = (int)std::round((element.geometry().center()[1] - 0.5*deltaY)/deltaY);
                int columnIdx = idxY*xCells + idxX; //returns a global column index
                mapColumnsFullD.insert(std::make_pair(columnIdx, element));
            }

            if(verbose)
            {
                //iterate over fullD elements via
                for(int columnIdx = 0; columnIdx<xCells*yCells; columnIdx++)
                {
                    std::cout << "For columnidx: " << columnIdx << " we have element: " << std::endl;
                    const auto range = mapColumnsFullD.equal_range(columnIdx);

                    int counterColumnElements = 0;
                    for(auto iterator = range.first; iterator!=range.second; iterator++)
                    {
                        counterColumnElements++;
                        const auto eIdx = gridGeometryFullD.elementMapper().index(iterator->second);
                        std::cout << "    " << eIdx << " at pos: " << (iterator->second).geometry().center() << std::endl;
                    }

                    //sanity check
                    if(counterColumnElements != numCells[dim-1])
                    {
                        std::ostringstream errMsg;
                        errMsg << "A column must always contain as many elements as there are cells in vertical direction. We counted: " << counterColumnElements << " but expected: " << numCells[dim-1] << std::endl;
                        throw std::length_error(errMsg.str());
                    }
                }
            }

            return mapColumnsFullD;
        }
    }



    /*!
     * \brief Print a multimap that has int as keys and elements as values
     *
     * \param mmap multimap
     */
    template <typename Element>
    void printMultimap(const std::multimap<int, Element>& mmap) {
        if (mmap.empty()) {
            std::cout << "The multimap is empty." << std::endl;
            return;
        }

        auto it = mmap.cbegin();
        while (it != mmap.cend()) {
            int key = it->first;

            // Get the range of elements matching the current key
            auto range = mmap.equal_range(key);

            // Print the key
            std::cout << key << ":\n";

            // Collect and print all values associated with the key
            for (auto valIt = range.first; valIt != range.second; ++valIt) {
                // std::cout << valIt->second;
                std::cout << "    " << (valIt->second).geometry().center() << std::endl;
                // auto nextIt = std::next(valIt);
                // if (nextIt != range.second) {
                //     std::cout << ", \n";
                // }
            }
            std::cout << std::endl;

            // Move to the next group of keys
            it = range.second;
        }
    }


    /*!
     * \brief Print a map that has int as keys and elements as values
     *
     * \param m map
     */
    template <typename Element>
    void printMap(const std::map<int, Element>& m) {
        if (m.empty()) {
            std::cout << "The map is empty." << std::endl;
            return;
        }

        for (const auto& pair : m) {
            std::cout << pair.first << " -> " << (pair.second).geometry().center() << std::endl;
        }
    }



    /*!
     * \brief Returns a reverse multimap which maps the fullD element indices to their virtual coarse-scale element indices
     *
     * Traverses all elements of the fullD grid and maps all elements belonging to one column of the domain to that column index
     *
     * \param gridGeometryFullD gridGeometry of the fullD grid
     * \param numberOfVerticalCellsFullD number of vertical cells in the fullD grid
     */
    template<typename Element, typename GridGeometry, typename Scalar>
    const std::multimap<int, int> createFullDMultiMapFineToCoarse(std::shared_ptr<const GridGeometry> gridGeometryFullD,
                                                                  const Scalar deltaX,
                                                                  const Scalar deltaY,
                                                                  std::string groupName = "")
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;
        std::multimap<int, int> mapColumnsFullDReverse{};
        auto numCells = getParamFromGroup<std::vector<Scalar>>(groupName, "Grid.Cells");
        int xCells = numCells[0];

        for (const auto& element : Dune::elements(gridGeometryFullD->gridView()))
        {
            const auto elementIdx = gridGeometryFullD->elementMapper().index(element);
            int columnIdx = -1;

            if constexpr(dim==2)
                columnIdx = std::round((element.geometry().center()[0] - 0.5*deltaX)/deltaX);
            else if constexpr(dim==3)
            {
                int idxX = (int)std::round((element.geometry().center()[0] - 0.5*deltaX)/deltaX);
                int idxY = (int)std::round((element.geometry().center()[1] - 0.5*deltaY)/deltaY);
                columnIdx = idxY*xCells + idxX;
            }
            mapColumnsFullDReverse.insert(std::make_pair(elementIdx, columnIdx));
        }

        return mapColumnsFullDReverse;
    }


    /*!
     * \brief Print the pairs of coarse-scale element indices and mapped fine-scale elements as well as the fine-scale element indices and the mapped coarse-scale elements
     *
     * \param problem coarse- or fine-scale problem
     * \param gridGeometryFine gridGeometry of the fine-scale grid
     * \param gridGeometryCoarse gridGeometry of the coarse-scale grid
     */
    template<class CriterionManager, typename GridGeometry>
    void printColumnMapFullD(const CriterionManager& criterionManagerFullD,
                             std::shared_ptr<GridGeometry> gridGeometryFullD)
    {
        std::cout << "Printing the column Map for fullD problem: " << std::endl;
        std::cout << "size of multimap: " << criterionManagerFullD.getMapColumns()->size() << std::endl;

        //connectivity map: column indices -> fullD elements
        std::cout << std::endl;
        std::cout << "---------------Printing column index keys and mapped fullD elements.---------------" << std::endl;
        for(auto columnIdxIterator = criterionManagerFullD.getMapColumns()->begin(); columnIdxIterator != criterionManagerFullD.getMapColumns()->end(); )
        {

            int idx = columnIdxIterator->first;
            std::cout << "Column index key: " << idx <<  " mapped fullD elems are: " << std::endl;
            const auto range = criterionManagerFullD.getMapColumns()->equal_range(idx);

            for(auto iterator = range.first; iterator!=range.second; iterator++)
            {
                std::cout << "   " << gridGeometryFullD->elementMapper().index(iterator->second) << " @pos: " << (iterator->second).geometry().center() << std::endl;
            }

           //avoid iterating over duplicate keys
           do
           {
               ++columnIdxIterator;
           }
           while (columnIdxIterator != criterionManagerFullD.getMapColumns()->end() && idx == columnIdxIterator->first);
        }
    }


    /*!
     * \brief Print the pairs of coarse-scale element indices and mapped fine-scale elements as well as the fine-scale element indices and the mapped coarse-scale elements
     *
     * \param problem coarse- or fine-scale problem
     * \param gridGeometryFine gridGeometry of the fine-scale grid
     * \param gridGeometryCoarse gridGeometry of the coarse-scale grid
     */
    template<class Problem, typename GridGeometry>
    void printColumnMapVE(Problem& problem,
                          std::shared_ptr<const GridGeometry> gridGeometryFine,
                          std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        std::cout << "Printing the column Map for problem: " << problem.paramGroup() << std::endl;
        std::cout << "size of multimap: " << problem.getMapColumns()->size() << std::endl;

        //connectivity map: coarse element indices -> fine elements
        std::cout << std::endl;
        std::cout << "---------------Printing coarse keys and mapped fine elements.---------------" << std::endl;
        for(auto coarseKey = problem.getMapColumns()->begin(); coarseKey != problem.getMapColumns()->end(); )
        {

            int idx = coarseKey->first;
            std::cout << "Coarse key: " << coarseKey->first << " , coarse elem @pos: " << (gridGeometryCoarse->element(coarseKey->first)).geometry().center() <<  " mapped fine elems are: " << std::endl;
            const auto range = problem.returnCorrespondingFineCellsIterator(coarseKey->first);
            for(auto iterator = range.first; iterator!=range.second; iterator++)
            {
                std::cout << "   " << gridGeometryFine->elementMapper().index(iterator->second) << " @pos: " << (iterator->second).geometry().center() << std::endl;
            }

           //avoid iterating over duplicate keys
           do
           {
               ++coarseKey;
           }
           while (coarseKey != problem.getMapColumns()->end() && idx == coarseKey->first);
        }

        //connectivity map: fine element indices -> coarse elements
        std::cout << std::endl;
        std::cout << "---------------Printing fine keys and mapped coarse elements.---------------" << std::endl;
        for(auto fineKey = problem.getFineScaleMap()->begin(); fineKey != problem.getFineScaleMap()->end(); fineKey++)
        {
            std::cout << "TEST LINE" << std::endl;
            std::cout << "Fine key: " << fineKey->first;
            std::cout << " @pos: " << (gridGeometryFine->element(fineKey->first)).geometry().center();
            std::cout << " with elem: " << gridGeometryFine->elementMapper().index(problem.returnCorrespondingCoarseElement(fineKey->first));
            std::cout <<" @pos: " << (fineKey->second).geometry().center() << std::endl;
        }
    }


    /*!
     * \brief Print the fiven grid geometry
     *
     * \param gridGeom grid geometry
     * \param geomName name of the model the grid geometry belongs to
     */
    template<typename GridGeometry>
    void printGeom(GridGeometry& gridGeom,
                   std::string geomName)
    {
        std::cout << "--------------------PRINTING GRIDGEOMETRY FOR DOMAIN: " << geomName << " --------------------" << std::endl;
        std::cout << "The global GridGeometry scv size is: " << gridGeom.numScv() << " and the scvf size is: " << gridGeom.numScvf() << std::endl;
        std::cout << "bBoxMin() is: " << gridGeom.bBoxMin() << " and bBoxMax() is: " << gridGeom.bBoxMax() << std::endl;
        for (const auto& element : elements(gridGeom.gridView()))
        {
            std::cout << "ELEMENT center: " << element.geometry().center() << " with index: " << gridGeom.elementMapper().index(element) << std::endl;

            auto localFvGeometry = localView(gridGeom);
            localFvGeometry.bindElement(element);

            for (const auto& scvf : scvfs(localFvGeometry))
            {
                std::cout << "SCVF center: " << localFvGeometry.geometry(scvf).center() << " with index: " << scvf.index() << std::endl;
            }
        }
        std::cout << "====================END PRINTING GRIDGEOMETRY====================" << std::endl;
    }


    /*
     * Define here which constraints need to be fulfilled.
     */
    template<typename GridGeometry>
    void checkForExceptions()
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;
        using CellAray = typename std::array<unsigned int, dim>;

        //number of vertical cells for coarse VE domain has to be 1!-------------------------------------------------
        auto coarseCellsVE = getParamFromGroup<CellAray>("DarcyVE", "Grid.Cells");
        auto cellsZCheckVE = coarseCellsVE[dim-1];
        std::cout << "cellsZCheckVE: " << cellsZCheckVE << std::endl;

        if(cellsZCheckVE!=1)
        {
            throw std::invalid_argument("Vertical Equilibrium requires DarcyVE.Grid.Cell[1] to be exactly 1!");
        }


        //UpperRight and LowerLeft have to be equal for fullD and VE domain!-----------------------------------------
        auto lowerLeftVE = getParamFromGroup<CellAray>("DarcyVE", "Grid.LowerLeft");
        auto upperRightVE = getParamFromGroup<CellAray>("DarcyVE", "Grid.UpperRight");
        auto lowerLeftDarcy = getParamFromGroup<CellAray>("Darcy", "Grid.LowerLeft");
        auto upperRightDarcy = getParamFromGroup<CellAray>("Darcy", "Grid.UpperRight");

        for(size_t i=0; i<dim; i++)
        {
            if(lowerLeftVE[i]!=lowerLeftDarcy[i] || upperRightVE[i]!=upperRightDarcy[i])
            {
                std::ostringstream errMsg;
                errMsg << "Vertical Equilibrium domain and Darcy domain need to have the same Grid.LowerLeft and Grid.UpperRight coordinates! Differing in dimension: " << i+1 << std::endl;
                throw std::invalid_argument(errMsg.str());
            }
        }

        //DarcyVE.Grid.FineCells needs to be equal to the Darcy.Cells!-----------------------------------------
        auto fineCellsVE = getParamFromGroup<CellAray>("DarcyVE", "Grid.FineCells");
        auto cellsDarcy = getParamFromGroup<CellAray>("Darcy", "Grid.Cells");

        for(size_t i=0; i<dim; i++)
        {
            if(fineCellsVE[i]!=cellsDarcy[i])
            {
                std::ostringstream errMsg;
                errMsg << "DarcyVE.Grid.FineCells needs to be equal to the Darcy.Grid.Cells! Differing in dimension: " << i+1 << std::endl;
                throw std::invalid_argument(errMsg.str());
            }
        }

        //DarcyVE.Grid.FineCells and DarcyVE.Grid.Cells need the same number of cells in all dimension EXCEPT z-direction!---------
        for(size_t i=0; i<dim-1; i++)
        {
            if(coarseCellsVE[i]!=fineCellsVE[i])
            {
                std::ostringstream errMsg;
                errMsg << "DarcyVE.Grid.FineCells needs to be equal to the DarcyVE.Grid.Cells except the z-direction! Differing in dimension: " << i+1 << std::endl;
                throw std::invalid_argument(errMsg.str());
            }
        }


        //TODO: Do we also need coarseVE.Cells[0] == darcy.Cells[0]? For elementSelector?
    }


    /*
     * Define here which constraints need to be fulfilled for the lone VE model.
     */
    void checkForExceptionsOnlyVE()
    {
        //number of vertical cells for coarse VE domain has to be 1!-------------------------------------------------
        auto coarseCellsVE = getParam<std::vector<int>>("Grid.Cells");
        auto fineCellsVE = getParam<std::vector<int>>("Grid.FineCells");

        if(coarseCellsVE.size()==2)
        {
            if(coarseCellsVE[0] != fineCellsVE[0])
            {
                throw std::invalid_argument("Number of horizontal cells for the coarse and fine VE model need to be the same!");
            }
        }
        else if(coarseCellsVE.size()==3)
        {
            if((coarseCellsVE[0] != fineCellsVE[0]) || (coarseCellsVE[1] != fineCellsVE[1]))
            {
                throw std::invalid_argument("Number of planar horizontal cells for the coarse and fine VE model need to be the same!");
            }
        }
    }


    /*!
     * \brief Print out the masses of the wetting phase and the non-wetting phase in the coarse VE, in the fine VE and in the darcy domain
     *
     * \param totalInjectedGasMass reference to the variable that lives longer than this function for storing the cumulative injected gas over time
     * \param fvGridGeometryVE pointer to the gridGeometry of the coarse-scale grid
     * \param fvGridGeometryVEFine pointer to the gridGeometry of the fine-scale grid
     * \param solPointer pointer to the solution vector containing the solutions for the coarse VE and the fullD darcy domains
     * \param darcyVerticalEquilibriumProblem pointer to the problem of the coarse-scale VE domain
     * \param fvGridGeometryDarcy pointer to the gridGeometry of the fullD darcy grid
     * \param darcyProblem pointer to the problem of the fullD darcy domain
     * \param timeLoop pointer to the timeLoop
     */
    template<typename VolumeVariablesFullD, typename GridGeometry, typename MultiDomainSolutionVector, typename ProblemVE, typename ProblemDarcy, typename TimeLoop, typename Scalar, typename CouplingManager>
    void printMassBalance(double& expectedInjectedGasMass, //change value per reference
                          std::shared_ptr<const GridGeometry> fvGridGeometryVE,
                          std::shared_ptr<const GridGeometry> fvGridGeometryVEFine,
                          std::shared_ptr<MultiDomainSolutionVector> solPointer,
                          std::shared_ptr<ProblemVE> darcyVerticalEquilibriumProblem,
                          std::shared_ptr<const GridGeometry> fvGridGeometryDarcy,
                          std::shared_ptr<ProblemDarcy> darcyProblem,
                          std::shared_ptr<TimeLoop> timeLoop)
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;
        using CellAray = typename std::array<unsigned int, dim>;
        auto upperRightFullD = getParam<CellAray>("Darcy.Grid.UpperRight");
        auto lowerLeftFullD = getParam<CellAray>("Darcy.Grid.LowerLeft");
        auto injectionRate = getParam<Scalar>("BoundaryConditions.InjectionRate");
        constexpr auto darcyDomainIndex = CouplingManager::darcyDomainIndex;
        constexpr auto darcyVEDomainIndex = CouplingManager::darcyVEDomainIndex;
        VolumeVariablesFullD volVarsFullD; //initialize fullD darcy volVars

        Scalar expectedVolume = 1;
        for(int i=0; i<dim; i++)
            expectedVolume *= upperRightFullD[i] - lowerLeftFullD[i];

        //UpperRight of darcy and VE domain are supposed to be the same so far. Does not matter which domain's parameter we read in here
        Scalar domainHeight = upperRightFullD[dim-1] - lowerLeftFullD[dim-1]; //TODO: - lowerleft[1]

        //Using no volVars for VE domain
        std::cout << "\n ---------------------------------------info about mass conservation------------------------------------------------------------" << std::endl;
        //iterate over VE domain
        Scalar totalVolumeVE = 0.0;
        Scalar totalMassWVE = 0.0;
        Scalar totalMassNwVE = 0.0;

        //for fine-scale VE elements
        Scalar totalVolumeVEFine = 0.0;
        Scalar totalMassWVEFine = 0.0;
        Scalar totalMassNwVEFine = 0.0;

        Scalar myPorosity = getParam<Scalar>("SpatialParams.Porosity");
        Scalar dummyTemperature = 326.0; 

        auto fineSol = darcyVerticalEquilibriumProblem->getOtherProblem()->getFineSolution();

        //----------------------------------ITERATION OVER COARSE-SCALE ELEMENTS----------------------------------
        for (const auto& element : Dune::elements(fvGridGeometryVE->gridView()))
        {
            auto elementIdx = fvGridGeometryVE->elementMapper().index(element);

            auto satWCoarse = 1.0 - ((*solPointer)[darcyVEDomainIndex])[elementIdx][1];
            auto satNwCoarse = ((*solPointer)[darcyVEDomainIndex])[elementIdx][1];
            auto pWCoarse = ((*solPointer)[darcyVEDomainIndex])[elementIdx][0];

            auto testDensityW = darcyVerticalEquilibriumProblem->returnDensityLiquidAndViscosity(dummyTemperature, pWCoarse)[0];
            auto testDensityNw = darcyVerticalEquilibriumProblem->returnDensityGasAndViscosity(dummyTemperature, pWCoarse)[0];

            auto fvGeometryVE = localView(*fvGridGeometryVE);
            fvGeometryVE.bind(element);

            for (const auto& scvVE : scvs(fvGeometryVE))
            {
                //strictly, using scvVE.volume() is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
                totalVolumeVE += scvVE.volume();
                totalMassWVE += myPorosity * testDensityW * satWCoarse * scvVE.volume();
                totalMassNwVE += myPorosity * testDensityNw * satNwCoarse * scvVE.volume();
            }


            //----------------------------------ITERATION OVER FINE-SCALE ELEMENTS----------------------------------
            auto it = darcyVerticalEquilibriumProblem->getMapColumns()->lower_bound(elementIdx);

            for(; it != darcyVerticalEquilibriumProblem->getMapColumns()->upper_bound(elementIdx); it++)
            {
                auto elementIdxFine = fvGridGeometryVEFine->elementMapper().index(it->second);
                auto pWFine = (*fineSol)[elementIdxFine][0];

                auto testDensityWFine = darcyVerticalEquilibriumProblem->returnDensityLiquidAndViscosity(dummyTemperature, pWFine)[0];
                auto testDensityNwFine = darcyVerticalEquilibriumProblem->returnDensityGasAndViscosity(dummyTemperature, pWFine)[0];

                auto satWFine = 1.0 - (*fineSol)[elementIdxFine][1];
                auto satNwFine = (*fineSol)[elementIdxFine][1];

                auto fvGeometryVEFine = localView(*fvGridGeometryVEFine);
                fvGeometryVEFine.bind(it->second);

                for (const auto& scv : scvs(fvGeometryVEFine))
                {
                    totalVolumeVEFine += scv.volume();
                    totalMassWVEFine += myPorosity * testDensityWFine * satWFine * scv.volume();
                    totalMassNwVEFine += myPorosity * testDensityNwFine * satNwFine * scv.volume();
                }
            }
        }


        //for fullD
        Scalar totalVolumeFullD = 0.0;

        Scalar totalMassWFullD = 0.0;
        Scalar totalMassNwFullD = 0.0;

        for (const auto& element : Dune::elements(fvGridGeometryDarcy->gridView()))
        {
            auto fvGeometryFullD = localView(*fvGridGeometryDarcy);
            fvGeometryFullD.bind(element);

            const auto elemSolFullD = elementSolution(element, (*solPointer)[darcyDomainIndex], *fvGridGeometryDarcy);

            for (const auto& scv : scvs(fvGeometryFullD))
            {
                volVarsFullD.update(elemSolFullD, *darcyProblem, element, scv);

                totalVolumeFullD += scv.volume();
                totalMassWFullD += volVarsFullD.porosity() * volVarsFullD.density(0) * volVarsFullD.saturation(0) * scv.volume();
                totalMassNwFullD += volVarsFullD.porosity() * volVarsFullD.density(1) * volVarsFullD.saturation(1) * scv.volume();
            }
        }

        if constexpr(dim==2)
        {
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * (-injectionRate);
        }
        else if constexpr(dim==3)
        {
            auto numCells = getParam<CellAray>("Darcy.Grid.Cells");
            Scalar deltaY = (upperRightFullD[1]-lowerLeftFullD[1])/((double) numCells[1]);
            Scalar deltaZ = (upperRightFullD[dim-1]-lowerLeftFullD[dim-1])/((double) numCells[dim-1]);
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * deltaY * (-injectionRate) / (deltaY);
        }

        double errorCoarseScale = (expectedInjectedGasMass - (totalMassNwVE+totalMassNwFullD))/expectedInjectedGasMass;
        double errorFineScale = (expectedInjectedGasMass - (totalMassNwVEFine+totalMassNwFullD))/expectedInjectedGasMass;

        std::cout << "Total volume is: " << totalVolumeFullD+totalVolumeVE << ", one total on fine scale: " << totalVolumeFullD+totalVolumeVEFine<< " while expected: " << expectedVolume << std::endl;
        std::cout << "Expected total injected gas mass is: " << expectedInjectedGasMass << " while sum of gas masses in both domains is (coarse+fullD): " << totalMassNwVE+totalMassNwFullD<<  " and for (fine+fullD): " << totalMassNwVEFine+totalMassNwFullD << ". Error on FD+CoarseVE scale: " << errorCoarseScale << ", error on FD+fineVE scale: " << errorFineScale << std::endl;

        std::cout << "---------------------------------------------info end------------------------------------------------------------" << std::endl;

    }



    /*!
     * \brief Print out the masses of the wetting phase and the non-wetting phase in the coarse VE and in the fine VE domain. For usage in test_2pve.
     *
     * \param totalInjectedGasMass reference to the variable that lives longer than this function for storing the cumulative injected gas over time
     * \param fvGridGeometryVE pointer to the gridGeometry of the coarse-scale grid
     * \param fvGridGeometryVEFine pointer to the gridGeometry of the fine-scale grid
     * \param solPointer pointer to the solution vector containing the solution for the coarse VE domain
     * \param darcyVerticalEquilibriumProblem pointer to the problem of the coarse-scale VE domain
     * \param timeLoop pointer to the timeLoop
     */
    template<typename GridGeometry, typename SolutionVector, typename ProblemVE, typename TimeLoop, typename Scalar>
    void printMassBalanceVE(double& expectedInjectedGasMass, //change value per reference
                            std::shared_ptr<const GridGeometry> fvGridGeometryVE,
                            std::shared_ptr<const GridGeometry> fvGridGeometryVEFine,
                            std::shared_ptr<SolutionVector> solPointer,
                            std::shared_ptr<ProblemVE> darcyVerticalEquilibriumProblem,
                            std::shared_ptr<TimeLoop> timeLoop)
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;
        using CellAray = typename std::array<unsigned int, dim>;

        auto injectionRate = getParam<Scalar>("BoundaryConditions.InjectionRate");
        CellAray lowerLeft = getParam<CellAray>("Grid.LowerLeft");
        CellAray upperRight = getParam<CellAray>("Grid.UpperRight");

        //Using no volVars for VE domain
        std::cout << "\n ---------------------------------------info about mass conservation------------------------------------------------------------" << std::endl;
        //iterate over VE domain
        Scalar totalVolumeVE = 0.0;
        Scalar totalMassWVE = 0.0;
        Scalar totalMassNwVE = 0.0;

        //for fine-scale VE elements
        Scalar totalVolumeVEFine = 0.0;
        Scalar totalMassWVEFine = 0.0;
        Scalar totalMassNwVEFine = 0.0;

        Scalar myPorosity = getParam<Scalar>("SpatialParams.Porosity");

        Scalar dummyTemperature = 326.0; //TODO: do not hardcode the temperature here

        auto fineSol = darcyVerticalEquilibriumProblem->getOtherProblem()->getFineSolution();

        //----------------------------------ITERATION OVER COARSE-SCALE ELEMENTS----------------------------------
        for (const auto& element : Dune::elements(fvGridGeometryVE->gridView()))
        {
            auto elementIdx = fvGridGeometryVE->elementMapper().index(element);

            auto satWCoarse = 1.0 - (*solPointer)[elementIdx][1];
            auto satNwCoarse = (*solPointer)[elementIdx][1];
            auto pWCoarse = (*solPointer)[elementIdx][0];

            auto testDensityW = darcyVerticalEquilibriumProblem->returnDensityLiquidAndViscosity(dummyTemperature, pWCoarse)[0];
            auto testDensityNw = darcyVerticalEquilibriumProblem->returnDensityGasAndViscosity(dummyTemperature, pWCoarse)[0];

            auto fvGeometryVE = localView(*fvGridGeometryVE);
            fvGeometryVE.bind(element);

            for (const auto& scvVE : scvs(fvGeometryVE))
            {
                //strictly, using scvVE.volume() is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
                totalVolumeVE += scvVE.volume();
                totalMassWVE += myPorosity * testDensityW * satWCoarse * scvVE.volume();
                totalMassNwVE += myPorosity * testDensityNw * satNwCoarse * scvVE.volume();
            }

            //----------------------------------ITERATION OVER FINE-SCALE ELEMENTS----------------------------------
            auto it = darcyVerticalEquilibriumProblem->getMapColumns()->lower_bound(elementIdx);

            for(; it != darcyVerticalEquilibriumProblem->getMapColumns()->upper_bound(elementIdx); it++)
            {
                //TODO: this requires a rework, stop using quantityStorage
                auto elementIdxFine = fvGridGeometryVEFine->elementMapper().index(it->second);
                auto pWFine = (*fineSol)[elementIdxFine][0];

                auto testDensityWFine = darcyVerticalEquilibriumProblem->returnDensityLiquidAndViscosity(dummyTemperature, pWFine)[0];
                auto testDensityNwFine = darcyVerticalEquilibriumProblem->returnDensityGasAndViscosity(dummyTemperature, pWFine)[0];

                auto satWFine = 1.0 - (*fineSol)[elementIdxFine][1];
                auto satNwFine = (*fineSol)[elementIdxFine][1];

                auto fvGeometryVEFine = localView(*fvGridGeometryVEFine);
                fvGeometryVEFine.bind(it->second);

                for (const auto& scv : scvs(fvGeometryVEFine))
                {
                    totalVolumeVEFine += scv.volume();
                    totalMassWVEFine += myPorosity * testDensityWFine * satWFine * scv.volume();
                    totalMassNwVEFine += myPorosity * testDensityNwFine * satNwFine * scv.volume();
                }
            }
        }

        Scalar expectedVolume = 1.0;
        for(size_t i=0; i<dim; i++)
        {
            expectedVolume *= upperRight[i] - lowerLeft[i];
        }
        Scalar domainHeight = upperRight[dim-1] - lowerLeft[dim-1];

        if constexpr(dim==2)
        {
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * (-injectionRate);
        }
        else if constexpr(dim==3)
        {
            auto numCells = getParam<CellAray>("Grid.FineCells");
            Scalar deltaY = (upperRight[1]-lowerLeft[1])/((double) numCells[1]);
            Scalar deltaZ = (upperRight[dim-1]-lowerLeft[dim-1])/((double) numCells[dim-1]);
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * deltaY * (-injectionRate) / (deltaY); //last part is necessary as in params file we pass a mass instead of a mass per area, so the total mass stays constant for different discretizations
        }


        double errorCoarseScale = (expectedInjectedGasMass - (totalMassNwVE))/expectedInjectedGasMass;
        double errorFineScale = (expectedInjectedGasMass - (totalMassNwVEFine))/expectedInjectedGasMass;

        std::cout << "Total VE coarse volume is: " << totalVolumeVE << ", total VE fine volume is: " << totalVolumeVEFine << " while expected: " << expectedVolume << std::endl;
        std::cout << "Expected injected gas mass is: " << expectedInjectedGasMass << " while gas mass in VE coarse system is: " << totalMassNwVE<<  " and in VE fine system is: " << totalMassNwVEFine << ". Error on FD+CoarseVE scale: " << errorCoarseScale << ", error on FD+fineVE scale: " << errorFineScale << std::endl;

        std::cout << "---------------------------------------------info end------------------------------------------------------------" << std::endl;
    }


    /*!
     * \brief Print out the masses of the wetting phase and the non-wetting phase in the coarse VE and in the fine VE domain. For usage in test_2pve.
     *
     * \param totalInjectedGasMass reference to the variable that lives longer than this function for storing the cumulative injected gas over time
     * \param fvGridGeometryVE pointer to the gridGeometry of the coarse-scale grid
     * \param fvGridGeometryVEFine pointer to the gridGeometry of the fine-scale grid
     * \param solPointer pointer to the solution vector containing the solution for the coarse VE domain
     * \param darcyVerticalEquilibriumProblem pointer to the problem of the coarse-scale VE domain
     * \param timeLoop pointer to the timeLoop
     */
    template<typename GridGeometry, typename SolutionVector, typename Problem, typename TimeLoop, typename Scalar, typename VolumeVariablesFullD>
    void printMassBalanceFullD(double& expectedInjectedGasMass, //change value per reference
                               std::shared_ptr<const GridGeometry> gridGeometryFullD,
                               const SolutionVector& solVector,
                               std::shared_ptr<Problem> problemFullD,
                               std::shared_ptr<TimeLoop> timeLoop)
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;

        //for mass balance printing
        using CellAray = typename std::array<unsigned int, dim>;
        auto upperRightFullD = getParam<CellAray>("Grid.UpperRight");
        auto lowerLeftFullD = getParam<CellAray>("Grid.LowerLeft");
        auto injectionRate = getParam<Scalar>("BoundaryConditions.InjectionRate");

        VolumeVariablesFullD volVarsFullD;

        std::cout << "\n ---------------------------------------info about mass conservation------------------------------------------------------------" << std::endl;
        //for fullD
        Scalar totalVolumeFullD = 0.0;

        Scalar totalMassWFullD = 0.0;
        Scalar totalMassNwFullD = 0.0;

        for (const auto& element : Dune::elements(gridGeometryFullD->gridView()))
        {
            auto fvGeometryFullD = localView(*gridGeometryFullD);
            fvGeometryFullD.bind(element);

            const auto elemSolFullD = elementSolution(element, solVector, *gridGeometryFullD);

            for (const auto& scv : scvs(fvGeometryFullD))
            {
                volVarsFullD.update(elemSolFullD, *problemFullD, element, scv);

                totalVolumeFullD += scv.volume();
                totalMassNwFullD += volVarsFullD.porosity() * volVarsFullD.density(1) * volVarsFullD.saturation(1) * scv.volume();
            }
        }

        Scalar expectedVolume = 1.0;
        for(size_t i=0; i<dim; i++)
        {
            expectedVolume *= upperRightFullD[i]-lowerLeftFullD[i];
        }
        std::cout << "totalVolumeFullD: " << totalVolumeFullD << " and should be: " << expectedVolume << std::endl;
        std::cout << "totalMassNwFullD: " << totalMassNwFullD << std::endl;
        Scalar domainHeight = upperRightFullD[dim-1]-lowerLeftFullD[dim-1];

        if constexpr(dim==2)
        {
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * /* extrusionfactor (which is supposed to be 1) * */ (-injectionRate);
        }
        else if constexpr(dim==3) // assumes that one SINGLE column is used for injection
        {
            auto numCells = getParam<CellAray>("Grid.Cells");
            Scalar deltaY = (upperRightFullD[1]-lowerLeftFullD[1])/((double) numCells[1]);
            Scalar deltaZ = (upperRightFullD[dim-1]-lowerLeftFullD[dim-1])/((double) numCells[dim-1]);
            expectedInjectedGasMass += timeLoop->timeStepSize() * domainHeight * deltaY * (-injectionRate) / (deltaY);
        }

        double errorMass = (expectedInjectedGasMass - (totalMassNwFullD))/expectedInjectedGasMass;
        std::cout << "Expected injected gas mass is: " << expectedInjectedGasMass << " while gas mass in domain is: " << totalMassNwFullD << ". Error on in massNw: " << errorMass << std::endl;
        std::cout << "---------------------------------------------info end------------------------------------------------------------\n\n" << std::endl;
    }


    /*!
     * \brief Reads in a column of data from a file into a vector
     *
     * \param inputFileName Read data from this file (expected to only have one column of data)
     * \param vectorToBeFilled Vector used for storing the read data
     */
    #include <fstream>
    template<typename Scalar>
    void readIn(std::string& inputFileName, std::vector<Scalar>& vectorToBeFilled)
    {
        std::ifstream inputFile(inputFileName);

        // test file open
        if (inputFile) {
            double value;

            // read the elements in the file into a vector
            while ( inputFile >> value ) {
                vectorToBeFilled.push_back(value);
            }
        }
        inputFile.close();
    }


    /*!
     * \brief Find the position of the gas plume (during injection)
     *
     * \param gridGeometry pointer to the grid geomtry object
     * \param solVector solution vector
     * \param eps threshold which is the lower boundary from when an element counts as containing gas saturation
     */
    template<typename Scalar, typename GridGeometry, typename SolutionVector>
    Scalar findPlumeDistance(std::shared_ptr<const GridGeometry> gridGeometry,
                             const SolutionVector& solVector,
                             const Scalar eps)
    {
        Scalar plumeSpread = 0.0;

        for (const auto& element : elements(gridGeometry->gridView()))
        {
            int elemetIdx = gridGeometry->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry);
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto scvPos = fvGeometry.geometry(scv).center();

                if(solVector[elemetIdx][1] > eps)
                {
                    if(scvPos[0] > plumeSpread)
                    {
                        plumeSpread = scvPos[0];
                    }
                }
            }
        }

        return plumeSpread;
    }


    /*!
     * \brief Find the maximum (not absolute value) Darcy velocity in the domain (during injection), output vector contains the max velocity for wetting phase and also the max velocity for non-wetting phase.
     *
     * \param gridVariables grid variables
     * \param gridGeometry pointer to the grid geomtry object
     * \param solVector solution vector
     * \param leafGridView view on the grid leaves
     */
    template<typename Scalar, typename GridVariables, typename GridGeometry, typename SolutionVector, typename GridView, typename VelocityOutput>
    std::array<Scalar,2> findMaxVelocity(const GridVariables& gridVariables,
                                         std::shared_ptr<const GridGeometry> gridGeometry,
                                         const SolutionVector& solVector,
                                         const GridView& leafGridView)
    {
        // instantiate the velocity output
        VelocityOutput velocityOutput(gridVariables);
        using VelocityVector = typename VelocityOutput::VelocityVector;
        VelocityVector velocityW, velocityNw;

        const auto numDofs = gridGeometry->numDofs();
        auto numVelocities = numDofs;

        velocityW.resize(numVelocities);
        velocityNw.resize(numVelocities);

        auto fvGeometry = localView(*gridGeometry);
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
        for (const auto& element : elements(leafGridView, Dune::Partitions::interior))
        {
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, solVector);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            velocityOutput.calculateVelocity(velocityW, element, fvGeometry, elemVolVars, elemFluxVarsCache, 0);
            velocityOutput.calculateVelocity(velocityNw, element, fvGeometry, elemVolVars, elemFluxVarsCache, 1);
        }

        constexpr int dim = GridView::dimension;

        // velocityW[i][0] is x-component of velocity for element i
        // velocityW[i][1] is y-component of velocity for element i
        Scalar maxVelocityW = 0.0;
        for(int i=0; i<velocityW.size(); i++)
        {
            if constexpr(dim==2)
            {
                if(velocityW[i][0] > maxVelocityW)
                {
                    maxVelocityW = velocityW[i][0];
                }
            }
            else if constexpr(dim==3)
            {
                auto xVeloQuad = velocityW[i][0]*velocityW[i][0];
                auto yVeloQuad = velocityW[i][1]*velocityW[i][1];
                auto planarVelocity = std::sqrt(xVeloQuad + yVeloQuad);

                if(planarVelocity > maxVelocityW)
                {
                    maxVelocityW = planarVelocity;
                }
            }  
        }

        Scalar maxVelocityNW = 0.0;
        for(int i=0; i<velocityNw.size(); i++)
        {
            if constexpr(dim==2)
            {
                if(velocityNw[i][0] > maxVelocityNW)
                {
                    maxVelocityNW = velocityNw[i][0];
                }
            }
            else if constexpr(dim==3)
            {
                auto xVeloQuad = velocityNw[i][0]*velocityNw[i][0];
                auto yVeloQuad = velocityNw[i][1]*velocityNw[i][1];
                auto planarVelocity = std::sqrt(xVeloQuad + yVeloQuad);

                if(planarVelocity > maxVelocityNW)
                {
                    maxVelocityNW = planarVelocity;
                }
            }  
        }

        std::array<double,2> maxVelocities = {maxVelocityW, maxVelocityNW};

        return maxVelocities;
    }


    /*!
     * \brief Check if a given file is empty
     *
     * \param myFile read (only) stream of a file
     */
    bool is_empty(std::ifstream& myFile)
    {
        return myFile.peek() == std::ifstream::traits_type::eof();
    }


    /*!
     * \brief Write data to a comma separated text file
     *
     * \param oneDataSet vector containing the data
     * \param nameContainer vector containing the names (name handles) of the data
     * \param fileName name of the file to write to
     */
    // delete the file on disk before running the simulation again
    template <typename Scalar>
    void writeToFile(std::vector<Scalar> oneDataSet,
                     std::vector<std::string> nameContainer,
                     std::string fileName)
    {
        if(oneDataSet.size() != nameContainer.size())
        {
            std::ostringstream errMsg;
            errMsg << "In writeToFile: oneDataSet and nameContainer must be of same length! Size of dataSet is: " << oneDataSet.size() << " while size of nameContainer is: " << nameContainer.size() << std::endl;
            throw std::invalid_argument(errMsg.str());
        }

        std::ifstream readMyFile;
        readMyFile.open (fileName);

        std::ofstream writeMyFile;
        writeMyFile.open (fileName, std::ofstream::app);

        if(is_empty(readMyFile)) //if the file is empty, write column headers
        {
            for(size_t i=0; i<nameContainer.size(); i++)
            {
                if(i<nameContainer.size()-1)
                    writeMyFile << nameContainer[i] << ",";
                else
                    writeMyFile << nameContainer[i];
            }

            writeMyFile << "\n";
            readMyFile.close ();
        }

        for(size_t i=0; i<oneDataSet.size(); i++)
        {
            if(i<oneDataSet.size()-1)
                writeMyFile << std::fixed << std::setprecision(12) << oneDataSet[i] << ",";
            else
                writeMyFile << std::fixed << std::setprecision(12) << oneDataSet[i];
        }

        writeMyFile << "\n";
    }

    /*!
     * \brief Delete a file
     *
     * \param fileName name of the file to write to
     */
    void deleteFile(std::string fileName)
    {
        std::ifstream readMyFile;
        readMyFile.open(fileName);

        if(!is_empty(readMyFile)) //if the file is empty, write column headers
        {
            std::remove(fileName.c_str());
        }
    }


    /*!
     * \brief Update the map between global column indices and local model element indices
     * \param gridGeometryFullD grid geometry belonging to the fullD model
     * \param gridGeometryVECoarse grid geometry belonging to the coarse VE model
     * \param numberColumns number of horizontal cells in the discretization
     * \param deltaX width between to horizontal discretizational elements
     * \param verbose flag if verbose output should be enabled
     */
    template<typename GridGeometry>
    std::vector<std::vector<size_t>> getCurrentGlobalToLocalIndex(const GridGeometry& gridGeometryFullD,
                                                                  const GridGeometry& gridGeometryVECoarse,
                                                                  const int& numberColumns,
                                                                  const double& deltaX,
                                                                  const double& deltaY,
                                                                  bool verbose = false)
    {
        using GridView = typename GridGeometry::GridView;
        constexpr int dim = GridView::dimension;
        auto numCells = getParam<std::vector<unsigned int>>("Darcy.Grid.Cells");
        unsigned int xCells = numCells[0]; 

        std::vector<std::vector<size_t>> globalToLocalIndex;
        globalToLocalIndex.resize(numberColumns);

        //iterate over fullD elements
        for (const auto& element : elements(gridGeometryFullD.gridView()))
        {
            const auto currentColumnPos = element.geometry().center();
            int currentColumnIdx = -1;

            if constexpr(dim==2)
            {
                currentColumnIdx = std::round((currentColumnPos[0] - 0.5*deltaX)/deltaX);
            }
            else if constexpr(dim==3)
            {
                int idxX = (int)std::round((currentColumnPos[0] - 0.5*deltaX)/deltaX);
                int idxY = (int)std::round((currentColumnPos[1] - 0.5*deltaY)/deltaY);
                currentColumnIdx = idxY*xCells + idxX; //returns a global column index
            }

            size_t currentFullDElementIdx = gridGeometryFullD.elementMapper().index(element);
            globalToLocalIndex[currentColumnIdx].push_back(currentFullDElementIdx);
        }

        //iterate over VE elements
        for (const auto& element : elements(gridGeometryVECoarse.gridView()))
        {
            const auto currentColumnPos = element.geometry().center();
            int currentColumnIdx = -1;

            if constexpr(dim==2)
            {
                currentColumnIdx = std::round((currentColumnPos[0] - 0.5*deltaX)/deltaX);
            }
            else if constexpr(dim==3)
            {
                int idxX = (int)std::round((currentColumnPos[0] - 0.5*deltaX)/deltaX);
                int idxY = (int)std::round((currentColumnPos[1] - 0.5*deltaY)/deltaY);
                currentColumnIdx = idxY*xCells + idxX; //returns a global column index
            }

            unsigned int elementIdx = gridGeometryVECoarse.elementMapper().index(element);
            globalToLocalIndex[currentColumnIdx].push_back(elementIdx);
        }

        if(verbose)
        {
            for(int i=0; i<globalToLocalIndex.size(); i++)
            {
                std::cout << "Current globalToLocalIndex at index " << i << ": ";
                for(int j=0; j < globalToLocalIndex[i].size(); j++)
                {
                    std::cout << globalToLocalIndex[i][j] << " ";
                }
                std::cout << std::endl;
            }
        }

        return globalToLocalIndex;
    }


    /*!
     * \brief Compute conservative satNw when transfering a fullD column to VE
     * \param massNwToConserve mass in a fullD column that should be conserved
     * \param virtualPressureW deduced wetting-phase pressure for the new VE column
     * \param problemCoarseVE problem of the VE grid
     * \param virtualVolume element volume of a virtual coarse VE element
     * \param virtualPorosity porosity of a virtual coarse VE element
     * \param upperDomainBoundaryHeight vertical position of upper domain boundary
     * \param lowerDomainBoundaryHeight vertical position of lower domain boundary
     */
    template<typename Scalar, typename Problem>
    Scalar computeConservativeSatNWHelper(const Scalar& massNwToConserve,
                                          const Scalar& virtualPressureW,
                                          const Problem& problemCoarseVE,
                                          const Scalar& virtualVolume,
                                          const Scalar& virtualPorosity,
                                          const Scalar& upperDomainBoundaryHeight,
                                          const Scalar& lowerDomainBoundaryHeight)
    {
        auto dummyPos = problemCoarseVE.gridGeometry().bBoxMin(); //this is only valid if temperature, gravitation and porosity are independent of the position!
        auto normalizedPorosity = virtualPorosity/(upperDomainBoundaryHeight-lowerDomainBoundaryHeight); //TODO: this should be multiplied by width of element to obtain volume?
        auto temperature = problemCoarseVE.spatialParams().temperatureAtPos(dummyPos);
        auto virtualCoarseDensityNw = problemCoarseVE.returnDensityGasAndViscosity(temperature, virtualPressureW)[0];

        //strictly, using virtualVolume of the whole VE column is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
        Scalar conservativeSatNw = massNwToConserve/(normalizedPorosity * virtualCoarseDensityNw * virtualVolume);

        return conservativeSatNw;
    }


    /*!
     * \brief Assemble a a meta data vector for output (for adaptive test)
     * \param fvGridGeometryDarcy grid geometry of the fullD model
     * \param fvGridGeometryVE grid geometry of the coarse-scale VE model
     * \param fvGridGeometryVEFine grid geometry of the fine-scale VE model
     * \param timeLoop the time loop object
     * \param couplingManager ht ecoupling manager
     * \param numberHorizontalCells number of horizontal cells (globally)
     * \param numberVerticalCells number of vertical cells (globally)
     * \param enableLenses whether permeability lenses should be enabled
     */
    template<typename GridGeom, typename TimeLoop, typename CouplingManager>
    std::vector<double> assembleMetaDataVectorAdaptiveModel(const GridGeom& fvGridGeometryDarcy,
                                                            const GridGeom& fvGridGeometryVE,
                                                            const GridGeom& fvGridGeometryVEFine,
                                                            const TimeLoop& timeLoop,
                                                            const CouplingManager& couplingManager,
                                                            const std::vector<size_t> numCells,
                                                            const bool& enableLenses)
    {
        unsigned int timeStep = timeLoop.timeStepIndex();
        double timeStepSize = timeLoop.timeStepSize();
        double simulatedTime = timeLoop.time();
        double wallTime = timeLoop.wallClockTime();
        unsigned int numberElementsFullD = fvGridGeometryDarcy.numScv();
        unsigned int numberElementsVE = fvGridGeometryVE.numScv();
        unsigned int numberElementsVEFine = fvGridGeometryVEFine.numScv();
        unsigned int numberElementsTotal = numberElementsFullD+numberElementsVE;
        unsigned int numberVESubdomains = couplingManager.numberOfSubdomains();
        unsigned int numberFullDSubdomains = numberVESubdomains;
        std::vector<double> metaDataVector = {(double)timeStep, simulatedTime, timeStepSize, wallTime, (double)numberElementsFullD, (double)numberElementsVE, (double)numberElementsVEFine, (double)numberElementsTotal, (double)numCells[0], (double)numCells[1], (double)numCells[2],  (double)numberVESubdomains, (double)numberFullDSubdomains, (double)enableLenses};

        return metaDataVector;
    }


    /*!
     * \brief Assemble a a meta data vector for output (for fullD test)
     * \param fvGridGeometryDarcy grid geometry of the fullD model
     * \param timeLoop the time loop object
     * \param numberHorizontalCells number of horizontal cells (globally)
     * \param numberVerticalCells number of vertical cells (globally)
     * \param enableLenses whether permeability lenses should be enabled
     */
    template<typename GridGeom, typename TimeLoop>
    std::vector<double> assembleMetaDataVectorOnlyFullDModel(const GridGeom& fvGridGeometryDarcy,
                                                             const TimeLoop& timeLoop,
                                                             const std::vector<size_t> numCells,
                                                             const bool& enableLenses)
    {
        unsigned int timeStep = timeLoop.timeStepIndex();
        double timeStepSize = timeLoop.timeStepSize();
        double simulatedTime = timeLoop.time();
        double wallTime = timeLoop.wallClockTime();
        unsigned int numberElementsFullD = fvGridGeometryDarcy.numScv(); //TODO: is only true for TPFA, numScv=numElem
        std::vector<double> metaDataVector = {(double)timeStep, simulatedTime, timeStepSize, wallTime, (double)numberElementsFullD, (double)numCells[0], (double)numCells[1], (double)numCells[2], (double)enableLenses};

        return metaDataVector;
    }


    /*!
     * \brief Assemble a a meta data vector for output (for VE test)
     * \param fvGridGeometryVE grid geometry of the coarse-scale VE model
     * \param fvGridGeometryVEFine grid geometry of the fine-scale VE model
     * \param timeLoop the time loop object
     * \param numberHorizontalCells number of horizontal cells (globally)
     * \param numberVerticalCells number of vertical cells (globally)
     * \param enableLenses whether permeability lenses should be enabled
     */
    template<typename GridGeom, typename TimeLoop>
    std::vector<double> assembleMetaDataVectorOnlyVEModel(const GridGeom& fvGridGeometryVE,
                                                          const GridGeom& fvGridGeometryVEFine,
                                                          const TimeLoop& timeLoop,
                                                          const std::vector<size_t> numCells,
                                                          const bool& enableLenses)
    {
        unsigned int timeStep = timeLoop.timeStepIndex();
        double timeStepSize = timeLoop.timeStepSize();
        double simulatedTime = timeLoop.time();
        double wallTime = timeLoop.wallClockTime();
        unsigned int numberElementsVE = fvGridGeometryVE.numScv();
        unsigned int numberElementsVEFine = fvGridGeometryVEFine.numScv();
        std::vector<double> metaDataVector = {(double)timeStep, simulatedTime, timeStepSize, wallTime, (double)numberElementsVE, (double)numberElementsVEFine, (double)numCells[0], (double)numCells[1], (double)numCells[2], (double)enableLenses};
        return metaDataVector;
    }


} // end namespace VerticalEquilibriumModel

} // end namespace Dumux
#endif
