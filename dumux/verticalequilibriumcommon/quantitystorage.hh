// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Stores quantities for the fine scale and the coarse scale
 */
#ifndef DUMUX_TWOPVE_QUANTITYSTORAGE_HH
#define DUMUX_TWOPVE_QUANTITYSTORAGE_HH

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPVEQuantityStorage;

/*!
 * \ingroup TwoPVE
 * \brief This class stores quantities for the fine scale and the coarse scale
 */
template<class TypeTag>
class TwoPVEQuantityStorage
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    enum {
        dim = GridView::dimension,
    };

public:

    TwoPVEQuantityStorage(std::shared_ptr<const GridGeometry> fvGridGeometryCoarse,
                          std::shared_ptr<const GridGeometry> fvGridGeometryFine)
        : minGasPlumeDistElementCoarseVector_(fvGridGeometryCoarse->numScv(), fvGridGeometryCoarse->bBoxMax()[dim-1] - fvGridGeometryCoarse->bBoxMin()[dim-1])
    {
        std::cout << "In quantityStorage. #fvGridGeometryCoarse scvs: " << fvGridGeometryCoarse->numScv() << std::endl;
        std::cout << "In quantityStorage. #fvGridGeometryFine scvs: " << fvGridGeometryFine->numScv() << std::endl;
    }

    void update(std::shared_ptr<const GridGeometry> fvGridGeometryCoarse,
                std::shared_ptr<const GridGeometry> fvGridGeometryFine)
    {
        auto upperRightDomainZCoord = fvGridGeometryCoarse->bBoxMax()[dim-1] - fvGridGeometryCoarse->bBoxMin()[dim-1];
        minGasPlumeDistElementCoarseVector_.clear();
        minGasPlumeDistElementCoarseVector_.resize(fvGridGeometryCoarse->numScv(), upperRightDomainZCoord);
    }


    /*!
     * \brief Store the minimal gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     * \param minGasPlumeDist minimal gas plume distance to be stored
     */
    void storeMinGasPlumeDist(const unsigned int& elementCoarseIdx, const Scalar& minGasPlumeDist)
    {
        minGasPlumeDistElementCoarseVector_[elementCoarseIdx] = minGasPlumeDist;
    }

    /*!
     * \brief Return the stored minimal gas plume distance of a coarse-scale element
     *
     * \param elementCoarseIdx Index of the coarse-scale element
     */
    const Scalar getMinGasPlumeDist(const unsigned int elementCoarseIdx) const
    {
        return minGasPlumeDistElementCoarseVector_[elementCoarseIdx];
    }

private:
    std::vector<Scalar> minGasPlumeDistElementCoarseVector_;
};

} // end namespace Dumux

#endif
