// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Reconstructs quantities from the coarse scale to the fine scale for the incompressible 2p test.
 */
#ifndef DUMUX_TWOPVE_QUANTITYRECONSTRUCTION_HH
#define DUMUX_TWOPVE_QUANTITYRECONSTRUCTION_HH

#include <math.h>
#include <tuple>
#include <chrono>
#include <optional>

#include <dumux/common/properties.hh>
#include <dumux/nonlinear/findscalarroot.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/common/integrate.hh>
#include <dumux/timemeasurer/timemeasurements.hh>
#include <dumux/verticalequilibriumcommon/util.hh>

#include "gasplumedistfunctions.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPVEQuantityReconst;

/*!
 * \ingroup TwoPVE
 * \brief This class reconstructs the fine-scale quantities from the coarse-scale quantities.
 */
template<class TypeTag>
class TwoPVEQuantityReconst
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum {
        waterPhaseIdx = FluidSystem::phase0Idx, // = 0
        gasPhaseIdx = FluidSystem::phase1Idx, // = 1
        numPhases = FluidSystem::numPhases
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;


public:

    TwoPVEQuantityReconst(TimeMeasuring& timeMeasurer,
                          const std::string& spatialParamsGroup = "")
        : timeMeasurer_(timeMeasurer)
    {
        numCalls_ = 0;
        sumIterations_ = 0;
    }


    /*!
     * \brief Computes the gas plume distance gasPlumeDist
     *
     * \param storedMinGasPlumeDist the previously computed gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param domainHeight height of the whole domain
     * \param satWCoarse wetting-phase saturation (on the coarse scale)
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const Scalar computeGasPlumeDist(/*const Scalar& storedMinGasPlumeDist,*/
                                     const std::tuple<Scalar,Scalar>& densityPhases,
                                     const std::tuple<Scalar,Scalar>& residualSaturationPhases,
                                     const Scalar& gravityNorm,
                                     const Scalar& domainHeight,
                                     const Scalar& satWCoarse,
                                     const std::tuple<Scalar,Scalar>& BrooksCoreyParameters,
                                     const Scalar absTol = 1e-11)
    { 
        std::optional<std::chrono::steady_clock::time_point> timeGasPlumeDistStart; //required as placeholder to use in the whole function
        if constexpr(TimeMeasuring::enableTimeMeasuringZp)
            timeGasPlumeDistStart = std::chrono::steady_clock::now();

        auto [densityW, densityNw] = densityPhases;
        auto [resSatW, resSatNw] = residualSaturationPhases;
        auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

        TwoPVEGasPlumeFunctions localZpFunctionsHolder(BCLambda, BCEntryP, densityW, densityNw, gravityNorm, resSatW, resSatNw, satWCoarse, domainHeight);
        Scalar gasPlumeDist = domainHeight;
        Scalar xiStart = domainHeight/2.0;

        if(satWCoarse<1.0)
        {
            numCalls_++;
            std::vector<Scalar> deterministicResult{};
            deterministicResult = findScalarRootNewtonAndIter(xiStart, localZpFunctionsHolder.columnMassConservation(), localZpFunctionsHolder.derivativeColumnMassConservation(), 1e-8);

            gasPlumeDist = deterministicResult[0];
            sumIterations_ += deterministicResult[1];
        }

        //check if gas plume distance is larger than domain height
        if(gasPlumeDist<0.0) //TODO: this assumes, that lowerBoundaryYPos = 0.0!!!
        {
            std::ostringstream errMsg;
            errMsg << "Domain height is too small in comparison to the amount of injected gas. Increase domain height or inject less gas! Wetting-phase saturation was: " << satWCoarse << " and zp was: " << gasPlumeDist << ". Densities were: " << densityW << " " << densityNw;
            throw std::invalid_argument(errMsg.str());
        }

        if constexpr(TimeMeasuring::enableTimeMeasuringZp)
        {
            std::chrono::steady_clock::time_point timeGasPlumeDistEnd = std::chrono::steady_clock::now();
            const double singleRunZpTime = std::chrono::duration_cast<std::chrono::microseconds>(timeGasPlumeDistEnd -
                *timeGasPlumeDistStart).count();
            timeMeasurer_.addToTotalTimeZp(singleRunZpTime);
        }

        return gasPlumeDist;
    }



    /*!
     * \brief Computes the capillary pressure on the coarse scale
     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const Scalar computeCapillaryPressureCoarse(const Scalar& gasPlumeDist,
                                                const std::tuple<Scalar,Scalar>& densityPhases,
                                                const Scalar& gravityNorm,
                                                const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;

        //calculate the coarse-scale capillary pressure
        const Scalar pcCoarse = gravityNorm * gasPlumeDist * (referenceDensityNw - referenceDensityW) + BCEntryP;
        return pcCoarse;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase and non-wetting phase pressures

     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param pressureWCoarse wetting-phase pressure on the coarse scale
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const std::vector<Scalar> reconstPressure(const Scalar& gasPlumeDist,
                                              const std::tuple<Scalar,Scalar>& densityPhases,
                                              const Scalar& gravityNorm,
                                              const Scalar& height,
                                              const Scalar& pressureWCoarse,
                                              const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;

        std::vector<Scalar> reconstructedPressures(numPhases, 0.0);

        //So far, only the capillaryFringe model is implemented
        if(height <= gasPlumeDist)
        {
            reconstructedPressures[waterPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * height;
            reconstructedPressures[gasPhaseIdx] = reconstructedPressures[waterPhaseIdx] + BCEntryP;
        }
        else
        {
            reconstructedPressures[waterPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * height;
            reconstructedPressures[gasPhaseIdx] = pressureWCoarse - referenceDensityW * gravityNorm * gasPlumeDist
                                                - referenceDensityNw * gravityNorm * (height - gasPlumeDist) + BCEntryP;
        }

        return reconstructedPressures;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase saturation
     *
     * Compute the saturation as the mean of the reconstructed saturation over the height of a fine-scale cell.
     *
     * \param gasPlumeDistances contains the gas plume distance and the minimum gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param deltaZ discretizaion width of the fine-scale grid in vertical direction
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const Scalar computeSaturationIntegral(const std::tuple<Scalar, Scalar>& gasPlumeDistances,
                                           const std::tuple<Scalar, Scalar>& densityPhases,
                                           const std::tuple<Scalar, Scalar>& residualSaturationPhases,
                                           const Scalar& gravityNorm,
                                           const Scalar& height,
                                           const Scalar& deltaZ,
                                           const std::tuple<Scalar, Scalar>&  BrooksCoreyParameters) const
    {
        auto [gasPlumeDist, minGasPlumeDist] = gasPlumeDistances;
        auto [referenceDensityW, referenceDensityNw] = densityPhases;
        auto [resSatW, resSatNw] = residualSaturationPhases;
        auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

        const Scalar lowerBound = height - deltaZ/2.0;
        const Scalar upperBound = height + deltaZ/2.0;

        Scalar integral = 0.0;

/*      differentiate between three cases: 1) fine-scale element is completely under the gas plume,
                                           2) fine-scale element intersects with gas plume,
                                           3) fine-scale element is completely above the gas plume */

        if(lowerBound < minGasPlumeDist)
        {
            const Scalar upperPartBound = std::min(minGasPlumeDist, upperBound);
            integral += upperPartBound - lowerBound;
        }
        if(upperBound > minGasPlumeDist && lowerBound < gasPlumeDist )
        {
            const Scalar lowerpartBound = std::max(minGasPlumeDist, lowerBound);
            const Scalar upperPartBound = std::min(gasPlumeDist, upperBound);
            integral += (upperPartBound - lowerpartBound)*(1.0-resSatNw);
        }
        if(upperBound > gasPlumeDist)
        {
            const Scalar lowerPartBound = std::max(gasPlumeDist, lowerBound);

            integral += (std::pow(BCEntryP, BCLambda) * (1 - resSatW - resSatNw)) /
            ((1 - BCLambda) * (referenceDensityW - referenceDensityNw) * gravityNorm) *
            (std::pow(((upperBound - gasPlumeDist) * (referenceDensityW - referenceDensityNw) *
            gravityNorm + BCEntryP), (1 - BCLambda))
            - std::pow(((lowerPartBound - gasPlumeDist) * (referenceDensityW - referenceDensityNw) *
            gravityNorm + BCEntryP), (1 - BCLambda)))
            + resSatW * (upperBound - lowerPartBound);
        }

        integral = integral/(upperBound-lowerBound);

        return integral;
    }


    /*!
     * \brief Reconstructs the fine-scale capillary pressure
     *
     * \param gasPlumeDist value of the gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param BCEntryP entry pressure of the Brook-Corey model
     */
    const Scalar reconstCapillaryPressure(const Scalar& gasPlumeDist,
                                          const std::tuple<Scalar,Scalar>& densityPhases,
                                          const Scalar& gravityNorm,
                                          const Scalar& height,
                                          const Scalar& BCEntryP) const
    {
        auto [referenceDensityW, referenceDensityNw] = densityPhases;

        Scalar reconstCapillaryPressure = 0.0;

        //for capillary fringe model
        if(height <= gasPlumeDist)
        {
            reconstCapillaryPressure = BCEntryP;
        }
        else if(height > gasPlumeDist)
        {
            reconstCapillaryPressure = referenceDensityW * gravityNorm * (height - gasPlumeDist) + BCEntryP
                                     - referenceDensityNw * gravityNorm * (height - gasPlumeDist);
        }

        return reconstCapillaryPressure;
    }


    /*!
     * \brief Reconstructs the fine-scale wetting-phase and non wetting-phase mobilities
     *
     * In relation of the fine element's location to the gasPlumeDist, the reconstruction is conducted. The fine element is divided into even finer
     * segments in order to allow a better approximation of the integral.
     *
     * \param gasPlumeDistances contains the gas plume distance and the minimum gas plume distance
     * \param densityPhases contains the phase densities (here: 2 phases)
     * \param viscosityPhases contains the phase viscosities (here: 2 phases)
     * \param residualSaturationPhases contains the phase residual saturation (here: 2 phases)
     * \param gravityNorm norm of the gravity
     * \param height height at which the pressures should be reconstructed (TODO: include check that height < domainHeight && > lowerLeft[1])
     * \param deltaZ discretizaion width of the fine-scale grid in vertical direction
     * \param BrooksCoreyParameters contains the two Brooks-Corey parameters (lambda and entry pressure)
     */
    const std::vector<Scalar> reconstMobilitiesFine(const std::tuple<Scalar,Scalar>& gasPlumeDistances,
                                                    const std::tuple<Scalar,Scalar>& densityPhases,
                                                    const std::tuple<Scalar,Scalar>& viscosityPhases,
                                                    const std::tuple<Scalar,Scalar>& residualSaturationPhases,
                                                    const Scalar& gravityNorm,
                                                    const Scalar& height,
                                                    const Scalar& deltaZ,
                                                    const std::tuple<Scalar,Scalar>& BrooksCoreyParameters)
    {
        auto [gasPlumeDist, minGasPlumeDist] = gasPlumeDistances;
        auto [viscosityW, viscosityNw] = viscosityPhases;

        std::vector<Scalar> mobilitesFine(numPhases);

        const Scalar lowerBound = height - deltaZ/2.0;
        const Scalar upperBound = height + deltaZ/2.0;

        Scalar mobilityWFine = 0.0;
        Scalar mobilityNwFine = 0.0;

        //define function for fine-scale wetting-phase saturation
        std::function<Scalar(Scalar)> saturationW = [&gasPlumeDist, &densityPhases, &gravityNorm, &BrooksCoreyParameters, &residualSaturationPhases](Scalar z)
        {
            auto [referenceDensityW, referenceDensityNw] = densityPhases;
            auto [resSatW, resSatNw] = residualSaturationPhases;
            auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

            Scalar satW = -1.0;

            if(z<gasPlumeDist)
            {
                satW =  1.0;
            }
            else if(z>=gasPlumeDist)
            {
                satW =  std::pow((BCEntryP + (referenceDensityW-referenceDensityNw)*gravityNorm*(z-gasPlumeDist)),-BCLambda)*std::pow(BCEntryP, BCLambda)*(1-resSatW-resSatNw)+resSatW;
            }

            return satW;
        };

        //define function for wetting-phase relative peremability (here Brooks-Corey)
        std::function<Scalar(Scalar)> relPermW = [&gasPlumeDist, &densityPhases, &gravityNorm, &saturationW, &BrooksCoreyParameters, &residualSaturationPhases](Scalar z)
        {
            auto [resSatW, resSatNw] = residualSaturationPhases;
            auto [BCLambda, BCEntryP] = BrooksCoreyParameters;
            Scalar swe = (saturationW(z)-resSatW)/(1-resSatW-resSatNw);

            return std::pow(swe, 2.0/BCLambda + 3.0);
        };

        //define function for non-wetting-phase relative peremability (here Brooks-Corey)
        std::function<Scalar(Scalar)> relPermNW = [&gasPlumeDist, &densityPhases, &gravityNorm, &saturationW, &BrooksCoreyParameters, &residualSaturationPhases](Scalar z)
        {
            auto [resSatW, resSatNw] = residualSaturationPhases;
            auto [BCLambda, BCEntryP] = BrooksCoreyParameters;

            Scalar swe = (saturationW(z)-resSatW)/(1-resSatW-resSatNw);
            const Scalar exponent = 2.0/BCLambda + 1.0;
            const Scalar sne = 1.0 - swe;
            return sne*sne*(1.0 - std::pow(swe, exponent));
        };


        //integrate using Simpson's rule
//         mobilityWFine = simpson38Rule_(relPermW, lowerBound, upperBound);
//         mobilityNwFine = simpson38Rule_(relPermNW, lowerBound, upperBound);

        //integrate using Gauss quadrature
        mobilityWFine = gaussian3PointRule_(relPermW, lowerBound, upperBound);
        mobilityNwFine = gaussian3PointRule_(relPermNW, lowerBound, upperBound);

        //average
        mobilityWFine = mobilityWFine/(upperBound - lowerBound);
        mobilityNwFine = mobilityNwFine/(upperBound - lowerBound);

        //turn relative permeability to mobility
        mobilityWFine = mobilityWFine/viscosityW;
        mobilityNwFine = mobilityNwFine/viscosityNw;

        //store
        mobilitesFine[waterPhaseIdx] = mobilityWFine;
        mobilitesFine[gasPhaseIdx] = mobilityNwFine;

        return mobilitesFine;
    }

    /*!
     * \brief Returns the averaged number of iterations for the local Newton solver of the gas plume distance zp
     */
    const Scalar getAverageNumberOfLocalNewtonIterations()
    {
        return (double)sumIterations_/(double)numCalls_;
    }

private:

    /*!
     * \brief Apply Simpson 3/8 quadrature rule
     *
     * \param integrand function to be integrated
     * \param lowerBoundary lower integration boundary
     * \param upperBoundary upper integration boundary
     */
    Scalar simpson38Rule_(std::function<Scalar(Scalar)> integrand,
                         Scalar lowerBoundary,
                         Scalar upperBoundary)
    {
        Scalar firstEvaluationPoint = lowerBoundary;
        Scalar secondEvaluationPoint = (2*lowerBoundary+upperBoundary)/3.0;
        Scalar thirdEvaluationPoint = (lowerBoundary+2*upperBoundary)/3.0;
        Scalar fourthEvaluationPoint = upperBoundary;

        Scalar integral = (upperBoundary-lowerBoundary)/8.0 * (integrand(firstEvaluationPoint) +
                                                               3*integrand(secondEvaluationPoint) +
                                                               3*integrand(thirdEvaluationPoint) +
                                                               integrand(fourthEvaluationPoint));
        return integral;
    }


    /*!
     * \brief Apply Gaussian (Gauss-Legendre) 3-point quadrature rule
     *
     * \param integrand function to be integrated
     * \param lowerBoundary lower integration boundary
     * \param upperBoundary upper integration boundary
     */
    Scalar gaussian3PointRule_(std::function<Scalar(Scalar)> integrand,
                               Scalar lowerBoundary,
                               Scalar upperBoundary)
    {

        Scalar firstEvaluationPoint = 0.0*(upperBoundary-lowerBoundary)/2.0 + (lowerBoundary+upperBoundary)/2.0;
        Scalar firstWeight = 8.0/9.0;
        Scalar secondEvaluationPoint = -std::sqrt(3.0/5.0)*(upperBoundary-lowerBoundary)/2.0 + (lowerBoundary+upperBoundary)/2.0;
        Scalar secondWeight = 5.0/9.0;
        Scalar thirdEvaluationPoint = std::sqrt(3.0/5.0)*(upperBoundary-lowerBoundary)/2.0 + (lowerBoundary+upperBoundary)/2.0;
        Scalar thirdWeight = 5.0/9.0;

        Scalar integral = (upperBoundary-lowerBoundary)/2.0 * (firstWeight*integrand(firstEvaluationPoint) +
                                                               secondWeight*integrand(secondEvaluationPoint) +
                                                               thirdWeight*integrand(thirdEvaluationPoint));
        return integral;
    }


    int numCalls_;
    int sumIterations_;

    TimeMeasuring &timeMeasurer_;
};

} // end namespace Dumux

#endif
