// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPModel
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase model.
 */

#ifndef DUMUX_TWOPVE_VOLUMEVARIABLES_HH
#define DUMUX_TWOPVE_VOLUMEVARIABLES_HH

#include <tuple>
#include <chrono>
#include <execution>

#include <dumux/porousmediumflow/volumevariables.hh>
#include <dumux/porousmediumflow/nonisothermal/volumevariables.hh>
#include <dumux/material/solidstates/updatesolidvolumefractions.hh>
#include <dumux/porousmediumflow/2pve/formulation.hh>

#include <dumux/timemeasurer/timemeasurements.hh> //to have access to namespace TimeMeasuringDetail

#include <dumux/verticalequilibriumcommon/util.hh>

namespace Dumux {

/*!
 * \ingroup TwoPModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase model.
 */
template <class Traits>
class TwoPVEVolumeVariables
: public PorousMediumFlowVolumeVariables<Traits>
, public EnergyVolumeVariables<Traits, TwoPVEVolumeVariables<Traits> >
{
    using ParentType = PorousMediumFlowVolumeVariables<Traits>;
    using EnergyVolVars = EnergyVolumeVariables<Traits, TwoPVEVolumeVariables<Traits> >;
    using PermeabilityType = typename Traits::PermeabilityType;
    using ModelTraits = typename Traits::ModelTraits;
    using Idx = typename ModelTraits::Indices;
    using Scalar = typename Traits::PrimaryVariables::value_type;
    using FS = typename Traits::FluidSystem;
    static constexpr int numFluidComps = ParentType::numFluidComponents();
    enum
    {
        pressureIdx = Idx::pressureIdx,
        saturationIdx = Idx::saturationIdx,

        phase0Idx = FS::phase0Idx,
        phase1Idx = FS::phase1Idx,
        numPhases = FS::numPhases
    };

    static constexpr auto formulation = ModelTraits::priVarFormulation();
    static constexpr int dimension = ModelTraits::dimension();

    using WettingPhase = typename Traits::WettingPhase;
    using NonwettingPhase = typename Traits::NonwettingPhase;

public:
    //! Export the type used for the primary variables
    using PrimaryVariables = typename Traits::PrimaryVariables;

    //! Export type of fluid system
    using FluidSystem = typename Traits::FluidSystem;
    //! Export type of fluid state
    using FluidState = typename Traits::FluidState;
    //! Export the indices
    using Indices = typename ModelTraits::Indices;
    //! Export type of solid state
    using SolidState = typename Traits::SolidState;
    //! Export type of solid system
    using SolidSystem = typename Traits::SolidSystem;

    /*!
     * \brief Updates all quantities for a given control volume.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub control volume
    */
    template<class ElemSol, class Problem, class Element, class Scv>
    void update(const ElemSol &elemSol,
                const Problem &problem,
                const Element &element,
                const Scv& scv) //TODO: now it is "hard-coded" for phase0 and phase1Idx instead of "wettingPhase" and "nonWettingPhase"
    {
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        std::vector<Scalar> mobilitiesCoarse(numPhases, 0.0);

        priVars_ = elemSol[scv.localDofIndex()];
        extrusionFactor_ = problem.spatialParams().extrusionFactor(element, scv, elemSol);

        //if problem is problemFine, elementIdx will return index of element in fine grid, else the index of the coarse grid
        const unsigned int elementIdx = problem.gridGeometry().elementMapper().index(element);
        const Scalar deltaZ = problem.getDeltaZ();

        const GlobalPosition elementPos = element.geometry().center();
        const Scalar temperature = problem.spatialParams().temperatureAtPos(elementPos);

        //Distinguish the coarse-scale (first if-branch) and fine-scale problem (second if-branch)
        if(problem.isCoarseVEModel() == true)
        {
            Scalar dim = dimension; //in our case dim and dimWorld should always be the same
            const Scalar domainHeight = problem.gridGeometry().bBoxMax()[dim-1] - problem.gridGeometry().bBoxMin()[dim-1];
            const Scalar gravityNorm = problem.spatialParams().gravity(elementPos).two_norm();
            const auto fluidMatrixInteraction = problem.spatialParams().fluidMatrixInteractionAtPos(elementPos);
            const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
            const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
            const Scalar lambda = problem.spatialParams().getBrooksCoreyLambda();
            const Scalar entryP = problem.spatialParams().getBrooksCoreyEntryP();

            //----------coarse-scale volume variables----------
            //instead of using coarse element GlobalPosition, use its index -> faster computation
            const int columnIdx = problem.gridGeometry().elementMapper().index(element);
            const auto& priVars = elemSol[scv.localDofIndex()];

            Scalar coarseSatW = 1.0 - priVars[1];
            Scalar coarsePW = priVars[0];

            //use wetting-phase pressure for computation of all coarse-scale densities and viscosities
            fluidState_.setDensity(phase0Idx, WettingPhase::density(temperature, coarsePW));
            fluidState_.setViscosity(phase0Idx, WettingPhase::viscosity(temperature, coarsePW));
            fluidState_.setDensity(phase1Idx, NonwettingPhase::density(temperature, coarsePW));
            fluidState_.setViscosity(phase1Idx, NonwettingPhase::viscosity(temperature, coarsePW));

            Scalar gasPlumeDistThisColumn = problem.getQuantityReconstructor()->computeGasPlumeDist
                (std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                std::make_tuple(resSatW,resSatNw),
                gravityNorm,
                domainHeight,
                coarseSatW,
                std::make_tuple(lambda,entryP));

            //compute coarse-scale capillary pressure
            const Scalar pcCoarse = problem.getQuantityReconstructor()->computeCapillaryPressureCoarse
                (gasPlumeDistThisColumn,
                 std::make_tuple(this->density(phase0Idx), this->density(phase1Idx)),
                 gravityNorm,
                 entryP);

            //reconstruct fine-scale quantities
            std::vector<Scalar> reconstructedMobilites(numPhases, 0.0);
            GlobalPosition fineElementPos{};

            const Scalar permeabilityCoarse = problem.spatialParams().permeability(element, scv, elemSol);

            //time measurement
            std::optional<std::chrono::steady_clock::time_point> timeMobilitiesStart; //required as placeholder to use in the whole function
            if constexpr(Dumux::TimeMeasuringDetail::enableTimeMeasuringCoarseMobilities)
                timeMobilitiesStart = std::chrono::steady_clock::now();

           
            Scalar storedMinGasPlumeDistCurrent = domainHeight;
            auto lowerBoundIterator = problem.getMapColumns()->lower_bound(columnIdx);
            auto upperBoundIterator2 = problem.getMapColumns()->upper_bound(columnIdx);


            //utilize Dumux:parallelFor
            int countColumnEntries = std::distance(lowerBoundIterator, upperBoundIterator2);
            std::vector<Scalar> mobWEntries(countColumnEntries, 0.0);
            std::vector<Scalar> mobNwEntries(countColumnEntries, 0.0);
            Dumux::parallelFor(countColumnEntries, [&](const std::size_t columnElementIdx)
            {
                auto targetIterator = lowerBoundIterator;
                std::advance(targetIterator, columnElementIdx); //move iterator forward to desired index
                auto pair = *targetIterator;

                GlobalPosition fineElementPos = (pair.second).geometry().center();
                //calculate fine-scale mobilities
                std::vector<Scalar> reconstructedMobilites = problem.getQuantityReconstructor()->reconstMobilitiesFine
                    (std::make_tuple(gasPlumeDistThisColumn,storedMinGasPlumeDistCurrent),
                     std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
                     std::make_tuple(this->viscosity(phase0Idx),this->viscosity(phase1Idx)),
                     std::make_tuple(resSatW,resSatNw),
                     gravityNorm,
                     fineElementPos[dim-1],
                     deltaZ,
                     std::make_tuple(lambda,entryP));

                //calculate coarse-scale mobilities as integrals of fine-scale mobilities
                mobWEntries[columnElementIdx] = problem.spatialParams().permeabilityFineAtElement(pair.second) * reconstructedMobilites[phase0Idx] * deltaZ;
                mobNwEntries[columnElementIdx] = problem.spatialParams().permeabilityFineAtElement(pair.second) * reconstructedMobilites[phase1Idx] * deltaZ;
            });
            for(int columnElementIdx=0; columnElementIdx<countColumnEntries; columnElementIdx++)
            {
                mobilitiesCoarse[phase0Idx] += mobWEntries[columnElementIdx];
                mobilitiesCoarse[phase1Idx] += mobNwEntries[columnElementIdx];
            }

            completeFluidStateCoarse(elemSol, problem, element, scv, fluidState_, solidState_, elementIdx, pcCoarse, gasPlumeDistThisColumn);

            mobilitiesCoarse[phase0Idx] /= permeabilityCoarse;
            mobilitiesCoarse[phase1Idx] /= permeabilityCoarse;

            mobility_[phase0Idx] = mobilitiesCoarse[phase0Idx];
            mobility_[phase1Idx] = mobilitiesCoarse[phase1Idx];




            // Scalar storedMinGasPlumeDistCurrent = domainHeight;
            // auto lowerBoundIterator = problem.getMapColumns()->lower_bound(columnIdx);
            // auto upperBoundIterator2 = problem.getMapColumns()->upper_bound(columnIdx);

            // std::for_each(std::execution::par, lowerBoundIterator, upperBoundIterator2, [&dim, &fineElementPos, &reconstructedMobilites, &problem, &gasPlumeDistThisColumn, &storedMinGasPlumeDistCurrent, this, &resSatW, &resSatNw, &gravityNorm, &deltaZ, &lambda, &entryP, &mobilitiesCoarse](auto& pair) {

            //     fineElementPos = (pair.second).geometry().center();
            //     //calculate fine-scale mobilities
            //     reconstructedMobilites = problem.getQuantityReconstructor()->reconstMobilitiesFine
            //         (std::make_tuple(gasPlumeDistThisColumn,storedMinGasPlumeDistCurrent),
            //          std::make_tuple(this->density(phase0Idx),this->density(phase1Idx)),
            //          std::make_tuple(this->viscosity(phase0Idx),this->viscosity(phase1Idx)),
            //          std::make_tuple(resSatW,resSatNw),
            //          gravityNorm,
            //          fineElementPos[dim-1],
            //          deltaZ,
            //          std::make_tuple(lambda,entryP));

            //     //calculate coarse-scale mobilities as integrals of fine-scale mobilities
            //     mobilitiesCoarse[phase0Idx] += problem.spatialParams().permeabilityFineAtElement(pair.second) * reconstructedMobilites[phase0Idx] * deltaZ;
            //     mobilitiesCoarse[phase1Idx] += problem.spatialParams().permeabilityFineAtElement(pair.second) * reconstructedMobilites[phase1Idx] * deltaZ;

            //     // std::cout << "@fine pos: " << fineElementPos << ", we have fine perms: " << problem.spatialParams().permeabilityFineAtElement(pair.second) << " and reconst mobs: " << reconstructedMobilites[phase0Idx] << " "  << reconstructedMobilites[phase1Idx] << std::endl;

            //     std::cout << "@fine pos: " << fineElementPos << ", we have fine perms: " << problem.spatialParams().permeabilityFineAtElement(pair.second) << " and reconst mobs: " << reconstructedMobilites[phase0Idx] << " "  << reconstructedMobilites[phase1Idx] << std::endl; 
            // });


            // mobilitiesCoarse[phase0Idx] /= permeabilityCoarse;
            // mobilitiesCoarse[phase1Idx] /= permeabilityCoarse;

            // std::cout << "ML mobs: " << mobility_[phase0Idx] << " " << mobility_[phase1Idx] << ", analy mobs: " << mobilitiesCoarse[phase0Idx] << " " << mobilitiesCoarse[phase1Idx] << " @pos: " << elementPos << std::endl;






            //for time measurement
            if constexpr(Dumux::TimeMeasuringDetail::enableTimeMeasuringCoarseMobilities)
            {
                std::chrono::steady_clock::time_point timeMobilitiesEnd = std::chrono::steady_clock::now();
                const double singleRunZpTime = std::chrono::duration_cast<std::chrono::microseconds>(timeMobilitiesEnd -
                    *timeMobilitiesStart).count();
                problem.getTimeMeasurer().addToTotalTimeCoarseLambdas(singleRunZpTime);
            }

            const auto& spatialParams = problem.spatialParams();
            // porosity calculation over inert volumefraction
            updateSolidVolumeFractions(elemSol, problem, element, scv, solidState_, numFluidComps);
            EnergyVolVars::updateSolidEnergyParams(elemSol, problem, element, scv, solidState_);
            permeability_ = permeabilityCoarse;
            EnergyVolVars::updateEffectiveThermalConductivity();
            porosity_ = spatialParams.porosityCoarseAtElement(element); //porosity might already (also) be set in updateSolidVolumeFractions()
            swDerivative_ = problem.computeCentralDerivativeSw(elementPos);
        }
        else //fine-scale
        {
            //----------fine-scale volume variables----------
            auto coarseSolution = problem.getCoarseSolution();
            auto coarseElement = problem.getOtherProblem()->returnCorrespondingCoarseElement(elementIdx);
            const auto coarseElementIdx = problem.getOtherProblem()->gridGeometry().elementMapper().index(coarseElement);

            const auto pWCoarse = coarseSolution[coarseElementIdx][0];
            const auto satWCoarse = 1.0 - coarseSolution[coarseElementIdx][1];

            const Scalar temperature = problem.spatialParams().temperatureAtPos(elementPos);
            Scalar densityWCoarse = WettingPhase::density(temperature, pWCoarse);
            Scalar densityNwCoarse = NonwettingPhase::density(temperature, pWCoarse);
            Scalar viscosityWCoarse = WettingPhase::viscosity(temperature, pWCoarse);
            Scalar viscosityNwCoarse = NonwettingPhase::viscosity(temperature, pWCoarse);

            Scalar dim = dimension; //TODO: read in from gridView
            const Scalar domainHeight = problem.gridGeometry().bBoxMax()[dim-1] - problem.gridGeometry().bBoxMin()[dim-1];
            const Scalar gravityNorm = problem.spatialParams().gravity(elementPos).two_norm();
            const auto fluidMatrixInteraction = problem.spatialParams().fluidMatrixInteractionAtPos(elementPos);
            const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
            const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
            const Scalar lambda = problem.spatialParams().getBrooksCoreyLambda();
            const Scalar entryP = problem.spatialParams().getBrooksCoreyEntryP();
            const Scalar deltaZ = problem.getDeltaZ();

            Scalar gasPlumeDistThisColumn = problem.getQuantityReconstructor()->computeGasPlumeDist
                (std::make_tuple(densityWCoarse, densityNwCoarse),
                    std::make_tuple(resSatW, resSatNw),
                    gravityNorm,
                    domainHeight,
                    satWCoarse,
                    std::make_tuple(lambda, entryP));

            this->updateFine(problem, element, pWCoarse, satWCoarse, gasPlumeDistThisColumn, densityWCoarse, densityNwCoarse, viscosityWCoarse, viscosityNwCoarse, domainHeight, gravityNorm, resSatW, resSatNw, lambda, entryP, deltaZ); //this implementation is used for output. The function updateFine on its own is used in the couplingManager
        }
    }


    /*!
     * \brief Updates all quantities for a fine-scale element.
     *
     * \param problem The object specifying the problem which ought to
     *                be simulated (here: fine-scale problem)
     * \param element An element which contains part of the control volume
    */
    template<class Problem, class Element>
    void updateFine(const Problem &problem,
                    const Element &element,
                    const double &coarsePw,
                    const double &coarseSatW,
                    const double &gasPlumeDistThisColumn,
                    const double &densityWCoarse,
                    const double &densityNwCoarse,
                    const double &viscosityWCoarse,
                    const double &viscosityNwCoarse,
                    const double &domainHeight,
                    const double &gravityNorm,
                    const double &resSatW,
                    const double &resSatNw,
                    const double &lambda,
                    const double &entryP,
                    const double &deltaZ)
    {
        Scalar dim = dimension; //in our case dim and dimWorld should always be the same

        //----------fine-scale volume variables----------
        const auto fineElementPos = element.geometry().center();

        //use wetting-phase pressure for computation of all coarse-scale densities
        const Scalar temperature = problem.spatialParams().temperatureAtPos(fineElementPos);

        //reconstruct fine-scale pressures
        std::vector<Scalar> reconstructedPressures = problem.getQuantityReconstructor()->reconstPressure
            (gasPlumeDistThisColumn,
            std::make_tuple(densityWCoarse, densityNwCoarse),
            gravityNorm,
            fineElementPos[dim-1],
            coarsePw,
            entryP);

        //reconstruct fine-scale saturations
        Scalar storedMinGasPlumeDistCurrent = gasPlumeDistThisColumn; //TODO: this should be properly minGasPlumeDist
        Scalar reconstructedSaturationW = problem.getQuantityReconstructor()->computeSaturationIntegral
            (std::make_tuple(gasPlumeDistThisColumn, storedMinGasPlumeDistCurrent),
            std::make_tuple(densityWCoarse, densityNwCoarse),
            std::make_tuple(resSatW, resSatNw),
            gravityNorm,
            fineElementPos[dim-1],
            deltaZ,
            std::make_tuple(lambda, entryP));
        Scalar reconstructedSaturationNw = 1.0 - reconstructedSaturationW;

        //reconstruct fine-scale capillary pressures
        Scalar reconstructedCapillaryPressure = problem.getQuantityReconstructor()->reconstCapillaryPressure
            (gasPlumeDistThisColumn,
            std::make_tuple(densityWCoarse, densityNwCoarse),
            gravityNorm,
            fineElementPos[dim-1],
            entryP);

        //calculate fine-scale mobilities
        std::vector<Scalar> reconstructedMobilites(numPhases, 0.0);
        reconstructedMobilites = problem.getQuantityReconstructor()->reconstMobilitiesFine
            (std::make_tuple(gasPlumeDistThisColumn, storedMinGasPlumeDistCurrent),
                std::make_tuple(densityWCoarse, densityNwCoarse),
                std::make_tuple(viscosityWCoarse, viscosityNwCoarse),
                std::make_tuple(resSatW,resSatNw),
                gravityNorm,
                fineElementPos[dim-1],
                deltaZ,
                std::make_tuple(lambda,entryP));

        fluidState_.setTemperature(temperature);
        //TODO: This ONLY holds if phase0Idx is wetting phase and phase1Idx is non-wetting phase
        fluidState_.setSaturation(phase1Idx, reconstructedSaturationNw);
        fluidState_.setSaturation(phase0Idx, reconstructedSaturationW);

        fluidState_.setPressure(phase0Idx, reconstructedPressures[0]);
        pc_ = reconstructedCapillaryPressure;
        fluidState_.setPressure(phase1Idx, reconstructedPressures[1]);

        fluidState_.setDensity(phase0Idx, densityWCoarse);
        fluidState_.setDensity(phase1Idx, densityNwCoarse);
        fluidState_.setViscosity(phase0Idx, viscosityWCoarse);
        fluidState_.setViscosity(phase1Idx, viscosityNwCoarse);

        mobility_[phase0Idx] = reconstructedMobilites[0];
        mobility_[phase1Idx] = reconstructedMobilites[1];

        const auto& spatialParams = problem.spatialParams();
        permeability_ = spatialParams.permeabilityFineAtElement(element);
        porosity_ = spatialParams.porosityFineAtElement(element);
        extrusionFactor_ = spatialParams.extrusionFactorAtElement(element);
    }


    /*!
     * \brief Sets complete fluid state. This function is used for the coarse-scale elements.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problemCoarse The object specifying the coarse-scale problem which ought to be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub-control volume
     * \param fluidState A container with the current (physical) state of the fluid
     * \param solidState A container with the current (physical) state of the solid
     * \param elementCoarseIdx Index of the coase-scale element
     * \param pcCoarse Coarsened capillary pressure of coarse-scale element
     * \param gasPlumeDist gas plume distance/height belonging to the coarse-scale element
     *
     * Set temperature, saturations, capillary pressures, viscosities, densities and enthalpies.
     */
    template<class ElemSol, class Problem, class Element, class Scv>
    void completeFluidStateCoarse(const ElemSol& elemSol,
                                  const Problem& problemCoarse,
                                  const Element& element,
                                  const Scv& scv,
                                  FluidState& fluidState,
                                  SolidState& solidState,
                                  const int& elementCoarseIdx,
                                  const Scalar& pcCoarse,
                                  const Scalar& gasPlumeDist)
    {
        EnergyVolVars::updateTemperature(elemSol, problemCoarse, element, scv, fluidState, solidState);

        const auto& spatialParams = problemCoarse.spatialParams();

        const auto& priVars = elemSol[scv.localDofIndex()];

        const auto wPhaseIdx = spatialParams.template wettingPhase<FluidSystem>(element, scv, elemSol);
        fluidState.setWettingPhase(wPhaseIdx);
        if (formulation == TwoPVEFormulation::p0s1)
        {
            fluidState.setPressure(phase0Idx, priVars[pressureIdx]);
            if (fluidState.wettingPhase() == phase1Idx)
            {
                fluidState.setSaturation(phase1Idx, priVars[saturationIdx]);
                fluidState.setSaturation(phase0Idx, 1 - priVars[saturationIdx]);

                pc_ = pcCoarse; //TODO: not tested yet
                fluidState.setPressure(phase1Idx, priVars[pressureIdx] + pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
            else
            {
                const auto Sn = Traits::SaturationReconstruction::reconstructSn(spatialParams, element,
                                                                                scv, elemSol, priVars[saturationIdx]);

                fluidState.setSaturation(phase1Idx, Sn);
                fluidState.setSaturation(phase0Idx, 1 - Sn);

                pc_ = pcCoarse; //this is our case currently
                fluidState.setPressure(phase1Idx, priVars[pressureIdx] + pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
        }
        else if (formulation == TwoPVEFormulation::p1s0)
        {
            fluidState.setPressure(phase1Idx, priVars[pressureIdx]);
            if (wPhaseIdx == phase1Idx)
            {
                const auto Sn = Traits::SaturationReconstruction::reconstructSn(spatialParams, element,
                                                                                scv, elemSol, priVars[saturationIdx]);
                fluidState.setSaturation(phase0Idx, Sn);
                fluidState.setSaturation(phase1Idx, 1 - Sn);

                pc_ = pcCoarse; //TODO: not tested yet
                fluidState.setPressure(phase0Idx, priVars[pressureIdx] - pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
            else
            {
                fluidState.setSaturation(phase0Idx, priVars[saturationIdx]);
                fluidState.setSaturation(phase1Idx, 1 - priVars[saturationIdx]);

                pc_ = pcCoarse; //TODO: not tested yet
                fluidState.setPressure(phase0Idx, priVars[pressureIdx] - pc_);

                gasPlumeDist_ = gasPlumeDist;
            }
        }


        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState);

        for (int phaseIdx = 0; phaseIdx < ModelTraits::numFluidPhases(); ++phaseIdx) {
            // compute and set the enthalpy
            Scalar h = EnergyVolVars::enthalpy(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }



    /*!
     * \brief Returns the phase state for the control volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \brief Returns the phase state for the control volume.
     */
    const SolidState &solidState() const
    { return solidState_; }

    /*!
     * \brief Returns the saturation of a given phase within
     *        the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar saturation(int phaseIdx) const
    { return fluidState_.saturation(phaseIdx); }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume in \f$[kg/m^3]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar density(int phaseIdx) const
    { return fluidState_.density(phaseIdx); }

    /*!
     * \brief Returns the effective pressure of a given phase within
     *        the control volume in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar pressure(int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \brief Returns the capillary pressure within the control volume
     * in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     */
    Scalar capillaryPressure() const
    { return pc_; }

    /*!
     * \brief Returns temperature inside the sub-control volume
     * in \f$[K]\f$.
     *
     * Note that we assume thermodynamic equilibrium, i.e. the
     * temperature of the rock matrix and of all fluid phases are
     * identical.
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }

    /*!
     * \brief Returns the dynamic viscosity of the fluid within the
     *        control volume in \f$\mathrm{[Pa s]}\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar viscosity(int phaseIdx) const
    { return fluidState_.viscosity(phaseIdx); }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume in \f$[s*m/kg]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar mobility(int phaseIdx) const
    { return mobility_[phaseIdx]; }

//     /*!
//      * \brief Returns the average porosity within the control volume in \f$[-]\f$.
//      */
//     Scalar porosity() const
//     { return solidState_.porosity(); }

    /*!
     * \brief Returns the average porosity within the control volume in \f$[-]\f$.
     */
    Scalar porosity() const
    { return porosity_; }

    /*!
     * \brief Returns the permeability within the control volume in \f$[m^2]\f$.
     */
    const PermeabilityType& permeability() const
    { return permeability_; }

    /*!
     * \brief Returns the wetting phase index
     */
    int wettingPhase() const
    {  return fluidState_.wettingPhase(); }


    /*!
     * \brief Returns the gas plume distance within a coarse-scale element
     */
    const Scalar& gasPlumedist() const
    { return gasPlumeDist_; }


    const Scalar& swDerivative() const
    { return swDerivative_; }


    /*!
     * \brief Returns how much the sub-control volume is extruded.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    Scalar extrusionFactor() const
    { return extrusionFactor_; }

protected:
    FluidState fluidState_;
    SolidState solidState_;

private:
    Scalar pc_;
    Scalar porosity_;
    PermeabilityType permeability_;
    Scalar mobility_[ModelTraits::numFluidPhases()];

    Scalar gasPlumeDist_;
    Scalar extrusionFactor_; //extrusionFactor from ParentType is "overloaded"

    Scalar swDerivative_;

    PrimaryVariables priVars_;
};

} // end namespace Dumux

#endif
