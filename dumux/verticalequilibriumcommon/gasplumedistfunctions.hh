// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPVE
 * \brief Holds lambda functions for solving for the gas plume distance gasPlumeDist.
 */

#ifndef DUMUX_TWOPVE_GASPLUMEDISTFUNCTIONS_HH
#define DUMUX_TWOPVE_GASPLUMEDISTFUNCTIONS_HH

namespace Dumux {

class TwoPVEGasPlumeFunctions;

/*!
 * \ingroup TwoPVE
 * \brief This class holds all the necessary residuals and derivatives for calculating the gasPlumeDist by solving a balance of brine phase: balance 
 * overall volume of brine phase and the integral of the brine phase saturation over the vertical direction.
 */

class TwoPVEGasPlumeFunctions
{    

public:
    TwoPVEGasPlumeFunctions(double lambda,
                            double entryP,
                            double densityW,
                            double densityNw,
                            double gravityNorm,
                            double resSatW,
                            double resSatNw,
                            double satW,
                            double domainHeight)
        :   lambda_(lambda),
            entryP_(entryP),
            densityW_(densityW),
            densityNw_(densityNw),
            gravityNorm_(gravityNorm),
            resSatW_(resSatW),
            resSatNw_(resSatNw),
            satW_(satW),
            domainHeight_(domainHeight)
    {}


    /*!
     * \brief For testing purposes TODO: delete
     */
    std::function<double (double)> functionTest()
    {
        return [&](double x)
        {   
            return x*x - 5.0;
        };
    }
    

    /*!
     * \brief For testing purposes TODO: delete
     */
    std::function<double (double)> derivativeTest()
    {
        return [&](double x)
        { 
            return 2*x;
        };
    }


    //TODO: expects the lower boundary height to be at 0.0!!!
    /*!
     * \brief Evaluates the residual function for the case that the gasPlumedist z_p is larger than the minimal gasPlumeDist z_p^min
     * and the the integral over the fine-scale saturation (with z_p = z_p^min) is smaller than the overall volume of brine phase in this column
     */
    const std::function<double (double)> columnMassConservation() const
    {
        return [&](double x)
        {
            auto A = std::pow(entryP_,lambda_) * (1.0-resSatW_-resSatNw_);
            return
            x - 0.0
            - satW_ * domainHeight_
            + (1.0/(1.0-lambda_)) * (1.0/((densityW_ - densityNw_)*gravityNorm_)) * A * ( std::pow(entryP_+(densityW_ - densityNw_)*gravityNorm_*(domainHeight_-x), 1.0-lambda_) - std::pow(entryP_, 1.0-lambda_) )
            + resSatW_*(domainHeight_-x);
        };
    }


    //TODO: expect the lower boundary height to be at 0.0!!!
    /*!
     * \brief Evaluates the derivative of the residual function for the case that the gasPlumedist z_p is LARGER than the minimal gasPlumeDist z_p^min
     * and the the integral over the fine-scale saturation (with z_p = z_p^min) is SMALLER than the overall volume of brine phase in this column
     */
    const std::function<double (double)> derivativeColumnMassConservation() const
    {
        return [&](double x)
        {
            auto A = std::pow(entryP_,lambda_) * (1.0-resSatW_-resSatNw_);
            return
            1.0
            + A * ( std::pow(entryP_+(densityW_ - densityNw_)*gravityNorm_*(domainHeight_-x), -lambda_) ) *(-1)
            -resSatW_;
        };
    }


private:
    double lambda_;
    double entryP_;
    double densityW_;
    double densityNw_;
    double gravityNorm_;
    double resSatW_;
    double resSatNw_;
    double satW_;
    double domainHeight_;
};
} // end namespace Dumux

#endif
