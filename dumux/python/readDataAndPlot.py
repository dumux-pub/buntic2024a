import os
import matplotlib.pyplot as plt
from functionCollectionAutomateDataPlot import returnDataSet, find_metaData_files, returnPartOfFileName, returnDataListAndLabelList, plot2pVE, plot2p, plotAdaptive, findCorrespondingDataFields, plotSpecificQuantities, removeEntries, onlyKeepEntries

import matplotlib
font = {'family' : 'serif',
        'weight' : 'normal',
        'size'   : 20}
matplotlib.rc('font', **font)
matplotlib.rc('lines', linewidth=2)

def sortFunc(string):
    substrings = string.split('_')
    discrPart = substrings[1] #0th entry is supposed to be model name, 2nd entry is supposed to be lens string
    numberCells = discrPart.split('x')
    return int(numberCells[0])*int(numberCells[2]) # we expect format xCells x yCells x zCells - yCells will be 0 in 2D

def sortFuncTuple(myTuple): #myTuple should consist of 2 element: 1st: label, 2nd: dataContainer
    substrings = myTuple[0].split('_')
    discrPart = substrings[1] #0th entry is supposed to be model name, 2nd entry is supposed to be lens string
    numberCells = discrPart.split('x')
    return int(numberCells[0])*int(numberCells[2]) # we expect format xCells x yCells x zCells - yCells will be 0 in 2D

def sortAccordingToTotalDiscretization(labelContainer, dataContainer):
    newSortedCont1 = [x for _,x in sorted(zip(labelContainer,dataContainer), key=sortFuncTuple)]
    dataContainer = newSortedCont1
    labelContainer = sorted(labelContainer, key=sortFunc)

    return labelContainer, dataContainer

# Read in 2pVE model data
folder_path_2pVE = "../../build-cmake/test/porousmediumflow/2pve/"
metaData_files_2pVE = find_metaData_files(folder_path_2pVE)
dataFields_2pVE, labelList_2pVE = returnDataListAndLabelList(sorted(metaData_files_2pVE)) #sort metaData list alphabetically, so the plots appear from coarsest to finest discretization

# Read in  2p model data
folder_path_2p = "../../build-cmake/test/porousmediumflow/2p/"
metaData_files_2p = find_metaData_files(folder_path_2p)
dataFields_2p, labelList_2p = returnDataListAndLabelList(sorted(metaData_files_2p)) #sort metaData list alphabetically, so the plots appear from coarsest to finest discretization

# Read in  adaptive model data
folder_path_adaptive = "../../build-cmake/test/multidomain/couplingsubgrid/"
metaData_files_adaptive = find_metaData_files(folder_path_adaptive)
dataFields_adaptive, labelList_adaptive = returnDataListAndLabelList(sorted(metaData_files_adaptive)) #sort metaData list alphabetically, so the plots appear from coarsest to finest discretization


# Only keep data sets which occur in both data fields
findCorrespondingDataFields(dataFields_2p, labelList_2p, dataFields_adaptive, labelList_adaptive) #changes the list by reference. Is this desired?

## Remove specified entries, to stop cluttering the graph
#toRemove = ('125x0x15_Lenses1', '125x0x30_Lenses1', '150x0x25_Lenses1', '250x0x15_Lenses1', '1000x0x60_Lenses1')
#for label in toRemove:
    #removeEntries(dataFields_2pVE, labelList_2pVE, label)
    #removeEntries(dataFields_2p, labelList_2p, label)
    #removeEntries(dataFields_adaptive, labelList_adaptive, label)

# Keep only specified entries
toKeep = ('500x0x30_Lenses1', '500x0x60_Lenses1', '1000x0x60_Lenses1')
# toKeep = ('250x0x30_Lenses1')
onlyKeepEntries(dataFields_2pVE, labelList_2pVE, toKeep)
onlyKeepEntries(dataFields_2p, labelList_2p, toKeep)
onlyKeepEntries(dataFields_adaptive, labelList_adaptive, toKeep)

#sort labels and data containers according to number of elements
labelList_2pVE, dataFields_2pVE = sortAccordingToTotalDiscretization(labelList_2pVE, dataFields_2pVE)
labelList_2p, dataFields_2p = sortAccordingToTotalDiscretization(labelList_2p, dataFields_2p)
labelList_adaptive, dataFields_adaptive = sortAccordingToTotalDiscretization(labelList_adaptive, dataFields_adaptive)



# Plot individual data
plot2pVE(dataFields_2pVE, labelList_2pVE, folder_path_2pVE)
plot2p(dataFields_2p, labelList_2p, folder_path_2p)
plotAdaptive(dataFields_adaptive, labelList_adaptive, folder_path_adaptive)
plt.show() #keep the generated figures open at the end of the program

#Generate desired plots----------------------------------------------------------------------------------------------------------------------------------
print(f"Generating comparative plots.")
# Compare 2p and adaptive
#plotSpecificQuantities(dataFields_2p, dataFields_adaptive, labelList_2p, labelList_adaptive, 'simulatedTime', 'wallTime', 'Simulation time [days]', 'CPU time [s]')
plotSpecificQuantities(dataFields_2p, dataFields_adaptive, labelList_2p, labelList_adaptive, 'simulatedTime', 'timeStepSize', 'Simulation time [days]', 'Time step size [s]')
#plotSpecificQuantities(dataFields_2p, dataFields_adaptive, labelList_2p, labelList_adaptive, 'simulatedTime', 'timeStep')

## Compare 2p and VE
#plotSpecificQuantities(dataFields_2p, dataFields_2pVE, labelList_2p, labelList_2pVE, 'simulatedTime', 'wallTime', 'Simulation time [days]', 'CPU time [s]')
#plotSpecificQuantities(dataFields_2p, dataFields_2pVE, labelList_2p, labelList_2pVE, 'simulatedTime', 'timeStepSize', 'Simulation time [days]', 'Time step size [s]')
#plotSpecificQuantities(dataFields_2p, dataFields_2pVE, labelList_2p, labelList_2pVE, 'simulatedTime', 'timeStep')

plt.show()
