import os
import re
import sys
from prettytable import PrettyTable

# Function to calculate average iterations from a log file
def calculate_average_iterations(log_file):
    pattern = r' IT=(\d+)'
    sum_iterations = 0
    count_iterations = 0

    try:
        with open(log_file, 'r') as file:
            for line in file:
                match = re.search(pattern, line)
                if match:
                    iteration_count = int(match.group(1)) + 1
                    sum_iterations += iteration_count
                    count_iterations += 1

        if count_iterations > 0:
            return sum_iterations / count_iterations
        else:
            return 0

    except Exception as e:
        print(f"Error processing linear solver iterations in file '{log_file}': {e}")
        return None


def deduce_newton_iterations(log_file):
    pattern = r'Total\s+Newton\s+iterations:\s+(\d+)'

    try:
        with open(log_file, 'r') as file:
            for line in file:
                match = re.search(pattern, line)
                if match:
                    newton_iterations = int(match.group(1))

        if newton_iterations > 0:
            return newton_iterations
        else:
            return 0

    except Exception as e:
        print(f"Error processing Newton iterations in file '{log_file}': {e}")
        return None



def deduce_massbalance_error(log_file):
    pattern1 = r'Error on in massNw: (-?\d+(\.\d+)?(e[\+-]?\d+)?)'
    pattern2 = r'Error on FD\+CoarseVE scale: (-?\d+(\.\d+)?(e[\+-]?\d+)?)'

    try:
        with open(log_file, 'r') as file:
                text = file.read()

    except Exception as e:
        print(f"Error processing mass balance error in file '{log_file}': {e}")
        return None

    matches1 = re.findall(pattern1, text)
    matches2 = re.findall(pattern2, text)

    if matches1:
        # Extract the last occurrence (last element of the list)
        last_occurrence = matches1[-1][0]
    elif matches2:
        # Extract the last occurrence (last element of the list)
        last_occurrence = matches2[-1][0]
    else:
        print("Pattern not found in the text.")

    return last_occurrence


def deduce_runtime(log_file):
    pattern = r"Simulation took (\d+(\.\d+)?)"

    try:
        with open(log_file, 'r') as file:
            for line in file:
                match = re.search(pattern, line)
                if match:
                    runtime = match.group(1)
                    return runtime

    except Exception as e:
        print(f"Error processing runtimes in file '{log_file}': {e}")
        return None


def sort_table(table, columnIdx):
    rows = [tuple(row) for row in table._rows]

    # Sort the rows by the first column (index columnIdx)
    sorted_rows = sorted(rows, key=lambda row: row[columnIdx])

    # Create a new PrettyTable and add sorted rows
    sorted_table = PrettyTable()
    sorted_table.field_names = table.field_names  # Ensure field names are set
    for row in sorted_rows:
        sorted_table.add_row(row)

    # print(sorted_table)

    return sorted_table


# Main function to traverse directories and create the table
def create_table(top_dir):
    # Create a PrettyTable instance with column headers
    table = PrettyTable(['Subfolder', 'Average Linear Solver Iterations', 'Total Newton iterations', 'Total linear solver iterations', 'Mass (nw) Balance Error', 'Runtime'])
    table.align['Subfolder'] = 'l'  # Left align subfolder names

    for root, dirs, files in os.walk(top_dir):
        for dir_name in dirs:
            log_file = os.path.join(root, dir_name, f"{dir_name}_log.txt")
            if os.path.isfile(log_file):
                print(f"log_file: {log_file}")
                avg_linsolver_iterations = calculate_average_iterations(log_file)
                newton_iterations = deduce_newton_iterations(log_file)
                linsolver_iterations = newton_iterations*avg_linsolver_iterations
                massbalance_error = deduce_massbalance_error(log_file)
                runtime = deduce_runtime(log_file)
                # if avg_linsolver_iterations is not None:
                table.add_row([dir_name, f"{avg_linsolver_iterations:.2f}", f"{newton_iterations}", f"{linsolver_iterations:.2f}", f"{massbalance_error}", f"{runtime}"])
                    # Print the formatted table

    table = sort_table(table, 0)
    print(table)

# Check if the top-level directory is provided as a command-line argument
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py <top_level_directory>")
        sys.exit(1)

    top_dir = sys.argv[1]
    create_table(top_dir)

