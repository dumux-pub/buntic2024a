import pyvista as pv
import math
import re

from functionCollectionAutomateDataPlot import find_vtu_files, findCorrespondingDataFields, createLabelList, sortAllContainers, onlyKeepEntries

def convertToInnerBoundary(boundary):
    eps = 1e-10

    boundary[0] = eps + boundary[0]
    boundary[1] = -eps + boundary[1]
    boundary[2] = eps + boundary[2]
    boundary[3] = -eps + boundary[3]
    boundary[4] = 0.0
    boundary[5] = 0.0


def coarsenSolution(elementMapping, refSolution):
    coarsenedSolution = []

    numberLocalReferenceElements = len(elementMapping[0])
    for refElements in elementMapping:
        meanValue = 0.0
        for refIndices in refElements:
            meanValue += refSolution[refIndices]
        meanValue /= numberLocalReferenceElements
        coarsenedSolution.append(meanValue)

    if len(elementMapping) != len(coarsenedSolution):
        raise IndexError("Something went wrong during the coarsening process, the reference solution does not seem to be coarsened to the right size!")

    return coarsenedSolution


def computeError(solution1, solution2):
    if len(solution1) != len(solution2):
        raise IndexError("The solutions have differing sizes!")

    sLiqError = 0.0
    for index in range(0, len(solution1)):
        sLiqError += (solution1[index] - solution2[index]) ** 2

    sLiqError = math.sqrt((1/len(solution1))*sLiqError)

    #print(f"Total RMSE-error in wetting-phase saturation: {sLiqError}")

    return sLiqError

def checkSizes(refPath1, path2):
    discrString1 = re.search(r"\d+x\d+", refPath1).group() # reference file follows the naming convention "XCELLSxZCELLS"
    discrString2 = re.search(r"\d+x\d+x\d+", path2).group() # comparing files follow the convention xCells "XCELLSxYCELLSxZCELLS" 

    print(f"Reference discretization: {discrString1}")
    print(f"Comparing discretization: {discrString2}")

    stringParts1 = discrString1.split('x')
    stringParts2 = discrString2.split('x')

    refXNumberCells = int(stringParts1[0])
    refZNumberCells = int(stringParts1[1])
    roughXNumberCells = int(stringParts2[0])
    roughZNumberCells = int(stringParts2[2])

    if (refXNumberCells%roughXNumberCells != 0) or (refZNumberCells%roughZNumberCells != 0):
        return True
        #raise ValueError(f"The number of reference cells ({refXNumberCells}x{refZNumberCells}) divided by the number of coarse cells ({roughXNumberCells}x{roughZNumberCells}) in each direction must be a whole number!")
    else:
        return False


def integratedReferenceValue(refSol):
    refIntegral = 0.0
    for index in range(0, len(refSol)):
        refIntegral += (refSol[index]) ** 2

    refIntegral = math.sqrt((1/len(refSol))*refIntegral)

    return refIntegral


from prettytable import PrettyTable
def computeAllErrors(refVTU, myVTUs_adaptive, labelList_adaptive):
    myTable = PrettyTable(["Model", "Discretization", "WithLens?", "RMSE"])
    for index in range(0,len(myVTUs_adaptive)):
        substrings = labelList_adaptive[index].split('_')
        modelName = substrings[0]
        discrName = substrings[1]
        lensName = substrings[2][-1] #get last character of string

        print(f"Currently working on {modelName}_{discrName}_Lenses{lensName}")

        shouldSkip = checkSizes(refVTU, myVTUs_adaptive[index])
        if shouldSkip:
            print(f"Sizes do not match, skipping {modelName}_{discrName}_Lenses{lensName}")
            continue

        mesh_adaptive = pv.read(myVTUs_adaptive[index])
        sLiq_adaptive = mesh_adaptive['S_liq']

        mesh_ref = pv.read(refVTU)
        sLiq_ref = mesh_ref['S_liq']





        onePercenthPart = round(mesh_adaptive.number_of_cells/100)
        indexMappingCoarseToFine = []
        for i in range(0,mesh_adaptive.number_of_cells):
        #for i in range(0,20):
            if i % onePercenthPart == 0:
                print(f"Currently at {100*i/mesh_adaptive.number_of_cells:.2f}% with i: {i}")

            currentCell = mesh_adaptive.get_cell(i)
            currentCenter = currentCell.center
            currentBoundsTuple = currentCell.bounds
            currentBoundsList = list(currentBoundsTuple)
            convertToInnerBoundary(currentBoundsList)
            currentBoundsTuple = tuple(currentBoundsList)

            fineCellsContained = list(mesh_ref.find_cells_within_bounds(currentBoundsTuple))
            indexMappingCoarseToFine.append(fineCellsContained)
        #print(indexMappingCoarseToFine)
        print(f"In one coarse element there are {len(indexMappingCoarseToFine[0])} fine elements.")

        #length check
        for fineElemIdxs in indexMappingCoarseToFine:
            if len(fineElemIdxs) != len(indexMappingCoarseToFine[0]):
                raise IndexError("The number of reference elements within on coarse element must be consistent for all elements!")


        coarsenedSolution = coarsenSolution(indexMappingCoarseToFine, sLiq_ref)




        #RMSE = computeError(myVTUs_adaptive[index], myVTUs_2p[index])
        RMSE = computeError(sLiq_adaptive, coarsenedSolution) #absolute error
        RMSE = RMSE/integratedReferenceValue(sLiq_ref) #relative error
        print(f"Computed the relative error: {RMSE}")
        myTable.add_row([f"{modelName}", f"{discrName}", f"{lensName}", f"{RMSE}"])
    print(myTable)




folder_path_2pVE = "../../build-cmake/test/porousmediumflow/2pve/"
folder_path_2p = "../../build-cmake/test/porousmediumflow/2p/"
folder_path_adaptive = "../../build-cmake/test/multidomain/couplingsubgrid"

myVTUs_2pve = find_vtu_files(folder_path_2pVE)
labelList_2pve = createLabelList(myVTUs_2pve, '2pve')

myVTUs_2p = find_vtu_files(folder_path_2p)
labelList_2p = createLabelList(myVTUs_2p, '2p')
refVTU_lens = "test_cc2p_plume1000x60_Lenses1-00001.vtu"
refVTU_no_lens = "test_cc2p_plume1000x60_Lenses0-00001.vtu"
#mesh_ref = pv.read(refVTU)
#sLiq_ref = mesh_ref['S_liq']

# print(myVTUs_2p)

myVTUs_adaptive = find_vtu_files(folder_path_adaptive)
labelList_adaptive = createLabelList(myVTUs_adaptive, 'adaptive')

print(myVTUs_adaptive)

findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_2pve, labelList_2pve)
findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_adaptive, labelList_adaptive)
findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_2pve, labelList_2pve)
myVTUs_2p, myVTUs_2pve, myVTUs_adaptive, labelList_2p, labelList_2pve, labelList_adaptive = sortAllContainers(myVTUs_2p, myVTUs_2pve, myVTUs_adaptive, labelList_2p, labelList_2pve, labelList_adaptive)

#print(labelList_2pve)
#print(myVTUs_2pve)


computeAllErrors(refVTU_lens, myVTUs_adaptive, labelList_adaptive)

computeAllErrors(refVTU_lens, myVTUs_2pve, labelList_2pve)

computeAllErrors(refVTU_lens, myVTUs_2p, labelList_2p)




#toKeep = ('1000x0x30_Lenses1', '1000x0x60_Lenses1')
#onlyKeepEntries(myVTUs_adaptive, labelList_adaptive, toKeep)
#onlyKeepEntries(myVTUs_2p, labelList_2p, toKeep)

#computeAllErrors(refVTU_lens, myVTUs_2p, labelList_2p)
#computeAllErrors(refVTU_lens, myVTUs_adaptive, labelList_adaptive)



# toKeep = ('500x0x30_Lenses0', '500x0x60_Lenses0')
# onlyKeepEntries(myVTUs_adaptive, labelList_adaptive, toKeep)
# onlyKeepEntries(myVTUs_2p, labelList_2p, toKeep)
# onlyKeepEntries(myVTUs_2pve, labelList_2pve, toKeep)

# computeAllErrors(refVTU_no_lens, myVTUs_2p, labelList_2p)
# computeAllErrors(refVTU_no_lens, myVTUs_adaptive, labelList_adaptive)
# computeAllErrors(refVTU_no_lens, myVTUs_2pve, labelList_2pve)

