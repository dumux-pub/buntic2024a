import subprocess

subprocess.run([f"python3 ../../build-cmake/test/porousmediumflow/2pve/adaptivity_paper_generate_results_pure_ve.py"], shell=True)
subprocess.run([f"python3 ../../build-cmake/test/porousmediumflow/2p/adaptivity_paper_generate_results_pure_fd.py"], shell=True)
subprocess.run([f"python3 ../../build-cmake/test/multidomain/couplingsubgrid/adaptivity_paper_generate_results_multidomain.py"], shell=True)
