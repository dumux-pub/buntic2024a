import pyvista as pv
import math


from functionCollectionAutomateDataPlot import find_vtu_files, findCorrespondingDataFields, createLabelList, sortAllContainers
#TODO: careful, in this file the absolute error and not the relative error is computed! Keep that in mind!
def computeError(vtuPath1, vtuPath2):
    # Load vtu file
    mesh_1 = pv.read(vtuPath1)
    mesh_2 = pv.read(vtuPath2)

    #print(f"2p nCells: {mesh_1.number_of_cells}")
    #print(f"2pVE nCells: {mesh_2.number_of_cells}")

    if mesh_1.number_of_cells != mesh_2.number_of_cells:
        raise IndexError("The meshes have differing numbers of cells!")

    for cellIndex in range(0, mesh_1.number_of_cells):
        myCell2p = mesh_1.get_cell(cellIndex)
        pos2p = myCell2p.center

        myCell2pve = mesh_2.get_cell(cellIndex)
        pos2pve = myCell2pve.center

        if pos2p != pos2pve:
            raise IndexError("The meshes use different indexing for their cells!")
        #print(f"Pos: {pos}")

    sLiq_1 = mesh_1['S_liq']
    sLiq_2 = mesh_2['S_liq']

    sLiqError = 0.0
    for cellIndex in range(0, mesh_1.number_of_cells):
        sLiqError += (sLiq_1[cellIndex] - sLiq_2[cellIndex]) ** 2

    sLiqError = math.sqrt(sLiqError)

    #print(f"Total L2-error in wetting-phase saturation: {sLiqError}")

    return sLiqError





folder_path_2pVE = "../../build-cmake/test/porousmediumflow/2pve/"
folder_path_2p = "../../build-cmake/test/porousmediumflow/2p/"
folder_path_adaptive = "../../build-cmake/test/multidomain/couplingsubgrid"

myVTUs_2pve = find_vtu_files(folder_path_2pVE)
labelList_2pve = createLabelList(myVTUs_2pve, '2pve')

myVTUs_2p = find_vtu_files(folder_path_2p)
labelList_2p = createLabelList(myVTUs_2p, '2p')

#print(myVTUs_2p)

myVTUs_adaptive = find_vtu_files(folder_path_adaptive)
labelList_adaptive = createLabelList(myVTUs_adaptive, 'adaptive')


findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_2pve, labelList_2pve)
findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_adaptive, labelList_adaptive)
findCorrespondingDataFields(myVTUs_2p, labelList_2p, myVTUs_2pve, labelList_2pve)
myVTUs_2p, myVTUs_2pve, myVTUs_adaptive, labelList_2p, labelList_2pve, labelList_adaptive = sortAllContainers(myVTUs_2p, myVTUs_2pve, myVTUs_adaptive, labelList_2p, labelList_2pve, labelList_adaptive)


# TODO maybe use two different myVTUs_2p, one for comparing to 2p and one for adaptive model. Maybe do not find the common set between all three models but only two of them


from prettytable import PrettyTable
myTable = PrettyTable(["Model", "Discretization", "WithLens?", "L2 Error"])
for index in range(0,len(myVTUs_adaptive)):
    substrings = labelList_adaptive[index].split('_')
    modelName = substrings[0]
    discrName = substrings[1]
    lensName = substrings[2][-1] #get last character of string
    l2Error = computeError(myVTUs_adaptive[index], myVTUs_2p[index])
    myTable.add_row([f"{modelName}", f"{discrName}", f"{lensName}", f"{l2Error}"])
print(myTable)

myTable2 = PrettyTable(["Model", "Discretization", "WithLens?", "L2 Error"])
for index in range(0,len(myVTUs_2pve)):
    substrings = labelList_2pve[index].split('_')
    modelName = substrings[0]
    discrName = substrings[1]
    lensName = substrings[2][-1] #get last character of string
    l2Error = computeError(myVTUs_2pve[index], myVTUs_2p[index])
    myTable2.add_row([f"{modelName}", f"{discrName}", f"{lensName}", f"{l2Error}"])
print(myTable2)


