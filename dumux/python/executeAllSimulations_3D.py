import subprocess

subprocess.run([f"python3 ../../build-cmake/test/porousmediumflow/2pve/adaptivity_paper_generate_results_pure_ve_3D.py"], shell=True)
subprocess.run([f"python3 ../../build-cmake/test/porousmediumflow/2p/adaptivity_paper_generate_results_pure_fd_3D.py"], shell=True)
subprocess.run([f"python3 ../../build-cmake/test/multidomain/couplingsubgrid/adaptivity_paper_generate_results_multidomain_3D.py"], shell=True)
