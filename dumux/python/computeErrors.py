import subprocess

#Compute the error between a reference and another specified solution. The reference solution was computed on a very fine discretization, and is then restricted/averaged to match the coarser discretization of the provided solution.
subprocess.run([f"python3 -u relErrorToFineDiscretization.py 2>&1 | tee computedErrros.txt"], shell=True) #allow unbuffered output and additionally to stdout also stderr

##Compute the error between a reference and another specified solution which were computed on the SAME discretization
#subprocess.run([f"python3 -u relErrorToSameDiscretization.py 2>&1 | tee computedErrros.txt"], shell=True) #allow unbuffered output and additionally to stdout also stderr
