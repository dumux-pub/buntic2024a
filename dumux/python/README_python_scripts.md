# General idea
So far, for automatically generating results, there are three python scripts which are located in this folder, namely `executeAllSimulations.py`, `executeAllSimulations_3D.py` and `readDataAndPlot.py`

# executeAllSimulations.py (for 2D tests)
Run this script in this python folder. By doing so, different test scenarios for the three models (pureVE, pureFUllD and adaptive) will be run and the results will be stored in their respective build-cmake path. Running all simulations takes up to several hours.

# readDataAndPlot.py - only used for the 2D tests
Before running this script, make sure to run `executeAllSimulations.py` as the latter one produces the data files. Also execute `readDataAndPlot.py` in this python folder, it will search for the data files in the build-cmake paths of the models and use the stored data to plot the desired graphs.

# executeAllSimulations_3D.py (for 3D tests)
Run this script in this python folder. By doing so, different test scenarios for the three models (pureVE, pureFUllD and adaptive) will be run and the results will be stored in their respective build-cmake path. Running all simulations takes up to several hours. Make sure to enable the usage of three dimensions in the respective `properties.hh` file, e.g. `test/porousmediumflow/2p/properties.hh`. Use the respective line `struct Grid<TypeTag, TTag::TwoPCompressible> { using type = Dune::YaspGrid<3>; };` instead of `struct Grid<TypeTag, TTag::TwoPCompressible> { using type = Dune::YaspGrid<2>; };`. 

# Creating tables
Once all simulations have been run and the generated data has been moved to dedicated subfolders in an external folder, the scripts `table_total.py` or `table_total_minimal.py` can be copied to the root folder containing all data. By executing `python3 table_total.py .` from the root folder, the table should be printed on the consoler. We suggest a folder structure that resembles 

```code
externalFolder
├── 2p/
├── 2pve/
├── adaptive/
└── table_total_minimal.py
```

Use the script `table_total.py` for the non-minimal configuration setting and `table_total_minimal.py` for the minimal configuration. By default, the minimal solution is produced via the scripts. Either of the table script requires you to install the python module `prettytable` via `pip` and can be should be executed in the `externalFolder` via the command `python3 table_total_minimal.py .`.  

# Sequence for 2D tests
* delete all `.vtu` and `.txt` files in the build folder which are not part of discretization folders.
* execute `python3 executeAllSimulations.py` in `dumux/python`: run all specified simulations.
* execute `python3 readDataAndPlot.py` in `dumux/python`: generate run time plots.
* execute `python3 computeErrors.py`: the relative errors between 2pve and fullD as well as adaptive and fullD solution are computed.
* copy the created `computedErrors.txt` file to a desired location for backup.
* if tables should be generated, create a folder structure as suggested above outside of the Dumux folder. Move all generated, respective result folders into the respective subfolder of the external folder. Move/Copy the table creation script (e.g. `table_total_minimal.py` to the root of the external folder and execute it.)

# Sequence for 3D tests
* delete all `.vtu` and `.txt` files in the build folder which are not part of discretization folders.
* Comment the block 
```code
std::cout << "Creating combined output. This may take a few seconds..." << std::endl;

    const auto& gridView = gridManager.grid().leafGridView();
    CCTpfaFVGridGeometry<typename HostGrid::LeafGridView> gridGeometry(gridView);
    const auto& bBoxTree = gridGeometry.boundingBoxTree();
    // copy data from the subdomains to full domain data vectors
    std::vector<int> processRank(gridView.size(0), 0); // sequential simulation
    std::vector<double> satNw(gridView.size(0));
    std::vector<double> satW(gridView.size(0));
    std::vector<double> pressureW(gridView.size(0));
    std::vector<double> pressureNw(gridView.size(0));
    std::vector<double> densityW(gridView.size(0));
    std::vector<double> densityNw(gridView.size(0));
    std::vector<double> mobilityW(gridView.size(0));
    std::vector<double> mobilityNw(gridView.size(0));

    //fill FD entries
    for (const auto& element : elements(gridViewDarcy))
    {
        const auto eIdx = fvGridGeometryDarcy->elementMapper().index(element);
        const auto eIdxHost = intersectingEntities(element.geometry().center(), bBoxTree)[0];
        satNw[eIdxHost] = (*solPointer)[darcyDomainIndex][eIdx][1];
        satW[eIdxHost] = 1.0-(*solPointer)[darcyDomainIndex][eIdx][1];
        pressureW[eIdxHost] = (*solPointer)[darcyDomainIndex][eIdx][0];

        using VolumeVariablesFD = typename GetPropType<DarcyFullDTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
        VolumeVariablesFD localVolVarsFD = darcyProblem->returnFDVolVar(element, (*solPointer)[darcyDomainIndex]);
        pressureNw[eIdxHost] = localVolVarsFD.pressure(1);
        densityW[eIdxHost] = localVolVarsFD.density(0);
        densityNw[eIdxHost] = localVolVarsFD.density(1);
        mobilityW[eIdxHost] = localVolVarsFD.mobility(0);
        mobilityNw[eIdxHost] = localVolVarsFD.mobility(1);
    }


    //fill fine VE entries
    problemVEFine->updateFineSol();
    for (const auto& element : elements(gridViewVEFine))
    {
        const auto eIdx = fvGridGeometryVEFine->elementMapper().index(element);
        const auto eIdxHost = intersectingEntities(element.geometry().center(), bBoxTree)[0];
        satNw[eIdxHost] = (*solVEFine)[eIdx][1]; //this only works if VE fine scale volVars are called before at some point
        satW[eIdxHost] = 1.0-(*solVEFine)[eIdx][1]; //this only works if VE fine scale volVars are called before at some point
        pressureW[eIdxHost] = (*solVEFine)[eIdx][0]; //this only works if VE fine scale volVars are called before at some point

        using VolumeVariablesVE = typename GetPropType<VerticalEquilibriumTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
        VolumeVariablesVE localVolVarsVEFine = problemVEFine->returnFineVolVar(element);
        pressureNw[eIdxHost] = localVolVarsVEFine.pressure(1);
        densityW[eIdxHost] = localVolVarsVEFine.density(0);
        densityNw[eIdxHost] = localVolVarsVEFine.density(1);
        mobilityW[eIdxHost] = localVolVarsVEFine.mobility(0);
        mobilityNw[eIdxHost] = localVolVarsVEFine.mobility(1);
    }

    Dune::VTKWriter<typename HostGrid::LeafGridView> vtkWriter(gridView);
    vtkWriter.addCellData(processRank, "process rank");
    vtkWriter.addCellData(satW, "S_liq");
    vtkWriter.addCellData(satNw, "S_gas");
    vtkWriter.addCellData(pressureW, "p_liq");
    vtkWriter.addCellData(pressureNw, "p_gas");
    vtkWriter.addCellData(densityW, "rho_liq");
    vtkWriter.addCellData(densityNw, "rho_gas");
    vtkWriter.addCellData(mobilityW, "mob_liq");
    vtkWriter.addCellData(mobilityNw, "mob_gas");
    std::string combinedOutputName = "combinedFineScaleOutput_adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    const auto filename = combinedOutputName;
    vtkWriter.write(filename);
```
at the end of the `main.cc` file located in the `test/multidomain/couplingsubgrid/` as this plotting function is very expensive for 3D and only helps visualize the results.
* execute `python3 executeAllSimulations_3D.py` in `dumux/python`: run all specified simulations.
* if tables should be generated, create a folder structure as suggested above outside of the Dumux folder. Move all generated, respective result folders into the respective subfolder of the external folder. Move/Copy the table creation script (e.g. `table_total_minimal.py` to the root of the external folder and execute it.)


