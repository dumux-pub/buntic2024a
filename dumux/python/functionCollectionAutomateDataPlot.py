import numpy as np
import os
import matplotlib.pyplot as plt
import subprocess
import re

def returnDataSet(dataFilePath):
    #read in the data which should be plotted
    dataField = np.genfromtxt(dataFilePath, delimiter=',', dtype=None, names=True, encoding='utf-8')

    return dataField


def find_metaData_files(directory):
    txt_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith("metaData.txt"):
                txt_files.append(os.path.join(root, file))
    return txt_files


#this function assumes that a file is structured like MODELNAME_DISCR_LENSES_metadata.txt, so splitting at the character '_' is helpful. substringNumber=0 is the model name, substringNumber=1 is the discretization string and so on
def returnPartOfFileName(filePath, substringNumber):
    fileName = os.path.basename(filePath)
    substrings = fileName.split('_') #split string into substrings which are divided by `_`
    specificSubstring = substrings[substringNumber]
    return specificSubstring


def returnDataListAndLabelList(metaDataList):
    dataFields = []
    labelList = []
    for file in metaDataList:
        localDataField = returnDataSet(file)
        dataFields.append(localDataField)

        modelName = returnPartOfFileName(file, 0)
        discrString = returnPartOfFileName(file, 1)
        lensesString = returnPartOfFileName(file, 2)
        labelList.append(modelName+'_'+discrString+'_'+lensesString)

    return dataFields, labelList


def plot2pVE(dataFieldList, labelList, folderPath2pVE):
    #Generate desired plots----------------------------------------------------------------------------------------------------------------------------------
    print(f"Generating 2pVE plots.")

    #define first figure and first plot (left y-axis)
    fig, ax = plt.subplots()
    #create a twin x-axis to have a second graph plotted in the same figure but another y-axis
    axTwin = ax.twinx()
    #ax.set_xlabel('Simulation time [s]')
    ax.set_xlabel('Simulation time [days]', fontsize=22)
    ax.set_ylabel('Wall time [s]', fontsize=22)
    ax.grid(axis='both', color='0.95')

    #define second figure and first plot (left y-axis)
    #fig2, ax2 = plt.subplots()
    #axTwin2 = ax2.twinx()
    #ax2.set_xlabel('Simulation time [s]')
    ##ax2.set_ylabel('Wall time [s]')
    #ax2.grid(axis='both', color='0.95')

    for i in range(len(dataFieldList)):
        #read in stored data
        simTimeArray = dataFieldList[i]['simulatedTime']
        simTimeArray = simTimeArray/86400 #convert from second to to days
        wallTimeArray = dataFieldList[i]['wallTime']
        numberFullDElementsArray = dataFieldList[i]['numberElementsVEFine']

        #plot desired graphs
        ax.plot(simTimeArray, wallTimeArray, label=labelList[i])
        axTwin.set_ylabel('number fine-scale elements [-]', fontsize=22)
        axTwin.plot(simTimeArray, numberFullDElementsArray, label=labelList[i], linewidth=1, linestyle='dotted')

    ax.legend()

    #ax.set_title('Pure VE model')

    fig.savefig(f"{folderPath2pVE}/2pVE_simTime_wallTime.png", dpi=300)

    plt.show(block=False) #prevent execution of programm from freezing
    plt.pause(0.001) #show figures during execution, not just the end


def plot2p(dataFieldList, labelList, folderPath2p):
    #Generate desired plots----------------------------------------------------------------------------------------------------------------------------------
    print(f"Generating 2p plots.")

    #define first figure and first plot (left y-axis)
    fig, ax = plt.subplots()
    #create a twin x-axis to have a second graph plotted in the same figure but another y-axis
    axTwin = ax.twinx()
    #ax.set_xlabel('Simulation time [s]')
    ax.set_xlabel('Simulation time [days]', fontsize=22)
    ax.set_ylabel('Wall time [s]', fontsize=22)
    ax.grid(axis='both', color='0.95')

    for i in range(len(dataFieldList)):
        #read in stored data
        simTimeArray = dataFieldList[i]['simulatedTime']
        simTimeArray = simTimeArray/86400 #convert from second to to days
        wallTimeArray = dataFieldList[i]['wallTime']
        numberFullDElementsArray = dataFieldList[i]['numberElementsFullD']

        #plot desired graphs
        ax.plot(simTimeArray, wallTimeArray, label=labelList[i])
        axTwin.set_ylabel('Number fullD elements [-]', fontsize=22)
        axTwin.plot(simTimeArray, numberFullDElementsArray, label=labelList[i], linestyle='dotted')

    ax.legend()

    #ax.set_title('Pure fullD model')

    fig.savefig(f"{folderPath2p}/2p_simTime_wallTime.png", dpi=300)

    plt.show(block=False) #prevent execution of programm from freezing
    plt.pause(0.001) #show figures during execution, not just the end


def plotAdaptive(dataFieldList, labelList, folderPathAdaptive):
    #Generate desired plots----------------------------------------------------------------------------------------------------------------------------------
    print(f"Generating adaptive plots.")

    #define first figure and first plot (left y-axis)
    fig, ax = plt.subplots()
    #create a twin x-axis to have a second graph plotted in the same figure but another y-axis
    axTwin = ax.twinx()
    ax.set_xlabel('Simulation time [days]', fontsize=22)
    ax.set_ylabel('CPU time [s]', fontsize=22)
    ax.grid(axis='both', color='0.95')

    #define second figure and first plot (left y-axis)
    fig2, ax2 = plt.subplots()
    axTwin2 = ax2.twinx()
    ax2.set_xlabel('Simulation time [s]', fontsize=22)
    #ax2.set_ylabel('Wall time [s]')
    ax2.grid(axis='both', color='0.95')

    for i in range(len(dataFieldList)):
        #read in stored data
        simTimeArray = dataFieldList[i]['simulatedTime']
        simTimeArray = simTimeArray/86400 #convert from second to to days
        wallTimeArray = dataFieldList[i]['wallTime']
        VESubdomainsArray = dataFieldList[i]['numberVESubdomains']
        totalNumberElementsArray = dataFieldList[i]['numberElementsTotal']
        numberVEElementsArray = dataFieldList[i]['numberElementsVE']
        numberFullDElementsArray = dataFieldList[i]['numberElementsFullD']
        numberFineScaleElementsArray = dataFieldList[i]['numberElementsVEFine']

        #plot desired graphs
        ax.plot(simTimeArray, wallTimeArray, label=labelList[i]+'_cpu')
        axTwin.set_ylabel('Number of coupling interfaces [-]', fontsize=22)
        axTwin.plot(simTimeArray, 2*VESubdomainsArray-1, label=labelList[i]+'_numInterfaces', linestyle='dotted')
        #axTwin.set_ylabel('#totalElements')
        #axTwin.plot(simTimeArray, totalNumberElementsArray, label=labelList[i], linewidth=1, linestyle='dotted')
        #axTwin.set_ylabel('#VE elements')
        #axTwin.plot(simTimeArray, numberVEElementsArray, label=labelList[i], linewidth=1, linestyle='dotted')
        #axTwin.set_ylabel('#fullD elements')
        #axTwin.plot(simTimeArray, numberFullDElementsArray, label=labelList[i], linewidth=1, linestyle='dotted')
        #axTwin.set_ylabel('#fine-scale elements')
        #axTwin.plot(simTimeArray, numberFineScaleElementsArray, label=labelList[i], linewidth=1, linestyle='dotted')

        #ax2.set_yticks([1,2,3,4,5, 6, 7, 8, 9, 10])
        #ax2.set_ylabel('Number VE subdomains.')
        ax2.set_ylabel('Number of coupling interfaces [-]', fontsize=22)
        ax2.plot(simTimeArray, 2*VESubdomainsArray-1, label=labelList[i])
        axTwin2.set_ylabel('Number fine-scale elements [-]', fontsize=22)
        axTwin2.plot(simTimeArray, numberFineScaleElementsArray, label=labelList[i], linestyle='dotted')


    #combine labels into one legend
    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = axTwin.get_legend_handles_labels()
    ax.legend(lines + lines2, labels + labels2, loc='upper center')

    #ax.legend(loc = 'upper center')
    #axTwin.legend(loc = 'upper center')
    ax2.legend(loc = 'upper center')

    #ax.set_title('Adaptive model')
    #ax2.set_title('Adaptive model')

    fig.savefig(f"{folderPathAdaptive}/adaptive_simTime_wallTime.png", dpi=300)
    fig2.savefig(f"{folderPathAdaptive}/adaptive_simTime_numberElements.png", dpi=300)

    plt.show(block=False) #prevent execution of programm from freezing
    plt.pause(0.001) #show figures during execution, not just the end


def findCorrespondingDataFields(dataFields1, labelList1, dataFields2, labelList2):
    for label in reversed(labelList1): #traverse from behind so indices don't get messed up when popping elements
        model = (label.split('_'))[0]
        discretizationString = (label.split('_'))[1]
        if any(discretizationString in comparativeLabel for comparativeLabel in labelList2):
            #print(f"Found {label}")
            continue
        else:
            index = labelList1.index(label)
            otherModel = (labelList2[0].split('_'))[0] #take 1st word before '_' from an arbitrary element of labelList2 (here entry 0)
            print(f"No matching partner found in {otherModel} for {discretizationString}. Removing {label} with index {index} from {model} labels and data fields.")
            labelList1.pop(index)
            dataFields1.pop(index)

    for label in reversed(labelList2): #traverse from behind so indices don't get messed up when popping elements
        model = (label.split('_'))[0]
        discretizationString = (label.split('_'))[1]
        if any(discretizationString in comparativeLabel for comparativeLabel in labelList1):
            #print(f"Found {label}")
            continue
        else:
            index = labelList2.index(label)
            otherModel = (labelList1[0].split('_'))[0] #take 1st word before '_' from an arbitrary element of labelList1 (here entry 0)
            print(f"No matching partner found in {otherModel} for {discretizationString}. Removing {label} with index {index} from {model} labels and data fields.")
            labelList2.pop(index)
            dataFields2.pop(index)

def plotSpecificQuantities(dataFields1, dataFields2, labelList1, labelList2, xAxisQuantityString, yAxisQuantityString, xLabel='x', yLabel='y'):
    #TODO: make sure that datFields and labelLists have same lenghts
    if len(dataFields1) != len(dataFields2) or len(labelList1) != len(labelList1):
        print(f"Careful, the data fields have differing number of data sets!")
    if len(dataFields1) != len(labelList1) or len(dataFields2) != len(labelList2):
        raise IndexError(f"One of the data fields has a differing number of elements compared to its respective label list!")

    #define first figure and first plot (left y-axis)
    fig, ax = plt.subplots()
    ax.set_xlabel(f"{xLabel}", fontsize=22)
    ax.set_ylabel(f"{yLabel}", fontsize=22)
    ax.grid(axis='both', color='0.95')

    for i in range(len(dataFields1)):
        #read in stored data
        xAxisDataArray1 = dataFields1[i][xAxisQuantityString]
        yAxisDataArray1 = dataFields1[i][yAxisQuantityString]

        xAxisDataArray2 = dataFields2[i][xAxisQuantityString]
        yAxisDataArray2 = dataFields2[i][yAxisQuantityString]

        if xAxisQuantityString is 'simulatedTime':
            xAxisDataArray1 = xAxisDataArray1/86400
            xAxisDataArray2 = xAxisDataArray2/86400

        #plot desired graphs
        line1, = ax.plot(xAxisDataArray1, yAxisDataArray1, label=labelList1[i])
        color = line1.get_color()
        ax.plot(xAxisDataArray2, yAxisDataArray2, label=labelList2[i], linestyle='dotted', color=color)

    ax.legend()

    substrings1 = labelList1[0].split('_')
    model1 = substrings1[0]
    substrings2 = labelList2[0].split('_')
    model2 = substrings2[0]

    #ax.set_title(f"Comparing {model1} model and {model2} model")
    fig.savefig(f"comparison_{xAxisQuantityString}_{yAxisQuantityString}.png", dpi=300)
    plt.show(block=False) #prevent execution of programm from freezing
    plt.pause(0.001) #show figures during execution, not just the end

#the directory names should NOT end with '/'
def copyDataToDir(buildDir, targetDir):
    txt_files = []
    for root, dirs, files in os.walk(buildDir):
        for dir in dirs:
            #if dir != "CMakeFiles" or not 'dir' in dir:
            #if dir != "CMakeFiles" or not 'dir' in dir:
            if (not '.dir' in dir) and (dir != "CMakeFiles"):
                #dirPath = os.path.dirname(dir)

                buildDirFolder = buildDir+'/'+dir
                targetDirectory = targetDir+'/'+dir
                if not os.path.isdir(targetDirectory):
                    print(f"{targetDirectory} does not exist yet. Copying data.")
                    subprocess.run([f"cp -r {buildDirFolder} {targetDirectory}"], shell=True)
                else:
                    print(f"{targetDirectory}: Already exists. Not doing anything.")

def removeEntries(dataField, labelList, labelName):
    for label in labelList: #does not need to be traversed from the back as in onlyKeepEntries, as this function is called for each labelName
        if labelName in label:
            index = labelList.index(label)
            labelList.pop(index)
            dataField.pop(index)

def onlyKeepEntries(dataField, labelList, labelNamesToKeep):
    for label in reversed(labelList): #important to traverse from the back, as by popping entries the indices change. Would lead to scuffed traversal when iterating from front
        shouldKeep = False
        for toKeepLabel in labelNamesToKeep: #for each label, check if label contains toKeepLabel
            if toKeepLabel in label:
                shouldKeep = True
        if shouldKeep == False:
            index = labelList.index(label)
            labelList.pop(index)
            dataField.pop(index)

def find_vtu_files(directory):
    vtu_files = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            isVEFine = (file.endswith('001.vtu')) and ('VE_fine' in file) and ('adaptive' not in root) #last condition necessary to not include the fine scale vtus in the adaptive folder
            isFullDFine = (file.endswith('001.vtu')) and ('test_cc2p' in file)
            isAdaptiveCombined = 'combinedFineScaleOutput' in file
            if isVEFine or isFullDFine or isAdaptiveCombined:
                #print(file)
                vtu_files.append(os.path.join(root, file))

    return vtu_files

def createLabelList(vtuPathList, modelName):
    #print(vtuPathList)

    labelList = []
    for path in vtuPathList:
        #print(path)
        x = re.search(r"\d+x\d+", path)
        discrString = x.group()
        x = re.search(r"Lenses\d+", path)
        lensString = x.group()
        #print(discrString)
        #print(lensString)
        labelList.append(modelName+'_'+discrString+'_'+lensString)

    return labelList

def sortAllContainers(cont1, cont2, cont3, contLabel1, contLabel2, contLabel3):

    newSortedCont1 = [x for _,x in sorted(zip(contLabel1,cont1))]
    cont1 = newSortedCont1

    newSortedCont2 = [x for _,x in sorted(zip(contLabel2,cont2))]
    cont2 = newSortedCont2

    newSortedCont3 = [x for _,x in sorted(zip(contLabel3,cont3))]
    cont3 = newSortedCont3

    contLabel1 = sorted(contLabel1)
    contLabel2 = sorted(contLabel2)
    contLabel3 = sorted(contLabel3)

    return cont1, cont2, cont3, contLabel1, contLabel2, contLabel3




