// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup DarcyDarcyCoupling
 * \copydoc Dumux::DarcyDarcyBoundaryCouplingMapper
 */

#ifndef DUMUX_MULTIDOMAIN_DARCYDARCYVERTICALEQUILIBRIUM_TWOBOUNDARIES_COUPLINGMAPPER_HH
#define DUMUX_MULTIDOMAIN_DARCYDARCYVERTICALEQUILIBRIUM_TWOBOUNDARIES_COUPLINGMAPPER_HH

#include <iostream>
#include <unordered_map>
#include <tuple>
#include <vector>

#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/indices.hh>
#include <dune/common/fvector.hh>

#include <dumux/geometry/intersectingentities.hh>
#include <dumux/discretization/method.hh>

#include "couplingmanager.hh"

//Custom hash function for GlobalPosition data type. Converts GlobalPosition to one dimension lower and creates hash for this reduced GlobalPosition (ignores z coordinate)
//Careful, there is no guarantee that this hash function generated unique values, is there?
//For hash function two things are important: same hash value and equality operation has to be true
struct GlobalPositionHash {
  template <typename ReducedGlobalPosition>
  auto operator()(const ReducedGlobalPosition &globalPos) const -> size_t {

    // Define a rounding factor (e.g., to round to the nearest 0.001)
    const double precision = 1e-8;

    // Round the x and y coordinates
    auto round = [&precision](double value) {
        return std::round(value / precision) * precision;
    };

    auto globalPosHash = std::hash<double>()(round(globalPos[0]));
    if(globalPos.size()==3)
        globalPosHash = globalPosHash ^ (std::hash<double>()(round(globalPos[1])) << 1); //bitwise shift helps to avoid equality in case coordinated are permutated

    return globalPosHash;
  }
};


namespace Dumux {

/*!
 * \ingroup DarcyDarcyVerticalEquilibriumCoupling
 * \brief mapper for conforming d and (d-1) dimensional boundary coupling between two domains (box or cc)
 * \todo how to extend to arbitrary number of domains?
 */
template<class MDTraits>
class DarcyDarcyVerticalEquilibriumBoundaryCouplingMapper
{
    using Scalar = typename MDTraits::Scalar;

    template<std::size_t i> using GridGeometry = typename MDTraits::template SubDomain<i>::GridGeometry;
    template<std::size_t i> using SubControlVolumeFace = typename GridGeometry<i>::SubControlVolumeFace;
    template<std::size_t i> using GridView = typename GridGeometry<i>::GridView;
    template<std::size_t i> using Element = typename GridView<i>::template Codim<0>::Entity;

    template<std::size_t i> using SubDomainTypeTag = typename MDTraits::template SubDomain<i>::TypeTag;
    template<std::size_t i> using Problem = GetPropType<SubDomainTypeTag<i>, Properties::Problem>;

    template<std::size_t i>
    static constexpr auto domainIdx()
    { return typename MDTraits::template SubDomain<i>::Index{}; }

    template<std::size_t i>
    static constexpr bool isCCTpfa()
    { return GridGeometry<i>::discMethod == DiscretizationMethods::cctpfa; }

    struct ScvfInfo //TODO: can delete? Not used right now...
    {
        std::size_t eIdxOutside;
        std::size_t flipScvfIdx;
    };

    using FlipScvfMapType = std::unordered_map<std::size_t, ScvfInfo>; //TODO: delete, when done with this
    using MapType = std::unordered_map<std::size_t, std::vector<std::size_t>>; //TODO: delete, when done with this
    static constexpr std::size_t numSD = MDTraits::numSubDomains;
    using GlobalPosition = typename Element<domainIdx<1>()>::Geometry::GlobalCoordinate; //1 reflects the VE domainIdx
    using TestTuple = std::tuple<int, int, int >;

    static constexpr std::size_t dim = GridView<domainIdx<1>()>::dimension;
    using ReducedGlobalPosition = Dune::FieldVector<Scalar, dim-1>;


public:
    using TupleVectorType = std::vector<std::tuple<int, int, int, int, int, int>>; //tuple order is: 0) own domainIdx, 1) own elementIdx, 2) own scvfIdx, 3) coupled domainIdx, 4) coupled elementIdx from domain 3), 5) coupled scvfIdx from domain 3)
    using StencilType = std::vector<TupleVectorType>;

    GlobalPosition globalUpperRight = getParam<GlobalPosition>("DarcyVE.Grid.UpperRight");
    GlobalPosition globalLowerLeft = getParam<GlobalPosition>("DarcyVE.Grid.LowerLeft");

    /*!
     * \brief Main update routine
     */
    template<class CouplingManager>
    void update(const CouplingManager& couplingManager)
    {
        Dune::Timer watch;

        for (std::size_t domainIdx = 0; domainIdx < numSD; ++domainIdx)
        {
            isCoupledScvf_[domainIdx].clear();
            isCoupledElement_[domainIdx].clear();

            myStencils_[domainIdx].clear();
            scvfCouplingInfo_[domainIdx].clear();
        }

        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::index_constant<MDTraits::numSubDomains>{}), [&](const auto i)
        {
            static_assert(isCCTpfa<i>(), "Only cctpfa implemented!");
            isCoupledScvf_[i].resize(couplingManager.problem(domainIdx<i>()).gridGeometry().numScvf(), false);
            isCoupledElement_[i].resize(couplingManager.problem(domainIdx<i>()).gridGeometry().numScv(), false);
            myStencils_[i].resize(couplingManager.problem(domainIdx<i>()).gridGeometry().numScv(), myEmptyStencil_);
            scvfCouplingInfo_[i].resize(couplingManager.problem(domainIdx<i>()).gridGeometry().numScvf(), myEmptyStencil_);

            std::cout << "In couplingmapper update(). For domain " << i << " there are " << couplingManager.problem(domainIdx<i>()).gridGeometry().numScv() << "scvs." << std::endl;
        });

        
        std::cout << "Number of subdomains: " << numSD << std::endl;
        std::cout << "Initializing the coupling map..." << std::endl;
        fillCouplingInterfaceVEMap(couplingManager);
        createCouplingMap(couplingManager);
        std::cout << "Done with initializing the coupling map..." << std::endl;
    }



    template<class CouplingManager>
    bool isOnGlobalBoundary(const GlobalPosition& facePos,
                            const GlobalPosition& globalUpperRight,
                            const GlobalPosition& globalLowerLeft) const
    {
        bool isOnGlobalBoundary = false;
        // static constexpr std::size_t dim = GridView<CouplingManager::darcyVEDomainIndex>::dimension;

        if constexpr(dim==2)
        {
            if(facePos[0]<globalLowerLeft[0]+eps_ || facePos[0]>globalUpperRight[0]-eps_ ||
               facePos[1]<globalLowerLeft[1]+eps_ || facePos[1]>globalUpperRight[1]-eps_)
                isOnGlobalBoundary = true;
        }
        else if constexpr(dim==3)
        {
            if(facePos[0]<globalLowerLeft[0]+eps_ || facePos[0]>globalUpperRight[0]-eps_ ||
               facePos[1]<globalLowerLeft[1]+eps_ || facePos[1]>globalUpperRight[1]-eps_ ||
               facePos[2]<globalLowerLeft[2]+eps_ || facePos[2]>globalUpperRight[2]-eps_)
                isOnGlobalBoundary = true;
        }

        return isOnGlobalBoundary;
    }

    //removes z-coordinate
    ReducedGlobalPosition reduceGlobalPosition(const GlobalPosition& pos) const
    {
        ReducedGlobalPosition convertedGlobalPos{};
        for(int i=0; i<dim-1; i++)
            convertedGlobalPos[i] = pos[i];

        return convertedGlobalPos;
    }



    template<class CouplingManager>
    void fillCouplingInterfaceVEMap(const CouplingManager& couplingManager)
    {
        couplingInterfaceVEMap_.clear();

        auto VESubdomainIndex = CouplingManager::darcyVEDomainIndex;
        for (const auto& elementVE : elements(couplingManager.problem(VESubdomainIndex).gridGeometry().gridView()))
        {
            auto fvGeometryVE = localView(couplingManager.problem(VESubdomainIndex).gridGeometry());
            fvGeometryVE.bindElement(elementVE);

            auto elementVEIdx = couplingManager.problem(VESubdomainIndex).gridGeometry().elementMapper().index(elementVE);

            for (const auto& scvfVE : scvfs(fvGeometryVE))
            {
                GlobalPosition scvfVEPos = scvfVE.center();
                // skip all non-boundaries
                if (!scvfVE.boundary())
                    continue;
                if(isOnGlobalBoundary<CouplingManager>(scvfVEPos,globalUpperRight,globalLowerLeft))
                    continue;

                ReducedGlobalPosition scvfVEPosRed = reduceGlobalPosition(scvfVE.center());
                auto couplingInterfaceInfo = std::make_tuple(VESubdomainIndex, elementVEIdx, scvfVE.index());
                couplingInterfaceVEMap_[scvfVEPosRed] = couplingInterfaceInfo;
            }
        }
    }

    template<class CouplingManager>
    void createCouplingMap(const CouplingManager& couplingManager)
    {
        auto FDSubdomainIndex = CouplingManager::darcyDomainIndex;
        auto VESubdomainIndex = CouplingManager::darcyVEDomainIndex;
        for (const auto& elementFD : elements(couplingManager.problem(FDSubdomainIndex).gridGeometry().gridView()))
        {
            TupleVectorType tmp{};

            auto fvGeometryFD = localView(couplingManager.problem(FDSubdomainIndex).gridGeometry());
            fvGeometryFD.bindElement(elementFD);

            auto elementFDIdx = couplingManager.problem(FDSubdomainIndex).gridGeometry().elementMapper().index(elementFD);

            for (const auto& scvfFD : scvfs(fvGeometryFD))
            {
                GlobalPosition scvfFDPos = scvfFD.center();
                // skip all non-boundaries
                if (!scvfFD.boundary())
                    continue;
                if(isOnGlobalBoundary<CouplingManager>(scvfFDPos,globalUpperRight,globalLowerLeft))
                    continue;

                auto scvfFDIdx = scvfFD.index();

                auto it = couplingInterfaceVEMap_.find(reduceGlobalPosition(scvfFDPos));
                if (it != couplingInterfaceVEMap_.end()) 
                {
                    TestTuple coupledVEData = it->second;
                    const auto otherVEElementIdx = std::get<1>(coupledVEData);
                    const auto otherVEScvfIdx    = std::get<2>(coupledVEData);

                    auto tmpForScvfInfo = std::make_tuple(FDSubdomainIndex, elementFDIdx, scvfFDIdx, VESubdomainIndex, otherVEElementIdx, otherVEScvfIdx);
                    tmp.push_back(tmpForScvfInfo); //collect all relevant scvfs for one element into tmp

                    isCoupledScvf_[FDSubdomainIndex][scvfFDIdx] = true;
                    isCoupledElement_[FDSubdomainIndex][elementFDIdx] = true;

                    //reverse
                    isCoupledScvf_[VESubdomainIndex][otherVEScvfIdx] = true;
                    isCoupledElement_[VESubdomainIndex][otherVEElementIdx] = true;

                    //
                    scvfCouplingInfo_[FDSubdomainIndex][scvfFDIdx].push_back(tmpForScvfInfo);
                    auto tmpForScvfInfoReversed = std::make_tuple(VESubdomainIndex, otherVEElementIdx, otherVEScvfIdx, FDSubdomainIndex, elementFDIdx, scvfFDIdx);
                    scvfCouplingInfo_[VESubdomainIndex][otherVEScvfIdx].push_back(tmpForScvfInfoReversed);
                }
                else
                {
                    //As the face has to be on a coupling boundary (checked earlier: is on boundary but not on global domain boundary), we expect to find a coupling neighbor
                    std::ostringstream errMsg;
                    errMsg << "Failed to find a coupling neighbor for FD face at pos: " << scvfFDPos << "!" << std::endl;
                    throw std::domain_error(errMsg.str());
                }
            }


            myStencils_[FDSubdomainIndex][elementFDIdx] = tmp;
            for(int coupledElem=0; coupledElem<tmp.size(); coupledElem++)
            {
                const auto scvfFDIdx = std::get<2>(tmp[coupledElem]);
                const auto elementVEIdx = std::get<4>(tmp[coupledElem]);
                const auto scvfVEIndex = std::get<5>(tmp[coupledElem]);
                auto reversedEntry = std::make_tuple(VESubdomainIndex, elementVEIdx, scvfVEIndex, FDSubdomainIndex, elementFDIdx, scvfFDIdx);
                myStencils_[VESubdomainIndex][elementVEIdx].push_back(reversedEntry);
            }

        }

//         //for checking if everything has been filled in correctly
//         printWholeCouplingStencil_(couplingManager);

//         printWholeIsCoupledElement_(couplingManager);
//         printWholeIsCoupledScvf_(couplingManager);
//         printWholeScvfCouplingInfo_(couplingManager);
    }







    /*!
     * \brief returns an iteratable container of all indices of degrees of freedom which are coupled to element with eIdxI of domainI,
     *        contains information about the coupled elements, coupled scvfs and their domain indices
     *
     * \param domainI the domain index of domain i
     * \param eIdxI the index of the coupled element of domain í
     *
     * It is VERY important that this functions return ONLY the coupled elements of domainJ, NOT ALL elements which are coupled to domainI, otherwise this leads to an error with matrix sizes in the assembler
     *
     * \note  The element residual definition depends on the discretization scheme of domain i
     *        box: a container of the residuals of all sub control volumes
     *        cc : the residual of the (sub) control volume
     *        fem: the residual of the element
     * \note  This function has to be implemented by all coupling managers for all combinations of i and j //TODO: so far, j has been removed, is that ok?
     */
    template<std::size_t i>
    const TupleVectorType& myCouplingStencil(Dune::index_constant<i> domainI,
                                             const std::size_t eIdxI) const
    {
        if (myIsCoupledElement(domainI, eIdxI))
        {
            return myStencils_[i][eIdxI];
        }
        else
        {
            return myEmptyStencil_;
        }
    }


    /*!
     * \brief Return if an element residual with index eIdx of domain i is coupled to another domain
     */
    template<std::size_t i>
    bool myIsCoupledElement(Dune::index_constant<i>,
                            std::size_t eIdx) const
    { return isCoupledElement_[i][eIdx]; }


    /*!
     * \brief Boolean value which tells if the boundary entity is on a coupling boundary
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i>
    bool myIsCoupled(Dune::index_constant<i> domainI,
                     const SubControlVolumeFace<i>& scvf) const
    {
        return isCoupledScvf_[i][scvf.index()];
    }


    /*!
     * \brief Boolean value which tells if the boundary entity is on a coupling boundary
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i, std::size_t j>
    bool myIsCoupled(Dune::index_constant<i> domainI,
                     const SubControlVolumeFace<i>& scvf,
                     Dune::index_constant<j> domainJ) const
    {
        bool isCoupled = false;
        if(isCoupledScvf_[i][scvf.index()])
        {
            if(std::get<3>(scvfCouplingInfo_[domainI][scvf.index()][0]) == domainJ)
            {
                isCoupled = true;
            }
            else
            {
                isCoupled = false;
            }
        }
        return isCoupled;
    }


    /*!
     * \brief Return the scvfs indices of the flipped scvfs in the other domain
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i>
    std::vector<std::size_t> myFlipScvfIndices(Dune::index_constant<i> domainI,
                                               const SubControlVolumeFace<i>& scvf) const
    {
        std::vector<std::size_t> flipScvfIndices{};
        for(int coupledElem=0; coupledElem<scvfCouplingInfo_[i][scvf.index()].size(); coupledElem++)
        {
            auto flipScvfIndex = std::get<5>(scvfCouplingInfo_[i][scvf.index()][coupledElem]);
            flipScvfIndices.push_back(flipScvfIndex);
        }
        return flipScvfIndices;
    }


    /*!
     * \brief Return the outside elements indices (the element indices of the other domain) of ALL coupled (neighboring) subdomains, not one specific subdomain
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i>
    std::vector<std::size_t> myOutsideElementIndices(Dune::index_constant<i> domainI,
                                                     std::size_t eIdx) const
    {
        if (myIsCoupledElement(domainI, eIdx))
        {
            std::vector<std::size_t> outsideElementIndices{};
            for(int coupledElem=0; coupledElem<myStencils_[i][eIdx].size(); coupledElem++)
            {
                auto outsideElementIndex = std::get<4>(myStencils_[i][eIdx][coupledElem]);
                outsideElementIndices.push_back(outsideElementIndex);
            }

            return outsideElementIndices;
        }
        else
        {
            return emptyStencil_;
        }
    }




    /*!
     * \brief Return the outside elements indices (the element indices of the other domain) which are specifically coupled to domainJ
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i, std::size_t j>
    std::vector<std::size_t> myOutsideElementIndices(Dune::index_constant<i> domainI,
                                                     std::size_t eIdx,
                                                     Dune::index_constant<j> domainJ) const
    {
        if (myIsCoupledElement(domainI, eIdx))
        {
            std::vector<std::size_t> outsideElementIndices{};
            for(int coupledElem=0; coupledElem<myStencils_[i][eIdx].size(); coupledElem++)
            {
                if( std::get<3>(myStencils_[i][eIdx][coupledElem]) == domainJ)
                {
                    auto outsideElementIndex = std::get<4>(myStencils_[i][eIdx][coupledElem]);
                    outsideElementIndices.push_back(outsideElementIndex);
                }
            }

            return outsideElementIndices;
        }
        else
        {
            return emptyStencil_;
        }
    }




    //TODO: maybe substitute the private member functions by this public member functions
    //Printer functions
    template<class CouplingManager>
    void printWholeCouplingStencil(const CouplingManager& couplingManager)
    {
        printWholeCouplingStencil_(couplingManager);
    }

    template<class CouplingManager>
    void printWholeIsCoupledElement(const CouplingManager& couplingManager)
    {
        printWholeIsCoupledElement_(couplingManager);
    }

    template<class CouplingManager>
    void printWholeIsCoupledScvf(const CouplingManager& couplingManager)
    {
        printWholeIsCoupledScvf_(couplingManager);
    }

    template<class CouplingManager>
    void printWholeScvfCouplingInfo(const CouplingManager& couplingManager)
    {
        printWholeScvfCouplingInfo_(couplingManager);
    }


    /*!
     * \brief Return the number of individual coupled subdomains for the VE model
     * \param couplingManager the coupling manager
     */
    template<class CouplingManager>
    int numberOfSubdomains(const CouplingManager& couplingManager) const
    {
        static constexpr std::size_t dim = GridView<CouplingManager::darcyVEDomainIndex>::dimension;
        static constexpr auto darcyVEDomainIndex = CouplingManager::darcyVEDomainIndex;

        int numVESubDomains = 0;
        int curElemIdx = -1;

        if constexpr(dim==2)
        {
            //iterate only over element of the VE subdomain that participate in the coupling
            for (const auto& element : elements(couplingManager.problem(darcyVEDomainIndex).gridGeometry().gridView()))
            {
                auto elementIdx = couplingManager.problem(darcyVEDomainIndex).gridGeometry().elementMapper().index(element);

                for(int coupledElem=0; coupledElem<myStencils_[darcyVEDomainIndex][elementIdx].size(); coupledElem++)
                {
                    //we always choose the left boundary of the domain to be fullD and the right boundary to be VE. If any additional subdomains are added in between, one subdomain from each model will be added, meaning the number of subdomains of the two models will always be the same.

                    int inspectedElemIdx = std::get<1>(myStencils_[darcyVEDomainIndex][elementIdx][coupledElem]);

                    //only look at new element which participates in the coupling
                    if(curElemIdx != inspectedElemIdx)
                    {
                        //if the next coupling element is ownIdx+1, that means the ownIdx is last element of a subdomain and ownIdx+1 will be the first coupling element of a new subdomain. This also means that a new subdomain is introduced and thus the counter for subdomains can be raised by one.
                        //TODO: Careful! The subdomains have their own indices and do not share them? This approach only works if we are working with global element indices?
                        if(inspectedElemIdx == curElemIdx+1)
                        {
                            numVESubDomains++;
                        }
                        curElemIdx = inspectedElemIdx;
                    }
                }
            }
        }
        else if constexpr(dim==3)
        {
            std::cout << "!!!!numberOfSubdomains(...) was not implemented for 3D yet!!!! Returning 0 as number of VE domains." << std::endl;
        }

        

        return numVESubDomains;
    }



private:

    /*!
     * \brief Helper function, print the whole coupling stencil
     *
     * \param couplingManager couplingManager of the simulation
     */
    template<class CouplingManager>
    void printWholeCouplingStencil_(const CouplingManager& couplingManager)
    {
        std::cout << "--------------------Printing the whole coupling stencil--------------------" << std::endl;

        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::index_constant<MDTraits::numSubDomains>{}), [&](const auto i)
        {
            std::cout << "Index of current subdomain: " << i << std::endl;

            for (const auto& element : elements(couplingManager.problem(i).gridGeometry().gridView()))
            {
                auto elementIdx = couplingManager.problem(i).gridGeometry().elementMapper().index(element);

                for(int coupledElem=0; coupledElem<myStencils_[i][elementIdx].size(); coupledElem++)
                {
                    std::cout << "ownDomain is: " << std::get<0>(myStencils_[i][elementIdx][coupledElem]) <<
                    " while ownElement is : "  << std::get<1>(myStencils_[i][elementIdx][coupledElem]) <<
                    " while ownScvf is : "  << std::get<2>(myStencils_[i][elementIdx][coupledElem]) <<
                    " . Meanwhile otherDomain is: " <<  std::get<3>(myStencils_[i][elementIdx][coupledElem]) <<
                    " with coupledElement from that domain is: " << std::get<4>(myStencils_[i][elementIdx][coupledElem]) <<
                    " with coupledScvf from that domain is: " << std::get<5>(myStencils_[i][elementIdx][coupledElem]) << std::endl;
                }
            }
        });

        std::cout << "====================END PRINTING whole coupling stencil====================" << std::endl;
    }


    /*!
     * \brief Helper function, print the whole container of which elements are coupled
     *
     * \param couplingManager couplingManager of the simulation
     */
    template<class CouplingManager>
    void printWholeIsCoupledElement_(const CouplingManager& couplingManager)
    {
        std::cout << "--------------------Printing the whole isCoupledElement_--------------------" << std::endl;

        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::index_constant<MDTraits::numSubDomains>{}), [&](const auto i)
        {
            for (const auto& element : elements(couplingManager.problem(i).gridGeometry().gridView()))
            {
                auto elementIdx = couplingManager.problem(i).gridGeometry().elementMapper().index(element);
                std::cout << "For domainIdx: " << i << " with elementIdx: " << elementIdx << " the isCoupled value is: " << isCoupledElement_[i][elementIdx] << std::endl;
            }
        });

        std::cout << "====================END PRINTING isCoupledElement_====================" << std::endl;
    }


    /*!
     * \brief Helper function, print the whole container of which svcfs are coupled
     *
     * \param couplingManager couplingManager of the simulation
     */
    template<class CouplingManager>
    void printWholeIsCoupledScvf_(const CouplingManager& couplingManager)
    {
        std::cout << "--------------------Printing the whole isCoupledScvf_--------------------" << std::endl;

        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::index_constant<MDTraits::numSubDomains>{}), [&](const auto i)
        {
            for (const auto& element : elements(couplingManager.problem(i).gridGeometry().gridView()))
            {
                auto fvGeometry = localView(couplingManager.problem(i).gridGeometry());
                fvGeometry.bindElement(element);

                for (const auto& scvf : scvfs(fvGeometry))
                {
                    std::cout << "For domainIdx: " << i << " with scvfIdx: " << scvf.index() << " the isCoupled value is: " << isCoupledScvf_[i][scvf.index()] << " with scvf.pos: " << scvf.center() << std::endl;
                }
            }
        });

        std::cout << "====================END PRINTING isCoupledScvf_====================" << std::endl;
    }


    /*!
     * \brief Helper function, prints the scvfCouplingInfo container. Here, each entry contains the coupling domains, the coupling elements and the coupling scvfs
     *
     * \param couplingManager couplingManager of the simulation
     */
    template<class CouplingManager>
    void printWholeScvfCouplingInfo_(const CouplingManager& couplingManager)
    {
        std::cout << "--------------------Printing the whole printWholeScvfCouplingInfo_--------------------" << std::endl;

        using namespace Dune::Hybrid;
        forEach(integralRange(Dune::index_constant<MDTraits::numSubDomains>{}), [&](const auto i)
        {
            for (const auto& element : elements(couplingManager.problem(i).gridGeometry().gridView()))
            {
                auto fvGeometry = localView(couplingManager.problem(i).gridGeometry());
                fvGeometry.bindElement(element);

                for (const auto& scvf : scvfs(fvGeometry))
                {

                    for(int coupledElem=0; coupledElem<scvfCouplingInfo_[i][scvf.index()].size(); coupledElem++)
                    {
                        std::cout << "ownDomain is: " << std::get<0>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) <<
                        " while ownElement is : "  << std::get<1>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) <<
                        " while ownScvf is : "  << std::get<2>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) <<
                        " . Meanwhile otherDomain is: " <<  std::get<3>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) <<
                        " with coupledElement from that domain is: " << std::get<4>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) <<
                        " with coupledScvf from that domain is: " << std::get<5>(scvfCouplingInfo_[i][scvf.index()][coupledElem]) << std::endl;
                    }
                }
            }
        });

        std::cout << "====================END PRINTING printWholeScvfCouplingInfo_====================" << std::endl;
    }




    // std::unordered_map<GlobalPosition, TestTuple> couplingInterfaceVEMap_;
    std::unordered_map<ReducedGlobalPosition, TestTuple, GlobalPositionHash> couplingInterfaceVEMap_;
    Scalar eps_ = 1e-7;

    //TODO: maybe delete, belongs to old implementation
    std::array<MapType, numSD> stencils_; //TODO: delete, when done with this
    std::vector<std::size_t> emptyStencil_; //TODO: delete, when done with this
    std::array<FlipScvfMapType, numSD> scvfInfo_; //TODO: delete, when done with this

    //myStuff I actually use
    std::array<StencilType, numSD> myStencils_;
    std::array<StencilType, numSD> scvfCouplingInfo_;
    std::array<std::vector<bool>, numSD> isCoupledScvf_;
    std::array<std::vector<bool>, numSD> isCoupledElement_;
    TupleVectorType myEmptyStencil_;
};

} // end namespace Dumux

#endif
