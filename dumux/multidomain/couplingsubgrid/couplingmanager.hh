// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup DarcyDarcyVerticalEquilibriumCoupling
 * \brief Coupling manager for d and (d-1)-dimensional boundary coupling
 */

#ifndef DUMUX_MULTIDOMAIN_DARCYDARCYVERTICALEQUILIBRIUM_TWOBOUNDARIES_COUPLINGMANAGER_HH
#define DUMUX_MULTIDOMAIN_DARCYDARCYVERTICALEQUILIBRIUM_TWOBOUNDARIES_COUPLINGMANAGER_HH

#include <iostream>
#include <vector>

#include <dune/common/indices.hh>
#include <dune/common/exceptions.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/math.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include <dumux/multidomain/couplingmanager.hh>
#include "couplingmapper.hh"

#include <dumux/verticalequilibriumcommon/util.hh>

#include <algorithm> // std::sort

#include <iomanip> 
#include <chrono>
#include <cmath>
#include <tuple>


namespace Dumux {

namespace DarcyDarcyVEDetail {
// global subdomain indices
static constexpr auto darcyDomainIndex = Dune::index_constant<0>();
static constexpr auto darcyVEDomainIndex = Dune::index_constant<1>();
}; //end namespace DarcyDarcyVEDetail

/*!
 * \ingroup DarcyDarcyVerticalEquilibriumCoupling
 * \brief Coupling manager for d and (d-1)-dimensional boundary coupling of darcy model and a vertical equilibrium darcy model
 */
template<class MDTraits>
class DarcyDarcyVerticalEquilibriumBoundaryCouplingManager
: public CouplingManager<MDTraits>
{
    using ParentType = CouplingManager<MDTraits>;

    using Scalar = typename MDTraits::Scalar;
    using SolutionVector = typename MDTraits::SolutionVector;

    template<std::size_t i> using SubDomainTypeTag = typename MDTraits::template SubDomain<i>::TypeTag;
    template<std::size_t i> using Problem = GetPropType<SubDomainTypeTag<i>, Properties::Problem>;
    template<std::size_t i> using PrimaryVariables = GetPropType<SubDomainTypeTag<i>, Properties::PrimaryVariables>;
    template<std::size_t i> using NumEqVector = Dumux::NumEqVector<PrimaryVariables<i>>;
    template<std::size_t i> using ElementVolumeVariables = typename GetPropType<SubDomainTypeTag<i>, Properties::GridVolumeVariables>::LocalView;
    template<std::size_t i> using VolumeVariables = typename GetPropType<SubDomainTypeTag<i>, Properties::GridVolumeVariables>::VolumeVariables;
    template<std::size_t i> using GridGeometry = typename MDTraits::template SubDomain<i>::GridGeometry;
    template<std::size_t i> using FVElementGeometry = typename GridGeometry<i>::LocalView;
    template<std::size_t i> using SubControlVolumeFace = typename GridGeometry<i>::SubControlVolumeFace;
    template<std::size_t i> using SubControlVolume = typename GridGeometry<i>::SubControlVolume;
    template<std::size_t i> using GridView = typename GridGeometry<i>::GridView;
    template<std::size_t i> using Element = typename GridView<i>::template Codim<0>::Entity;

    using GlobalPosition = typename Element<DarcyDarcyVEDetail::darcyDomainIndex>::Geometry::GlobalCoordinate;

    template<std::size_t i> using GridVolumeVariables = GetPropType<SubDomainTypeTag<i>, Properties::GridVolumeVariables>;
    template<std::size_t id> using GridVariables = typename MDTraits::template SubDomain<id>::GridVariables;
    using GridVariablesTuple = typename MDTraits::template TupleOfSharedPtr<GridVariables>;

    using CellArray = std::array<unsigned int, GridView<0>::dimensionworld>; //TODO: is entry 0 ok in general? Depends...
    static constexpr std::size_t dim = GridView<0>::dimension; //TODO: is entry 0 ok in general? Depends...

    template<std::size_t i>
    static constexpr auto domainIdx()
    { return typename MDTraits::template SubDomain<i>::Index{}; }

    template<std::size_t i>
    static constexpr bool isCCTpfa()
    { return GridGeometry<i>::discMethod == DiscretizationMethods::cctpfa; }

    using CouplingStencil = std::vector<std::size_t>;
    
public:
    //! export traits
    using MultiDomainTraits = MDTraits;

    static constexpr auto darcyDomainIndex = DarcyDarcyVEDetail::darcyDomainIndex;
    static constexpr auto darcyVEDomainIndex = DarcyDarcyVEDetail::darcyVEDomainIndex;

    //! export stencil types
    using CouplingStencils = std::unordered_map<std::size_t, CouplingStencil>;


    /*!
     * \brief Methods to be accessed by main
     */
    // \{

    void init(std::shared_ptr<Problem<domainIdx<0>()>> problem0,
              std::shared_ptr<Problem<domainIdx<1>()>> problem1,
              const SolutionVector& curSol)
    {
        this->setSubProblems(std::make_tuple(problem0, problem1));
        this->updateSolution(curSol);
        couplingMapper_.update(*this);

        std::cout << "Initial coupling map:" << std::endl;
        couplingMapper_.printWholeCouplingStencil(*this);
        couplingMapper_.printWholeScvfCouplingInfo(*this);
    }


    void update(const SolutionVector& curSol)
    {
        this->updateSolution(curSol);
        couplingMapper_.update(*this);

//         couplingMapper_.printWholeCouplingStencil(*this);
//         couplingMapper_.printWholeScvfCouplingInfo(*this);
    }


    /*!
     * \brief Evaluate the boundary conditions for a coupled Neumann boundary segment. This function is used in within the VE problem.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent. It used for the coupling fluxes within the
     * neumann functions in the problem files. FLUX FROM DARCY TO VE (sum of all fluxes).
     *
     * \param domainI the domain index for which to compute the flux to, expected to be a VE domain
     * \param elementCoarse the element on the VE coarse scale
     * \param fvGeometryVE fvGeometry bound to the coupling, coarse-scale element
     * \param elemVolVarsVE element volume variables of the coarse-scale element
     * \param scvfCoarse coupling scvf of the coarse-scale element
     * \param domainJ the domain index for which to compute the flux from, expected to be a fullD domain
     * \param phaseIdx the phase for which to compute the flux
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    Scalar computeFluxFromDarcyToVE(Dune::index_constant<darcyVEDomainIndex> domainI,
                                    const Element<darcyVEDomainIndex>& elementCoarse,
                                    const FVElementGeometry<darcyVEDomainIndex>& fvGeometryVE,
                                    const ElementVolumeVariables<darcyVEDomainIndex>& elemVolVarsVE,
                                    const SubControlVolumeFace<darcyVEDomainIndex>& scvfCoarse,
                                    Dune::index_constant<darcyDomainIndex> domainJ,
                                    int phaseIdx = 0) const
    {
        //obtain necessary coarse-scale quantities
        auto problemVE = this->problem(domainI);
        auto fineProblem = problemVE.getOtherProblem();
        auto elementIdx = problemVE.gridGeometry().elementMapper().index(elementCoarse);
        const int columnIdx = elementIdx;
        const auto& insideScvCoarse = fvGeometryVE.scv(scvfCoarse.insideScvIdx());

        const auto elemSolCoarse = elementSolution(elementCoarse, this->curSol(domainI), this->problem(domainI).gridGeometry());
        const auto& priVarsCoarse = elemSolCoarse[insideScvCoarse.localDofIndex()];
        const auto pWCoarse = priVarsCoarse[0];
        const auto satWCoarse = 1.0 - priVarsCoarse[1];

        const auto coarsePos = insideScvCoarse.center();
        const Scalar temperature = fineProblem->spatialParams().temperatureAtPos(coarsePos); //TODO: position of wrong element! This is only ok with const temperature
        const auto wPhaseSecondaries = fineProblem->returnDensityLiquidAndViscosity(temperature,pWCoarse);
        const auto nwPhaseSecondaries = fineProblem->returnDensityGasAndViscosity(temperature,pWCoarse);
        Scalar densityWCoarse = wPhaseSecondaries[0];
        Scalar densityNwCoarse = nwPhaseSecondaries[0];
        Scalar viscosityWCoarse = wPhaseSecondaries[1];
        Scalar viscosityNwCoarse = nwPhaseSecondaries[1];

        const Scalar domainHeight = fineProblem->gridGeometry().bBoxMax()[dim-1] - fineProblem->gridGeometry().bBoxMin()[dim-1];
        const Scalar gravityNorm = fineProblem->spatialParams().gravity(coarsePos).two_norm();
        const auto fluidMatrixInteraction = fineProblem->spatialParams().fluidMatrixInteractionAtPos(coarsePos);
        const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
        const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
        const Scalar lambda = fineProblem->spatialParams().getBrooksCoreyLambda();
        const Scalar entryP = fineProblem->spatialParams().getBrooksCoreyEntryP();
        const Scalar deltaZ = fineProblem->getDeltaZ();

        Scalar gasPlumeDistThisColumn = fineProblem->getQuantityReconstructor()->computeGasPlumeDist
            (std::make_tuple(densityWCoarse, densityNwCoarse),
             std::make_tuple(resSatW, resSatNw),
             gravityNorm,
             domainHeight,
             satWCoarse,
             std::make_tuple(lambda, entryP));

        //initialize fine-scale volume variables for later usage. Always init coarse-scale volVars before initializing fine-scale volVars
        VolumeVariables<domainI> insideVolVarsFine;

        //obtain necessary fullD quantities
        auto problemDarcy = this->problem(domainJ);

        //coupled fullD element indices
        auto coupledStencilVector = couplingMapper_.myOutsideElementIndices(domainI, elementIdx, domainJ);

        //for 3D need to pick out correct coupling elements for the respective scvf
        if constexpr(dim==3)
        {
            GlobalPosition normalVec = elementCoarse.geometry().center() - scvfCoarse.center();
            coupledStencilVector = pickCorrectCoupledElements_<domainJ>(problemDarcy.gridGeometry(), coupledStencilVector, scvfCoarse.center(), normalVec);
        }

        //initialize the flux which will be the sum of all fluxes between the fullD and VE fine-scale elements
        Scalar totalFluxFromDarcyToVE = 0.0;
        // How Dumux::parallelFor works: give it a size (like coupledStencilVector.size()) and coupledStencilVectorIndex will iterator from 0 to this size. In contrary, std::for_each(std::execution::par, coupledStencilVector.cbegin(), coupledStencilVector.cend(), [&](auto&& coupledElementIndex){} iterates over the entries of coupledStencilVector which are stored in coupledElementIndex and coupledElementIndex does not iterate over the size of coupledStencilVector but its entries
        std::vector<Scalar> fluxVec(coupledStencilVector.size(), 0.0);
        Dumux::parallelFor(coupledStencilVector.size(), [&](const std::size_t coupledStencilVectorIndex)
        {
            VolumeVariables<domainI> insideVolVarsFine;
            const std::size_t coupledElementIndex = coupledStencilVector[coupledStencilVectorIndex];
            Scalar flux = 0.0;
            //obtain information on the outside element, which is part of the Darcy domain
            const auto outsideElementFullD = problemDarcy.gridGeometry().element(coupledElementIndex);
            auto fvGeometryOutsideFullD = localView(problemDarcy.gridGeometry());
            fvGeometryOutsideFullD.bindElement(outsideElementFullD);
            auto outsideCouplingScvfFullD = returnCoupledScvfOutside_(fvGeometryOutsideFullD, domainJ, scvfCoarse.center());
            const auto& outsideScvFullD = fvGeometryOutsideFullD.scv(outsideCouplingScvfFullD.insideScvIdx());
            const auto outsideVolVarsFullD = volVars(domainJ, outsideElementFullD, outsideScvFullD); //this is super expensive because of density and viscosity calls

            //obtain information on the inside element, which is part of the VE fine-scale domain
            const auto insideFineElement = this->returnCoupledElementFine_(columnIdx, outsideElementFullD.geometry().center());
            auto fvGeometryInsideFine = localView(fineProblem->gridGeometry());
            fvGeometryInsideFine.bindElement(insideFineElement);
            SubControlVolumeFace<domainI> insideFineCouplingScvf = this->returnCoupledScvfFine_(fvGeometryInsideFine, outsideCouplingScvfFullD.center());
            const SubControlVolume<domainI>& insideFineScv = fvGeometryInsideFine.scv(insideFineCouplingScvf.insideScvIdx());

            //Update fine-scale volume variables
            insideVolVarsFine.updateFine(*fineProblem, insideFineElement, pWCoarse, satWCoarse, gasPlumeDistThisColumn, densityWCoarse, densityNwCoarse, viscosityWCoarse,
                                         viscosityNwCoarse, domainHeight, gravityNorm, resSatW, resSatNw, lambda, entryP, deltaZ);
            flux = computeDarcyFlux(fvGeometryInsideFine, fvGeometryOutsideFullD, domainI, domainJ, insideVolVarsFine, outsideVolVarsFullD, insideFineCouplingScvf, outsideCouplingScvfFullD, insideFineScv, outsideScvFullD, phaseIdx);

            //cumulate all fluxes oer the vertical height
            fluxVec[coupledStencilVectorIndex] = flux*deltaZ;
        });
        for(int i=0; i<fluxVec.size(); i++)
            totalFluxFromDarcyToVE += fluxVec[i];

        return totalFluxFromDarcyToVE;
    }


    /*!
     * \brief Evaluate the boundary conditions for a coupled Neumann boundary segment. This function is used in within the fullD problem.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent. It used for the coupling fluxes within the
     * neumann functions in the problem files. FLUX FROM VE TO DARCY.
     *
     * \param domainI the domain index for which to compute the flux to, expected to be a fullD domain
     * \param elementDarcy the element on the fullD coarse scale
     * \param fvGeometryDarcy fvGeometry bound to the coupling, fullD element
     * \param elemVolVars element volume variables of the fullD element
     * \param scvfDarcy coupling scvf of the fullD element
     * \param domainJ the domain index for which to compute the flux from, expected to be a VE domain
     * \param phaseIdx the phase for which to compute the flux
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    Scalar computeFluxFromVEToDarcy(Dune::index_constant<darcyDomainIndex> domainI,
                                    const Element<darcyDomainIndex>& elementDarcy,
                                    const FVElementGeometry<darcyDomainIndex>& fvGeometryDarcy,
                                    const ElementVolumeVariables<darcyDomainIndex>& elemVolVars,
                                    const SubControlVolumeFace<darcyDomainIndex>& scvfDarcy,
                                    Dune::index_constant<darcyVEDomainIndex> domainJ,
                                    int phaseIdx = 0) const
    {
        // static constexpr std::size_t dim = GridView<darcyVEDomainIndex>::dimension;
        Scalar flux = 0.0;

        //obtain necessary coarse-scale and fullD quantities
        auto problemVE = this->problem(domainJ);
        auto fineProblem = problemVE.getOtherProblem();

        auto problemDarcy = this->problem(domainI);
        auto elementIdx = problemDarcy.gridGeometry().elementMapper().index(elementDarcy);

        //coupled VE coarse-scale element indices
        GlobalPosition normalVec = elementDarcy.geometry().center() - scvfDarcy.center();
        auto coupledStencilVector = couplingMapper_.myOutsideElementIndices(domainI, elementIdx, domainJ);
        
        //obtain information on the outside element, which is part of the VE domain, this is only needed to update coarse-scale volVars for updated fine-scale volVars
        //on the coarse-scale
        int columnIdx = -1;
        Element<darcyVEDomainIndex> outsideElementCoarse{};
        if constexpr(dim==2) //TODO: this can probably be unified with the 3D branch
        {
            columnIdx = coupledStencilVector[0]; //coupledStencilVector only has one entry in 2D, which is the index of the coupled coarse VE cell
            outsideElementCoarse = problemVE.gridGeometry().element(coupledStencilVector[0]);
        }
        else if constexpr(dim==3)
        {
            auto coupledVETuple = findCorrectCouplingTuple_(elementDarcy, scvfDarcy, coupledStencilVector, normalVec);
            columnIdx = std::get<0>(coupledVETuple);
            outsideElementCoarse = std::get<1>(coupledVETuple);
        }

        auto fvGeometryOutsideCoarse = localView(problemVE.gridGeometry());
        fvGeometryOutsideCoarse.bindElement(outsideElementCoarse);
        const auto& coupledScvfOutsideCoarse = returnCoupledScvfOutside_(fvGeometryOutsideCoarse, domainJ, scvfDarcy.center());
        const auto& outsideScvCoarse = fvGeometryOutsideCoarse.scv(coupledScvfOutsideCoarse.insideScvIdx());

        const auto elemSolCoarse = elementSolution(outsideElementCoarse, this->curSol(domainJ), this->problem(domainJ).gridGeometry());
        const auto& priVarsCoarse = elemSolCoarse[outsideScvCoarse.localDofIndex()];
        const auto pWCoarse = priVarsCoarse[0];
        const auto satWCoarse = 1.0 - priVarsCoarse[1];

        const auto coarsePos = outsideScvCoarse.center();
        const Scalar temperature = fineProblem->spatialParams().temperatureAtPos(coarsePos); //TODO: position of wrong element! This is onlyok with const temperature
        const auto wPhaseSecondaries = fineProblem->returnDensityLiquidAndViscosity(temperature,pWCoarse);
        const auto nwPhaseSecondaries = fineProblem->returnDensityGasAndViscosity(temperature,pWCoarse);
        Scalar densityWCoarse = wPhaseSecondaries[0];
        Scalar densityNwCoarse = nwPhaseSecondaries[0];
        Scalar viscosityWCoarse = wPhaseSecondaries[1];
        Scalar viscosityNwCoarse = nwPhaseSecondaries[1];

        // std::cout << "pWCoarse: " << pWCoarse << " and densities are: " << densityWCoarse << " " << densityNwCoarse << std::endl;

        const Scalar domainHeight = fineProblem->gridGeometry().bBoxMax()[dim-1] - fineProblem->gridGeometry().bBoxMin()[dim-1];
        const Scalar gravityNorm = fineProblem->spatialParams().gravity(coarsePos).two_norm();
        const auto fluidMatrixInteraction = fineProblem->spatialParams().fluidMatrixInteractionAtPos(coarsePos);
        const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
        const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
        const Scalar lambda = fineProblem->spatialParams().getBrooksCoreyLambda();
        const Scalar entryP = fineProblem->spatialParams().getBrooksCoreyEntryP();
        const Scalar deltaZ = fineProblem->getDeltaZ();

        Scalar gasPlumeDistThisColumn = fineProblem->getQuantityReconstructor()->computeGasPlumeDist
            (std::make_tuple(densityWCoarse, densityNwCoarse),
                std::make_tuple(resSatW, resSatNw),
                gravityNorm,
                domainHeight,
                satWCoarse,
                std::make_tuple(lambda, entryP));

        //on the fine-scale
        const auto outsideElement = this->returnCoupledElementFine_(columnIdx, elementDarcy.geometry().center());
        auto fvGeometryOutside = localView(fineProblem->gridGeometry());
        fvGeometryOutside.bindElement(outsideElement);
        auto outsideCouplingScvfFine = this->returnCoupledScvfFine_(fvGeometryOutside, scvfDarcy.center()); //TODO: too specific, introduce general check if on coupling interface!
        const auto& outsideScvFine = fvGeometryOutside.scv(outsideCouplingScvfFine.insideScvIdx());

        //Update fine-scale volume variables. Coarse-scale volVars NEED to be updated beforehand
        VolumeVariables<domainJ> outsideVolVarsFine;
        outsideVolVarsFine.updateFine(*fineProblem, outsideElement, pWCoarse, satWCoarse, gasPlumeDistThisColumn, densityWCoarse, densityNwCoarse, viscosityWCoarse, viscosityNwCoarse, domainHeight, gravityNorm, resSatW, resSatNw, lambda, entryP, deltaZ);

        //obtain information on the inside element, which is part of the fullD domain
        const auto& insideScv = fvGeometryDarcy.scv(scvfDarcy.insideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];

        //compute the coupling flux between the neighboring fullD and VE fine-scale element, careful when to use coarse-scale and where to use fine-scale quantities
        flux = computeDarcyFlux(fvGeometryDarcy, fvGeometryOutside, domainI, domainJ, insideVolVars, outsideVolVarsFine, scvfDarcy, outsideCouplingScvfFine, insideScv, outsideScvFine, phaseIdx);

        return flux;
    }


    /*!
     * \brief Computes the flux between a fine-scale VE element and a fullD element
     *
     * \param fvGeometrySelf fvGeometry bound to the coupling element belonging to domainSelf
     * \param fvGeometryOther fvGeometry bound to the coupling element belonging to domainOther
     * \param domainSelf domain index of own/inside domain
     * \param domainOther domain index of other/outside domain
     * \param insideVolVars updated volume variables of the inner domain
     * \param outsideVolVars updated volume variables of the outer domain
     * \param insideScvf sub-control volume face of the inner domain
     * \param outsideScvf sub-control volume face of the outer domain
     * \param insideScv sub-control volume of the inner domain
     * \param outsideScv sub-control volume of the outer domain
     * \param phaseIdx index of the phase (0 for wetting phase, 1 for non-wetting phase)
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    template<std::size_t i, std::size_t j>
    Scalar computeDarcyFlux(const FVElementGeometry<i>& fvGeometrySelf,
                            const FVElementGeometry<j>& fvGeometryOther,
                            Dune::index_constant<i> domainSelf,
                            Dune::index_constant<j> domainOther, //TODO: this is just here for template parameter deduction, any other way to leave this argument out?
                            const VolumeVariables<i>& insideVolVars,
                            const VolumeVariables<j>& outsideVolVars,
                            const SubControlVolumeFace<i>& insideScvf,
                            const SubControlVolumeFace<j>& outsideScvf,
                            const SubControlVolume<i>& insideScv,
                            const SubControlVolume<j>& outsideScv,
                            int phaseIdx = 0) const
    {
        Scalar flux = 0.0;
        static const bool enableGravity = getParam<bool>("Problem.EnableGravity");

        //compute transmissibilities
        using Extrusion = typename GridGeometry<i>::Extrusion;
        const auto ti = computeTpfaTransmissibility(fvGeometrySelf, insideScvf, insideScv, insideVolVars.permeability(), insideVolVars.extrusionFactor());
        const auto tj = computeTpfaTransmissibility(fvGeometryOther, outsideScvf, outsideScv, outsideVolVars.permeability(), outsideVolVars.extrusionFactor());
        Scalar tij = 0.0;
        if (ti*tj > 0.0)
            tij = Extrusion::area(fvGeometrySelf, insideScvf)*(ti * tj)/(ti + tj);

        if (enableGravity)
        {
            // do averaging for the density over all neighboring elements
            const auto rho = (insideVolVars.density(phaseIdx) + outsideVolVars.density(phaseIdx))*0.5;
            const auto& g = this->problem(domainSelf).spatialParams().gravity(insideScvf.ipGlobal());

            //! compute alpha := n^T*K*g
            const auto alpha_inside = vtmv(insideScvf.unitOuterNormal(), insideVolVars.permeability(), g)*insideVolVars.extrusionFactor();
            const auto alpha_outside = vtmv(insideScvf.unitOuterNormal(), outsideVolVars.permeability(), g)*outsideVolVars.extrusionFactor();

            // Obtain inside and outside pressures
            const auto pInside = insideVolVars.pressure(phaseIdx);
            const auto pOutside = outsideVolVars.pressure(phaseIdx);

            flux = tij*(pInside - pOutside) + rho*Extrusion::area(fvGeometrySelf, insideScvf)*alpha_inside - rho*tij/tj*(alpha_inside - alpha_outside);
        }
        else
        {
            // Obtain inside and outside pressures
            const auto pInside = insideVolVars.pressure(phaseIdx);
            const auto pOutside = outsideVolVars.pressure(phaseIdx);

            // return flux
            flux = tij*(pInside - pOutside);
        }

        // upwind scheme
        static const Scalar upwindWeight = getParam<Scalar>("Flux.UpwindWeight");

        auto upwindTerm = [phaseIdx](const auto& volVars){ return volVars.density(phaseIdx)*volVars.mobility(phaseIdx); };
        if (std::signbit(flux)) // if sign of flux is negative
            flux *= (upwindWeight*upwindTerm(outsideVolVars)
                     + (1.0 - upwindWeight)*upwindTerm(insideVolVars));
        else
            flux *= (upwindWeight*upwindTerm(insideVolVars)
                     + (1.0 - upwindWeight)*upwindTerm(outsideVolVars));

        // make this a area-specific flux
            flux /= Extrusion::area(fvGeometrySelf, insideScvf)*insideVolVars.extrusionFactor();
        return flux;
    }




    /*!
     * \brief returns an iteratable container of all indices of degrees of freedom of domain j
     *        that couple with / influence the element residual of the given element of domain i
     *
     * It is VERY important that this function returns ONLY the coupled elements of domainJ which are coupled to domainI (NOT ALL coupled elements of domainJ), otherwise this leads to an error with matrix sizes in the assembler
     *
     * \param domainI the domain index of domain i
     * \param element the coupled element of domain í
     * \param domainJ the domain index of domain j
     */
    template<std::size_t i, std::size_t j>
    const CouplingStencil couplingStencil(Dune::index_constant<i> domainI,
                                          const Element<i>& element,
                                          Dune::index_constant<j> domainJ) const
    {
        static_assert(i != j, "A domain cannot be coupled to itself!");
        const auto eIdx = this->problem(domainI).gridGeometry().elementMapper().index(element);

        return couplingMapper_.myOutsideElementIndices(domainI, eIdx, domainJ);
    }


    /*!
     * \brief If the boundary entity is on a coupling boundary
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i>
    bool isCoupled(Dune::index_constant<i> domainI,
                   const SubControlVolumeFace<i>& scvf) const
    {
        return couplingMapper_.myIsCoupled(domainI, scvf);
    }


    /*!
     * \brief If the boundary entity is on a coupling boundary
     * \param domainI the domain index for which to compute the flux
     * \param scvf the sub control volume face
     */
    template<std::size_t i, std::size_t j>
    bool isCoupled(Dune::index_constant<i> domainI,
                   const SubControlVolumeFace<i>& scvf,
                   Dune::index_constant<j> domainJ) const
    {
        return couplingMapper_.myIsCoupled(domainI, scvf, domainJ);
    }


    //! Return the volume variables of domain i for a given element and scv
    template<std::size_t i>
    VolumeVariables<i> volVars(Dune::index_constant<i> domainI,
                               const Element<i>& element,
                               const SubControlVolume<i>& scv) const
    {
        VolumeVariables<i> volVars;
        const auto elemSol = elementSolution(element, this->curSol(domainI), this->problem(domainI).gridGeometry());
        volVars.update(elemSol, this->problem(domainI), element, scv);
        return volVars;
    }


    //Printer functions
    void printWholeCouplingStencil()
    {
        couplingMapper_.printWholeCouplingStencil(*this);
    }

    void printWholeIsCoupledElement()
    {
        couplingMapper_.printWholeIsCoupledElement(*this);
    }

    void printWholeIsCoupledScvf()
    {
        couplingMapper_.printWholeIsCoupledScvf(*this);
    }

    void printWholeScvfCouplingInfo()
    {
        couplingMapper_.printWholeScvfCouplingInfo(*this);
    }

    int numberOfSubdomains() const
    {

        return couplingMapper_.numberOfSubdomains(*this);
    }


    /*!
     * \brief set the pointers to the grid variables
     * \param gridVariables A tuple of shared pointers to the grid variables
     */
    void setGridVariables(GridVariablesTuple&& gridVariables)
    { gridVariables_ = gridVariables; }

    /*!
     * \brief set a pointer to one of the grid variables
     * \param gridVariables a pointer to the grid variables
     * \param domainIdx the domain index of the grid variables
     */
    template<class  GridVariables, std::size_t i>
    void setGridVariables(std::shared_ptr<GridVariables> gridVariables, Dune::index_constant<i> domainIdx)
    { std::get<i>(gridVariables_) = gridVariables; }

    /*!
     * \brief Return a reference to the grid variables of a sub problem
     * \param domainIdx The domain index
     */
    template<std::size_t i>
    const GridVariables<i>& gridVariables(Dune::index_constant<i> domainIdx) const
    {
        if (std::get<i>(gridVariables_))
            return *std::get<i>(gridVariables_);
        else
            DUNE_THROW(Dune::InvalidStateException, "The gridVariables pointer was not set. Use setGridVariables() before calling this function");
    }


private:

    /*!
     * \brief Deduce if two normal vectors show in opposite directions
     * \param normVec1 normal vector one
     * \param normVec2 normal vector two
     */
    bool areNormalsVectorsOppositeInPlane_(const GlobalPosition& normVec1,
                                           const GlobalPosition& normVec2) const
    {
        bool areOpposite = false;
        if(normVec1.size() != normVec1.size())
            DUNE_THROW(Dune::InvalidStateException, "The two provided position vectors must match in size!");

        Scalar scalarProduct=0.0;
        for(int i=0; i<normVec1.size(); i++)
            scalarProduct += normVec1*normVec2;

        if(scalarProduct<0)
            areOpposite = true;

        return areOpposite;
    }


    /*!
     * \brief Deduce if any of the provided coordinates are equal
     * \param pos1 coordinate one
     * \param pos2 coordinate two
     */
    bool anyCoordMatch_(const GlobalPosition& pos1,
                        const GlobalPosition& pos2) const
    {
        if(pos1.size() != pos2.size())
            DUNE_THROW(Dune::InvalidStateException, "The two provided position vectors must match in size!");

        bool anyMatch = false;
        for(int i=0; i<pos1.size(); i++)
        {
            if(pos1[i]==pos2[i])
            {
                anyMatch=true;
                break;
            }
        }

        return anyMatch;
    }


    /*!
     * \brief Deduce if all (except z-direction) of the provided coordinates are equal
     * \param pos1 coordinate one
     * \param pos2 coordinate two
     */
    bool mostCoordMatch_(const GlobalPosition& pos1,
                         const GlobalPosition& pos2) const
    {
        if(pos1.size() != pos2.size())
            DUNE_THROW(Dune::InvalidStateException, "The two provided position vectors must match in size!");

        bool anyMatch = true;
        for(int i=0; i<pos1.size()-1; i++)
        {
            if(pos1[i]!=pos2[i])
            {
                anyMatch=false;
                break;
            }
        }

        return anyMatch;
    }


    /*!
     * \brief Given a vector of coupled element indices (from Darcy domain), pick the correct ones that belong the provided position of the coarse-scale scvf
     * \param outsideGridGeometry grid geometry of outside domaim
     * \param coupledElements     vector containing all coupled to element indices
     * \param posCoupledScvf      position of coupled from scvf
     * \param normalVec           points outwards of element that is coupled from
     */
    template<std::size_t i>
    std::vector<std::size_t> pickCorrectCoupledElements_(const GridGeometry<i>& outsideGridGeometry,
                                                         const std::vector<std::size_t>& coupledElements,
                                                         const GlobalPosition& posCoupledScvf,
                                                         const GlobalPosition& normalVec) const
    {
        std::vector<std::size_t> filteredElementIndices{};

        for(int j=0; j<coupledElements.size(); j++)
        {
            const auto outsideElement = outsideGridGeometry.element(coupledElements[j]);
            const GlobalPosition elementPos = outsideElement.geometry().center();
            if(anyCoordMatch_(elementPos,posCoupledScvf))
            {
                if(areNormalsVectorsOppositeInPlane_(normalVec, elementPos-posCoupledScvf))
                    filteredElementIndices.push_back(coupledElements[j]);
            }
        }

        return filteredElementIndices;
    }


    /*!
     * \brief Find the coupling (Darcy/VE) scvf of a provided coupling fine-scale element (VE/Darcy)
     * \param outsideFvGeometry  fvGeometry bound to the coupling, outside fine-scale element (Darcy or VE domain)
     * \param domainI            index of the vertical equilibrium domain,
     * \param posCoupledFromElem position of the coarse scvf that we are coupling from
     */
    template<std::size_t i>
    SubControlVolumeFace<i> returnCoupledScvfOutside_(FVElementGeometry<i>& outsideFvGeometry,
                                                      Dune::index_constant<i> domainI,
                                                      const GlobalPosition& posCoupledFromElem,
                                                      bool verbose = false) const
    {
        SubControlVolumeFace<i> couplingScvf{};

        bool foundCoupledScvf = false;
        for (const auto& scvf : scvfs(outsideFvGeometry))
        {
            if(verbose)
                std::cout << "Looking for match for " << posCoupledFromElem << " and currently @: " <<  scvf.center() << " with idx: " << scvf.index() << ". Is the current coupled?: " << isCoupled(domainI, scvf) << " and do any coords match=: " << mostCoordMatch_(scvf.center(),posCoupledFromElem) << std::endl;

            if(isCoupled(domainI, scvf) && mostCoordMatch_(scvf.center(),posCoupledFromElem))
            {
                couplingScvf = scvf;
                foundCoupledScvf = true;
                break;
            }
        }

        if(foundCoupledScvf==false)
        {
            std::ostringstream errMsg;
            errMsg << "In returnCoupledScvfOutside_, no matching coupled scvf found for pos: " << posCoupledFromElem << std::endl;
            throw std::length_error(errMsg.str());
        }

        return couplingScvf;
    }


    /*!
     * \brief Return index and element of correct coupling VE element
     * \param elementDarcyCoupledFrom Darcy element coupled from
     * \param scvfDarcyCoupledFrom    Darcy scvf coupled from
     * \param elemIndicesVECoupledTo  vector containing all coupled VE elements to elementDarcyCoupledFrom
     * \param normalVec               normal vector from coupled from element center to coupled from scvf center
     */
    std::tuple<int,Element<darcyVEDomainIndex>> findCorrectCouplingTuple_(const Element<darcyDomainIndex>& elementDarcyCoupledFrom,
                                                                          const SubControlVolumeFace<darcyDomainIndex>& scvfDarcyCoupledFrom,
                                                                          std::vector<std::size_t> elemIndicesVECoupledTo,
                                                                          const GlobalPosition& normalVec) const
    {
        auto problemVE = this->problem(darcyVEDomainIndex);
        auto fvGeometryOutsideCoarse = localView(problemVE.gridGeometry());
        auto elementDarcyPos = elementDarcyCoupledFrom.geometry().center();
        auto scvfDarcyPos = scvfDarcyCoupledFrom.center();
        int alignedDim = -1;

        // find dimension entry in which the coordinated of the Darcy element and scvf align
        for(int dimIter=0; dimIter<elementDarcyPos.size(); dimIter++)
        {
            if(elementDarcyPos[dimIter]==scvfDarcyPos[dimIter])
            {
                alignedDim=dimIter;
                break;
            }
        }

        // find VE element that has same coordinate in aligned dim
        int correctVEIndex = -1;
        Element<darcyVEDomainIndex> correctVEElement{};

        for(int i=0; i<elemIndicesVECoupledTo.size(); i++)
        {
            const auto outsideElementCoarse = problemVE.gridGeometry().element(elemIndicesVECoupledTo[i]);
            const auto couplingVEElementPos = outsideElementCoarse.geometry().center();

            if(couplingVEElementPos[alignedDim]==scvfDarcyPos[alignedDim])
            {
                if(areNormalsVectorsOppositeInPlane_(normalVec, couplingVEElementPos-scvfDarcyCoupledFrom.center()))
                {
                    correctVEIndex = elemIndicesVECoupledTo[i];
                    correctVEElement = outsideElementCoarse;
                    break;
                }
            }
        }
        
        if(correctVEIndex==-1)
            DUNE_THROW(Dune::InvalidStateException, "No correct VE coupling element was found!");
        return std::make_tuple(correctVEIndex,correctVEElement);
    }



    /*!
     * \brief Find and return the (VE) fine-scale scvf which is on the coupling interface
     * \param fvGeometryInsideFine fvGeometry bound to the coupling fine-scale element
     * \param couplingCoordinates global position of the coupled Darcy scvf
     */
    SubControlVolumeFace<darcyVEDomainIndex> returnCoupledScvfFine_(const FVElementGeometry<darcyVEDomainIndex>& fvGeometryInsideFine,
                                                                    const GlobalPosition& couplingCoordinates) const //TODO: NOT general enough, introduce coupling stencil for fine grid?
    {
        SubControlVolumeFace<darcyVEDomainIndex> couplingScvf{};
        bool neighborFound = false;

        for (const auto& scvf : scvfs(fvGeometryInsideFine))
        {
            if(scvf.center() == couplingCoordinates)
            {
                couplingScvf = scvf;
                neighborFound = true;
                break;
            }
        }

        if(neighborFound==false)
        {
            std::ostringstream errMsg;
            errMsg << "No coupling neighbor found for pos: " << couplingCoordinates << std::endl;
            throw std::invalid_argument(errMsg.str());
        }

        return couplingScvf;
    }


    /*!
     * \brief Find and return the VE fine-scale element which is on the same height as the provided Darcy element, used for the flux computations from Darcy to VE
     *
     * \param columnIdx column index of the coupling coarse-scale element
     * \param coupledDarcyElementPos global position of the coupled darcy element
     */
    Element<darcyVEDomainIndex> returnCoupledElementFine_(const unsigned int columnIdx,
                                                          const GlobalPosition& coupledDarcyElementPos) const //when coupling from the Darcy side
    {
        const auto problemVE = this->problem(darcyVEDomainIndex);
        typename std::map<int, Element<darcyVEDomainIndex>>::const_iterator it = problemVE.getMapColumns()->lower_bound(columnIdx);
        auto upperBoundIterator = problemVE.getMapColumns()->upper_bound(columnIdx);

        Element<darcyVEDomainIndex> coupledElementFine{};

        for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
        {
            GlobalPosition fineElementPos = (it->second).geometry().center();
            if(fineElementPos[dim-1] == coupledDarcyElementPos[dim-1])
            {
                coupledElementFine = it->second;
                continue;
            }
        }

        return coupledElementFine;
    }


    DarcyDarcyVerticalEquilibriumBoundaryCouplingMapper<MDTraits> couplingMapper_;
    GridVariablesTuple gridVariables_;
};

} // end namespace Dumux

#endif
