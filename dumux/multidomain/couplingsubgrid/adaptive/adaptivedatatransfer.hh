// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup DarcyDarcyVerticalEquilibriumCoupling
 * \brief Adaptivity manager for data management and transfer between a full-dimensional and a Vertical Equilibrium model (in both directions)
 */

#ifndef DUMUX_TWOPVEMULTIDIM_GRIDDATA_TRANSFER_HH
#define DUMUX_TWOPVEMULTIDIM_GRIDDATA_TRANSFER_HH

#include <iostream>
#include <vector>

#include <dune/common/indices.hh>
#include <dune/common/exceptions.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/math.hh>
#include <dumux/discretization/elementsolution.hh>

#include <algorithm> // std::sort
#include <dumux/multidomain/couplingsubgrid/adaptive/criterionmanager.hh>
#include <sstream> // For ostringstream
#include <dumux/nonlinear/findscalarroot.hh>

namespace Dumux {

/*!
 * \ingroup DarcyDarcyVerticalEquilibriumCoupling
 * \brief Coupling manager for d and (d-1)-dimensional boundary coupling of darcy model and a vertical equilibrium darcy model
 */
template<class MDTraits>
class TwoPVEGridDataTransfer
{
    using Scalar = typename MDTraits::Scalar;
    using SolutionVector = typename MDTraits::SolutionVector;

    template<std::size_t i> using SubDomainTypeTag = typename MDTraits::template SubDomain<i>::TypeTag;
    template<std::size_t i> using Problem = GetPropType<SubDomainTypeTag<i>, Properties::Problem>;
    template<std::size_t i> using GridGeometry = typename MDTraits::template SubDomain<i>::GridGeometry;
    template<std::size_t i> using GridView = typename GridGeometry<i>::GridView;
    template<std::size_t i> using GridVariables = GetPropType<SubDomainTypeTag<i>, Properties::GridVariables>;
    template<std::size_t i> using GridManager = GridManager<GetPropType<SubDomainTypeTag<i>, Properties::Grid>>;

    static constexpr auto darcyDomainIndex = DarcyDarcyVEDetail::darcyDomainIndex;
    static constexpr auto darcyVEDomainIndex = DarcyDarcyVEDetail::darcyVEDomainIndex;

    using ModelCriterionManager = CriterionManager<SubDomainTypeTag<darcyDomainIndex>,SubDomainTypeTag<darcyVEDomainIndex>>;
    using DataTransferContainer = std::vector<std::vector<std::array<Scalar, 2>>>;
    using IndexAssignmentVector = std::vector<std::vector<size_t>>;

    using SolutionVectorFullD = GetPropType<SubDomainTypeTag<darcyDomainIndex>, Properties::SolutionVector>;
    using SolutionVectorVE = GetPropType<SubDomainTypeTag<darcyVEDomainIndex>, Properties::SolutionVector>;

    using VolumeVariablesFullD = typename GetPropType<SubDomainTypeTag<darcyDomainIndex>, Properties::GridVolumeVariables>::VolumeVariables;
    using VolumeVariablesVE = typename GetPropType<SubDomainTypeTag<darcyVEDomainIndex>, Properties::GridVolumeVariables>::VolumeVariables;
    using GridVariablesDarcy = GetPropType<SubDomainTypeTag<darcyDomainIndex>, Properties::GridVariables>;
    using VelocityOutputDarcy = GetPropType<SubDomainTypeTag<darcyDomainIndex>, Properties::VelocityOutput>;

    static constexpr std::size_t dim = GridView<darcyVEDomainIndex>::dimension;

public:

    /*!
     * \brief Constructor
     *
     * \param gridGeometryFullD finite volume grid geometry of the fullD grid
     * \param gridGeometryVE finite volume grid geometry of the VE grid
     * \param sol total solution vector
     * \param modelCriterionManager object managing the computation and storage of the switching criteria
     * \param persistentSwContainer persistent, global container for storing wetting-phase saturations
     * \param persistentMassNwContainer persistent, global container for storing non-wetting-phase masses
     */
    TwoPVEGridDataTransfer(const std::shared_ptr<GridGeometry<darcyDomainIndex>> gridGeometryFullD,
                           const std::shared_ptr<GridGeometry<darcyVEDomainIndex>> gridGeometryVE,
                           const std::shared_ptr<SolutionVector> sol,
                           const std::shared_ptr<ModelCriterionManager> modelCriterionManager,
                           std::shared_ptr<std::vector<double>> persistentSwContainer,
                           std::shared_ptr<std::vector<double>> persistentMassNwContainer)
    : gridGeometryFullD_(gridGeometryFullD),
      gridGeometryVE_(gridGeometryVE),
      sol_(sol),
      deltaY_(1.0),
      solVEBeforeAdapt_({}),
      solFullDBeforeAdapt_({}),
      persistentSwContainer_(persistentSwContainer),
      persistentMassNwContainer_(persistentMassNwContainer),
      solutionTravelColumns_(0)
    {
        auto numCells = getParam<std::vector<Scalar>>("Darcy.Grid.Cells");
        numberColumns_ = 1;
        for(size_t i=0; i<dim-1; i++)
        {
            numberColumns_ *= numCells[i];
        }

        numberZCells_ = numCells[dim-1];
        dataTransferContainer_.resize(numberColumns_);
        globalToLocalIndex_.resize(numberColumns_);
        globalToLocalIndexBeforeAdapt_.resize(numberColumns_);
        globalToLocalIndexAfterAdapt_.resize(numberColumns_);

        currentModelDistribution_.resize(numberColumns_);

        std::cout << "dataTransferContainer_ size: " << dataTransferContainer_.size() << std::endl;
        modelCriterionManager_ = modelCriterionManager;

        auto upperRight = getParam<std::vector<Scalar>>("Darcy.Grid.UpperRight");
        auto lowerLeft = getParam<std::vector<Scalar>>("Darcy.Grid.LowerLeft");
        const Scalar xWidth = upperRight[0] - lowerLeft[0];
        deltaX_ = xWidth/numCells[0];
        if constexpr(dim==3)
        {
            const Scalar yWidth = upperRight[1] - lowerLeft[1];
            deltaY_ = yWidth/numCells[1];
        }
        const Scalar zWidth = upperRight[dim-1] - lowerLeft[dim-1]; //index 1 only makes sense for 2D
        deltaZ_ = zWidth/numberZCells_;
        lowerDomainBoundaryHeight_ = lowerLeft[dim-1];
        upperDomainBoundaryHeight_ = upperRight[dim-1];

        mySwitchIndicator_.resize(numberColumns_);

        porosity_ = getParam<Scalar>("SpatialParams.Porosity");
    }


    /*!
     * \brief Compute the current phase masses available within each column
     * \param problemFullD problem of the fullD grid
     * \param problemCoarseVE problem of the VE grid
     */
    void printCurrentMassColumns(const Problem<darcyDomainIndex>& problemFullD,
                                 const Problem<darcyVEDomainIndex>& problemCoarseVE)
    {
        this->updateGlobalToLocalIndex_();

        std::vector<Scalar> massWInColumnContainer(numberColumns_);
        std::vector<Scalar> massNWInColumnContainer(numberColumns_);

        VolumeVariablesFullD volVarsFullD;
        VolumeVariablesVE volVarsVE;

        for(int globalColumn=0; globalColumn<numberColumns_; globalColumn++)
        {
            //if VE
            if(globalToLocalIndex_[globalColumn].size() == 1)
            {
                auto localVEIndex = globalToLocalIndex_[globalColumn][0]; //as it is VE it should only have one entry
                auto localVEElement = gridGeometryVE_->element(localVEIndex);

                Scalar localVEColumnMassW = 0.0;
                Scalar localVEColumnMassNW = 0.0;

                auto fvGeometryVE = localView(*gridGeometryVE_);
                fvGeometryVE.bind(localVEElement);

                const auto elemSolVE = elementSolution(localVEElement, (*sol_)[darcyVEDomainIndex], *gridGeometryVE_);

                for (const auto& scv : scvs(fvGeometryVE))
                {
                    volVarsVE.update(elemSolVE, problemCoarseVE, localVEElement, scv);

                    //TODO: this is necessary, right?
                    Scalar normalizedPorosity = volVarsVE.porosity()/(upperDomainBoundaryHeight_-lowerDomainBoundaryHeight_);

                    //strictly, using scv.volume() of the whole VE column is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
                    localVEColumnMassW += normalizedPorosity * volVarsVE.density(0) * volVarsVE.saturation(0) * scv.volume();
                    localVEColumnMassNW += normalizedPorosity * volVarsVE.density(1) * volVarsVE.saturation(1) * scv.volume();
                }

                massWInColumnContainer[globalColumn] = localVEColumnMassW;
                massNWInColumnContainer[globalColumn] = localVEColumnMassNW;
            }
            else //if fullD
            {
                Scalar localFullDColumnMassW = 0.0;
                Scalar localFullDColumnMassNW = 0.0;

                auto fullDMapColumns = modelCriterionManager_->getMapColumns();
                auto it = fullDMapColumns->lower_bound(globalColumn);

                for(; it != fullDMapColumns->upper_bound(globalColumn); it++)
                {
                    auto fvGeometryFullD = localView(*gridGeometryFullD_);
                    fvGeometryFullD.bind(it->second);

                    const auto elemSolFullD = elementSolution(it->second, (*sol_)[darcyDomainIndex], *gridGeometryFullD_);

                    for (const auto& scv : scvs(fvGeometryFullD))
                    {
                        volVarsFullD.update(elemSolFullD, problemFullD, it->second, scv);

                        localFullDColumnMassW += volVarsFullD.porosity() * volVarsFullD.density(0) * volVarsFullD.saturation(0) * scv.volume();
                        localFullDColumnMassNW += volVarsFullD.porosity() * volVarsFullD.density(1) * volVarsFullD.saturation(1) * scv.volume();
                    }
                }
                massWInColumnContainer[globalColumn] = localFullDColumnMassW;
                massNWInColumnContainer[globalColumn] = localFullDColumnMassNW;
            }
        }

        Scalar totalMassW = 0.0;
        Scalar totalMassNW = 0.0;
        for(int i=0; i<massWInColumnContainer.size(); i++)
        {
            std::cout << "For column: " << i << " column mass w is: " << massWInColumnContainer[i] << " and column mass nw is: " << massNWInColumnContainer[i] << std::endl;
            totalMassW += massWInColumnContainer[i];
            totalMassNW += massNWInColumnContainer[i];
        }

        std::cout << "Alternative in fullD, totalMassW: " << totalMassW << " and totalMassNW: " << totalMassNW << std::endl;
    }


    /*!
     * \brief Execute all necessary adaption steps: suggest new model distribtution, adapt grid, update maps, update solutions (size+values), transfer data from old grid to new grid, update couplingmanager, update assembler
     * \param darcyProblem problem of the fullD grid
     * \param darcyVerticalEquilibriumProblem problem of the coarse VE grid
     * \param gridVariablesDarcy grid variables of the fullD model
     * \param gridVariablesVE grid variables of the coarse VE model
     * \param gridVariablesVEFine grid variables of the fine VE model
     * \param gridManagerDarcy grid manager of the fullD grid
     * \param gridManagerVE grid manager of the coarse VE grid
     * \param gridManagerVEFine grid manager of the fine VE grid
     * \param fvGridGeometryVEFine grid geometry of the fine VE grid, others are already member variables of this class
     * \param gridViewDarcy grid view of the fullD grid
     * \param gridViewVE grid view of the coarse VE grid
     * \param gridViewVEFine grid view of the fine VE grid
     * \param solVEFine solution vector of the fine VE model
     * \param oldSol container for storing old solution
     * \param assembler the assembler object - should be passed as a pointer
     * \param couplingManager the coupling manager - should be passed as a pointer
     */
    template<typename Assembler, typename CouplingManager>
    void adapt(Problem<darcyDomainIndex>& darcyProblem,
               Problem<darcyVEDomainIndex>& darcyVerticalEquilibriumProblem,
               std::shared_ptr<GridVariables<darcyDomainIndex>> gridVariablesDarcy,
               std::shared_ptr<GridVariables<darcyVEDomainIndex>> gridVariablesVE,
               std::shared_ptr<GridVariables<darcyVEDomainIndex>> gridVariablesVEFine,
               GridManager<darcyDomainIndex>& gridManagerDarcy,
               GridManager<darcyVEDomainIndex>& gridManagerVE,
               GridManager<darcyVEDomainIndex>& gridManagerVEFine,
               std::shared_ptr<GridGeometry<darcyVEDomainIndex>> fvGridGeometryVEFine,
               const GridView<darcyDomainIndex>& gridViewDarcy,
               const GridView<darcyVEDomainIndex>& gridViewVE,
               const GridView<darcyVEDomainIndex>& gridViewVEFine,
               SolutionVectorVE& solVEFine,
               SolutionVector& oldSol,
               Assembler assembler,
               CouplingManager couplingManager)
    {
        auto timeLoop = darcyVerticalEquilibriumProblem.getTimeLoop();

        //find maxVelocities (only for fullD, as max velocity should always be at injection boundary?)
        auto maxVelocityFullD = VerticalEquilibriumModelUtil::findMaxVelocity<Scalar, GridVariablesDarcy, GridGeometry<darcyDomainIndex>, SolutionVectorFullD, GridView<darcyDomainIndex>, VelocityOutputDarcy>(*gridVariablesDarcy, gridGeometryFullD_, (*sol_)[darcyDomainIndex], gridViewDarcy);
        double fullDPropagation = maxVelocityFullD[1]*timeLoop->timeStepSize()/porosity_; //TODO: dividing by the porosity like this is only ok, if porosity is homogenuous
        Scalar relaxationFactor = 1.6; //TODO: smart?
        int fullDCellsPropagation = std::ceil(relaxationFactor * fullDPropagation/darcyVerticalEquilibriumProblem.getDeltaX()); //TODO: rework for 3D
        solutionTravelColumns_ = fullDCellsPropagation;

        //compute the model distribution in the domain
        modelCriterionManager_->updateCriterionContainer((*sol_)[darcyDomainIndex], darcyProblem, darcyVerticalEquilibriumProblem, gridVariablesDarcy);
        this->store_(darcyProblem, darcyVerticalEquilibriumProblem);
        if constexpr(dim==2)
            this->deduceModelSwitching_(darcyVerticalEquilibriumProblem);
        else if constexpr(dim==3)
            this->deduceModelSwitchingTemp3D_(darcyProblem, darcyVerticalEquilibriumProblem);

        //optional
//         this->printCurrentMassColumns(darcyProblem, darcyVerticalEquilibriumProblem);

        //apply suggested model distribution to discretization -> update grid
        auto domainDarcyNew = [&darcyVerticalEquilibriumProblem, this](const auto& element)
        {
            int globalColumnIdx = -1;
            if constexpr(dim==2)
            {
                globalColumnIdx = (int)std::round((element.geometry().center()[0] - 0.5*deltaX_) / deltaX_); //TODO: this only holds when domain starts at 0.0/0.0!
            }
            else if constexpr(dim==3)
            {
                auto numCells = getParamFromGroup<std::vector<Scalar>>("Darcy", "Grid.Cells");
                int xCells = numCells[0];
                int idxX = (int)std::round((element.geometry().center()[0] - 0.5*deltaX_)/deltaX_);
                int idxY = (int)std::round((element.geometry().center()[1] - 0.5*deltaY_)/deltaY_);
                globalColumnIdx = idxY*xCells + idxX;
            }
            int newModel = this->shouldBeModel_(globalColumnIdx, false); //TODO: compute this only once!

            //TODO: make sure to catch all possible cases, what if newModel is not 0 and not 1?
            bool shouldBeFullD{};
            if(newModel == 0)
                shouldBeFullD = false;
            else if(newModel == 1)
                shouldBeFullD = true;
            else
                 throw std::invalid_argument("The variable shouldBeFullD needs to be either 0 or 1!");

            return shouldBeFullD;
        };

        auto elementSelectorDarcyNew = [&domainDarcyNew](const auto& element)
        {
            return domainDarcyNew(element);
        };

        auto elementSelectorVEComplementNew = [&domainDarcyNew](const auto& element)
        {
            return !domainDarcyNew(element);
        };

        //----------updating functions after grid adapt start here-------------------------------------------------------------------
        //update the sub-grid using the new element selector
        gridManagerDarcy.update(elementSelectorDarcyNew);
        gridManagerVE.update(elementSelectorVEComplementNew);
        gridManagerVEFine.update(elementSelectorVEComplementNew);

        //update the grid geometries using the automatically updated grid view (because they are references)
        gridGeometryFullD_->update(gridViewDarcy);
        gridGeometryVE_->update(gridViewVE);
        fvGridGeometryVEFine->update(gridViewVEFine);

        //optional
//         VerticalEquilibriumModelUtil::printGeom(*gridGeometryFullD_, "DarcyFullD");
//         VerticalEquilibriumModelUtil::printGeom(*gridGeometryVE_, "VE");
//         VerticalEquilibriumModelUtil::printGeom(*fvGridGeometryVEFine, "VE_fine");

        //update column maps (coarse to fine and fine to coarse for both fullD and VE)
        darcyVerticalEquilibriumProblem.updateVEMapCoarseToFine(fvGridGeometryVEFine, gridGeometryVE_);
        darcyVerticalEquilibriumProblem.updateVEMapFineToCoarse(fvGridGeometryVEFine, gridGeometryVE_);
        modelCriterionManager_->updateFullDMapCoarseToFine(gridGeometryFullD_);
        modelCriterionManager_->updateFullDMapFineToCoarse(gridGeometryFullD_);

        //update the quantityStorage -> update sizes of containers
        darcyVerticalEquilibriumProblem.updateQuantityStorage(gridGeometryVE_, fvGridGeometryVEFine);

        //update coarse-scale permeability and porosity, NEEDS to be called before applyInitialSolution if using that function
        darcyVerticalEquilibriumProblem.spatialParams().calcSpatialParamsPermeabilityAndPorosityCoarse();

        //store current solution vectore before resizing them to fit the geometry post adapt
        this->backupSolsBeforeAdapt_(sol_);

        //resize the solution vectors to the new number of DOFs
        (*sol_)[darcyDomainIndex].resize(gridGeometryFullD_->numDofs());
        (*sol_)[darcyVEDomainIndex].resize(gridGeometryVE_->numDofs());
        solVEFine.resize(fvGridGeometryVEFine->numDofs());

        //transfer non-adapted element solution to their new indices
        this->updateNonAdaptedSolutionIndices_();

        //transfer solution between models
        this->applyStoredSolutionToAdaptedGrid_(darcyProblem, darcyVerticalEquilibriumProblem, *gridGeometryFullD_, false);
        // re-initiliaze fine-scale solution vector after gird adaption. This might be necessary to avoid uninitialized solution entries in the fine-scale solution vector, especially when the vector is larger than before after adaption.
        darcyVerticalEquilibriumProblem.getOtherProblem()->updateFineSol(); //the only place fineSol is potentially used in is in util.hh for printing the mass balance

        //updating oldSol is necessary for the newton solver
        oldSol = *sol_;

        //update the grid variables after grid adaptation
        gridVariablesDarcy->updateAfterGridAdaption((*sol_)[darcyDomainIndex]);
        gridVariablesVE->updateAfterGridAdaption((*sol_)[darcyVEDomainIndex]);
        gridVariablesVEFine->updateAfterGridAdaption(solVEFine);

        //update couplingmap in couplingmanager
        couplingManager->update(*sol_);

        //update assembler
        assembler->updateAfterGridAdaption();

        //optional
//         this->printCurrentMassColumns(darcyProblem, darcyVerticalEquilibriumProblem);

        //optional
//         VerticalEquilibriumModelUtil::printGeom(*gridGeometryFullD_, "DarcyFullD");
//         VerticalEquilibriumModelUtil::printGeom(*gridGeometryVE_, "VE");
//         VerticalEquilibriumModelUtil::printGeom(*fvGridGeometryVEFine, "VE_fine");
    }

private:

    /*!
     * \brief Store the current grid solution (should be called before adapting the grid) within an appropriate container
     *
     * \param problemFullD problem of the fullD grid
     * \param problemCoarseVE problem of the VE grid
     */
    void store_(const Problem<darcyDomainIndex>& problemFullD,
                const Problem<darcyVEDomainIndex>& problemCoarseVE)
    {
        dataTransferContainer_.clear();
        dataTransferContainer_.resize(numberColumns_);

        std::vector<std::vector<Scalar>> fineDElementHeightsTmp{};
        fineDElementHeightsTmp.resize(numberColumns_);

        //iterate over fullD elements
        for (const auto& element : elements(gridGeometryFullD_->gridView()))
        {
            size_t currentFullDElementIdx = gridGeometryFullD_->elementMapper().index(element);

            const auto range = modelCriterionManager_->getMapColumnsReverse()->equal_range(currentFullDElementIdx);
            auto iterator = range.first; //should only contain one entry for fullD map

            auto solutionFullD = (*sol_)[darcyDomainIndex];
            const auto elemSolFullD = elementSolution(element, solutionFullD, *gridGeometryFullD_);

            std::array<Scalar, 2> localSol{0.0, 0.0}; //2 refers to the number of priVars
            for(int i=0; i<localSol.size(); i++)
            {
                localSol[i] = elemSolFullD[0][i];
            }

            dataTransferContainer_[iterator->second].push_back(localSol);
            //fullD vertical elements should be already ordered
            fineDElementHeightsTmp[iterator->second].push_back(element.geometry().center()[1]);
        }
        //first sort the heights in incresing order and then sort the local element solution with the same permutations -> guarantee that first element is the lowest one
        zipSort_(fineDElementHeightsTmp,dataTransferContainer_);
        //make sure that for each fullD column a bottom element was found which is crucial for computing a virtual coarse-scale VE pressure
        wasBottomElementFound_(fineDElementHeightsTmp);

        //iterate over VE elements
        for (const auto& element : elements(gridGeometryVE_->gridView()))
        {
            //be careful to store the global columnIdx and not only that of the VEgrid
            const auto currentColumnPos = element.geometry().center();

            int currentColumnIdx = -1;
            if constexpr(dim==2)
                currentColumnIdx = (int)std::round((currentColumnPos[0] - 0.5*deltaX_)/deltaX_);
            else if constexpr(dim==3)
            {
                auto numCells = getParamFromGroup<std::vector<Scalar>>("Darcy", "Grid.Cells");
                int xCells = numCells[0];
                int idxX = (int)std::round((currentColumnPos[0] - 0.5*deltaX_)/deltaX_);
                int idxY = (int)std::round((currentColumnPos[1] - 0.5*deltaY_)/deltaY_);
                currentColumnIdx = idxY*xCells + idxX;
            }

            auto solutionFullVE = (*sol_)[darcyVEDomainIndex];
            const auto elemSolFullVE = elementSolution(element, solutionFullVE, *gridGeometryVE_);

            std::array<Scalar, 2> localSolVE{0.0, 0.0}; //2 refers to the number of priVars
            for(int i=0; i<localSolVE.size(); i++)
            {
                localSolVE[i] = elemSolFullVE[0][i];
            }

            dataTransferContainer_[currentColumnIdx].push_back(localSolVE);
        }

        this->updateGlobalToLocalIndexBeforeAdapt_(false);
        //massNw is necessary for applyStoredSolutionToAdaptedGrid_ for computing a conservative saturation
        this->computeColumnMassesBeforeAdapt_(problemFullD, problemCoarseVE, false);
    }



    /*!
     * \brief Store the model solutions. Should be called before adapting the grid.
     * \param sol total solution vector
     */
    void backupSolsBeforeAdapt_(const std::shared_ptr<SolutionVector> sol)
    {
        this->updateGlobalToLocalIndexAfterAdapt_();
        auto totalSolCopy = *sol;
        auto solVE = totalSolCopy[darcyVEDomainIndex];
        auto solFullD = totalSolCopy[darcyDomainIndex];

        solVEBeforeAdapt_ = solVE;
        solFullDBeforeAdapt_ = solFullD;
    }


    /*!
     * \brief Update the local indices of element which do not participate in being adapted. The local indices might change from before to after adaption.
     * \param verbose flag if verbose output should be enabled
     */
    void updateNonAdaptedSolutionIndices_(bool verbose = false)
    {
        //overwriting already used indices should not be an issue, as we use solVEBeforeAdapt_ and solFullDBeforeAdapt_ which are copies from before adaption
        auto mySwitchIndicator = mySwitchIndicator_;

        //iterate over the global column indices
        for(int columnIdx=0; columnIdx < mySwitchIndicator.size(); columnIdx++)
        {
            //check if before adapt column was VE
            if(mySwitchIndicator[columnIdx][1] == 0)
            {
                if(mySwitchIndicator[columnIdx][0] == 0)
                {
                    auto newCoarseIndices = globalToLocalIndexAfterAdapt_[columnIdx][0];
                    auto oldCoarseIndices = globalToLocalIndexBeforeAdapt_[columnIdx][0];

                    if(verbose)
                    {
                        std::cout << "For VE. Global column: " << columnIdx << ". Mapping old idx: " << oldCoarseIndices << " to new index: " << newCoarseIndices << std::endl;
                    }

                    (*sol_)[darcyVEDomainIndex][newCoarseIndices][0] = solVEBeforeAdapt_[oldCoarseIndices][0];
                    (*sol_)[darcyVEDomainIndex][newCoarseIndices][1] = solVEBeforeAdapt_[oldCoarseIndices][1];
                }
                if(mySwitchIndicator[columnIdx][0] == 1)
                {
                    for(int i=0; i<globalToLocalIndexAfterAdapt_[columnIdx].size(); i++)
                    {
                        auto newFullDIndices = globalToLocalIndexAfterAdapt_[columnIdx][i];
                        auto oldFullDIndices = globalToLocalIndexBeforeAdapt_[columnIdx][i];
                        if(verbose)
                        {
                            std::cout << "For fullD. Global column: " << columnIdx << ". Mapping old idx: " << oldFullDIndices << " to new index: " << newFullDIndices << std::endl;
                        }

                        (*sol_)[darcyDomainIndex][newFullDIndices][0] = solFullDBeforeAdapt_[oldFullDIndices][0];
                        (*sol_)[darcyDomainIndex][newFullDIndices][1] = solFullDBeforeAdapt_[oldFullDIndices][1];
                    }
                }
                //TODO: make sure that each and every entry of the new sol has been touched
            }
        }
    }


    /*!
     * \brief Transfer stored data to newly adapted grid if necessary
     *
     * \param problemFullD reference to the problem object of the fullD model
     * \param problemCoarseVE reference to the problem object of the coarse VE model
     * \param fvGridGeometryDarcy ???needed???
     * \param verbose flag if verbose output should be enabled
     */
    void applyStoredSolutionToAdaptedGrid_(const Problem<darcyDomainIndex>& problemFullD,
                                           const Problem<darcyVEDomainIndex>& problemCoarseVE,
                                           const GridGeometry<darcyDomainIndex>& fvGridGeometryDarcy,
                                           bool verbose = false)
    {
        auto mySwitchIndicator = mySwitchIndicator_;
        auto fullDMapColumns = modelCriterionManager_->getMapColumns();

        auto dummyPos = problemCoarseVE.gridGeometry().bBoxMin(); //this is only valid if temperature, gravitation and porosity are independent of the position!

        //iterate over the global column indices
        for(int globalColumnIdx=0; globalColumnIdx < mySwitchIndicator.size(); globalColumnIdx++)
        {
            //check if before adapt column was VE
            if(mySwitchIndicator[globalColumnIdx][0] == 0)
            {
                //catch error if current columnModel is VE and has more than one entry
                if(dataTransferContainer_[globalColumnIdx].size() != 1)
                {
                    std::ostringstream errMsg;
                    errMsg << "dataTransferContainer_ at column: " << globalColumnIdx << " should only have exactly one entry for a previous VE coarse element, but it has: " << dataTransferContainer_[globalColumnIdx].size() << "!";
                    throw std::invalid_argument(errMsg.str());
                }

                //check if before adapt column was marked for model switch (VE->fullD in this column)
                if(mySwitchIndicator[globalColumnIdx][1] == 1)
                {
                    //compute reconstructed pressureW and saturationNw

                    //obtain coarse VE quantities from before adapt stored VE quantities
                    auto localStoredCoarsePW = dataTransferContainer_[globalColumnIdx][0][0]; //work with pressureIdx instead of hard coded 0?
                    auto localStoredCoarseSatW = 1.0 - dataTransferContainer_[globalColumnIdx][0][1]; //work with pressureIdx instead of hard coded 0?

                    //obtain necessary secondary variables
                    auto temperature = problemCoarseVE.spatialParams().temperatureAtPos(dummyPos);
                    auto coarseDensityW = problemCoarseVE.returnDensityLiquidAndViscosity(temperature, localStoredCoarsePW)[0];
                    auto coarseDensityNw = problemCoarseVE.returnDensityGasAndViscosity(temperature, localStoredCoarsePW)[0];
                    const auto fluidMatrixInteraction = problemCoarseVE.spatialParams().fluidMatrixInteractionAtPos(dummyPos);
                    const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
                    const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
                    const Scalar lambda = problemCoarseVE.spatialParams().getBrooksCoreyLambda();
                    const Scalar entryP = problemCoarseVE.spatialParams().getBrooksCoreyEntryP();
                    const Scalar gravityNorm = problemCoarseVE.spatialParams().gravity(dummyPos).two_norm();
                    const Scalar domainHeight = problemCoarseVE.gridGeometry().bBoxMax()[dim-1] - problemCoarseVE.gridGeometry().bBoxMin()[dim-1];
                    const Scalar deltaZ = deltaZ_;

                    auto localCoarseVEElementIdx = globalToLocalIndexBeforeAdapt_[globalColumnIdx][0];
                    Scalar storedMinGasPlumeDist = problemCoarseVE.getQuantityStorage()->getMinGasPlumeDist(localCoarseVEElementIdx); //TODO: check if this exists before/after adaption

                    Scalar gasPlumeDistThisColumn = problemCoarseVE.getQuantityReconstructor()->computeGasPlumeDist
                        (std::make_tuple(coarseDensityW,coarseDensityNw),
                        std::make_tuple(resSatW,resSatNw),
                        gravityNorm,
                        domainHeight,
                        localStoredCoarseSatW,
                        std::make_tuple(lambda,entryP));

                    storedMinGasPlumeDist = gasPlumeDistThisColumn; //TODO: this is only correct without hysteresis!

                    auto tmpColumnIdx = globalColumnIdx;

                    //CARAFEUL, THIS IS SUPPOSED TO BE THE NEW MAP AFTER ADAPTION
                    //get iterator which iterates over the fullD elements which belong to on fullD column
                    const auto range = fullDMapColumns->equal_range(tmpColumnIdx); //should be the udpated version of the map after grid adaptation!
                    for(auto iterator = range.first; iterator!=range.second; iterator++)
                    {
                        auto fineElementPos = (iterator->second).geometry().center();

                        //reconstruct fine-scale pressures
                        std::vector<Scalar> reconstructedPressures = problemCoarseVE.getQuantityReconstructor()->reconstPressure
                            (gasPlumeDistThisColumn,
                            std::make_tuple(coarseDensityW,coarseDensityNw),
                            gravityNorm,
                            fineElementPos[dim-1],
                            localStoredCoarsePW,
                            entryP);

                        //reconstruct fine-scale saturations
                        Scalar reconstructedSaturationW = problemCoarseVE.getQuantityReconstructor()->computeSaturationIntegral
                            (std::make_tuple(gasPlumeDistThisColumn,storedMinGasPlumeDist),
                            std::make_tuple(coarseDensityW,coarseDensityNw),
                            std::make_tuple(resSatW,resSatNw),
                            gravityNorm,
                            fineElementPos[dim-1],
                            deltaZ,
                            std::make_tuple(lambda,entryP));

                        Scalar reconstructedSaturationNw = 1.0 - reconstructedSaturationW;

                        Scalar fullDElementVol = -1; 
                        if constexpr(dim==2)
                            fullDElementVol = deltaX_*deltaZ_; //TODO: times extrusionFactor?
                        else if constexpr(dim==3)
                            fullDElementVol = deltaX_*deltaY_*deltaZ_; //TODO: times extrusionFactor?
                        Scalar fullDPorosity = problemFullD.spatialParams().porosityAtPos(dummyPos);
                        Scalar massGasToConserve = fullDElementVol * fullDPorosity * reconstructedSaturationNw * coarseDensityNw;
                        Scalar computedConservativeSatNw = 0.0;
                        //compute mass conservative satNw via a fix-point iteration
                        if(massGasToConserve!=0) //if zero, newton solver will not converge
                        {
                            computedConservativeSatNw = deduceConservativeSwFromVEToFullD_(massGasToConserve, problemCoarseVE, reconstructedPressures[0], reconstructedSaturationNw, fullDElementVol, fullDPorosity);
                        }

                        const auto newFullDElementIdx = problemFullD.gridGeometry().elementMapper().index(iterator->second);

                        //fill solution vector with reconstructed values
                        (*sol_)[darcyDomainIndex][newFullDElementIdx][0] = reconstructedPressures[0];
                        (*sol_)[darcyDomainIndex][newFullDElementIdx][1] = computedConservativeSatNw;

                        if(verbose)
                        {
                            std::cout << "Converting VE to fullD. At pos: " << fineElementPos << " we obtain sW: " << computedConservativeSatNw << " and pw: " << reconstructedPressures[0] << std::endl;
                        }
                    }
                }
            }
            else if(mySwitchIndicator[globalColumnIdx][0] == 1) //check if before adapt column was fullD
            {
                //check if before adapt column was marked for model switch (fullD->VE in this column)
                if(mySwitchIndicator[globalColumnIdx][1] == 1)
                {
                    //obtain necessary (secondary) variables
                    auto gravityNorm = problemFullD.spatialParams().gravity(dummyPos).two_norm();

                    Scalar virtualCoarsePW = 0.0;

                    //iterator over globalColumnIdx and compute satW mean as well as pW at the bottom
                    for(int currentTransferLocalElemIdx=0; currentTransferLocalElemIdx<dataTransferContainer_[globalColumnIdx].size(); currentTransferLocalElemIdx++)
                    {
                        auto localStoredFullDPW = dataTransferContainer_[globalColumnIdx][currentTransferLocalElemIdx][0]; //work with pressureIdx instead of hard coded 0?

                        if(currentTransferLocalElemIdx == 0)
                        {
                            //only for 0th entry
                            auto temperature = problemFullD.spatialParams().temperatureAtPos(dummyPos);
                            auto fullDDensityW = problemCoarseVE.returnDensityLiquidAndViscosity(temperature, localStoredFullDPW)[0];
                            virtualCoarsePW = localStoredFullDPW + fullDDensityW*gravityNorm*(deltaZ_/2.0);

                            if(verbose)
                            {
                                std::cout << "Should be in bottom cell of column: " << globalColumnIdx << ", with own pW: " << localStoredFullDPW << " and pW at bottom: " << virtualCoarsePW << std::endl;
                            }
                        }
                    }

                    auto newCoarseVEElementIdx = globalToLocalIndexAfterAdapt_[globalColumnIdx][0]; //maybe use after version here?

                    //compute satNw which conserves mass of column
                    auto conservativeSatNw = computeConservativeSatNW_(massNWInColumnContainer_[globalColumnIdx], virtualCoarsePW, newCoarseVEElementIdx, problemCoarseVE);

                    if(verbose)
                    {
                        std::cout << "conservativeSatNw: " << conservativeSatNw << " in column: " << globalColumnIdx << std::endl;
                    }

                    //fill solution vector with corresponding coarse-scale values
                    (*sol_)[darcyVEDomainIndex][newCoarseVEElementIdx][0] = virtualCoarsePW;
                    (*sol_)[darcyVEDomainIndex][newCoarseVEElementIdx][1] = conservativeSatNw;
                }
            }
        }
    }


    /*!
     * \brief Return which model should be used at the provided global column index.
     * \param columnIdx global column index
     * \param verbose flag if verbose output should be enabled
     */
    int shouldBeModel_(const int columnIdx,
                       bool verbose = false) const
    {
        //should be called after calling store() and during element selector
        if(verbose)
        {
            for(int i = 0; i<mySwitchIndicator_.size(); i++)
            {
                if(verbose)
                {
                    std::cout << "in shouldBeModel mySwitchIndicator_ at global index: " << i << " and mySwitchIndicator_[i][0]: " << mySwitchIndicator_[i][0] << " - " << mySwitchIndicator_[i][1] << std::endl;
                }
            }
        }

        int modelToUse = -1;
        if(mySwitchIndicator_[columnIdx][0] == 0) //if was VE
        {
            if(mySwitchIndicator_[columnIdx][1] == false) //if not marked for switching
            {
                modelToUse = 0; //stay VE
            }
            else
            {
                modelToUse = 1; //switch to fullD
            }
        }
        else if(mySwitchIndicator_[columnIdx][0] == 1) //if was fullD
        {
            if(mySwitchIndicator_[columnIdx][1] == false) //if not marked for switching
            {
                modelToUse = 1; //stay fullD
            }
            else
            {
                modelToUse = 0; //switch to VE
            }
        }

        //catch error if no valid model was assigned to a columnIdx
        if(modelToUse == -1)
        {
            std::ostringstream errMsg;
            errMsg << "For columnIdx: " << columnIdx << " no proper model to be used was defined!";
            throw std::invalid_argument(errMsg.str());
        }

        return modelToUse;
    }



    /*!
     * \brief First sort a container firstToSort and then sort sortLikeOther in the same permutations. This is helpful, so we exactly know that the first stored element is also the one on the bottom and the next ones stack on top of each other. Therefore, no extra loop over the elements is necessary.
     *
     * \param firstToSort reference to the container that should be sorted
     * \param sortLikeOther reference to the container that should be sorted through the same permutations as firstToSort
     * \param verbose flag if verbose output should be enabled
     */
    void zipSort_(std::vector<std::vector<Scalar>>& firstToSort,
                  DataTransferContainer& sortLikeOther,
                  bool verbose = false)
    {
        //catch error if firstToSort and sortLikeOther have different sizes
        if(firstToSort.size() != sortLikeOther.size())
        {
            std::ostringstream errMsg;
            errMsg << "firstToSort and sortLikeOther need to have the same size!";
            throw std::invalid_argument(errMsg.str());
        }

        if(verbose == true)
        {
            for(int i=0; i<sortLikeOther.size(); i++)
            {
                auto localDataContainer = sortLikeOther[i];
                std::cout << "sortLikeOther BEFORE at index " << i << ": ";
                for(int j=0; j < localDataContainer.size(); j++)
                {
                    std::cout << localDataContainer[j] << " ";
                }
                std::cout << std::endl;
            }

            for(int i=0; i<firstToSort.size(); i++)
            {
                auto localDataContainer = firstToSort[i];
                std::cout << "firstToSort BEFORE at index " << i << ": ";
                for(int j=0; j < localDataContainer.size(); j++)
                {
                    std::cout << localDataContainer[j] << " ";
                }
                std::cout << std::endl;
            }
        }

        std::vector<std::vector<Scalar>> tmp0{};
        tmp0.resize(firstToSort.size());
        DataTransferContainer tmp1{};
        tmp1.resize(sortLikeOther.size());

        for(int i = 0; i < sortLikeOther.size(); i++)
        {
            //catch error if firstToSort[i] and sortLikeOther[i] have different sizes i.e. a different number of elements is stored
            if(firstToSort[i].size() != sortLikeOther[i].size())
            {
                std::ostringstream errMsg;
                errMsg << "firstToSort and sortLikeOther need to have the same size/number of elements at global column: " << i << "!";
                throw std::invalid_argument(errMsg.str());
            }

            //only sort when there is more than one stored element i.e. a fullD column
            if(sortLikeOther[i].size()>1)
            {
                //create container for storing index permutation
                std::vector<size_t> sortingIndices(firstToSort[i].size());
                std::iota(sortingIndices.begin(), sortingIndices.end(), 0);
                //change initially sorted indices to a permuation of the then sorted container firstToSort[i]
                std::sort(sortingIndices.begin(), sortingIndices.end(), [&](size_t a, size_t b) -> bool { return firstToSort[i][a] < firstToSort[i][b];});

                //prepare sorting of the containers according to the index permutation
                for(size_t index : sortingIndices)
                {
                    tmp1[i].push_back(sortLikeOther[i][index]);
                    tmp0[i].push_back(firstToSort[i][index]);
                }

                //sort the containers
                firstToSort[i] = tmp0[i];
                sortLikeOther[i] = tmp1[i];
            }
        }

        if(verbose == true)
        {
            for(int i=0; i<firstToSort.size(); i++)
            {
                auto localDataContainer = firstToSort[i];
                std::cout << "firstToSort AFTER at index " << i << ": ";
                for(int j=0; j < localDataContainer.size(); j++)
                {
                    std::cout << localDataContainer[j] << " ";
                }
                std::cout << std::endl;
            }

            for(int i=0; i<sortLikeOther.size(); i++)
            {
                auto localDataContainer = sortLikeOther[i];
                std::cout << "sortLikeOther AFTER at index " << i << ": ";
                for(int j=0; j < localDataContainer.size(); j++)
                {
                    std::cout << localDataContainer[j] << " ";
                }
                std::cout << std::endl;
            }
        }
    }


    /*!
     * \brief Check for consistency: is the expected and the stored z-position of a bottom fullD element the same?
     *
     * \param firstToSort reference to the container that should be sorted
     * \param verbose flag if verbose output should be enabled
     */
    void wasBottomElementFound_(const std::vector<std::vector<Scalar>>& firstToSort,
                                bool verbose = false) const
    {
        //compute expected element center height of lowest fullD element in a fullD column
        Scalar targetBottomFullDElemZPos = lowerDomainBoundaryHeight_ + deltaZ_/2.0;
        for(int i=0; i<firstToSort.size(); i++)
        {
            auto localDataContainer = firstToSort[i];

            //only check height consistency when there is more than one stored element i.e. a fullD column
            if(localDataContainer.size()>1)
            {
                //are expected and stored z-position the same?
                bool fuzzyEqualBottom = (localDataContainer[0] <= targetBottomFullDElemZPos + eps_) || (localDataContainer[0] >= targetBottomFullDElemZPos - eps_);

                if(verbose)
                {
                    std::cout << "At column: " << i << " localDataContainer[0]: " << localDataContainer[0] << " while targetBottomFullDElemZPos: " << targetBottomFullDElemZPos << std::endl;
                }

                //localDataContainer[0] should always contain the lowest fullD element z-position as firstToSort should have been sorted via zipSort_ at this point
                if(!fuzzyEqualBottom)
                {
                    std::ostringstream errMsg;
                    errMsg << "At column: " << i << " no fineD element closest to the bottom boundary of the domain was found!";
                    throw std::invalid_argument(errMsg.str());
                }
            }
        }
    }


    /*!
     * \brief Compute and return a container with a size of the number of total columns in the domain. For each column, the current model is stored as well as a boolean if this column needs to change models or not.
     *
     * \param verbose optional argument that decides whether to print information.
     */
    void computeModelSwitchContainer_(bool verbose = false)
    {
        mySwitchIndicator_.clear();
        mySwitchIndicator_.resize(numberColumns_);

        if(verbose)
        {
            std::cout << "dataTransferContainer_.size(): " << dataTransferContainer_.size() << std::endl;
            for(int i=0; i<dataTransferContainer_.size(); i++)
            {
                std::cout << "At column: " << i << " the current sol size is: " << dataTransferContainer_[i].size() << std::endl;
            }
        }

        auto criterionVector = modelCriterionManager_->returnCriterionVector();
        std::vector<std::array<bool,2>> criterionManagerContainer{};
        Scalar epsThreshold = modelCriterionManager_->getEpsThreshold();
        bool currentModel = 1; //0=VE, 1=fullD
        bool shouldModelsSwitch = 0;

        //check for consistent container sizes
        if(criterionVector.size() != dataTransferContainer_.size())
        {
            throw std::invalid_argument("dataTransferContainer_ and criterionVector must have the same size!");
        }

        for(int i = 0; i<criterionVector.size(); i++)
        {
            std::array<bool,2> tmpSingleColumnContainer{};

            if(verbose)
            {
                std::cout << "For i: " << i << " dataTransferContainer_[i].size(): " << dataTransferContainer_[i].size() << std::endl;
            }

            //need to differentiate if columns are VE or fullD, as each model has its individual switch criterion which is independent of the other model's criterion
            //enter if current model is VE
            if(dataTransferContainer_[i].size()==1)
            {
                currentModel = 0;

                //the function updateSwAndMassNwContainer(...) needs to be called before this at some point during the adapt function
                std::vector<Scalar> massNwDerivative =  modelCriterionManager_->returnMassNWDerivativeContainer();

                bool atTipOfPlume = (*persistentMassNwContainer_)[i]<0.5 && (*persistentMassNwContainer_)[i]>1e-12; //TODO: only works if only injection process is present
                bool criterionMet = massNwDerivative[i]<3.0 && criterionVector[i]<0.0024;
                bool shouldStayVE = !atTipOfPlume && criterionMet;
                bool shouldSwitchToFullD = !shouldStayVE;

                if(verbose)
                {
                    std::cout << "at globalColumm: " << i << " we have VE. CritValue is: " << criterionVector[i] << " and massNw: " << (*persistentMassNwContainer_)[i] << " and massNwDerivative: " << massNwDerivative[i] << ". Should it be switched to fullD?: " << shouldSwitchToFullD << ", !atTipOfPlume: " << !atTipOfPlume << ", criterionMet:  " << criterionMet << ", shouldStayVE: " << shouldStayVE << std::endl;
                }

                if(shouldSwitchToFullD)
                {
                    shouldModelsSwitch = true;
                    tmpSingleColumnContainer = {currentModel, shouldModelsSwitch};
                    criterionManagerContainer.push_back(tmpSingleColumnContainer);
                }
                else //if NO model change is needed
                {
                    shouldModelsSwitch = false;
                    tmpSingleColumnContainer = {currentModel, shouldModelsSwitch};
                    criterionManagerContainer.push_back(tmpSingleColumnContainer);
                }
            }
            else //enter if current model is fullD
            {
                currentModel = 1;

                Scalar minSaturationNw = 0.025;
                auto satWContainer = modelCriterionManager_->returnFullDSatNwContainer();
                bool isSmallerThanThreshold = (!std::isnan(criterionVector[i])) && (criterionVector[i] < epsThreshold);
                bool isLargerThanMinSaturation = (satWContainer[i] > minSaturationNw);
                bool shouldSwitchToVE = (isSmallerThanThreshold && isLargerThanMinSaturation) || satWContainer[i]<1e-8;

                if(verbose)
                {
                    std::cout << "at globalColumm: " << i << " we have fullD. CritValue is: " << criterionVector[i] << " and conservative virtual satNW is: " << satWContainer[i]  << ", isSmallerThanThreshold: " << isSmallerThanThreshold << ", isLargerThanMinSaturation: " << isLargerThanMinSaturation << ". Should it be switched to VE?: " << shouldSwitchToVE << std::endl;
                }

                //if model change is needed
                if(shouldSwitchToVE)
                {
                    shouldModelsSwitch = true;
                    tmpSingleColumnContainer = {currentModel, shouldModelsSwitch};
                    criterionManagerContainer.push_back(tmpSingleColumnContainer);
                }
                else //if NO model change is needed
                {
                    shouldModelsSwitch = false;
                    tmpSingleColumnContainer = {currentModel, shouldModelsSwitch};
                    criterionManagerContainer.push_back(tmpSingleColumnContainer);
                }
            }
        }

        if(verbose)
        {
            for(int i=0; i < criterionManagerContainer.size(); i++)
            {
                std::cout << "At column i: " << i << " the current model is: " << criterionManagerContainer[i][0] << " , should it be switched?: " << criterionManagerContainer[i][1] << std::endl;
            }
        }

        mySwitchIndicator_ = criterionManagerContainer;
    };


    /*!
     * \brief Compute and store the phase masses currently available within each column
     * \param problemFullD problem of the fullD grid
     * \param problemCoarseVE problem of the VE grid
     * \param verbose flag if verbose output should be enabled
     */
    void computeColumnMassesBeforeAdapt_(const Problem<darcyDomainIndex>& problemFullD,
                                         const Problem<darcyVEDomainIndex>& problemCoarseVE,
                                         bool verbose = false)
    {
        //massNWInColumnContainer_ is necessary as input for computeConservativeSatNW
        massWInColumnContainer_.clear();
        massNWInColumnContainer_.clear();
        massWInColumnContainer_.resize(numberColumns_);
        massNWInColumnContainer_.resize(numberColumns_);

        VolumeVariablesFullD volVarsFullD;
        VolumeVariablesVE volVarsVE;

        for(int globalColumn=0; globalColumn<numberColumns_; globalColumn++)
        {
            //if VE
            if(dataTransferContainer_[globalColumn].size() == 1)
            {
                auto localVEIndex = globalToLocalIndexBeforeAdapt_[globalColumn][0]; //as it is VE it should only have one entry
                auto localVEElement = gridGeometryVE_->element(localVEIndex);

                Scalar localVEColumnMassW = 0.0;
                Scalar localVEColumnMassNW = 0.0;

                auto fvGeometryVE = localView(*gridGeometryVE_);
                fvGeometryVE.bind(localVEElement);
                const auto elemSolVE = elementSolution(localVEElement, (*sol_)[darcyVEDomainIndex], *gridGeometryVE_);

                for (const auto& scv : scvs(fvGeometryVE))
                {
                    volVarsVE.update(elemSolVE, problemCoarseVE, localVEElement, scv);

                    //should not use upscaled version of porosity, correct?
                    Scalar normalizedPorosity = volVarsVE.porosity()/(upperDomainBoundaryHeight_-lowerDomainBoundaryHeight_);

                    //strictly, using scv.volume() of the whole VE column is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
                    localVEColumnMassW += normalizedPorosity * volVarsVE.density(0) * volVarsVE.saturation(0) * scv.volume();
                    localVEColumnMassNW += normalizedPorosity * volVarsVE.density(1) * volVarsVE.saturation(1) * scv.volume();
                }

                massWInColumnContainer_[globalColumn] = localVEColumnMassW;
                massNWInColumnContainer_[globalColumn] = localVEColumnMassNW;
            }
            else //if fullD
            {
                Scalar localFullDColumnMassW = 0.0;
                Scalar localFullDColumnMassNW = 0.0;

                auto fullDMapColumns = modelCriterionManager_->getMapColumns();
                auto it = fullDMapColumns->lower_bound(globalColumn);

                for(; it != fullDMapColumns->upper_bound(globalColumn); it++)
                {
                    auto fvGeometryFullD = localView(*gridGeometryFullD_);
                    fvGeometryFullD.bind(it->second);

                    const auto elemSolFullD = elementSolution(it->second, (*sol_)[darcyDomainIndex], *gridGeometryFullD_);

                    for (const auto& scv : scvs(fvGeometryFullD))
                    {
                        volVarsFullD.update(elemSolFullD, problemFullD, it->second, scv);

                        localFullDColumnMassW += volVarsFullD.porosity() * volVarsFullD.density(0) * volVarsFullD.saturation(0) * scv.volume();
                        localFullDColumnMassNW += volVarsFullD.porosity() * volVarsFullD.density(1) * volVarsFullD.saturation(1) * scv.volume();
                    }
                }
                massWInColumnContainer_[globalColumn] = localFullDColumnMassW;
                massNWInColumnContainer_[globalColumn] = localFullDColumnMassNW;
            }
        }

        Scalar totalMassW = 0.0;
        Scalar totalMassNW = 0.0;
        for(int i=0; i<massWInColumnContainer_.size(); i++)
        {
            if(verbose)
            {
                std::cout << "For column: " << i << " column mass w is: " << massWInColumnContainer_[i] << " and column mass nw is: " << massNWInColumnContainer_[i] << std::endl;
            }
            totalMassW += massWInColumnContainer_[i];
            totalMassNW += massNWInColumnContainer_[i];
        }

        if(verbose)
        {
            std::cout << "Alternative in fullD, totalMassW: " << totalMassW << " and totalMassNW: " << totalMassNW << std::endl;
        }
    }


    /*!
     * \brief Given a reconstructed fine-scale element and its mass to conserve, compute the non-wetting phase saturation of the corresponding fullD element. As the the capillary pressure for the fullD model is computed slightly differently than when reconstructing the fine-scale non-wetting phase pressure, one hase to solve a fix-point equation to obtain a conservative non-wetting phase fullD saturation such that the mass conservation is fulfilled when checked via using volume variables (volVars)
     *
     * \param massToBeConserved non-wetting phase mass of the fine-scale VE element to conserve
     * \param problemCoarseVE problem of the VE grid
     * \param pw wetting-phase pressure on the reconstructed fine-scale VE scale
     * \param initSatNw initial guess for the non-wetting phase saturation, computed as the reconstrcuted fine-scale non-wetting phaes saturation
     * \param scvVolume volume of the fullD scv/element
     * \param porosity porosity of the fullD scv/element
     * \param verbose flag if verbose output should be enabled
     */
    Scalar deduceConservativeSwFromVEToFullD_(const Scalar& massToBeConserved,
                                              const Problem<darcyVEDomainIndex>& problemCoarseVE,
                                              const Scalar& pw,
                                              const Scalar& initSatNw,
                                              const Scalar& scvVolume,
                                              const Scalar& porosity,
                                              bool verbose = false) const
    {
        auto dummyPos = problemCoarseVE.gridGeometry().bBoxMin(); //this is only valid if temperature, gravitation and porosity are independent of the position!

        Scalar temperature = problemCoarseVE.spatialParams().temperatureAtPos(dummyPos);
        const auto fluidMatrixInteraction = problemCoarseVE.spatialParams().fluidMatrixInteractionAtPos(dummyPos);

        //define function to find root of
        const std::function<double (double)> massConservationDiff = [&](double conservativeSatNw)
        {
            Scalar newFixedPnw = pw + fluidMatrixInteraction.pc(1.0-conservativeSatNw);
            //the target function is the mass (difference)
            Scalar targetFunc =  conservativeSatNw*problemCoarseVE.returnDensityGasAndViscosity(temperature, newFixedPnw)[0] * scvVolume * porosity - massToBeConserved;
            return targetFunc;
        };

        Scalar localEps = 1e-7;
        //define numerical derivative of the function
        const std::function<double (double)> massConservationDiffDerivative = [&](double conservativeSatNw)
        {
            Scalar leftShiftedConsSatNW = conservativeSatNw - conservativeSatNw*localEps;
            Scalar rightShiftedConsSatNW = conservativeSatNw + conservativeSatNw*localEps;

            Scalar newFixedPnwLeft = pw + fluidMatrixInteraction.pc(1.0-leftShiftedConsSatNW);
            Scalar leftMass = leftShiftedConsSatNW*problemCoarseVE.returnDensityGasAndViscosity(temperature, newFixedPnwLeft)[0]* scvVolume * porosity;

            Scalar newFixedPnwRight = pw + fluidMatrixInteraction.pc(1.0-rightShiftedConsSatNW);
            Scalar rightMass = rightShiftedConsSatNW*problemCoarseVE.returnDensityGasAndViscosity(temperature, newFixedPnwRight)[0]* scvVolume * porosity;

            Scalar derivative = (rightMass-leftMass)/(rightShiftedConsSatNW-leftShiftedConsSatNW);
            return derivative;
        };

        //solve for the root of the provided function, basically Newton-Rhapson method
        Scalar computedConservativeSatNw = findScalarRootNewton(initSatNw, massConservationDiff, massConservationDiffDerivative, 1e-4);

        if(verbose)
        {
            Scalar fluidStatePc = fluidMatrixInteraction.pc(1.0-computedConservativeSatNw);
            Scalar newFixedPnw = pw + fluidStatePc;
            Scalar reconstDensityNw = problemCoarseVE.returnDensityGasAndViscosity(temperature, newFixedPnw)[0];
            Scalar newMass = 225.0 * 0.2 * computedConservativeSatNw * reconstDensityNw; // TODO: do not hardcode!

            std::cout << "     -----Deducing satNw which belongs to the massToBeConserved: " << massToBeConserved << "\n";
            std::cout << "     Final satNw: " << computedConservativeSatNw << " results in local mass: " << newMass << " while initially was suggested: " << initSatNw << std::endl;
        }

        return computedConservativeSatNw;
    }


    /*!
     * \brief Compute conservative satNw when transfering a fullD column to VE
     * \param massNwToConserve mass in a fullD column that should be conserved
     * \param virtualPressureW deduced wetting-phase pressure for the new VE column
     * \param localVEIndex local element index of the new VE element (after adapt)
     * \param problemCoarseVE problem of the VE grid
     */
    Scalar computeConservativeSatNW_(const Scalar& massNwToConserve,
                                     const Scalar& virtualPressureW,
                                     const int& localVEIndex,
                                     const Problem<darcyVEDomainIndex>& problemCoarseVE)
    {
        auto dummyPos = problemCoarseVE.gridGeometry().bBoxMin(); //TODO: this is only valid if temperature, gravitation and porosity are independent of the position!

        VolumeVariablesVE volVarsVE;
        auto localVEElement = gridGeometryVE_->element(localVEIndex);
        auto fvGeometryVE = localView(*gridGeometryVE_);
        fvGeometryVE.bind(localVEElement);
        const auto elemSolVE = elementSolution(localVEElement, (*sol_)[darcyVEDomainIndex], *gridGeometryVE_);

        auto localVEElementVolume = 0.0;
        auto normalizedPorosity = 0.0;

        //for cctpfa, there should only be one scv per element
        for (const auto& scv : scvs(fvGeometryVE))
        {
            volVarsVE.update(elemSolVE, problemCoarseVE, localVEElement, scv);
            //should not use upscaled version of porosity, correct?
            normalizedPorosity = volVarsVE.porosity()/(upperDomainBoundaryHeight_-lowerDomainBoundaryHeight_);
            localVEElementVolume = scv.volume();
        }

        auto temperature = problemCoarseVE.spatialParams().temperatureAtPos(dummyPos);
        auto virtualCoarseDensityNw = problemCoarseVE.returnDensityGasAndViscosity(temperature, virtualPressureW)[0];

        //strictly, using localVEElementVolume of the whole VE column is incorrect. It would be correct to use the coarse-scale porosity and the coarse-scale volume - which is phyiscally only the length of the 1D interval and not the volume of the whole column. But for constant porosities the change cancels out.
        Scalar conservativeSatNw = massNwToConserve/(normalizedPorosity * virtualCoarseDensityNw * localVEElementVolume);

        return conservativeSatNw;
    }


    /*!
     * \brief Check if (single AND double) isolated model columns would occur after adapt. If so, make sure that there are no (single or double) isolated model columns, as the current implementation of the coupling mapper does not support isolated model columns.
     * \param verbose flag if verbose output should be enabled
     */
    void checkForIsolatedColumns_(bool verbose = false)
    {
        if(verbose)
        {
            for(int i = 0; i<mySwitchIndicator_.size(); i++)
            {
                std::cout << "Before isolated column check. mySwitchIndicator_ at global index: " << i << " and mySwitchIndicator_[i][0]: " << mySwitchIndicator_[i][0] << std::endl;
            }
        }

        //initial check what the distribution would like after applying the current suggested model switches
        std::vector<int> potentialNewModelDistribution{};
        for(int globalColumnIdx = 0; globalColumnIdx<mySwitchIndicator_.size(); globalColumnIdx++)
        {
            //if model should be switched for current column
            if(mySwitchIndicator_[globalColumnIdx][1]==true)
            {
                potentialNewModelDistribution.push_back(1 - mySwitchIndicator_[globalColumnIdx][0]); //store other model (which is either 0 or 1)
            }
            else
            {
                potentialNewModelDistribution.push_back(mySwitchIndicator_[globalColumnIdx][0]);
            }
        }


        if constexpr(dim==2)
        {
            //go over all columns and check for isolated columns
            for(int globalColumn=0; globalColumn<mySwitchIndicator_.size(); globalColumn++)
            {
                //no need to check very first and very last column, they should always be fine
                if(globalColumn>1 && globalColumn<potentialNewModelDistribution.size()-2)
                {
                    int ownModel = potentialNewModelDistribution[globalColumn];

                    //direct neighbors are different model
                    bool isSingleIsolated = potentialNewModelDistribution[globalColumn-1] != ownModel && potentialNewModelDistribution[globalColumn+1] != ownModel;
                    //one direct and one column 2 which is 2\delta x away are different model
                    bool isDoubleIsolated = (potentialNewModelDistribution[globalColumn-1] != ownModel &&
                                             potentialNewModelDistribution[globalColumn+1] == ownModel &&
                                             potentialNewModelDistribution[globalColumn+2] != ownModel);

                    if(isSingleIsolated)
                    {
                        if(verbose)
                        {
                            std::cout << "Found single isolated column at idx: " << globalColumn << std::endl;
                        }

                        if(mySwitchIndicator_[globalColumn][1] == 0)
                            mySwitchIndicator_[globalColumn][1] = 1;
                        else if(mySwitchIndicator_[globalColumn][1] == 1)
                            mySwitchIndicator_[globalColumn][1] = 0;
                    }
                    else if(isDoubleIsolated)
                    {
                        if(verbose)
                        {
                            std::cout << "Found double isolated column at idx: " << globalColumn << std::endl;
                        }

                        if(mySwitchIndicator_[globalColumn][1] == 0)
                            mySwitchIndicator_[globalColumn][1] = 1;
                        else if(mySwitchIndicator_[globalColumn][1] == 1)
                            mySwitchIndicator_[globalColumn][1] = 0;

                        if(mySwitchIndicator_[globalColumn+1][1] == 0)
                            mySwitchIndicator_[globalColumn+1][1] = 1;
                        else if(mySwitchIndicator_[globalColumn+1][1] == 1)
                            mySwitchIndicator_[globalColumn+1][1] = 0;

                        //skip next column as it was changed in the course of the current column
                        globalColumn++;
                    }

                    //continuous check what the distribution would like after applying the current suggested elimination of isolated columns, maybe new isolated column appeared
                    for(int globalColumnIdx = 0; globalColumnIdx<mySwitchIndicator_.size(); globalColumnIdx++)
                    {
                        //if model should be switched for current column
                        if(mySwitchIndicator_[globalColumnIdx][1]==true)
                        {
                            potentialNewModelDistribution.push_back(1 - mySwitchIndicator_[globalColumnIdx][0]); //store other model (which is either 0 or 1)
                        }
                        else
                        {
                            potentialNewModelDistribution.push_back(mySwitchIndicator_[globalColumnIdx][0]);
                        }
                    }
                }
            }
        }
        else if constexpr(dim==3)
        {
            //go over all columns and check for isolated columns
            for(int globalColumn=0; globalColumn<mySwitchIndicator_.size(); globalColumn++)
            {
                mySwitchIndicator_[globalColumn][1] = 0; //keep model, do not switch
            }
            std::cout << "Switch indicator needs to be implemented in 3D! Not suggesting any change in models automatically." << std::endl;
        }
        

        if(verbose)
        {
            for(int i = 0; i<mySwitchIndicator_.size(); i++)
            {
                int newModel = -1;
                if(mySwitchIndicator_[i][0] == 0)
                {
                    if(mySwitchIndicator_[i][1] == 0)
                        newModel = 0;
                    else
                        newModel = 1;
                }
                if(mySwitchIndicator_[i][0] == 1)
                {
                    if(mySwitchIndicator_[i][1] == 0)
                        newModel = 1;
                    else
                        newModel = 0;
                }
                std::cout << "After isolated column check. mySwitchIndicator_ at global index: " << i << " and mySwitchIndicator_[i][0]: " << mySwitchIndicator_[i][0] << " - " << mySwitchIndicator_[i][1] << " --> new model will be: " << newModel << std::endl;
            }
        }
    }


    /*!
     * \brief Also change VE to fullD according to the suggested travel distance of the solution solutionTravelColumns_ (only ahead of the gas plume)
     * \param problemCoarse coarse-scale VE problem
     * \param verbose flag if verbose output should be enabled
     */
    void applySolutionTravelDistance_(const Problem<darcyVEDomainIndex>& problemCoarse,
                                      bool verbose = false)
    {
        //initial check what the distribution would like after applying the current suggested model switches
        std::vector<int> potentialNewModelDistribution{};
        for(int globalColumnIdx = 0; globalColumnIdx<mySwitchIndicator_.size(); globalColumnIdx++)
        {
            if(mySwitchIndicator_[globalColumnIdx][1]==true)
            {
                potentialNewModelDistribution.push_back(1 - mySwitchIndicator_[globalColumnIdx][0]); //store other model (which is either 0 or 1)
            }
            else
            {
                potentialNewModelDistribution.push_back(mySwitchIndicator_[globalColumnIdx][0]);
            }
        }

        if constexpr(dim==2)
        {
            bool foundTip = 0;
            for(int globalColumn=0; globalColumn<mySwitchIndicator_.size(); globalColumn++)
            {
                if(potentialNewModelDistribution[globalColumn] == 0) //==VE
                {
                    if(potentialNewModelDistribution[globalColumn-1] == 1) //check if left neighbor is fullD
                    {
                        if(verbose)
                        {
                            std::cout << "Current VE while left is fullD at globalColumn: " << globalColumn << ", with persistentMassNwContainer_: " << (*persistentMassNwContainer_)[globalColumn] << " at time: " << problemCoarse.getTimeLoop()->time() << std::endl;
                        }

                        if((*persistentMassNwContainer_)[globalColumn]<1.0) // is at tip of plume?
                        {
                            for(int i=0; i<solutionTravelColumns_; i++) //the length of the suggested solution travel distance (in horzontal cells)
                            {
                                //catch error if right boundary is exceeded
                                if(globalColumn>numberColumns_-1)
                                {
                                    std::cout << "globalColumn:" << globalColumn << " while numberColumns_: " << numberColumns_ << std::endl;
                                    std::cout << "Aborting applySolutionTravelDistance_ as global index would exceed right boundary." << std::endl;
                                    break;
                                }

                                if(potentialNewModelDistribution[globalColumn] == 0) //==VE
                                {
                                    if(mySwitchIndicator_[globalColumn][1]==1)
                                    {
                                        mySwitchIndicator_[globalColumn][1] = 0;
                                    }
                                    else if(mySwitchIndicator_[globalColumn][1]==0)
                                    {
                                        mySwitchIndicator_[globalColumn][1] = 1;
                                    }
                                }

                                //increase globalColumn with each i, error catch is handled above if right boundary is exceeded during this loop
                                globalColumn++;
                            }

                            foundTip = true;
                        }
                    }
                }

                //can stop as soon as tip of plume has been found, as to the right of the plume there should be nothing happening thus we can use VE
                if(foundTip)
                    break;
            }
        }
        else if constexpr(dim==3)
        {
            std::cout << "The function `applySolutionTravelDistance` has not been implemented yet for 3D!" << std::endl;
        }
        
    }


    /*!
     * \brief Update the map between global column indices and local model element indices
     * \param verbose flag if verbose output should be enabled
     */
    void updateGlobalToLocalIndex_(bool verbose = false)
    {
        globalToLocalIndex_ = VerticalEquilibriumModelUtil::getCurrentGlobalToLocalIndex<GridGeometry<darcyVEDomainIndex>>(*gridGeometryFullD_, *gridGeometryVE_, numberColumns_, deltaX_, deltaY_, verbose);

        if(verbose)
        {
            for(int i=0; i<globalToLocalIndex_.size(); i++)
            {
                auto globalToLocalIndex = globalToLocalIndex_[i];
                std::cout << "new globalToLocalIndex at index " << i << ": ";
                for(int j=0; j < globalToLocalIndex.size(); j++)
                {
                    std::cout << globalToLocalIndex[j] << " ";
                }
                std::cout << std::endl;
            }
        }
    }


    /*!
     * \brief Update the map between global column indices and local model element indices. Should be called BEFORE adapt.
     * \param verbose flag if verbose output should be enabled
     */
    void updateGlobalToLocalIndexBeforeAdapt_(bool verbose = false)
    {
        globalToLocalIndexBeforeAdapt_ = VerticalEquilibriumModelUtil::getCurrentGlobalToLocalIndex<GridGeometry<darcyVEDomainIndex>>(*gridGeometryFullD_, *gridGeometryVE_, numberColumns_, deltaX_, deltaY_, verbose);

        if(verbose)
        {
            for(int i=0; i<globalToLocalIndexBeforeAdapt_.size(); i++)
            {
                auto globalToLocalIndex = globalToLocalIndexBeforeAdapt_[i];
                std::cout << "BEFORE new globalToLocalIndex at index " << i << ": ";
                for(int j=0; j < globalToLocalIndex.size(); j++)
                {
                    std::cout << globalToLocalIndex[j] << " ";
                }
                std::cout << std::endl;
            }
        }
    }


    /*!
     * \brief Update the map between global column indices and local model element indices. Should be called AFTER adapt.
     * \param verbose flag if verbose output should be enabled
     */
    void updateGlobalToLocalIndexAfterAdapt_(bool verbose = false)
    {
        globalToLocalIndexAfterAdapt_ = VerticalEquilibriumModelUtil::getCurrentGlobalToLocalIndex<GridGeometry<darcyVEDomainIndex>>(*gridGeometryFullD_, *gridGeometryVE_, numberColumns_, deltaX_, deltaY_, verbose);

        if(verbose)
        {
            for(int i=0; i<globalToLocalIndexAfterAdapt_.size(); i++)
            {
                auto globalToLocalIndex = globalToLocalIndexAfterAdapt_[i];
                std::cout << "AFTER new globalToLocalIndex at index " << i << ": ";
                for(int j=0; j < globalToLocalIndex.size(); j++)
                {
                    std::cout << globalToLocalIndex[j] << " ";
                }
                std::cout << std::endl;
            }
        }
    }


    /*!
     * \brief Enforce the fullD model in the leftmost columns, as here fullD should always be used
     * \param numberColumns the first `numberColumns` columns should always be fullD
     */
    void enforceFullDFirstColumns_(const int numberColumns)
    {
        if constexpr(dim==2)
        {
            //enforce that the first `numberColumns` columns are always fullD
            for(int i=0; i<numberColumns; i++)
            {
                if(mySwitchIndicator_[i][0]==0) //if model is VE
                {
                    mySwitchIndicator_[i][1]=1; //switch
                }
                else if(mySwitchIndicator_[i][0]==1) //if model is fullD
                {
                    mySwitchIndicator_[i][1]=0; //stay
                }
            }
        }
        else if constexpr(dim==3)
        {
            std::cout << "The function enforceFullDFirstColumns_ needs to be updated to 3D!" << std::endl;
        }
    }

    /*!
     * \brief Enforce the VE model in the rightmost columns, as here VE should always be used. It also serves as a check: if in these columns gas enters, the right Dirichlet boundary is probably already violated
     * \param numberColumns the last `numberColumns` columns should always be VE
     */
    void enforceVELastColumns_(const int numberColumns)
    {
        if constexpr(dim==2)
        {
            //enforce that the last `numberColumns` columns are always VE
            for(int i=numberColumns_-1; i>(numberColumns_-1-numberColumns); i--)
            {
                if(mySwitchIndicator_[i][0]==0) //if model is VE
                {
                    mySwitchIndicator_[i][1]=0; //stay
                }
                else if(mySwitchIndicator_[i][0]==1) //if model is fullD
                {
                    mySwitchIndicator_[i][1]=1; //switch
                }
            }
        }
        else if constexpr(dim==3)
        {
            std::cout << "The function enforceVELastColumns_ needs to be updated to 3D!" << std::endl;
        }
        
    }


    /*!
     * \brief Enforce the VE model in the rightmost columns and the fullD model in the leftmost columns
     * \param leftBoundaryColumns number of columns from the leftmost column that should always be fullD
     * \param rightBoundaryColumns number of columns from the rightmost column that should always be VE
     */
    void enforceBoundaryModels_(const int leftBoundaryColumns,
                                const int rightBoundaryColumns)
    {
        //enforced number of columns should not be smaller than 3, so that the function `checkForIsolatedColumns_` does not overwrite anything, as it removes single but also double isolated columns
        if(leftBoundaryColumns < 3 || rightBoundaryColumns < 3)
        {
            std::ostringstream errMsg;
            errMsg << "Aborting enforceBoundaryModels_(...): leftBoundaryColumns and rightBoundaryColumns are not allowed to be smaller than 3 to avoid removal in checkForIsolatedColumns_(...)!";
            throw std::invalid_argument(errMsg.str());
        }

        if constexpr(dim==2)
        {
            //enforce that the first #leftBoundaryColumns columns are always fullD
            enforceFullDFirstColumns_(leftBoundaryColumns);
            //enforce that the last #rightBoundaryColumns columns are always fullD. This will also ensure that a subgrid is never empty. Additionally, this can serve as a indicator if plume hast travelled to far/is too close to right boundary. As soon as you see that these boundary VE columns are filling with gas, the domain should probably be extended to the right.
            enforceVELastColumns_(rightBoundaryColumns);
        }
        else if constexpr(dim==3)
        {
            std::cout << "The function enforceBoundaryModels_ needs to be updated to 3D!" << std::endl;
        }
        
    }


    /*!
     * \brief From all the switching criteria and post-processing steps, deduce the final suggested model switches
     * \param problemCoarseVE problem of the coarse-scale VE model
     */
    void deduceModelSwitching_(const Problem<darcyVEDomainIndex>& problemCoarseVE)
    {
        //apply changes suggested by the switching criteria of the adaptive algorithm
        this->computeModelSwitchContainer_(false);
        //if previous function introduced isolated columns, remove them
        this->checkForIsolatedColumns_(false);
        //expand the fullD subdomain at the tip of the plume by the predicted solution travel distance
        this->applySolutionTravelDistance_(problemCoarseVE, false);
        //enforce models on boundary columns
        this->enforceBoundaryModels_(5,3); //minimal number should be three as checkForIsolatedColumns_ also removes "double isolated" columns
        //if previous function introduced isolated columns, remove them
        this->checkForIsolatedColumns_(false);
    }


    /*!
     * \brief From a given FD distribution, deduce the final suggested model switches
     * \param darcyProblem                    problem of the fullD model
     * \param darcyVerticalEquilibriumProblem problem of the coarse-scale VE model
     */
    void deduceModelSwitchingTemp3D_(Problem<darcyDomainIndex>& darcyProblem,
                                     Problem<darcyVEDomainIndex>& darcyVerticalEquilibriumProblem)
    {
        mySwitchIndicator_.clear();
        mySwitchIndicator_.resize(numberColumns_);

        //sanity check
        if(numberColumns_!=globalToLocalIndexBeforeAdapt_.size())
        {
            std::ostringstream errMsg;
            errMsg << "numberColumns_ and globalToLocalIndexBeforeAdapt_.size() must be of same length. numberColumns_: " << numberColumns_ << " but globalToLocalIndexBeforeAdapt_.size(): " << globalToLocalIndexBeforeAdapt_.size() << std::endl;
            throw std::length_error(errMsg.str());
        }

        auto gridGeometryFullD = darcyProblem.gridGeometry();
        auto gridGeometryVE = darcyVerticalEquilibriumProblem.gridGeometry();

        //define the new, desired fullD distribution
        auto domainDarcyNew = [&darcyVerticalEquilibriumProblem](const auto& element)
        {
            // return (element.geometry().center()[0] <= 30.0);

            // double xPos = element.geometry().center()[0];
            // double yPos = element.geometry().center()[1] - 50.0; 
            // bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 40.0; //use with number of cells: 10 7 4 to obtain semi isolated Darcy cell
            // // bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 50.0;
            // return isInCircle;

            
            // bool inFirstBlock = element.geometry().center()[0] < 30.0 && element.geometry().center()[1] < 50.0;
            // bool inSecondBlock = element.geometry().center()[0] < 40.0 && element.geometry().center()[1] >= 50.0;
            // return inFirstBlock || inSecondBlock;

            auto currentTime = darcyVerticalEquilibriumProblem.getTimeLoop()->time();
            // if(currentTime<3e5)
            // {
            //     double xPos = element.geometry().center()[0];
            //     double yPos = element.geometry().center()[1] - 50.0; 
            //     bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 40.0; //use with number of cells: 10 7 4 to obtain semi isolated Darcy cell
            //     // bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 50.0;
            //     return isInCircle;
            // }
            // else
            // {
            //     bool inFirstBlock = element.geometry().center()[0] < 30.0 && element.geometry().center()[1] < 50.0;
            //     bool inSecondBlock = element.geometry().center()[0] < 40.0 && element.geometry().center()[1] >= 50.0;
            //     return inFirstBlock || inSecondBlock;
            // }


            // if(currentTime<1e5)
            // {
            //     double xPos = element.geometry().center()[0];
            //     double yPos = element.geometry().center()[1] - 50.0; 
            //     bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 90.0; //use with number of cells: 10 7 4 to obtain semi isolated Darcy cell
            //     // bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 50.0;
            //     return isInCircle;
            // }
            // else if(currentTime<3e5)
            // {
            //     double xPos = element.geometry().center()[0];
            //     double yPos = element.geometry().center()[1] - 50.0; 
            //     bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 40.0; //use with number of cells: 10 7 4 to obtain semi isolated Darcy cell
            //     // bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 50.0;
            //     return isInCircle;
            // }
            // else
            // {
            //     bool inFirstBlock = element.geometry().center()[0] < 30.0 && element.geometry().center()[1] < 50.0;
            //     bool inSecondBlock = element.geometry().center()[0] < 40.0 && element.geometry().center()[1] >= 50.0;
            //     return inFirstBlock || inSecondBlock;
            // }
            
            // double xPos = element.geometry().center()[0];
            // double yPos = element.geometry().center()[1]; 
            // bool isAroundLens = xPos>10.0 && xPos<90.0 && yPos>25.0 && yPos<75.0;
            // double yPosR = element.geometry().center()[1] - 50.0;
            // bool isInInnerCircle = std::sqrt(xPos*xPos + yPosR*yPosR) < 40.0;
            // // bool isInOuterSlice = (xPos<=200.0) && (xPos>=160.0);
            // return isInInnerCircle || isAroundLens;


            if(currentTime<9e4)
            {
                double xPos = element.geometry().center()[0];
                double yPos = element.geometry().center()[1] - 50.0; 
                bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 40.0;
                return isInCircle;
            }
            else if(currentTime<4.3e6)
            {
                double xPos = element.geometry().center()[0];
                double yPos = element.geometry().center()[1];
                double yPosShifted = element.geometry().center()[1] - 50.0; 
                bool isInCircle = std::sqrt(xPos*xPos + yPosShifted*yPosShifted) < 40.0;
                // bool isInLens = pos[0]>20.0 && pos[0]<80 && pos[1]>35.0 && pos[1]<65.0;
                bool isAroundLens = xPos>15.0 && xPos<85 && yPos>30.0 && yPos<70.0;
                return isInCircle || isAroundLens;
            }
            else
            {
                double xPos = element.geometry().center()[0];
                double yPos = element.geometry().center()[1];
                double yPosShifted = element.geometry().center()[1] - 50.0;
                bool isInCircle = std::sqrt(xPos*xPos + yPosShifted*yPosShifted) < 40.0;
                // bool isInLens = pos[0]>20.0 && pos[0]<80 && pos[1]>35.0 && pos[1]<65.0;
                bool isAroundLens = xPos>15.0 && xPos<85 && yPos>30.0 && yPos<70.0;
                // bool isAroundFront = xPos>80.0 && xPos <= 120.0;
                bool isAroundFront = xPos>180.0 && xPos <= 220.0;
                return isInCircle || isAroundLens || isAroundFront;
                // return isInCircle || isAroundLens;
            }
            
        };

        //Fill mySwitchIndicator_
        std::array<bool,2> tmpSingleColumnContainer{};
        for(size_t globalColumnIdx=0; globalColumnIdx<globalToLocalIndexBeforeAdapt_.size(); globalColumnIdx++)
        {
            size_t numElementsInColumn = globalToLocalIndexBeforeAdapt_[globalColumnIdx].size();
            int currentModel = -1;
            int newModel = -1;
            bool shouldModelsSwitch = false;

            if(numElementsInColumn>1) //case fullD
            {
                currentModel = 1; //is fullD if there is more than one element per column
                //just check for first FD element in a column, should be same for all elements within a column
                auto localFullDElement = gridGeometryFullD.element(globalToLocalIndexBeforeAdapt_[globalColumnIdx][0]);
                if(domainDarcyNew(localFullDElement))
                    newModel = 1; //fullD
                else
                    newModel = 0; //VE

                if(currentModel==newModel)
                    shouldModelsSwitch = false;
                else
                    shouldModelsSwitch = true;

                tmpSingleColumnContainer = {(bool)currentModel, shouldModelsSwitch};
                // tmpSingleColumnContainer = {(bool)currentModel, 0}; //TEST
                mySwitchIndicator_[globalColumnIdx] = tmpSingleColumnContainer;
            }
            else if(numElementsInColumn==1) //case VE
            {
                currentModel = 0; ////is VE if there is exactly one element per column
                auto localVEElement = gridGeometryVE.element(globalToLocalIndexBeforeAdapt_[globalColumnIdx][0]);
                if(domainDarcyNew(localVEElement))
                    newModel = 1; //fullD
                else
                    newModel = 0; //VE
                if(currentModel==newModel)
                    shouldModelsSwitch = false;
                else
                    shouldModelsSwitch = true;

                tmpSingleColumnContainer = {(bool)currentModel, shouldModelsSwitch};
                // tmpSingleColumnContainer = {(bool)currentModel, 0}; //TEST
                mySwitchIndicator_[globalColumnIdx] = tmpSingleColumnContainer;
            }
        }
    }


    std::shared_ptr<GridGeometry<darcyDomainIndex>> gridGeometryFullD_;
    std::shared_ptr<GridGeometry<darcyVEDomainIndex>> gridGeometryVE_;
    std::shared_ptr<SolutionVector> sol_;
    DataTransferContainer dataTransferContainer_;
    IndexAssignmentVector currentModelDistribution_; //TODO: not used currently
    std::shared_ptr<ModelCriterionManager> modelCriterionManager_;
    int numberColumns_;
    Scalar deltaX_, deltaY_;
    int numberZCells_;
    Scalar deltaZ_;
    Scalar lowerDomainBoundaryHeight_;
    Scalar upperDomainBoundaryHeight_;
    IndexAssignmentVector globalToLocalIndex_;
    IndexAssignmentVector globalToLocalIndexBeforeAdapt_;
    IndexAssignmentVector globalToLocalIndexAfterAdapt_;
    SolutionVectorVE solVEBeforeAdapt_;
    SolutionVectorFullD solFullDBeforeAdapt_;
    std::vector<Scalar> massWInColumnContainer_;
    std::vector<Scalar> massNWInColumnContainer_;
    std::vector<std::array<bool,2>> mySwitchIndicator_; //first entry is model, second entry is if should be switched
    Scalar eps_ = 1e-9;
    std::shared_ptr<std::vector<double>> persistentSwContainer_;
    std::shared_ptr<std::vector<double>> persistentMassNwContainer_;
    int solutionTravelColumns_;
    Scalar porosity_;
};

} // end namespace Dumux

#endif
