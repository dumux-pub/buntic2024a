// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPVE
 * \brief Stores quantities for the fine scale and the coarse scale
 */
#ifndef DUMUX_TWOPVE_CRITERIONMANAGER_HH
#define DUMUX_TWOPVE_CRITERIONMANAGER_HH

#include <dumux/common/properties.hh>

#include <dumux/verticalequilibriumcommon/util.hh>
#include <dumux/verticalequilibriumcommon/quantityreconstruction.hh>
#include <dumux/timemeasurer/timemeasurements.hh>

#include <dumux/porousmediumflow/velocity.hh>

#include <limits>
#include <tuple>
#include <chrono>

namespace Dumux {
// forward declarations
template<class TypeTag, class VerticalEquilibriumTypeTag> class CriterionManager;

//global operator overload
std::ostream& operator << (std::ostream& os, const std::vector<double>& v)
{
    os << "[";
    for (typename std::vector<double>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << " ]";
    return os;
}

//global operator overload
std::ostream& operator<<(std::ostream& os, const std::array<double, 2>& arr)
{
    os << "[" << arr[0] << " " << arr[1] << "]";
    return os;
}

/*!
 * \ingroup TwoPVE
 * \brief This class stores quantities for the fine scale and the coarse scale
 */
template<class TypeTag, class VerticalEquilibriumTypeTag>
class CriterionManager
{
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum {
        waterPhaseIdx = FluidSystem::phase0Idx, // = 0
        gasPhaseIdx = FluidSystem::phase1Idx, // = 1
        numPhases = FluidSystem::numPhases
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using CellArray = std::array<unsigned int, dimWorld>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;

    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;

    using TwoPVEQuantityReconstor = TwoPVEQuantityReconst<VerticalEquilibriumTypeTag>;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

public:

    CriterionManager(std::shared_ptr<const GridGeometry> fvGridGeometryFullD,
                     const Scalar numberOfVerticalCells,
                     std::shared_ptr<TwoPVEQuantityReconstor> quantityReconstructor,
                     TimeMeasuring& timeMeasurer)
        :   fvGridGeometryFullD_(fvGridGeometryFullD),
            numberOfScvsFullD_(fvGridGeometryFullD_->numScv()),
            numberOfColumnsFullD_(numberOfScvsFullD_ / numberOfVerticalCells),
            numberOfVerticalCellsFullD_(numberOfVerticalCells),
            quantityReconstructor_(quantityReconstructor),
            quantityCriterionIndicator_(getParam<int>("VerticalEquilibrium.CriterionQuantity")),
            timeMeasurer_(timeMeasurer)
    {
        //good estimate from experience, the more vertical cells, the more lenient the threshold should be
        epsThreshold_ = 0.00015*numberOfVerticalCells;

        CellArray lowerLeft = getParam<CellArray>("Darcy.Grid.LowerLeft");
        CellArray upperRight = getParam<CellArray>("Darcy.Grid.UpperRight");
        CellArray numCells = getParam<CellArray>("Darcy.Grid.Cells");

        deltaX_ = ((double)upperRight[0]-(double)lowerLeft[0])/(double)numCells[0];
        if constexpr(dim==2)
            deltaY_ = 0;
        else if constexpr(dim==3)
            deltaY_ = ((double)upperRight[1]-(double)lowerLeft[1])/(double)numCells[1];

        totalNumberOfColumns_ = 1;
        for(int i=0; i<dim-1; i++)
        {
            totalNumberOfColumns_ *= numCells[i];
        }
        std::cout << "totalNumberOfColumns_" << totalNumberOfColumns_ << std::endl;

        criterionContainer_ = std::vector<Scalar>(totalNumberOfColumns_, std::numeric_limits<Scalar>::quiet_NaN());
        std::cout << "criterionContainer_: " << criterionContainer_ << std::endl;
        std::cout << "size critContainer: " << criterionContainer_.size() << std::endl;

        //map from column indices to fullD elements
        mapColumnsFullD_ = std::make_shared<std::multimap<int, Element>>(VerticalEquilibriumModelUtil::createFullDMultiMapCoarseToFine<Element, GridGeometry, Scalar>(*fvGridGeometryFullD, "Darcy", false));

        //map from fullD element indices to column indices
        mapColumnsFullDReverse_ = std::make_shared<std::multimap<int, int>>(VerticalEquilibriumModelUtil::createFullDMultiMapFineToCoarse<Element, GridGeometry, Scalar>(fvGridGeometryFullD, deltaX_, deltaY_, "Darcy"));

        fullDSatNwContainer_.resize(totalNumberOfColumns_, 0.0);
        massNWDerivativeContainer_.resize(totalNumberOfColumns_, 0.0);
    }


    /*!
     * \brief Update the containers which are necessary for computing the model-choice criteria. There are containers for storing the real, fullD values of quantities over the vertical height and containers for the virtual, reconstructed values over the height IF the respective column was to be computed via the VE model
     *
     * \param solutionFullD solution belonging to the fullD domain
     * \param problemFullD problem belonging to the fullD domain
     * \param darcyVerticalEquilibriumProblem problem belonging to the coarse VE domain
     * \param gridVariablesFullD grid variables belonging to the fullD domain
     * \param verbose flag if verbose output should be enabled
     */
    template<typename ProblemVE>
    void updateCriterionContainer(const SolutionVector& solutionFullD,
                                  const Problem& problemFullD,
                                  ProblemVE& darcyVerticalEquilibriumProblem,
                                  const std::shared_ptr<GridVariables> gridVariablesFullD,
                                  bool verbose = false)
    {
        //for time measurement
        std::optional<std::chrono::steady_clock::time_point> timeAdaptCritStart; //required as placeholder to use in the whole function
        if constexpr(TimeMeasuring::enableTimeMeasuringAdaptivityCriteria)
            timeAdaptCritStart = std::chrono::steady_clock::now();

        std::fill(criterionContainer_.begin(), criterionContainer_.end(), std::numeric_limits<Scalar>::quiet_NaN());

        numberOfScvsFullD_ = fvGridGeometryFullD_->numScv();
        int numberOfVerticalCells = getParam<CellArray>("Darcy.Grid.Cells")[dim-1]; //TODO: Reuse private member!
        numberOfColumnsFullD_ = numberOfScvsFullD_ / numberOfVerticalCells;
        numberOfVerticalCellsFullD_ = numberOfVerticalCells;

        //read in the quantity on which the criterion computation should be based
        std::string quantityCriterionName{};

        //instantiate the velocity output
        VelocityOutput velocityOutput(*gridVariablesFullD);
        using VelocityVector = typename VelocityOutput::VelocityVector;
        std::vector<VelocityVector> velocity(numPhases); //is used for fullD Darcy velocity later on, therefore resize to fullD scvs
        velocity[waterPhaseIdx].resize(numberOfScvsFullD_); //this is only correct for tpfa where #scv = #cells
        velocity[gasPhaseIdx].resize(numberOfScvsFullD_); //this is only correct for tpfa where #scv = #cells

        //solely for the velocity output
        auto fvGeometryFullD = localView(*fvGridGeometryFullD_);
        auto elemVolVarsFullD = localView(gridVariablesFullD->curGridVolVars());
        auto elemFluxVarsCacheFullD = localView(gridVariablesFullD->gridFluxVarsCache());

        //initialize members needed for later on
        const auto spatialParamsFullD = problemFullD.spatialParams();
        GlobalPosition dummyPos{};
        const Scalar temperature = spatialParamsFullD.temperatureAtPos(dummyPos);
        VolumeVariables volVarsFullD;
        Scalar topHeight = getParamFromGroup<CellArray>(problemFullD.paramGroup(), "Grid.UpperRight")[dim-1];
        Scalar lowerHeight = getParamFromGroup<CellArray>(problemFullD.paramGroup(), "Grid.LowerLeft")[dim-1];
        Scalar verticalHeight = topHeight - lowerHeight;
        Scalar deltaZ = verticalHeight/numberOfVerticalCellsFullD_; //TODO: CAREFUL to not do integer division

        const auto fluidMatrixInteraction = problemFullD.spatialParams().fluidMatrixInteractionAtPos(dummyPos); //TODO: maybe create for each element instead for some arbitrary dummyPos
        const Scalar resSatW = fluidMatrixInteraction.pcSwCurve().effToAbsParams().swr();
        const Scalar resSatNw = fluidMatrixInteraction.pcSwCurve().effToAbsParams().snr();
        const Scalar lambda = problemFullD.spatialParams().getBrooksCoreyLambda();
        const Scalar entryP = problemFullD.spatialParams().getBrooksCoreyEntryP();

        //find which columns are fullD
        std::vector<size_t> fullDColumns{};
        for(typename std::multimap<int,Element>::iterator it = mapColumnsFullD_->begin(); it != mapColumnsFullD_->end(); it = mapColumnsFullD_->upper_bound(it->first))
        {
            fullDColumns.push_back(it->first);
        }

        //iterate over fullD columns to compute local switching criteria
        for(int i = 0; i < fullDColumns.size(); i++)
        {
            int columnIdx = fullDColumns[i];
            //containers to store the real values of the fullD elements
            std::vector<Scalar> fullDSatWColumn{};
            std::vector<Scalar> fullDPWColumn{};
            std::vector<Scalar> fullDMobilityWColumn{};
            std::vector<std::vector<Scalar>> fullDDarcyVelocityZColumn(numPhases);

            //containers to store the recontructed values of the virtual elements
            std::vector<Scalar> virtualReconstructedSatW{};
            std::vector<Scalar> virtualReconstructedPW{};
            std::vector<Scalar> virtualReconstructedMobilityW{};

            //initialize quantities, virtualCoarsePW will be overwritten
            Scalar virtualCoarsePW = 0.0;
            Scalar gravityNorm = 9.81; //initialize with the default value of 9.81
            Scalar virtualCoarseSatNwConservative = 0.0;
            Scalar massNwToConserve = 0.0;
            Scalar virtualVolume = 0.0;
            Scalar virtualPorosity = 0.0;

            //get iterator which iterates over the fullD elements which belong to on fullD column
            const auto range = mapColumnsFullD_->equal_range(columnIdx);

            //===========DEDUCE VIRTUAL COARSE SCALE QUANTITIES
            for(auto iterator = range.first; iterator!=range.second; iterator++)
            {
                const auto eIdx = fvGridGeometryFullD_->elementMapper().index(iterator->second);
                const auto elementPos = (iterator->second).geometry().center();
                gravityNorm = spatialParamsFullD.gravity(elementPos).two_norm();

                //update volVars
                fvGeometryFullD.bind(iterator->second);
                const auto elemSolFullD = elementSolution(iterator->second, solutionFullD, *fvGridGeometryFullD_);
                for (const auto& scvFullD : scvs(fvGeometryFullD))
                {
                    volVarsFullD.update(elemSolFullD, problemFullD, iterator->second, scvFullD);

                    massNwToConserve += volVarsFullD.porosity() * scvFullD.volume() * volVarsFullD.saturation(1) * volVarsFullD.density(1);
                    virtualVolume += scvFullD.volume();
                    virtualPorosity += volVarsFullD.porosity() * deltaZ;
                }
                //compute PW at the bottom. Find bottom element in a column via y=deltaZ
                if(elementPos[dim-1] == (deltaZ/2.0 + lowerHeight))
                {
                    virtualCoarsePW = volVarsFullD.pressure(0) + volVarsFullD.density(0)*gravityNorm*(deltaZ/2.0);
                    dummyPos = elementPos;
                }

                //-----------FILL FULLD QUANTITIES IN CONTAINERS
                fullDSatWColumn.push_back(volVarsFullD.saturation(0));

                if(quantityCriterionIndicator_==1)
                    fullDPWColumn.push_back(volVarsFullD.pressure(0));

                if(quantityCriterionIndicator_==2)
                    fullDMobilityWColumn.push_back(volVarsFullD.mobility(0));

                if(quantityCriterionIndicator_==3 || quantityCriterionIndicator_==4)
                {
                    //compute darcy velocity over column and store the y-component for both phases
                    for(int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
                    {
                        elemVolVarsFullD.bind(iterator->second, fvGeometryFullD, solutionFullD);
                        elemFluxVarsCacheFullD.bind(iterator->second, fvGeometryFullD, elemVolVarsFullD);

                        //defined in dumux/porousmediumflow/velocity.hh
                        velocityOutput.calculateVelocity(velocity[phaseIdx], iterator->second, fvGeometryFullD, elemVolVarsFullD, elemFluxVarsCacheFullD, phaseIdx);

                        fullDDarcyVelocityZColumn[phaseIdx].push_back(velocity[phaseIdx][eIdx][1]);
                    //-----------------------------------------------
                    }
                }
            }

            virtualCoarseSatNwConservative = VerticalEquilibriumModelUtil::computeConservativeSatNWHelper<Scalar, ProblemVE>(massNwToConserve, virtualCoarsePW, darcyVerticalEquilibriumProblem, virtualVolume, virtualPorosity, topHeight, lowerHeight);
            fullDSatNwContainer_[columnIdx] = virtualCoarseSatNwConservative;
            Scalar virtualCoarseSatW = 1.0 - virtualCoarseSatNwConservative;

            //both densities are then computed using only the wetting-phase pressure (as in the VE case)
            const Scalar virtualCoarseDensityW = WettingPhase::density(temperature, virtualCoarsePW); //TODO: be careful to insert correct position for temperature if not isothermal
            const Scalar virtualCoarseDensityNw = NonwettingPhase::density(temperature, virtualCoarsePW); //TODO: be careful to insert correct position for temperature if not isothermal

            const Scalar virtualGasPlumedistance = quantityReconstructor_->computeGasPlumeDist
                (std::make_tuple(virtualCoarseDensityW,virtualCoarseDensityNw),
                 std::make_tuple(resSatW,resSatNw),
                 gravityNorm,
                 verticalHeight,
                 virtualCoarseSatW,
                 std::make_tuple(lambda,entryP));

            //___________DEDUCE VIRTUAL FINE SCALE/RECONSTRUCTED QUANTITIES
            for(auto iterator = range.first; iterator!=range.second; iterator++)
            {
                GlobalPosition fullDElementPos = (iterator->second).geometry().center();

                Scalar reconstructedSaturationW = quantityReconstructor_->computeSaturationIntegral
                    (std::make_tuple(virtualGasPlumedistance,virtualGasPlumedistance),
                    std::make_tuple(virtualCoarseDensityW,virtualCoarseDensityNw),
                    std::make_tuple(resSatW,resSatNw),
                    gravityNorm,
                    fullDElementPos[dim-1],
                    deltaZ,
                    std::make_tuple(lambda,entryP));

                //fill the column containers with the virtual reconstructed wetting-phase pressure, saturation and mobility
                virtualReconstructedSatW.push_back(reconstructedSaturationW);

                if(quantityCriterionIndicator_==1)
                {
                    //reconstruct the virtual fine-scale pressures and wetting-phase saturation
                    std::vector<Scalar> virtualReconstructedP = quantityReconstructor_->reconstPressure
                        (virtualGasPlumedistance,
                        std::make_tuple(virtualCoarseDensityW,virtualCoarseDensityNw),
                        gravityNorm,
                        fullDElementPos[dim-1],
                        virtualCoarsePW,
                        entryP);

                    virtualReconstructedPW.push_back(virtualReconstructedP[0]);
                }

                if(quantityCriterionIndicator_==2)
                {
                    //reconstruct the virtual fine-scale pressures and wetting-phase saturation
                    std::vector<Scalar> virtualReconstructedP = quantityReconstructor_->reconstPressure
                        (virtualGasPlumedistance,
                        std::make_tuple(virtualCoarseDensityW,virtualCoarseDensityNw),
                        gravityNorm,
                        fullDElementPos[dim-1],
                        virtualCoarsePW,
                        entryP);

                    //TODO: CARFEUL here! Should I use densities and viscosities using the reconstructed fine-scale pressure OR as until now the coarse-scale pressure? - use the same as in VE volVars
                    //densities and viscosities are necessary for computation of virtual, reconstructed mobilities
                    Scalar reconstDensityW = WettingPhase::density(temperature, virtualReconstructedP[FluidSystem::phase0Idx]);
                    Scalar reconstDensityNw = NonwettingPhase::density(temperature, virtualReconstructedP[FluidSystem::phase1Idx]);
                    Scalar reconstViscosityW = WettingPhase::viscosity(temperature, virtualReconstructedP[FluidSystem::phase0Idx]);
                    Scalar reconstViscosityNw = NonwettingPhase::viscosity(temperature, virtualReconstructedP[FluidSystem::phase1Idx]);

//                     Scalar reconstDensityW = virtualCoarseDensityW;
//                     Scalar reconstDensityNw = virtualCoarseDensityNw;
//                     Scalar reconstViscosityW = WettingPhase::viscosity(temperature, virtualCoarsePW);
//                     Scalar reconstViscosityNw = NonwettingPhase::viscosity(temperature, virtualCoarsePW);

                    //reconstruct the virtual fine-scale mobilities
                    //TODO: is it ok to use virtualGasPlumedistance for z_p and z_p,min - as there is no history of z_p for the fullD columns
                    std::vector<Scalar> virtualReconstructedMobilites = quantityReconstructor_->reconstMobilitiesFine
                        (std::make_tuple(virtualGasPlumedistance,virtualGasPlumedistance),
                        std::make_tuple(reconstDensityW,reconstDensityNw),
                        std::make_tuple(reconstViscosityW,reconstViscosityNw),
                        std::make_tuple(resSatW,resSatNw),
                        gravityNorm,
                        fullDElementPos[dim-1],
                        deltaZ,
                        std::make_tuple(lambda,entryP));

                    virtualReconstructedMobilityW.push_back(virtualReconstructedMobilites[0]);
                }
            }


            if(verbose)
            {
                std::cout << "\nFor columnIdx: " << columnIdx << " (one possible element from that column is @: " << dummyPos << ")" << std::endl;

                std::cout << "fullDPWColumn:                 " << fullDPWColumn << std::endl;
                std::cout << "virtualReconstructedPW:        " << virtualReconstructedPW << std::endl;

                std::cout << "fullDSatWColumn:               " << fullDSatWColumn << std::endl;
                std::cout << "virtualReconstructedSatW:      " << virtualReconstructedSatW << std::endl;

                std::cout << "fullDMobilityWColumn:          " << fullDMobilityWColumn << std::endl;
                std::cout << "virtualReconstructedMobilityW: " << virtualReconstructedMobilityW << std::endl;

                std::cout << "fullD DarcyVelocityZW:         " << fullDDarcyVelocityZColumn[0] << std::endl;
                std::cout << "fullD DarcyVelocityZNw:        " << fullDDarcyVelocityZColumn[1] << "\n" << std::endl;
            }

            //_______________________________________________


            Scalar criterionError = 0.0;

            switch(quantityCriterionIndicator_)
            {
                case 0: //"saturationW"
                    criterionError = computeCriterionError(verticalHeight, fullDSatWColumn, virtualReconstructedSatW);
                    quantityCriterionName = "saturationW";
                    break;
                case 1: //"pressureW"
                    criterionError = computeCriterionError(verticalHeight, fullDPWColumn, virtualReconstructedPW);
                    quantityCriterionName = "pressureW";
                    break;
                case 2: //"mobilityW"
                    criterionError = computeCriterionError(verticalHeight, fullDMobilityWColumn, virtualReconstructedMobilityW);
                    quantityCriterionName = "mobilityW";
                    break;
                case 3: //"darcyYW"
                    criterionError = computeCriterionError(verticalHeight, fullDDarcyVelocityZColumn[0]);
                    quantityCriterionName = "darcyYW";
                    break;
                case 4: //"darcyYNw"
                    criterionError = computeCriterionError(verticalHeight, fullDDarcyVelocityZColumn[1]);
                    quantityCriterionName = "darcyYNw";
                    break;
                default: //use saturationW as default
                    criterionError = computeCriterionError(verticalHeight, fullDSatWColumn, virtualReconstructedSatW);
                    quantityCriterionName = "saturationW";
            }

            if(verbose)
            {
                std::cout << "In column: " << columnIdx << " the criterion error is: " << criterionError << " based on " << quantityCriterionName << std::endl;
            }

            criterionContainer_[columnIdx] = criterionError;
        } //end loop over columns


        //for VE to fullD criteria
        darcyVerticalEquilibriumProblem.updateSwAndMassNwContainer(false); //TODO: find proper position for this.

        //compute massNw derivative globally
        for(int i=0; i<totalNumberOfColumns_; i++)
        {
            
            GlobalPosition posWithDummyHeight{};
            if constexpr(dim==2)
            {
                auto xPos = deltaX_/2.0 + deltaX_*i;
                posWithDummyHeight = {xPos, 0.0};
            }
            else if constexpr(dim==3)
            {
                auto numCells = getParamFromGroup<CellArray>(problemFullD.paramGroup(), "Grid.Cells");
                auto xCells = numCells[0];
                Scalar xPos = i%xCells * deltaX_ + deltaX_/2.0;
                Scalar yPos = std::floor((double)i/(double)xCells) * deltaY_ + deltaY_/2.0;

                posWithDummyHeight = {xPos, yPos, 0.0};
            }


            if constexpr(dim==2)
            {
                massNWDerivativeContainer_[i] = darcyVerticalEquilibriumProblem.computeCentralDerivativeMassNw(posWithDummyHeight);
            }
            else if constexpr(dim==3)
            {   
                static bool alreadyPrinted = false;
                massNWDerivativeContainer_[i] = -1.0;

                if(alreadyPrinted==false)
                {
                    std::cout << "Missing computation of central derivative mass nw for 3D!" << std::endl;
                    alreadyPrinted=true;
                }
            }

            if(verbose)
            {
                std::cout << "At global Idx: " << i << " derivative massNW is:" << massNWDerivativeContainer_[i] << std::endl;
            }
        }

        
        

        //find VE column
        std::vector<size_t> VEColumns{};
        for(int i=0; i<totalNumberOfColumns_; i++)
        {
            if(! (std::find(fullDColumns.begin(), fullDColumns.end(), i) != fullDColumns.end()))
            {
                VEColumns.push_back(i);
            }
        }


        //compute sW derivative globally
        for(int i=0; i<VEColumns.size(); i++)
        {
            auto globalColumnIdx = VEColumns[i];

            GlobalPosition posWithDummyHeight{};
            if constexpr(dim==2)
            {
                auto xPos = deltaX_/2.0 + deltaX_*globalColumnIdx;
                posWithDummyHeight = {xPos, 0.0};
            }
            else if constexpr(dim==3)
            {
                auto numCells = getParamFromGroup<CellArray>(problemFullD.paramGroup(), "Grid.Cells");
                auto xCells = numCells[0];
                Scalar xPos = globalColumnIdx%xCells * deltaX_ + deltaX_/2.0;
                Scalar yPos = std::floor((double)globalColumnIdx/(double)xCells) * deltaY_ + deltaY_/2.0;

                posWithDummyHeight = {xPos, yPos, 0.0};
            }

            // auto xPos = deltaX_/2.0 + deltaX_*globalColumnIdx;
            // GlobalPosition posWithDummyHeight{xPos, 0.0};

            if constexpr(dim==2)
            {
                criterionContainer_[globalColumnIdx] = darcyVerticalEquilibriumProblem.computeCentralDerivativeSw(posWithDummyHeight);
            }
            else if constexpr(dim==3)
            {
                static bool alreadyPrintedVE = false;
                criterionContainer_[globalColumnIdx] = -1.0;

                if(alreadyPrintedVE==false)
                {
                    std::cout << "Missing computation of central derivative mass nw for 3D and VE!" << std::endl;
                    alreadyPrintedVE=true;
                }
            }

            if(verbose)
            {
                std::cout << "Computing derivative for globalIdx: " << globalColumnIdx << " at pos: " << posWithDummyHeight << ", it is: " << darcyVerticalEquilibriumProblem.computeCentralDerivativeSw(posWithDummyHeight) << std::endl;
            }
        }
        


        if(verbose)
        {
            std::cout << "Printing fullDColumns: ";
            for(int i=0; i<fullDColumns.size(); i++)
            {
                std::cout << fullDColumns[i] << " ";
            }
            std::cout << std::endl;

            std::cout << "Printing VEColumns: ";
            for(int i=0; i<VEColumns.size(); i++)
            {
                std::cout << VEColumns[i] << " with crit value: " << criterionContainer_[VEColumns[i]] << std::endl;;
            }
            std::cout << std::endl;

            std::cout << "Print criterionVector: " << criterionContainer_ << " based on " << quantityCriterionName << std::endl;
        }

        //for time measurement
        if constexpr(TimeMeasuring::enableTimeMeasuringAdaptivityCriteria)
        {
            std::chrono::steady_clock::time_point timeAdaptCritEnd = std::chrono::steady_clock::now();
            const double singleRunZpTime = std::chrono::duration_cast<std::chrono::microseconds>(timeAdaptCritEnd -
                *timeAdaptCritStart).count();
            timeMeasurer_.addToTotalTimeAdaptivityCriteria(singleRunZpTime);
        }
    }


    /*!
     * \brief Return the vector containing virtual coarse-scale non-wetting-phase saturation values. Only has non-zero entries for fullD columns, but has as many entries as global columns.
     */
    std::vector<Scalar> returnFullDSatNwContainer() const
    {
        return fullDSatNwContainer_;
    }


    /*!
     * \brief Return the vector containing horizontal (x-) derivatives of the non-wetting-phase masses.
     */
    std::vector<Scalar> returnMassNWDerivativeContainer() const
    {
        return massNWDerivativeContainer_;
    }


    /*!
     * \brief Computes the criterion error for one column
     *
     * \param domainHeight height of the domain
     * \param fullDValues fullD values belongign to the respective column
     * \param virtualValues virtual, reconstructed values belongign to the respective column
     */
    Scalar computeCriterionError(Scalar& domainHeight,
                                 std::vector<Scalar>& fullDValues,
                                 std::vector<Scalar>& virtualValues)
    {
        if(fullDValues.size() != virtualValues.size())
        {
            throw std::invalid_argument("The two provided vectors for the criterion error computation MUST be of same length!");
        }
        else
        {
            Scalar criterionError = 0.0;

            for(int vectorIdx = 0; vectorIdx < fullDValues.size(); vectorIdx++)
            {
                criterionError += std::abs(fullDValues[vectorIdx] - virtualValues[vectorIdx]) / fullDValues[vectorIdx]; //divide by the fullD value for normalisation
            }

            criterionError /= domainHeight; //further normalisation

            return criterionError;
        }
    }


    /*!
     * \brief Computes the criterion error for one column. This function is used in the case, that no virtual reconstructed values are at hand, e.g. for the darcy velocity in y direction
     *
     * \param domainHeight height of the domain
     * \param fullDValues fullD values belongign to the respective column
     */
    Scalar computeCriterionError(Scalar& domainHeight,
                                 std::vector<Scalar>& fullDValues)
    {
        Scalar criterionError = 0.0;

        for(int vectorIdx = 0; vectorIdx < fullDValues.size(); vectorIdx++)
        {
            criterionError += std::abs(fullDValues[vectorIdx]); //TODO: does this make any sense?
        }

        criterionError /= domainHeight; //further normalisation

        return criterionError;
    }


//     /*!
//      * \brief Return the criterion value for a provided column index
//      *
//      * \param columnIdx index of a column
//      */
//     Scalar returnCriterionValueForColumn(const int columnIdx) const
//     {
//         if(columnIdx < totalNumberOfColumns_)
//         {
//             return criterionContainer_[columnIdx];
//         }
//         else
//         {
//             throw std::invalid_argument("Index is too large for the size of criterionContainer_.");
//         }
//     }


    /*!
     * \brief Return the vector containing the criterion values for each column
     */
    std::vector<Scalar> returnCriterionVector() const
    {
        return criterionContainer_;
    }


    /*!
     * \brief Return a shared pointer to the map containing the column indices and their respective fullD elements
     */
    const std::shared_ptr<std::multimap<int, Element>> getMapColumns() const
    {
        return mapColumnsFullD_;
    }


    /*!
     * \brief Return a shared pointer to the map containing the fullD elements and their respecitve column indices
     */
    const std::shared_ptr<std::multimap<int, int>> getMapColumnsReverse() const
    {
        return mapColumnsFullDReverse_;
    }


    /*!
     * \brief Return the threshold for the criterion for switching from fullD to VE
     */
    const Scalar getEpsThreshold() const
    {
        return epsThreshold_;
    }


    /*!
     * \brief Recompute the map which maps column indices to fullD elements
     *
     * \param gridGeometryFullD fullD grid geometry
     */
    void updateFullDMapCoarseToFine(std::shared_ptr<const GridGeometry> gridGeometryFullD)
    {
        *mapColumnsFullD_ = std::multimap<int, Element>(VerticalEquilibriumModelUtil::createFullDMultiMapCoarseToFine<Element, GridGeometry, Scalar>(*gridGeometryFullD, "Darcy"));
    }


    /*!
     * \brief Recompute the map which maps fullD element indices to column indices
     *
     * \param gridGeometryFullD fullD grid geometry
     */
    void updateFullDMapFineToCoarse(std::shared_ptr<const GridGeometry> gridGeometryFullD)
    {
        *mapColumnsFullDReverse_ = std::multimap<int, int>(VerticalEquilibriumModelUtil::createFullDMultiMapFineToCoarse<Element, GridGeometry, Scalar>(gridGeometryFullD, deltaX_, deltaY_, "Darcy"));
    }


private:

    Scalar epsThreshold_;
    std::shared_ptr<const GridGeometry> fvGridGeometryFullD_;
    size_t numberOfScvsFullD_, numberOfColumnsFullD_, numberOfVerticalCellsFullD_;
    size_t totalNumberOfColumns_;
    Scalar deltaX_;
    Scalar deltaY_;
    std::vector<Scalar> criterionContainer_;
    std::shared_ptr<std::multimap<int, Element>> mapColumnsFullD_;
    std::shared_ptr<std::multimap<int, int>> mapColumnsFullDReverse_;

    std::shared_ptr<TwoPVEQuantityReconstor> quantityReconstructor_;

    int quantityCriterionIndicator_;

    std::vector<Scalar> fullDSatNwContainer_;
    std::vector<Scalar> massNWDerivativeContainer_;

    TimeMeasuring &timeMeasurer_;
};

} // end namespace Dumux

#endif
