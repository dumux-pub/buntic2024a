// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTests
 * \brief Test for the two-phase porousmedium flow model.
 */

#include <config.h>

#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "properties.hh"

#include <dumux/verticalequilibriumcommon/quantityreconstruction.hh>
#include <dumux/multidomain/couplingsubgrid/adaptive/criterionmanager.hh>
#include <dumux/multidomain/couplingsubgrid/adaptive/adaptivedatatransfer.hh>
#include <dumux/timemeasurer/timemeasurements.hh>

int main(int argc, char** argv)
{
    std::chrono::steady_clock::time_point mainStart = std::chrono::steady_clock::now();

    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using TypeTag = Properties::TTag::CCTpfaTypeTag;
    using DarcyFullDTypeTag = Properties::TTag::DarcyTwoP;
    using VerticalEquilibriumTypeTag = Properties::TTag::VerticalEquilibriumTwoP;

    TimeMeasuring timeMeasurer{};

    //--------------------------------------------RESTRICTIONS CHECK--------------------------------------------
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    VerticalEquilibriumModelUtil::checkForExceptions<GridGeometry>();
    //----------------------------------------------------------------------------------------------------------

    //--------------------------------------------GRIDMANAGERS--------------------------------------------
    // create the full grid that we are gonna split in different domains
    using HostGrid = typename GetProp<TypeTag, Properties::Grid>::HostGrid;

    GridManager<HostGrid> gridManager;
    GridManager<HostGrid> gridManagerHostVE;

    gridManager.init("Darcy");
    gridManagerHostVE.init("DarcyVE");

    using GridView = typename GetPropType<DarcyFullDTypeTag, Properties::GridGeometry>::GridView;
    constexpr unsigned int dim =  GridView::dimension;

    //initial model distribution
    auto domainDarcy = [](const auto& element)
    {
        if constexpr(dim==2)
        {
            //choose this such that no subdomain is empty
            return (element.geometry().center()[0] <= 10.0);
        }
        else if constexpr(dim==3)
        {
            double xPos = element.geometry().center()[0];
            double yPos = element.geometry().center()[1] - 50.0; 
            bool isInCircle = std::sqrt(xPos*xPos + yPos*yPos) < 40.0;
            return isInCircle;
        }
    };

    auto elementSelectorDarcy = [domainDarcy](const auto& element)
    {
        return domainDarcy(element);
    };

    auto elementSelectorVEComplement = [domainDarcy](const auto& element)
    {
        return !domainDarcy(element);
    };

    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManagerDarcy, gridManagerVE, gridManagerVEFine;
    gridManagerDarcy.init(gridManager.grid(), elementSelectorDarcy);
    gridManagerVE.init(gridManagerHostVE.grid(), elementSelectorVEComplement);
    gridManagerVEFine.init(gridManager.grid(), elementSelectorVEComplement);


    //--------------------------------------------GRIDVIEWS--------------------------------------------
    // we compute on the leaf grid views
    const auto& gridViewDarcy = gridManagerDarcy.grid().leafGridView();
    const auto& gridViewVE = gridManagerVE.grid().leafGridView();
    const auto& gridViewVEFine = gridManagerVEFine.grid().leafGridView();


    //--------------------------------------------GRIDGEOMETRIES--------------------------------------------
    // create the finite volume grid geometries
    // using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto fvGridGeometryDarcy = std::make_shared<GridGeometry>(gridViewDarcy);
    auto fvGridGeometryVE = std::make_shared<GridGeometry>(gridViewVE);
    auto fvGridGeometryVEFine = std::make_shared<GridGeometry>(gridViewVEFine);

    // the mixed dimension type traits
    using Traits = MultiDomainTraits<DarcyFullDTypeTag, VerticalEquilibriumTypeTag>;
    constexpr auto darcyDomainIndex = Traits::template SubDomain<0>::Index();
    constexpr auto darcyVEDomainIndex = Traits::template SubDomain<1>::Index();


    //print geometries
//     VerticalEquilibriumModelUtil::printGeom(*fvGridGeometryDarcy, "DarcyFullD");
//     VerticalEquilibriumModelUtil::printGeom(*fvGridGeometryVE, "VE");
//     VerticalEquilibriumModelUtil::printGeom(*fvGridGeometryVEFine, "VE_fine");

    bool minimalRun = getParam<bool>("Vtk.MinimalRun");

    //--------------------------------------------QUANTITYRECONSTRUCTOR--------------------------------------------
    //introduce quantityReconstructor for the VE model
    using QuantityReconstructorOne = TwoPVEQuantityReconst<VerticalEquilibriumTypeTag>; //could also take DarcyVerticalEquilibriumTwoTypeTag
    QuantityReconstructorOne quantityReconstructor{timeMeasurer};
    auto quantityReconstructorShared = std::make_shared<QuantityReconstructorOne>(quantityReconstructor);

    //--------------------------------------------COUPLINGMANAGER--------------------------------------------

    // the coupling manager
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    auto couplingManager = std::make_shared<CouplingManager>();

    //--------------------------------------------PROBLEMS--------------------------------------------
    //problem for the darcy model
    using DarcyProblem = GetPropType<DarcyFullDTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(fvGridGeometryDarcy, couplingManager, timeMeasurer, "Darcy");

    //problem for the coarse VE model
    using DarcyVerticalEquilibriumProblem = GetPropType<VerticalEquilibriumTypeTag, Properties::Problem>;
    auto darcyVerticalEquilibriumProblem = std::make_shared<DarcyVerticalEquilibriumProblem>(fvGridGeometryVE, fvGridGeometryVEFine, quantityReconstructorShared, couplingManager, darcyProblem, timeMeasurer, "DarcyVE");
    //problem for the fine VE model
    auto problemVEFine = std::make_shared<DarcyVerticalEquilibriumProblem>(fvGridGeometryVEFine, darcyVerticalEquilibriumProblem, darcyProblem);
    //share the coarse and fine problems for the VE specific problems
    darcyVerticalEquilibriumProblem->setOtherProblem(problemVEFine);
    problemVEFine->setOtherProblem(darcyVerticalEquilibriumProblem);

    //--------------------------------------------TIMELOOP--------------------------------------------
    // get some time loop parameters
    using Scalar = Traits::Scalar;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    darcyVerticalEquilibriumProblem->setTimeLoop(timeLoop);
    problemVEFine->setTimeLoop(timeLoop);

    //--------------------------------------------SOLUTION VECTOR--------------------------------------------
    auto solPointer = std::make_shared<Traits::SolutionVector>();
    (*solPointer)[darcyDomainIndex].resize(fvGridGeometryDarcy->numDofs());
    (*solPointer)[darcyVEDomainIndex].resize(fvGridGeometryVE->numDofs());
    using SolutionVector = GetPropType<VerticalEquilibriumTypeTag, Properties::SolutionVector>;
    auto solVEFine = std::make_shared<SolutionVector>(fvGridGeometryVEFine->numDofs());

    //--------------------------------------------INITIALIZE COUPLINGMANAGER--------------------------------------------
    couplingManager->init(darcyProblem, darcyVerticalEquilibriumProblem, *solPointer);

    //--------------------------------------------APPLY INITIAL SOLUTIONS--------------------------------------------
    darcyProblem->applyInitialSolution((*solPointer)[darcyDomainIndex]);
    darcyVerticalEquilibriumProblem->applyInitialSolution((*solPointer)[darcyVEDomainIndex]);
    problemVEFine->applyInitialSolution(*solVEFine);

    auto oldSol = *solPointer;

    //provide fine-scale and coarse-scale solutions to fine-scale and coarse-scale problems
    darcyVerticalEquilibriumProblem->setFineSolution(solVEFine);
    darcyVerticalEquilibriumProblem->setTotalSolution(solPointer);
    problemVEFine->setFineSolution(solVEFine);
    problemVEFine->setTotalSolution(solPointer);

    //set up global containers for holding saturations and masses
    auto swContainer = std::make_shared<std::vector<double>>();
    swContainer->resize(darcyVerticalEquilibriumProblem->getNumberOfCells()[0], 1.0);
    darcyVerticalEquilibriumProblem->initializeSwContainer(swContainer);

    auto massNwContainer = std::make_shared<std::vector<double>>();
    massNwContainer->resize(darcyVerticalEquilibriumProblem->getNumberOfCells()[0]);
    darcyVerticalEquilibriumProblem->initializeMassNwContainer(massNwContainer);

    darcyVerticalEquilibriumProblem->updateSwAndMassNwContainer(false);


    //--------------------------------------------INITIALIZE COUPLINGMANAGER--------------------------------------------
    // couplingManager->init(darcyProblem, darcyVerticalEquilibriumProblem, *solPointer);

    //Print coupling entities
//     couplingManager->printWholeCouplingStencil();
//     couplingManager->printWholeIsCoupledElement();
//     couplingManager->printWholeIsCoupledScvf();
//     couplingManager->printWholeScvfCouplingInfo();

    //--------------------------------------------GRIDVARIABLES--------------------------------------------
    //the grid variables
    using GridVariablesDarcy = GetPropType<DarcyFullDTypeTag, Properties::GridVariables>;
    auto gridVariablesDarcy = std::make_shared<GridVariablesDarcy>(darcyProblem, fvGridGeometryDarcy);
    gridVariablesDarcy->init((*solPointer)[darcyDomainIndex]);

    using GridVariablesVE = GetPropType<VerticalEquilibriumTypeTag, Properties::GridVariables>;
    auto gridVariablesVE = std::make_shared<GridVariablesVE>(darcyVerticalEquilibriumProblem, fvGridGeometryVE);
    gridVariablesVE->init((*solPointer)[darcyVEDomainIndex]);

    auto gridVariablesVEFine = std::make_shared<GridVariablesVE>(problemVEFine, fvGridGeometryVEFine);
    gridVariablesVEFine->init(*solVEFine);

    couplingManager->setGridVariables(std::make_tuple(gridVariablesDarcy, gridVariablesVE)); //currently nothing done with gridvars in couplingmanager?

    //--------------------------------------------CRITERIONMANAGER--------------------------------------------
    using ModelCriterionManager = CriterionManager<DarcyFullDTypeTag, VerticalEquilibriumTypeTag>;
    // using GridView = typename GetPropType<DarcyFullDTypeTag, Properties::GridGeometry>::GridView;
    using CellArray = std::array<unsigned int, GridView::dimensionworld>;
    const auto cellsFullD = getParamFromGroup<CellArray>("Darcy", "Grid.Cells");
    const auto numberVerticalCellsFullD = cellsFullD[dim-1];
    

    auto criterionManagerPtr = std::make_shared<ModelCriterionManager>(fvGridGeometryDarcy, numberVerticalCellsFullD, quantityReconstructorShared, timeMeasurer);

    //uncomment this to print fullD column map
//     VerticalEquilibriumModelUtil::printColumnMapFullD(*criterionManagerPtr, fvGridGeometryDarcy);

    criterionManagerPtr->updateCriterionContainer((*solPointer)[darcyDomainIndex], *darcyProblem, *darcyVerticalEquilibriumProblem, gridVariablesDarcy);

    //--------------------------------------------ADAPTIVEDATATRANSFER--------------------------------------------
    auto myAdaptDataTransfer = std::make_shared<TwoPVEGridDataTransfer<Traits>>(fvGridGeometryDarcy, fvGridGeometryVE, solPointer, criterionManagerPtr, swContainer, massNwContainer);

    //--------------------------------------------VTKOUTPUTMODULES--------------------------------------------

    bool enableLenses = getParam<bool>("SpatialParams.EnableLenses", false); //one time read in
    size_t numCellsDim1 = cellsFullD[0];
    size_t numCellsDim2 = 0;
    if constexpr(dim==3)
        numCellsDim2 = cellsFullD[1];
    size_t numCellsDim3 = cellsFullD[dim-1];


    // initialize the darcyFullD vtk output module
    using SolutionVectorDarcy = std::decay_t<decltype((*solPointer)[darcyDomainIndex])>;
    std::string fullDOutputName = darcyProblem->name() + "_adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariablesDarcy, SolutionVectorDarcy> vtkWriterDarcy(*gridVariablesDarcy, (*solPointer)[darcyDomainIndex], fullDOutputName);
    using IOFieldsDarcyFullD = GetPropType<DarcyFullDTypeTag, Properties::IOFields>;
    using VelocityOutput = GetPropType<DarcyFullDTypeTag, Properties::VelocityOutput>;
    vtkWriterDarcy.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariablesDarcy));
    IOFieldsDarcyFullD::initOutputModule(vtkWriterDarcy);
    vtkWriterDarcy.write(0.0);

    // initialize the darcyVECoarse vtk output module
    using SolutionVectorVE = std::decay_t<decltype((*solPointer)[darcyVEDomainIndex])>;
    std::string VECoarseOutputName = darcyVerticalEquilibriumProblem->name() + "_adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariablesVE, SolutionVectorVE> vtkWriterVECoarse(*gridVariablesVE, (*solPointer)[darcyVEDomainIndex], VECoarseOutputName);
    using IOFieldsVECoarse = GetPropType<VerticalEquilibriumTypeTag, Properties::IOFields>;
    using VelocityOutputVE = GetPropType<VerticalEquilibriumTypeTag, Properties::VelocityOutput>;
    vtkWriterVECoarse.addVelocityOutput(std::make_shared<VelocityOutputVE>(*gridVariablesVE));
    IOFieldsVECoarse::initOutputModule(vtkWriterVECoarse, 1);
    vtkWriterVECoarse.write(0.0);

    // initialize the darcyVEFine vtk output module
    std::string VEFineOutputName = problemVEFine->name() + "_adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariablesVE, SolutionVectorVE> vtkWriterVEFine(*gridVariablesVEFine, *solVEFine, VEFineOutputName);
    IOFieldsVECoarse::initOutputModule(vtkWriterVEFine, 0); // Add model specific output fields
    vtkWriterVEFine.write(0.0);

    //--------------------------------------------PRINT COLUMN MAPS--------------------------------------------
    // using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    // using Element = typename GridView::template Codim<0>::Entity;
    // VerticalEquilibriumModelUtil::printColumnMapVE<DarcyVerticalEquilibriumProblem, GridGeometry>(*darcyVerticalEquilibriumProblem, fvGridGeometryVEFine, fvGridGeometryVE);

    //--------------------------------------------ASSEMBLER--------------------------------------------
    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(darcyProblem, darcyVerticalEquilibriumProblem),
                                                 std::make_tuple(fvGridGeometryDarcy, fvGridGeometryVE),
                                                 std::make_tuple(gridVariablesDarcy, gridVariablesVE),
                                                 couplingManager, timeLoop, oldSol);

    //--------------------------------------------SOLVER--------------------------------------------

    // the linear solver
    using LinearSolver = ILUBiCGSTABIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    //--------------------------------------------HELPERS FOR PRINT MASS BALANCE--------------------------------------------
    auto totalInjectedGasMass = 0.0;
    //for fullD
    using SubDomainTypeTagFullD = typename Traits::template SubDomain<0>::TypeTag;
    using VolumeVariablesFullD = typename GetPropType<SubDomainTypeTagFullD, Properties::GridVolumeVariables>::VolumeVariables;
    //============================================FINISH HELPERS FOR PRINT MASS BALANCE============================================


    //Write meta data file
    std::vector<std::string> metaDataNameContainer = {"timeStep", "simulatedTime", "timeStepSize", "wallTime", "numberElementsFullD", "numberElementsVE", "numberElementsVEFine", "numberElementsTotal", "numElemDim1", "numElemDim2", "numElemDim3", "numberVESubdomains", "numberFullDSubdomains", "enableLenses"};
    std::string fileName = "adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses) + "_metaData.txt";
    if(!minimalRun)
    {
        std::cout << "fvGridGeometryDarcy->numDofs() start: " << fvGridGeometryDarcy->numDofs() << std::endl;
        std::cout << "fvGridGeometryVE->numDofs() start: " << fvGridGeometryVE->numDofs() << std::endl;
        std::cout << "fvGridGeometryVEFine->numDofs() start: " << fvGridGeometryVEFine->numDofs() << std::endl;
    }
    //delete existing files
    VerticalEquilibriumModelUtil::deleteFile(fileName);
    //assemble and write meta data
    std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorAdaptiveModel<GridGeometry, TimeLoop<Scalar>, CouplingManager>
        (*fvGridGeometryDarcy, *fvGridGeometryVE, *fvGridGeometryVEFine, *timeLoop, *couplingManager, {numCellsDim1,numCellsDim2,numCellsDim3}, enableLenses);
    VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);


    //time loop
    timeLoop->start();
    while (!timeLoop->finished())
    {
        myAdaptDataTransfer->adapt(*darcyProblem, *darcyVerticalEquilibriumProblem, gridVariablesDarcy, gridVariablesVE, gridVariablesVEFine, gridManagerDarcy, gridManagerVE, gridManagerVEFine, fvGridGeometryVEFine, gridViewDarcy, gridViewVE, gridViewVEFine, *solVEFine, oldSol, assembler, couplingManager);
        //write vtk output. It is IMPORTANT to call the write functions AFTER updating the couplingManager when enabling velocityOutput in the writer!

        if(!minimalRun)
        {
            //write vtk output. It is IMPORTANT to call the write functions AFTER updating the couplingManager when enabling velocityOutput in the writer!
            vtkWriterDarcy.write(timeLoop->time()+1.0);
            vtkWriterVECoarse.write(timeLoop->time()+1.0);
            vtkWriterVEFine.write(timeLoop->time()+1.0);
        }
        //----------updating functions after grid adapt end here-------------------------------------------------------------------

        // solve the non-linear system with time step control
        nonLinearSolver.solve(*solPointer, *timeLoop);
        problemVEFine->updateFineSol();
        gridVariablesVEFine->update(*solVEFine);

        //--------------------------------------------HELPER FUNCTION FOR PRINT MASS BALANCE--------------------------------------------
        if(!minimalRun)
        {
            //helper function to print out wetting-phase and non-wetting-phase masses in the respective domains during the simulation
            VerticalEquilibriumModelUtil::printMassBalance<VolumeVariablesFullD, GridGeometry, Traits::SolutionVector, DarcyVerticalEquilibriumProblem, DarcyProblem, TimeLoop<Scalar>, Scalar, CouplingManager>(totalInjectedGasMass, fvGridGeometryVE, fvGridGeometryVEFine, solPointer, darcyVerticalEquilibriumProblem, fvGridGeometryDarcy, darcyProblem, timeLoop);
        }


        //Print coupling entities
//         couplingManager->printWholeCouplingStencil();

        //make the new solution the old solution
        oldSol = *solPointer;
        gridVariablesDarcy->advanceTimeStep();
        gridVariablesVE->advanceTimeStep();
        gridVariablesVEFine->advanceTimeStep();

        //advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        if(!minimalRun)
        {
            //write vtk output. It is IMPORTANT to call the write functions AFTER updating the couplingManager when enabling velocityOutput in the writer!
            vtkWriterDarcy.write(timeLoop->time());
            vtkWriterVECoarse.write(timeLoop->time());
            vtkWriterVEFine.write(timeLoop->time());
        }

        //report statistics of this time step
        timeLoop->reportTimeStep();

        //set new dt as suggested by newton controller
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));


        //assemble and write meta data
        std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorAdaptiveModel<GridGeometry, TimeLoop<Scalar>, CouplingManager>
            (*fvGridGeometryDarcy, *fvGridGeometryVE, *fvGridGeometryVEFine, *timeLoop, *couplingManager, {numCellsDim1,numCellsDim2,numCellsDim3}, enableLenses);
        VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);
    }



    if(!minimalRun)
    {
        using JacobianMatrix = typename Assembler::JacobianMatrix;
        using ResidualVector = typename Assembler::ResidualType;
        auto A = MatrixConverter<JacobianMatrix>::multiTypeToBCRSMatrix(assembler->jacobian()); //jacobian
        auto b = VectorConverter<ResidualVector>::multiTypeToBlockVector(assembler->residual()); //residual
        auto x = VectorConverter<Traits::SolutionVector>::multiTypeToBlockVector(*solPointer); //solVector
        Dune::storeMatrixMarket(A, "Matrix");
        Dune::storeMatrixMarket(b, "Residual");
        Dune::storeMatrixMarket(x, "Solution");
        Dune::writeMatrixToMatlab(A, "A.mat");
        Dune::writeVectorToMatlab(b, "b.mat");
        Dune::writeVectorToMatlab(x, "x.mat");
    }



    //output of last time step
    vtkWriterDarcy.write(timeLoop->time());
    vtkWriterVECoarse.write(timeLoop->time());
    vtkWriterVEFine.write(timeLoop->time());


    //////////////////////////////////////////////////////////////////////////
    // write out a combined vtu for vtu comparison in the testing framework
    //////////////////////////////////////////////////////////////////////////

    std::cout << "Creating combined output. This may take a few seconds..." << std::endl;

    const auto& gridView = gridManager.grid().leafGridView();
    CCTpfaFVGridGeometry<typename HostGrid::LeafGridView> gridGeometry(gridView);
    const auto& bBoxTree = gridGeometry.boundingBoxTree();
    // copy data from the subdomains to full domain data vectors
    std::vector<int> processRank(gridView.size(0), 0); // sequential simulation
    std::vector<double> satNw(gridView.size(0));
    std::vector<double> satW(gridView.size(0));
    std::vector<double> pressureW(gridView.size(0));
    std::vector<double> pressureNw(gridView.size(0));
    std::vector<double> densityW(gridView.size(0));
    std::vector<double> densityNw(gridView.size(0));
    std::vector<double> mobilityW(gridView.size(0));
    std::vector<double> mobilityNw(gridView.size(0));

    //fill FD entries
    for (const auto& element : elements(gridViewDarcy))
    {
        const auto eIdx = fvGridGeometryDarcy->elementMapper().index(element);
        const auto eIdxHost = intersectingEntities(element.geometry().center(), bBoxTree)[0];
        satNw[eIdxHost] = (*solPointer)[darcyDomainIndex][eIdx][1];
        satW[eIdxHost] = 1.0-(*solPointer)[darcyDomainIndex][eIdx][1];
        pressureW[eIdxHost] = (*solPointer)[darcyDomainIndex][eIdx][0];

        using VolumeVariablesFD = typename GetPropType<DarcyFullDTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
        VolumeVariablesFD localVolVarsFD = darcyProblem->returnFDVolVar(element, (*solPointer)[darcyDomainIndex]);
        pressureNw[eIdxHost] = localVolVarsFD.pressure(1);
        densityW[eIdxHost] = localVolVarsFD.density(0);
        densityNw[eIdxHost] = localVolVarsFD.density(1);
        mobilityW[eIdxHost] = localVolVarsFD.mobility(0);
        mobilityNw[eIdxHost] = localVolVarsFD.mobility(1);
    }


    //fill fine VE entries
    problemVEFine->updateFineSol();
    for (const auto& element : elements(gridViewVEFine))
    {
        const auto eIdx = fvGridGeometryVEFine->elementMapper().index(element);
        const auto eIdxHost = intersectingEntities(element.geometry().center(), bBoxTree)[0];
        satNw[eIdxHost] = (*solVEFine)[eIdx][1]; //this only works if VE fine scale volVars are called before at some point
        satW[eIdxHost] = 1.0-(*solVEFine)[eIdx][1]; //this only works if VE fine scale volVars are called before at some point
        pressureW[eIdxHost] = (*solVEFine)[eIdx][0]; //this only works if VE fine scale volVars are called before at some point

        using VolumeVariablesVE = typename GetPropType<VerticalEquilibriumTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
        VolumeVariablesVE localVolVarsVEFine = problemVEFine->returnFineVolVar(element);
        pressureNw[eIdxHost] = localVolVarsVEFine.pressure(1);
        densityW[eIdxHost] = localVolVarsVEFine.density(0);
        densityNw[eIdxHost] = localVolVarsVEFine.density(1);
        mobilityW[eIdxHost] = localVolVarsVEFine.mobility(0);
        mobilityNw[eIdxHost] = localVolVarsVEFine.mobility(1);
    }

    Dune::VTKWriter<typename HostGrid::LeafGridView> vtkWriter(gridView);
    vtkWriter.addCellData(processRank, "process rank");
    vtkWriter.addCellData(satW, "S_liq");
    vtkWriter.addCellData(satNw, "S_gas");
    vtkWriter.addCellData(pressureW, "p_liq");
    vtkWriter.addCellData(pressureNw, "p_gas");
    vtkWriter.addCellData(densityW, "rho_liq");
    vtkWriter.addCellData(densityNw, "rho_gas");
    vtkWriter.addCellData(mobilityW, "mob_liq");
    vtkWriter.addCellData(mobilityNw, "mob_gas");
    std::string combinedOutputName = "combinedFineScaleOutput_adaptive_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    const auto filename = combinedOutputName;
    vtkWriter.write(filename);


    timeLoop->finalize(mpiHelper.getCommunication());

    // output some Newton statistics
    nonLinearSolver.report();

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }


    std::chrono::steady_clock::time_point mainEnd = std::chrono::steady_clock::now();
    const double mainTotalTime = std::chrono::duration_cast<std::chrono::microseconds>(mainEnd - mainStart).count()/1000000.0;

    timeMeasurer.printAllTimes(quantityReconstructorShared->getAverageNumberOfLocalNewtonIterations());
    std::cout << "- Total simulation time:                                                                           " << mainTotalTime << " seconds." << std::endl;
    std::cout << "- Total simulation time MINUS total VE time:                                                       " << mainTotalTime - timeMeasurer.getTotalVETime() << " seconds." << std::endl;
    std::cout << "- Total VE time:                                                                                   " << timeMeasurer.getTotalVETime() << " seconds." << std::endl;

    return 0;
} // end main
