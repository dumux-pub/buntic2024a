// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup VEFullDTests
 * \brief The spatial params for the coupled fullD and verical equilibrium Darcy test.
 */

#ifndef DUMUX_TEST_TWOPDARCYANDVERTICALQUILIBRIUM_SPATIALPARAMS_HH
#define DUMUX_TEST_TWOPDARCYANDVERTICALQUILIBRIUM_SPATIALPARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/porousmediumflow/2p/boxmaterialinterfaces.hh>

namespace Dumux {

/*!
 * \ingroup VEFullDTests
 * \brief The spatial params for the coupled fullD and verical equilibrium Darcy test.
 */
template<class GridGeometry, class Scalar>
class DarcyVESpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, DarcyVESpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = DarcyVESpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CellArray = std::array<unsigned int, dimWorld>;

    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;


public:
    using PermeabilityType = Scalar;

    DarcyVESpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                          const Scalar deltaZ,
                          const bool isCoarseVEModel,
                          const std::string& problemName = "")
        : ParentType(gridGeometry),
          gridGeometry_(gridGeometry),
          myPcKrSwCurve_("SpatialParams"),
          permeabilityFine_ (getParam<Scalar>("SpatialParams.Permeability")),
          porosityFine_ (getParam<Scalar>("SpatialParams.Porosity")),
          deltaZ_(deltaZ),
          isCoarseVEModel_(isCoarseVEModel),
          modelName_(problemName),
          lambda_(getParam<Scalar>("SpatialParams.BrooksCoreyLambda")),
          entryP_(getParam<Scalar>("SpatialParams.BrooksCoreyPcEntry")),
          enableLenses_(getParam<bool>("SpatialParams.EnableLenses", false))
    {
        std::cout << "In spatialparams constructor. modelName: " << modelName_ << std::endl;
    }


//----------------------------------------------------------permeability and porosity calls start here----------------------------------------------------------

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param element fine-scale or coarse-scale element
     * \param scv respective subcontrol volume
     * \param elemSol local solution for this element
     */
    template<class ElementSolution>
    decltype(auto) permeability(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        Scalar dualPermeability = 0.0;
        if(isCoarseVEModel_ == true)
        {
            dualPermeability = permeabilityCoarseAtElement(element);
        }
        else
        {
            dualPermeability = permeabilityFineAtElement(element);
        }

        return dualPermeability;
    }


    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$ for a specific coarse-scale element
     *
     * \param element coarse-scale element
     */
    decltype(auto) permeabilityCoarseAtElement(const Element& element) const
    {
        //it uses fine-scale gridGeometry_ if it is the fine-scale spatialParams, otherwise it uses the coarse-scale quantities
        const unsigned int vectorEntry = gridGeometry_->elementMapper().index(element);
        return permeabilityCoarsePreComputed_[vectorEntry];
    }

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$ for a specific fine-scale element
     *
     * \param element fine-scale element
     */
    decltype(auto) permeabilityFineAtElement(const Element& element) const
    {
        if constexpr(dimWorld==2)
        {
            if(!enableLenses_)
            {
                return permeabilityFine_;
            }
            else
            {
                auto pos = element.geometry().center();
                bool isInLowerLens = pos[1] < 10.0 && pos[0] > 30.0 && pos[0] < 40.0;
                bool isInMiddleLens = pos[1] > 10.0 && pos[1] < 15.0 && pos[0] > 50.0 && pos[0] < 60.0;
                bool isInUpperLens = pos[1] > 22.0 && pos[0] > 70.0 && pos[0] < 85.0;
                if(isInLowerLens || isInMiddleLens || isInUpperLens)
                    return permeabilityFine_*0.00001;
                else
                    return permeabilityFine_;
            }
        }
        else if constexpr(dimWorld==3)
        {
            if(!enableLenses_)
            {
                return permeabilityFine_;
            }
            else
            {
                auto pos = element.geometry().center();
                bool isInLens = pos[0]>20.0 && pos[0]<80 && pos[1]>35.0 && pos[1]<65.0 && pos[dimWorld-1]>15.0;
                if(isInLens)
                    return permeabilityFine_*0.00001;
                else
                    return permeabilityFine_;
            }
        }
    }



    /*!
     * \brief Returns the porosity
     *
     * \param element fine-scale or coarse-scale element
     * \param scv respective subcontrol volume
     * \param elemSol local solution for this element
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        Scalar dualPorosity = 0.0;
        if(isCoarseVEModel_ == true)
        {
            dualPorosity = porosityCoarseAtElement(element);
        }
        else
        {
            dualPorosity = porosityFineAtElement(element);
        }

        return dualPorosity;
    }


    /*!
     * \brief Returns the porosity for a specific coarse-scale element
     *
     * \param element coarse-scale element
     */
    Scalar porosityCoarseAtElement(const Element& element) const
    {
        const unsigned int vectorEntry = gridGeometry_->elementMapper().index(element);
        return porosityCoarsePreComputed_[vectorEntry];
    }

    /*!
     * \brief Returns the porosity for a specific fine-scale element
     *
     * \param element fine-scale element
     */
    Scalar porosityFineAtElement(const Element& element) const
    {
        return porosityFine_;
    }


//==========================================================permeability and porosity calls end here==========================================================
//----------------------------------------------------------permeability and porosity computations start here----------------------------------------------------------

    /*!
     * \brief Calculates the coarse-scale permeability once and stores the values in a container for the coarse-scale gridGeometry
     */
    void calcSpatialParamsPermeabilityAndPorosityCoarse()
    {
        if(isCoarseVEModel_ == true) //coarse gridGeometry
        {
            permeabilityCoarsePreComputed_ = preCalculatePermeabilityCoarse(gridGeometry_);
            porosityCoarsePreComputed_ = preCalculatePorosityCoarse(gridGeometry_);
        }
    }

//-----------------------------------COARSE PERMEABILITY-----------------------------------

    /*!
     * \brief Returns a vector of permeabilities for the coarse-grid \f$[m^2]\f$ elements
     *
     * \param gridGeometry gridGeometry belonging to the coarse-scale grid
     */
    std::vector<PermeabilityType> preCalculatePermeabilityCoarse(std::shared_ptr<const GridGeometry> gridGeometry)
    {
        std::vector<PermeabilityType> storeCoarsePermeabilities(0.0);
        int numberCoarseElements = gridGeometry->elementMapper().size(); //as it is supposed to be a coarse-scale gridGeometry, there is only one row of elements -> size() = #coarse-scale elements
        storeCoarsePermeabilities.resize(numberCoarseElements);

        for (const auto& element : Dune::elements(gridGeometry->gridView()))
        {
            const int coarseElementIdx = gridGeometry->elementMapper().index(element);
            storeCoarsePermeabilities[coarseElementIdx] = calculatePermeabilityCoarseAtElement(element);
        }

        return storeCoarsePermeabilities;
    }


    /*!
     * \brief Returns the intrinsic permeability tensor for the coarse-grid \f$[m^2]\f$ at the given position
     *
     * \param coarseElement the coarse-scale element
     */
    PermeabilityType calculatePermeabilityCoarseAtElement(const Element& coarseElement) const
    {
        const unsigned int coarseElementIdx = gridGeometry_->elementMapper().index(coarseElement);
        typename std::map<int, Element>::const_iterator it = mapColumns_->lower_bound(coarseElementIdx);
        Scalar integratedValue = 0.0;

        for (; it != mapColumns_->upper_bound(coarseElementIdx); ++it) //traverse over all fine-scale elements belonging to this one column
        {
            integratedValue += permeabilityFineAtElement(it->second)*deltaZ_;
        }

        return integratedValue;
    }

//-----------------------------------COARSE POROSITY-----------------------------------

    /*!
     * \brief Returns a vector of porosities for the coarse-grid \f$[-]\f$ elements
     *
     * \param gridGeometry gridGeometry belonging to the coarse-scale grid
     */
    std::vector<Scalar> preCalculatePorosityCoarse(std::shared_ptr<const GridGeometry> gridGeometry)
    {
        std::vector<Scalar> storeCoarsePorosities(0.0);
        int numberElements = gridGeometry->elementMapper().size(); //as it is supposed to be a coarse-scale gridGeometry, there is only one row of elements -> size() = #coarse-scale elements
        storeCoarsePorosities.resize(numberElements);

        for (const auto& element : Dune::elements(gridGeometry->gridView()))
        {
            const int coarseElementIdx = gridGeometry->elementMapper().index(element);
            storeCoarsePorosities[coarseElementIdx] = calculatePorosityCoarseAtElement(element);
        }

        return storeCoarsePorosities;
    }


    /*!
     * \brief Returns the porosity for the coarse-grid \f$[-]\f$ at the given position
     *
     * \param coarseElement the coarse-scale element
     */
    Scalar calculatePorosityCoarseAtElement(const Element& coarseElement) const
    {
        const unsigned int coarseElementIdx = gridGeometry_->elementMapper().index(coarseElement);
        typename std::map<int, Element>::const_iterator it = mapColumns_->lower_bound(coarseElementIdx);
        Scalar integratedValue = 0.0;

        for (; it != mapColumns_->upper_bound(coarseElementIdx); ++it) //traverse over all fine-scale elements belonging to this one column
        {
            integratedValue += porosityFineAtElement(it->second)*deltaZ_;
        }

        return integratedValue;
    }

//==========================================================permeability and porosity computations end here==========================================================

    /*!
     * \brief Returns the parameter object for the capillary-pressure/
     *        saturation material law
     *
     * \param globalPos The global position
     */
    const auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        return makeFluidMatrixInteraction(myPcKrSwCurve_);
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        return FluidSystem::phase0Idx;
    }


    /*
     * Return the object which contains the capillary pressure - saturation and relative permeability - saturation curves
     */
    const PcKrSwCurve getPcKrSwCurve() const
    {
        return myPcKrSwCurve_;
    }


    /*!
     * \brief Sets the multimap mapColumns_ within the spatialParams
     *
     * \param mapColumns mutltimap with column indices as keys and elements within respective column as values
     */
    void setMapColumns(std::shared_ptr<std::multimap<int, Element>> mapColumns)
    {
        mapColumns_ = mapColumns;
    }


    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPosition) const
    {
        return 326.0; // 53°C
    }


    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        return extrusionFactorAtElement(element);
    }


    Scalar extrusionFactorAtElement(const Element& element) const
    {
        return 1.0;
    }


    Scalar getBrooksCoreyEntryP() const
    {
        return entryP_;
    }

    Scalar getBrooksCoreyLambda() const
    {
        return lambda_;
    }

private:
    std::shared_ptr<const GridGeometry> gridGeometry_;
    const PcKrSwCurve myPcKrSwCurve_;

    PermeabilityType permeabilityFine_;
    Scalar porosityFine_;

    Scalar deltaZ_;

    std::vector<PermeabilityType> permeabilityCoarsePreComputed_;
    std::vector<Scalar> porosityCoarsePreComputed_;

    std::shared_ptr<std::multimap<int, Element>> mapColumns_;

    static constexpr Scalar eps_ = 1.5e-7;

    bool isCoarseVEModel_;
    std::string modelName_;

    Scalar lambda_;
    Scalar entryP_;

    bool enableLenses_;
};

} // end namespace Dumux

#endif
