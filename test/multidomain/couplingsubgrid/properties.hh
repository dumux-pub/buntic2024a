// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_TEST_TWOPVE_PROPERTIES_HH
#define DUMUX_TEST_TWOPVE_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/air.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/porousmediumflow/2p/modelfd.hh>
#include <dumux/porousmediumflow/immiscible/localresidual.hh>

#include <dumux/multidomain/couplingsubgrid/model.hh>
#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/couplingsubgrid/couplingmanager.hh>

#include "problemDarcy.hh"
#include "problemVE.hh"
#include "spatialparams_fulld.hh"
#include "spatialparams_ve.hh"

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct CCTpfaTypeTag { using InheritsFrom = std::tuple<CCTpfaModel>; };
struct DarcyTwoP { using InheritsFrom = std::tuple<TwoP, CCTpfaTypeTag>; };
struct VerticalEquilibriumTwoP { using InheritsFrom = std::tuple<TwoPVETemp, CCTpfaTypeTag>; };
} // end namespace TTag


// the coupling manager
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::CCTpfaTypeTag>
{ using type = DarcyDarcyVerticalEquilibriumBoundaryCouplingManager<MultiDomainTraits<Properties::TTag::DarcyTwoP, Properties::TTag::VerticalEquilibriumTwoP>>; };


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::CCTpfaTypeTag>
{
    // using HostGrid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double, 2>>;
    using HostGrid = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>;
    using type = Dune::SubGrid<HostGrid::dimension, HostGrid>;
};


// Set the spatial params property
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyTwoP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DarcyFullDSpatialParams<GridGeometry, Scalar>;
};

// Set the spatial params property
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::VerticalEquilibriumTwoP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DarcyVESpatialParams<GridGeometry, Scalar>;
};

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyTwoP> { using type = DarcyFullDSubProblemOne<TypeTag>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::VerticalEquilibriumTwoP> { using type = VerticalEquilibriumSubProblem<TypeTag>; };

// the local residual containing the analytic derivative methods
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::VerticalEquilibriumTwoP> { using type = ImmiscibleLocalResidual<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::CCTpfaTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::H2O<Scalar> >;
    using NonwettingPhase = FluidSystems::OnePGas<Scalar, Components::CH4<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};

// Enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::CCTpfaTypeTag> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::CCTpfaTypeTag> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::CCTpfaTypeTag> { static constexpr bool value = false; };

// Maybe enable the box-interface solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::CCTpfaTypeTag> { static constexpr bool value = ENABLEINTERFACESOLVER; };
} // end namespace Dumux::Properties

#endif
