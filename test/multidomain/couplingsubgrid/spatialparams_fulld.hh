// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPVETests
 * \brief The spatial params for the incompressible 2p VE test.
 */

#ifndef DUMUX_INCOMPRESSIBLE_DARCYFULLD_SPATIAL_PARAMS_HH
#define DUMUX_INCOMPRESSIBLE_DARCYFULLD_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/porousmediumflow/2p/boxmaterialinterfaces.hh>

namespace Dumux {

/*!
 * \ingroup TwoPVETests
 * \brief The spatial params for the incompressible 2p VE test.
 */
template<class GridGeometry, class Scalar>
class DarcyFullDSpatialParams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, DarcyFullDSpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = DarcyFullDSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, ThisType>;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;

public:
    using PermeabilityType = Scalar;

    DarcyFullDSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry),
      myPcKrSwCurve_("SpatialParams"),
      lambda_(getParam<Scalar>("SpatialParams.BrooksCoreyLambda")),
      entryP_(getParam<Scalar>("SpatialParams.BrooksCoreyPcEntry")),
      enableLenses_(getParam<bool>("SpatialParams.EnableLenses", false))
    {
        // intrinsic permeabilities
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");

        // porosities
        porosity_ = getParam<Scalar>("SpatialParams.Porosity");
    }

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param globalPos The global position
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if constexpr(dimWorld==2)
        {
            if(!enableLenses_)
            {
                return permeability_;
            }
            else
            {
                bool isInLowerLens = globalPos[1] < 10.0 && globalPos[0] > 30.0 && globalPos[0] < 40.0;
                bool isInMiddleLens = globalPos[1] > 10.0 && globalPos[1] < 15.0 && globalPos[0] > 50.0 && globalPos[0] < 60.0;
                bool isInUpperLens = globalPos[1] > 22.0 && globalPos[0] > 70.0 && globalPos[0] < 85.0;
                if(isInLowerLens || isInMiddleLens || isInUpperLens)
                    return permeability_*0.00001;
                else
                    return permeability_;
            }
        }
        else if constexpr(dimWorld==3)
        {
            if(!enableLenses_)
            {
                return permeability_;
            }
            else
            {
                bool isInLens = globalPos[0]>20.0 && globalPos[0]<80 && globalPos[1]>35.0 && globalPos[1]<65.0 && globalPos[dimWorld-1]>15.0;
                if(isInLens)
                    return permeability_*0.00001;
                else
                    return permeability_;
            }
        }
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        return porosity_;
    }


    /*!
     * \brief Returns the parameter object for the capillary-pressure/
     *        saturation material law
     *
     * \param globalPos The global position
     */
    const auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        return makeFluidMatrixInteraction(myPcKrSwCurve_);
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        return FluidSystem::phase0Idx;
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 326.0; // 53°C
    }


    /*
     * Return the object which contains the capillary pressure - saturation and relative permeability - saturation curves
     */
    const PcKrSwCurve getPcKrSwCurve() const
    {
        return myPcKrSwCurve_;
    }

    Scalar getBrooksCoreyEntryP() const
    {
        return entryP_;
    }

    Scalar getBrooksCoreyLambda() const
    {
        return lambda_;
    }


    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        return extrusionFactorAtElement(element);
    }


    Scalar extrusionFactorAtElement(const Element& element) const
    {
        return 1.0;
    }

private:
    const PcKrSwCurve myPcKrSwCurve_;

    Scalar permeability_;
    Scalar porosity_;

    Scalar lambda_;
    Scalar entryP_;

    static constexpr Scalar eps_ = 1.5e-7;

    bool enableLenses_;
};

} // end namespace Dumux

#endif
