// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup VEFullDTests
 * \brief A Darcy test problem.
 */

#ifndef DUMUX_DARCYFULLD_ONE_SUBPROBLEM_HH
#define DUMUX_DARCYFULLD_ONE_SUBPROBLEM_HH

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/timemeasurer/timemeasurements.hh>

#include "boundaryandinitialmanager.hh"

namespace Dumux {

template <class TypeTag>
class DarcyFullDSubProblemOne : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiFluidIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using CellArray = std::array<unsigned int, GridView::dimensionworld>;

    using BoundaryInitialConditionsManager = BoundaryAndInitialConditionsManager<TypeTag>;
    using VolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

public:

    DarcyFullDSubProblemOne(std::shared_ptr<const GridGeometry> gridGeometry,
                            std::shared_ptr<CouplingManager> couplingManager,
                            TimeMeasuring& timeMeasurer,
                            const std::string& modelParamGroup = "")
    : ParentType(gridGeometry, modelParamGroup), eps_(1e-6),
                                                 couplingManager_(couplingManager),
                                                 upperRightDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.UpperRight")),
                                                 lowerLeftDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.LowerLeft")),
                                                 timeMeasurer_(timeMeasurer)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        boundAndInitManager_ = std::make_shared<BoundaryInitialConditionsManager>(couplingManager, lowerLeftDomain_, upperRightDomain_);
    }


    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }


    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        auto selfDomain = CouplingManager::darcyDomainIndex;
        boundAndInitManager_->modifyGlobalBoundaryTypes(values, element, scvf, selfDomain);

        return values;

    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& pos) const
    {
        PrimaryVariables values;

        boundAndInitManager_->dirichletGlobalAtPos(values, pos, *this);

        return values;
    }


     /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        auto selfDomain = CouplingManager::darcyDomainIndex;
        auto otherDomain = CouplingManager::darcyVEDomainIndex;

        if(couplingManager().isCoupled(selfDomain, scvf, otherDomain))
        {
            std::optional<std::chrono::steady_clock::time_point> timeCouplingFluxesStart; //required as placeholder to use in the whole function
            if constexpr(TimeMeasuring::enableTimeMeasuringCouplingFluxesVEToFD)
                timeCouplingFluxesStart = std::chrono::steady_clock::now();


            //coupling values for water-phase
            values[contiFluidIdx] = couplingManager().computeFluxFromVEToDarcy(selfDomain, element, fvGeometry, elemVolVars, scvf, otherDomain, 0);
            //coupling values for gas-phase
            values[contiGasEqIdx] = couplingManager().computeFluxFromVEToDarcy(selfDomain, element, fvGeometry, elemVolVars, scvf, otherDomain, 1);

            if constexpr(TimeMeasuring::enableTimeMeasuringCouplingFluxesVEToFD)
            {
                std::chrono::steady_clock::time_point timeCouplingFluxesEnd = std::chrono::steady_clock::now();
                const double singleRunZpTime = std::chrono::duration_cast<std::chrono::microseconds>(timeCouplingFluxesEnd -
                    *timeCouplingFluxesStart).count();
                timeMeasurer_.addToTotalTimeCouplingFluxesVEToFD(singleRunZpTime);
            }
        }
        else
        {
            boundAndInitManager_->neumannGlobal(values, scvf.center());
        }

        return values;
    }


    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }


   /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        boundAndInitManager_->initialGlobalAtPos(values, globalPos, *this);

        return values;
    }



    //return localized fine-scale volume variable
    VolumeVariables returnFDVolVar(const Element& element,
                                   SolutionVector& solFD) const
    {
        VolumeVariables volVarsFD;

        auto gridGeometryFD = this->gridGeometry();


        auto fvGeometryFD = localView(gridGeometryFD);
        fvGeometryFD.bind(element);
        const auto elemSolFD = elementSolution(element, solFD, gridGeometryFD);

        for (const auto& scv : scvs(fvGeometryFD))
        {
            volVarsFD.update(elemSolFD, *this, element, scv);
        }

        return volVarsFD;
    }


private:
    Scalar eps_;
    std::string problemName_;
    std::shared_ptr<CouplingManager> couplingManager_;

    CellArray upperRightDomain_, lowerLeftDomain_;

    std::shared_ptr<BoundaryInitialConditionsManager> boundAndInitManager_;

    TimeMeasuring &timeMeasurer_;
};
} // end namespace Dumux

#endif
