// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_BOUNDARY_AND_INITIAL_CONDITIONS_MANAGER_HH
#define DUMUX_MULTIDOMAIN_BOUNDARY_AND_INITIAL_CONDITIONS_MANAGER_HH


#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dumux/multidomain/couplingsubgrid/couplingmanager.hh>

namespace Dumux {

template<class TypeTag>
class BoundaryAndInitialConditionsManager
{
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiFluidIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using CellArray = std::array<unsigned int, dimWorld>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;

public:

    static constexpr auto darcyDomainIndex = CouplingManager::darcyDomainIndex;
    static constexpr auto darcyVEDomainIndex = CouplingManager::darcyVEDomainIndex;

    //constructor
    BoundaryAndInitialConditionsManager(std::shared_ptr<CouplingManager> couplingManager,
                                        const CellArray& VELowerLeft,
                                        const CellArray& VEUpperRight)
    {
        couplingManager_ = couplingManager;
        globalLowerLeft_ = VELowerLeft; //LowerLeft of problem0 and problem1 are supposed to be the same!
        globalUpperRight_ = VEUpperRight; //UpperRight of problem0 and problem1 are supposed to be the same!

        //calling it y only makes sense if dim==3
        const Scalar yWidth = getParam<std::vector<Scalar>>("Darcy.Grid.UpperRight")[1] - getParam<std::vector<Scalar>>("Darcy.Grid.LowerLeft")[1];
        auto yCells = getParam<std::vector<Scalar>>("Darcy.Grid.Cells")[1];
        deltaY_ = yWidth/yCells;

        const Scalar zWidth = getParam<std::vector<Scalar>>("Darcy.Grid.UpperRight")[dim-1] - getParam<std::vector<Scalar>>("Darcy.Grid.LowerLeft")[dim-1];
        auto zCells = getParam<std::vector<Scalar>>("Darcy.Grid.Cells")[dim-1];
        deltaZ_ = zWidth/zCells;

        injectionRate_ = getParam<double>("BoundaryConditions.InjectionRate");
    }


    //Set the global boundary conditions here
    template <std::size_t i>
    void modifyGlobalBoundaryTypes(BoundaryTypes& values,
                                   const Element& element,
                                   const SubControlVolumeFace& scvf,
                                   Dune::index_constant<i> domainSelf) const
    {
        values.setAllNeumann();

        if(isOnGlobalRightBoundary_(scvf.center()))
        {
            values.setAllDirichlet();
        }

        if(couplingManager_->isCoupled(domainSelf, scvf))
        {
            values.setAllCouplingNeumann();
        }
    }


    /*!
     * \brief Evaluates the initial conditions for a multidomain setup.
     *
     * \param values primary variables
     * \param globalPos global position of an element belonging to the VE fine scale or fullD.
     * \param myProblem problem of fullD or VE model
     */
    void initialGlobalAtPos(PrimaryVariables& values,
                            const GlobalPosition& globalPos,
                            const Problem& myProblem) const
    {
        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = couplingManager_->problem(darcyVEDomainIndex).returnDensityLiquidAndViscosity(temperature, pressureWInit)[0]; //this function is only implemented in the VE problem to return a density for a given temperature and pressure

        bool enableGravity = getParamFromGroup<bool>(myProblem.paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * myProblem.spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }

        values[saturationGasIdx] = 0.0;
    }


    // /*!
    //  * \brief Check if provided position is located at a vertical non-zero neumann boundary
    //  *
    //  * \param globalPos global position of an element belonging to the VE fine scale or fullD.
    //  */
    // bool isAtVerticalNonZeroNeumann(const GlobalPosition& globalPos) const
    // {
    //     bool indicator = false;
    //     if(isOnGlobalLeftBoundary_(globalPos))
    //     {
    //         indicator = true;
    //     }

    //     return indicator;
    // }


    /*!
     * \brief Evaluates the neunann boundary conditions for a multidomain setup.
     *
     * \param values primary variables
     * \param globalPos global position of an element belonging to the VE fine scale or fullD.
     */
    void neumannGlobal(NumEqVector& values,
                       const GlobalPosition& globalPos) const
    {
        if constexpr(dim==2)
        {
            if (isOnGlobalLeftBoundary_(globalPos))
                values[contiGasEqIdx] = injectionRate_; // kg / (m * s)
        }
        else if constexpr(dim==3)
        {
            // put well at middle of y-direction boundary
            Scalar halfDepthBox = (globalUpperRight_[1] - globalLowerLeft_[1])/2.0;

            if(isOnGlobalLowerBoundary_(globalPos) || isOnGlobalUpperBoundary_(globalPos) || isOnGlobalFrontBoundary_(globalPos) || isOnGlobalBackBoundary_(globalPos))
            {
                values[contiGasEqIdx] = 0.0;
            }
            else if(isOnGlobalLeftBoundary_(globalPos) && halfDepthBox<=(globalPos[1]+deltaY_/2.0) && halfDepthBox>(globalPos[1]-deltaY_/2.0) )
            {
                // std::cout << "injection in cell @: " << globalPos << std::endl;
                // values[contiGasEqIdx] = injectionRate_;
                values[contiGasEqIdx] = injectionRate_/(deltaY_);
            }
        }
    }


    /*!
     * \brief Evaluates the dirichlet boundary conditions for a multidomain setup.
     *
     * \param values primary variables
     * \param globalPos global position of an element belonging to the VE fine scale or fullD.
     * \param myProblem problem of fullD or VE model
     */
    void dirichletGlobalAtPos(PrimaryVariables& values,
                              const GlobalPosition& globalPos,
                              const Problem& myProblem) const
    {
        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = couplingManager_->problem(darcyVEDomainIndex).returnDensityLiquidAndViscosity(temperature, pressureWInit)[0]; //this function is only implemented in the VE problem to return a density for a given temperature and pressure
        bool enableGravity = getParamFromGroup<bool>(myProblem.paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * myProblem.spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }

        values[saturationGasIdx] = 0.0;
    }

private:
    bool isOnGlobalLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < globalLowerLeft_[0] + eps_; }

    bool isOnGlobalRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > globalUpperRight_[0] - eps_; }

    // only used in 3D case
    bool isOnGlobalFrontBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < globalLowerLeft_[1] + eps_; }

    // only used in 3D case
    bool isOnGlobalBackBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > globalUpperRight_[1] - eps_; }

    bool isOnGlobalLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[dim-1] < globalLowerLeft_[dim-1] + eps_; }

    bool isOnGlobalUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[dim-1] > globalUpperRight_[dim-1] - eps_; }


    std::shared_ptr<CouplingManager> couplingManager_;
    CellArray globalLowerLeft_, globalUpperRight_;
    Scalar deltaY_, deltaZ_;
    Scalar injectionRate_;
    Scalar eps_ = 1e-7;
};

} // end namespace Dumux

#endif
