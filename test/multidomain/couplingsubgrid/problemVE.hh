// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup VEFullDTests
 * \brief A Darcy test problem with Vertical Equilibrium model.
 */

#ifndef DUMUX_DARCYVERTICALEQUILIBRIUM_ONE_SUBPROBLEM_HH
#define DUMUX_DARCYVERTICALEQUILIBRIUM_ONE_SUBPROBLEM_HH

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dumux/common/fvproblem.hh>
#include <dumux/verticalequilibriumcommon/quantityreconstruction.hh>
#include <dumux/verticalequilibriumcommon/util.hh>
#include <dumux/verticalequilibriumcommon/quantitystorage.hh>

#include <dumux/multidomain/couplingsubgrid/couplingmanager.hh>
#include <dumux/timemeasurer/timemeasurements.hh>

#include "boundaryandinitialmanager.hh"

namespace Dumux {

template <class TypeTag>
class VerticalEquilibriumSubProblem : public FVProblem<TypeTag>
{
    //Here, the spatialparams take a parameter "paramGroup" which is not handled by the spatialParams constructor used in
    //FVProblemWithSpatialParams. Therefore init spatialParams here and instead of inheriting from FVProblemWithSpatialParams, inherit from FVProblem directly.
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiFluidIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using QuantityReconstructor = TwoPVEQuantityReconst<TypeTag>;
    using CellArray = std::array<unsigned int, dimWorld>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using QuantityStorage = TwoPVEQuantityStorage<TypeTag>;

    using MultiDomainTraits = typename CouplingManager::MultiDomainTraits;

    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;

    using BoundaryInitialConditionsManager = BoundaryAndInitialConditionsManager<TypeTag>;

    static constexpr auto darcyDomainIndex = DarcyDarcyVEDetail::darcyDomainIndex;
    using FullDDomainTypeTag = typename MultiDomainTraits::template SubDomain<darcyDomainIndex>::TypeTag;
    using ProblemFullD = GetPropType<FullDDomainTypeTag, Properties::Problem>;

    using VolumeVariablesFullD = typename GetPropType<FullDDomainTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    using VolumeVariablesVE = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;

public:
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;

    //constructor for the fine-scale problem
    VerticalEquilibriumSubProblem(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                  std::shared_ptr<Problem> problemCoarse,
                                  std::shared_ptr<ProblemFullD> problemFullD)
        : ParentType(gridGeometryFine, problemCoarse->paramGroup()),
          eps_(1e-7),
          isCoarseVEModel_(false),
          couplingManager_(problemCoarse->couplingManagerPtr()),
          numberOfCells_(problemCoarse->getNumberOfCells()),
          upperRightDomain_(problemCoarse->getUpperRightDomain()),
          lowerLeftDomain_(problemCoarse->getLowerLeftDomain()),
          deltaX_(problemCoarse->getDeltaX()),
          deltaY_(problemCoarse->getDeltaY()),
          deltaZ_(problemCoarse->getDeltaZ()),
          quantityReconstructor_(problemCoarse->getQuantityReconstructor()),
          quantityStorage_(problemCoarse->getQuantityStorage()),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometryFine, deltaZ_, isCoarseVEModel_)),
          boundAndInitManager_(problemCoarse->getBoundAndInitManager()),
          problemFullD_(problemFullD),
          timeMeasurer_(problemCoarse->getTimeMeasurer())
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name") + "_fine";
    }


    //constructor for the coarse-scale problem which also takes the gridGeometry of the fine grid
    VerticalEquilibriumSubProblem(std::shared_ptr<const GridGeometry> gridGeometryCoarse,
                                  std::shared_ptr<const GridGeometry> gridGeometryFine,
                                  std::shared_ptr<QuantityReconstructor> quantityReconstructor,
                                  std::shared_ptr<CouplingManager> couplingManager,
                                  std::shared_ptr<ProblemFullD> problemFullD,
                                  TimeMeasuring& timeMeasurer,
                                  const std::string& modelParamGroup = "")
        : ParentType(gridGeometryCoarse, modelParamGroup),
          eps_(1e-7),
          problemName_(getParamFromGroup<std::string>(modelParamGroup, "Problem.Name")),
          isCoarseVEModel_(true),
          couplingManager_(couplingManager),
          numberOfCells_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.FineCells")),
          upperRightDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.UpperRight")),
          lowerLeftDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.LowerLeft")),
          deltaX_(((double)upperRightDomain_[0] - (double)lowerLeftDomain_[0])/(double)numberOfCells_[0]),
          deltaY_(1.0),
          deltaZ_(((double)upperRightDomain_[dim-1] - (double)lowerLeftDomain_[dim-1])/(double)numberOfCells_[dim-1]),
          quantityReconstructor_(quantityReconstructor),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometryCoarse, deltaZ_, isCoarseVEModel_, problemName_)),
          problemFullD_(problemFullD),
          timeMeasurer_(timeMeasurer)
    {
        std::cout << "In problemVE. upperRightDomain_[dim-1]: " << upperRightDomain_[dim-1] << ", lowerLeftDomain_[dim-1]: " << lowerLeftDomain_[dim-1] << ", numberOfCells_[dim-1]: " << numberOfCells_[dim-1] << " and finally deltaZ_: " << deltaZ_ << std::endl;

        std::cout << "in problem: problemName_: " << problemName_ << std::endl;

        if constexpr(dim==3)
            deltaY_ = ((double)upperRightDomain_[1] - (double)lowerLeftDomain_[1])/(double)numberOfCells_[1];

        //construct the member quantityStorage for this problem
        quantityStorage_ = std::make_shared<QuantityStorage>(gridGeometryCoarse, gridGeometryFine);
        mapColumns_ = std::make_shared<std::multimap<int, Element>>(VerticalEquilibriumModelUtil::createVEMultiMapCoarseToFine<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse));
        fineScaleMap_ = std::make_shared<std::map<int, Element>>(VerticalEquilibriumModelUtil::createVEMultiMapFineToCoarse<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse));
        //store the mapColumns into the spatialParams
        setSpatialParamsMapColumns_(mapColumns_);
        //pre-compute permeabilities and porosities for coarse grid and store them to avoid recomputations
        calcPermeabilityAndPorosityCoarseInSpatialParams_();

        boundAndInitManager_ = std::make_shared<BoundaryInitialConditionsManager>(couplingManager, lowerLeftDomain_, upperRightDomain_);
    }


    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }


    const bool& isCoarseVEModel() const
    {
        return isCoarseVEModel_;
    }

//----------------------------------------------------------initial and boundary conditions start here----------------------------------------------------------

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        auto selfDomain = CouplingManager::darcyVEDomainIndex;
        boundAndInitManager_->modifyGlobalBoundaryTypes(values, element, scvf, selfDomain);

        return values;
    }

//-----------------------------------DIRICHLET CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        values = dirichletCoarse(element, scvf);

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment of the coarse-scale grid. Intended for the coarse-scale grid.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichletCoarse(const Element &coarseElement, const SubControlVolumeFace &scvfCoarse) const
    {
        if(this->isCoarseVEModel() == true)
        {
            PrimaryVariables values(0.0);
            const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
            typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
            auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

            //evaluate pressure at bottom of column
            GlobalPosition globalPosFineElementBottom = (it->second).geometry().center();
            globalPosFineElementBottom[dim-1] -= 0.5*deltaZ_; //shift height by 0.5*cellHeight downwards, to evaluate p at the bottom of column and not in fineCell center
            values[pressureH2OIdx] = dirichletFineAtPos(globalPosFineElementBottom, scvfCoarse)[pressureH2OIdx]; //take pressure at bottom of domain as coarse-scale pressure

            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                globalPosFineElement[0] = scvfCoarse.center()[0];

                values[saturationGasIdx] += (dirichletFineAtPos(globalPosFineElement, scvfCoarse)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_; //integration of the saturation, see modelDescription
            }
            values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement); //average saturation by coarse-scale porosity

            return values;
        }
        else
        {
            PrimaryVariables values(0.0);
            GlobalPosition globalPosFineElement = coarseElement.geometry().center(); //in this case coarseElement is a fine element
            values[pressureH2OIdx] = dirichletFineAtPos(globalPosFineElement, scvfCoarse)[pressureH2OIdx];
            values[saturationGasIdx] = dirichletFineAtPos(globalPosFineElement, scvfCoarse)[saturationGasIdx];

            return values;
        }

    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment on the fine-scale grid. Intended for the fine-scale grid. Dirichlet conditions only need to be changed here.
     *
     * \param globalPos The global position of an element belonging to the fine scale.
     */
    PrimaryVariables dirichletFineAtPos(const GlobalPosition& globalPos, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values;
        boundAndInitManager_->dirichletGlobalAtPos(values, scvf.center(), *this);

        return values;
    }

//-----------------------------------NEUMANN CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        auto selfDomain = CouplingManager::darcyVEDomainIndex;
        auto otherDomain = CouplingManager::darcyDomainIndex;

        if(couplingManager().isCoupled(selfDomain, scvf, otherDomain))
        {
            std::optional<std::chrono::steady_clock::time_point> timeCouplingFluxesStart; //required as placeholder to use in the whole function
            if constexpr(TimeMeasuring::enableTimeMeasuringCouplingFluxesFDToVE)
                timeCouplingFluxesStart = std::chrono::steady_clock::now();

            //coupling values for water-phase
            values[contiFluidIdx] = couplingManager().computeFluxFromDarcyToVE(selfDomain, element, fvGeometry, elemVolVars, scvf, otherDomain, 0);
            //coupling values for gas-phase
            values[contiGasEqIdx] = couplingManager().computeFluxFromDarcyToVE(selfDomain, element, fvGeometry, elemVolVars, scvf, otherDomain, 1);


            if constexpr(TimeMeasuring::enableTimeMeasuringCouplingFluxesFDToVE)
            {
                std::chrono::steady_clock::time_point timeCouplingFluxesEnd = std::chrono::steady_clock::now();
                const double singleRunZpTime = std::chrono::duration_cast<std::chrono::microseconds>(timeCouplingFluxesEnd -
                    *timeCouplingFluxesStart).count();
                timeMeasurer_.addToTotalTimeCouplingFluxesFDToVE(singleRunZpTime);
            }
        }
        else
        {
            values = neumannCoarse(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);
        }

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumannCoarse(const Element& element,
                              const FVElementGeometry& fvGeometry,
                              const ElementVolumeVariables& elemVolVars,
                              const ElementFluxVariablesCache& elemFluxVarsCache,
                              const SubControlVolumeFace& scvf) const
    {
        const int columnIdx = this->gridGeometry().elementMapper().index(element); //needs to start at 0

        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);
        NumEqVector values(0.0);

        auto scvfPos = scvf.center();
        if(onLeftBoundary_(scvfPos)) //at vertical boundary
        {
            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                globalPosFineElement[0] = scvfPos[0];
                values += neumannFineAtPos(globalPosFineElement)*deltaZ_;
            }
        }
        else //at horizontal boundaries
        {
            //leave variable "values" at value zero
            values = neumannFineAtPos(scvf.center());
        }

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment on the fine-scale grid. Intended for the fine-scale grid. Neumann conditions only need to be changed here.
     *
     * \param globalPos The position of the integration point of the boundary segment belonging to the fine-scale grid.
     */
    NumEqVector neumannFineAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        boundAndInitManager_->neumannGlobal(values, globalPos);

        return values;
    }


//-----------------------------------INITIAL CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param element The element
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);
        GlobalPosition globalPos = element.geometry().center();

        if(this->isCoarseVEModel() == true)
        {
            values = initialCoarse(element);
        }
        else
        {
            values = initialFineAtPos(globalPos);
        }

        return values;
    }


    /*!
     * \brief Evaluates the initial values for a control volume of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param coarseElement The coarse-scale element
     */
    PrimaryVariables initialCoarse(const Element& coarseElement) const
    {
        PrimaryVariables values(0.0);

        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

        GlobalPosition columnBottomCoordinate = coarseElement.geometry().center(); //create a coordinate at the bottom of the column for the pressure calculation
        columnBottomCoordinate[dim-1] = lowerLeftDomain_[dim-1]; //coordinate at the bottom of the column

        values[pressureH2OIdx] = initialFineAtPos(columnBottomCoordinate)[pressureH2OIdx]; //coarse-scale pressure is supposed to be the fine-scale pressure at the bottom of the column

        for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
        {
            GlobalPosition globalPosFineElement = (it->second).geometry().center();
            values[saturationGasIdx] += (initialFineAtPos(globalPosFineElement)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_;
        }

        values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement);

        return values;
    }


    /*!
     * \brief Evaluates the initial values for a control volume of the fine-scale grid. Intended for the fine-scale grid. Initial conditions only need to be changed here.
     *
     * \param globalPos The global position of the scv belonging to the fine-scale grid.
     */
    PrimaryVariables initialFineAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        boundAndInitManager_->initialGlobalAtPos(values, globalPos, *this);

        return values;
    }

//----------------------------------------------------------initial and boundary conditions end here----------------------------------------------------------

    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }


    /*!
     * \brief Get the coupling manager pointer
     */
    const std::shared_ptr<CouplingManager> couplingManagerPtr() const
    { return couplingManager_; }


    /*!
     * \brief Return the shared pointer to the quantityReconstructor given during initialization of problem
     */
    const std::shared_ptr<QuantityReconstructor> getQuantityReconstructor() const
    {
        return quantityReconstructor_;
    }


    /*!
     * \brief Return the number of cells of the fine-scale domain
     */
    const CellArray getNumberOfCells() const
    {
        return numberOfCells_;
    }


    /*!
     * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
     */
    int getNumberOfCellsX() const
    {
        return numberOfCells_[0];
    }


    // Currently not in use!
    // /*!
    //  * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
    //  */
    // int getNumberOfCellsY() const
    // {
    //     return numberOfCells_[1];
    // }


    /*!
     * \brief Return a shared pointer to the map containing the coarse element and their respective fine-scale elements
     */
    const std::shared_ptr<std::multimap<int, Element>> getMapColumns() const
    {
        return mapColumns_;
    }


    /*!
     * \brief Return a shared pointer to the map containing the coarse element and their respective fine-scale elements
     */
    const std::shared_ptr<std::map<int, Element>> getFineScaleMap() const
    {
        return fineScaleMap_;
    }


    /*!
     * \brief Return the discretization width in x-direction
     */
    const Scalar getDeltaX() const
    {
        return deltaX_;
    }


    /*!
     * \brief Return the discretization width in y-direction
     */
    const Scalar getDeltaY() const
    {
        return deltaY_;
    }


    /*!
     * \brief Return the discretization width in z-direction
     */
    const Scalar getDeltaZ() const
    {
        return deltaZ_;
    }


    /*!
     * \brief Store a shared pointer to the fine-scale problem within the coarse-scale problem and vice versa
     */
    void setOtherProblem(std::shared_ptr<Problem> otherProblem)
    {
        otherProblem_ = otherProblem;
    }


    /*!
     * \brief Return the shared pointer of the fine-scale problem within the coarse-scale problem and vice versa
     */
    std::shared_ptr<Problem> getOtherProblem() const
    {
        return otherProblem_;
    }


    /*!
     * \brief Set the shared pointer to the fine-scale solution vector
     */
    void setFineSolution(std::shared_ptr<SolutionVector> fineSolution)
    {
        fineSolution_ = fineSolution;
    }


    /*!
     * \brief Return the shared pointer to the fine-scale solution vector
     */
    std::shared_ptr<SolutionVector> getFineSolution() const
    {
        return fineSolution_;
    }


    /*!
     * \brief Set the shared pointer to the coarse-scale solution vector
     */
    void setCoarseSolution(std::shared_ptr<SolutionVector> coarseSolution)
    {
        coarseSolution_ = coarseSolution;
    }


    /*!
     * \brief Set the shared pointer to the coarse-scale solution vector
     */
    void setTotalSolution(std::shared_ptr<typename MultiDomainTraits::SolutionVector> totalSolution)
    {
        totalSolution_ = totalSolution;
    }


    /*!
     * \brief Return the shared pointer to the coarse-scale solution vector
     */
    const SolutionVector getCoarseSolution() const
    {
        return (*totalSolution_)[DarcyDarcyVEDetail::darcyVEDomainIndex];
    }


    /*!
     * \brief Return the coordinates of the lower left corner of the domain
     */
    const CellArray getLowerLeftDomain() const
    {
        return lowerLeftDomain_;
    }


    /*!
     * \brief Return the coordinates of the upper right corner of the domain
     */
    const CellArray getUpperRightDomain() const
    {
        return upperRightDomain_;
    }


    /*!
     * \brief Return the pointer to the boundary and initial conditions manager
     */
    std::shared_ptr<BoundaryInitialConditionsManager> getBoundAndInitManager()
    {
        return boundAndInitManager_;
    }


    /*!
     * \brief Sets the time loop pointer.
     */
    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }


    /*!
     * \brief Sets the time loop pointer.
     */
    TimeLoopPtr getTimeLoop() const
    { return timeLoop_;}


    /*!
     * \brief Returns the time.
     */
    Scalar time() const
    { return timeLoop_->time(); }


    /*!
     * \brief Return the pointer to the quantity storage object
     */
    const std::shared_ptr<QuantityStorage> getQuantityStorage() const
    {
        return quantityStorage_;
    }


    /*!
     * \brief Update the quantity storage object given (new) grid geometries
     *
     * \param fvGridGeometryCoarse grid geometry of the VE's coarse scale
     * \param fvGridGeometryFine grid geometry of the VE's fine scale
     */
    void updateQuantityStorage(std::shared_ptr<const GridGeometry> fvGridGeometryCoarse,
                               std::shared_ptr<const GridGeometry> fvGridGeometryFine)
    {
        quantityStorage_->update(fvGridGeometryCoarse, fvGridGeometryFine);
    }


    //! Return a reference to the underlying spatial parameters
    const std::shared_ptr<SpatialParams> getSpatialParams() const
    { return spatialParams_; }


    //! Return a reference to the underlying spatial parameters
    const SpatialParams& spatialParams() const
    { return *spatialParams_; }


    //! Return a reference to the underlying spatial parameters
    SpatialParams& spatialParams()
    { return *spatialParams_; }


    /*!
     * \brief Return the coarse-scale element to which a given fine-scale element (index) belongs
     * \param fineElementIdx index of the fine-scale element
     */
    const Element& returnCorrespondingCoarseElement(int fineElementIdx) const
    {
        return fineScaleMap_->find(fineElementIdx)->second;
    }


    /*!
     * \brief Return an iterator which iterates over all fine element belonging to one certain coarse-scale VE element
     * \param coarseElementIdx index of the coarse-scale element
     */
    auto returnCorrespondingFineCellsIterator(int coarseElementIdx) const
    {
        return mapColumns_->equal_range(coarseElementIdx);
    }


    /*!
     * \brief Copute the density and visocisty for the liquid phase given a temperature and pressure
     * \param temperature temperature at which the density and vicosity should be computed
     * \param pressure pressure at which the density and vicosity should be computed
     */
    template<int isInCouplingFlux=0>
    std::vector<Scalar> returnDensityLiquidAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {WettingPhase::density(temperature,pressure), WettingPhase::viscosity(temperature,pressure)};
    }


    /*!
     * \brief Copute the density and visocisty for the gaseous liquid phase given a temperature and pressure
     * \param temperature temperature at which the density and vicosity should be computed
     * \param pressure pressure at which the density and vicosity should be computed
     */
    template<int isInCouplingFlux=0>
    std::vector<Scalar> returnDensityGasAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {NonwettingPhase::density(temperature,pressure), NonwettingPhase::viscosity(temperature,pressure)};
    }


    /*!
     * \brief Update VE map (that maps coarse-scale elemnt indices to fine-scale elements)
     * \param gridGeometryFine (updated) grid geometry of the VE fine scale
     * \param gridGeometryCoarse (updated) grid geometry of the VE coarse scale
     */
    void updateVEMapCoarseToFine(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                 std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        *mapColumns_ = std::multimap<int, Element>(VerticalEquilibriumModelUtil::createVEMultiMapCoarseToFine<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse));
    }


    /*!
     * \brief Update VE map (that maps fine-scale elemnt indices to coarse-scale elements)
     * \param gridGeometryFine (updated) grid geometry of the VE fine scale
     * \param gridGeometryCoarse (updated) grid geometry of the VE coarse scale
     */
    void updateVEMapFineToCoarse(std::shared_ptr<const GridGeometry> gridGeometryFine,
                                 std::shared_ptr<const GridGeometry> gridGeometryCoarse)
    {
        *fineScaleMap_ = std::map<int, Element>(VerticalEquilibriumModelUtil::createVEMultiMapFineToCoarse<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse));
    }


    /*!
     * \brief Update the (virtual) column saturation for each global column
     * \param verbose flag if verbose output should be enabled
     */
    void updateSwAndMassNwContainer(bool verbose = false)
    {
        if constexpr(dim==2)
        {
            auto gridGeometryFullD = problemFullD_->gridGeometry();
            auto gridGeometryVE = this->gridGeometry();

            currentModelDistribution_ = VerticalEquilibriumModelUtil::getCurrentGlobalToLocalIndex<GridGeometry>(gridGeometryFullD, gridGeometryVE, numberOfCells_[0], deltaX_, deltaY_, verbose);

            VolumeVariablesFullD volVarsFullD;
            VolumeVariablesVE volVarsVE;

            //necessary for fullD columns
            auto fullDMapColumns = VerticalEquilibriumModelUtil::createFullDMultiMapCoarseToFine<Element, GridGeometry, Scalar>(gridGeometryFullD, "Darcy", false);

            for(int globalColumn=0; globalColumn<numberOfCells_[0]; globalColumn++)
            {
                //if VE
                if(currentModelDistribution_[globalColumn].size() == 1)
                {
                    int localVEIndex = currentModelDistribution_[globalColumn][0];
                    auto localVEElement = gridGeometryVE.element(localVEIndex);
                    auto fvGeometryVE = localView(gridGeometryVE);
                    fvGeometryVE.bind(localVEElement);
                    const auto elemSolVE = elementSolution(localVEElement, (*totalSolution_)[CouplingManager::darcyVEDomainIndex], gridGeometryVE);

                    Scalar satW = 0.0;

                    Scalar columnMassNw = 0.0;

                    for (const auto& scv : scvs(fvGeometryVE))
                    {
                        volVarsVE.update(elemSolVE, *this, localVEElement, scv);
                        satW = volVarsVE.saturation(0);
                        columnMassNw = volVarsVE.saturation(1) * scv.volume() * volVarsVE.porosity()/(upperRightDomain_[1]-lowerLeftDomain_[1]) * volVarsVE.density(1);
                    }

                    (*persistentSwContainer_)[globalColumn] = satW;
                    (*persistentMassNwContainer_)[globalColumn] = columnMassNw;
                }
                else //if fullD
                {
                    auto it = fullDMapColumns.lower_bound(globalColumn);

                    Scalar virtualCoarsePW = 0.0;
                    Scalar virtualCoarseVolume = 0.0;
                    Scalar virtualPorosity = 0.0;
                    Scalar localFullDColumnMassNW = 0.0;

                    for(; it != fullDMapColumns.upper_bound(globalColumn); it++)
                    {
                        auto fvGeometryFullD = localView(gridGeometryFullD);
                        fvGeometryFullD.bind(it->second);
                        const auto elemSolFullD = elementSolution(it->second, (*totalSolution_)[CouplingManager::darcyDomainIndex], gridGeometryFullD);

                        for (const auto& scv : scvs(fvGeometryFullD))
                        {
                            volVarsFullD.update(elemSolFullD, *problemFullD_, it->second, scv);

                            virtualCoarseVolume += scv.volume();
                            virtualPorosity += volVarsFullD.porosity()*deltaZ_;

                            auto scvPos = fvGeometryFullD.geometry(scv).center();
                            if(scvPos[1] == deltaZ_/2.0) //TODO: this only holds for TPFA?
                            {
                                // what is the reason behind the 0.5* in the beginning of the second part?
                                // virtualCoarsePW = volVarsFullD.pressure(0) + 0.5*volVarsFullD.density(0)*deltaZ_/2.0*9.81; //TODO: test this
                                virtualCoarsePW = volVarsFullD.pressure(0) + volVarsFullD.density(0)*deltaZ_/2.0*9.81; //TODO: test this
                            }

                            localFullDColumnMassNW += volVarsFullD.porosity() * volVarsFullD.density(1) * volVarsFullD.saturation(1) * scv.volume();
                        }
                    }

                    auto conservativeSatNw = VerticalEquilibriumModelUtil::computeConservativeSatNWHelper<Scalar, Problem>(localFullDColumnMassNW, virtualCoarsePW, *this, virtualCoarseVolume, virtualPorosity, upperRightDomain_[1], lowerLeftDomain_[1]);
                    (*persistentSwContainer_)[globalColumn] = 1.0 - conservativeSatNw;
                    (*persistentMassNwContainer_)[globalColumn] = localFullDColumnMassNW;
                }
            }
        }
        else if constexpr(dim==3)
        {
            //TODO: fix this
            std::cout << "!!!!updateSwAndMassNwContainer was not implemented for 3D yet!!!!" << std::endl;
        }

        if(verbose)
        {
            for(size_t i=0; i<persistentSwContainer_->size(); i++)
            {
                std::cout << "globalIdx: " << i << ", here sw is: " << (*persistentSwContainer_)[i] << std::endl;
            }
        }
    }


    /*!
     * \brief Initialize the pointer to the global saturation container
     *
     * \param swdc wetting-phase saturation derivative container
     */
    void initializeSwContainer(std::shared_ptr<std::vector<double>> swc)
    {
        persistentSwContainer_ = swc;
    }


    /*!
     * \brief Initialize the pointer to the global saturation container
     *
     * \param swdc wetting-phase saturation derivative container
     */
    void initializeMassNwContainer(std::shared_ptr<std::vector<double>> mnwc)
    {
        persistentMassNwContainer_ = mnwc;
    }


    /*!
     * \brief Compute the wetting-phase saturation derivative on the coarse scale for all inner elements via central difference quotient and one-sided diffeerence quotients for the boundary elements
     *
     * \param coarsePos position of the coarse-scale element
     */
    Scalar computeCentralDerivativeSw(const GlobalPosition& coarsePos) const
    {
        //persistentSwContainer_ is initially created with a value of 1.0, so this function also works without previous updates of persistentSwContainer_
        Scalar swDerivative = 0.0;

        if constexpr(dim==2)
        {
            size_t globalColumnIdx = (coarsePos[0]-0.5*deltaX_)/(deltaX_);

            //this function will only be called for VE cells, so don't need to check if self (at globalColumnIdx) is VE or fullD, should always be VE
            //also isolated columns are not allowed, meaning if a fullD column is found at one side, on the other side of self cannot be another fullD column
            if(globalColumnIdx>0 && globalColumnIdx<(numberOfCells_[0]-1))
            {
                if(currentModelDistribution_[globalColumnIdx-1].size()>1)
                    swDerivative = ((*persistentSwContainer_)[globalColumnIdx+1] - (*persistentSwContainer_)[globalColumnIdx])/(deltaX_); //right-sided quotient
                else if(currentModelDistribution_[globalColumnIdx+1].size()>1)
                    swDerivative = ((*persistentSwContainer_)[globalColumnIdx] - (*persistentSwContainer_)[globalColumnIdx-1])/(deltaX_); //right-sided quotient
                else
                    swDerivative = ((*persistentSwContainer_)[globalColumnIdx+1] - (*persistentSwContainer_)[globalColumnIdx-1])/(2.0*deltaX_); //central quotient
            }
            else if(globalColumnIdx==0) //should never occur, because injection columns should always be fullD?
            {
                swDerivative = ((*persistentSwContainer_)[globalColumnIdx+1] - (*persistentSwContainer_)[globalColumnIdx])/(deltaX_); //right-sided quotient
            }
            else if(globalColumnIdx == (numberOfCells_[0]-1))
            {
                swDerivative = ((*persistentSwContainer_)[globalColumnIdx] - (*persistentSwContainer_)[globalColumnIdx-1])/(deltaX_);
            }
        }
        else if constexpr(dim==3)
        {
            static bool alreadyPrinted = false;
            //TODO: fix this
            if(alreadyPrinted==false)
            {
                std::cout << "!!!!computeCentralDerivativeSw was not implemented for 3D yet!!!!" << std::endl;
                alreadyPrinted = true;
            }
        }

        return swDerivative;
    }


    /*!
     * \brief Compute the wetting-phase saturation derivative on the coarse scale for all inner elements via central difference quotient and one-sided diffeerence quotients for the boundary elements
     *
     * \param coarsePos position of the coarse-scale element
     */
    Scalar computeCentralDerivativeMassNw(const GlobalPosition& coarsePos) const
    {
        Scalar massNwDerivative = 0.0;

        if constexpr(dim==2)
        {
            size_t globalColumnIdx = (coarsePos[0]-0.5*deltaX_)/(deltaX_);
            if(globalColumnIdx>0 && globalColumnIdx<(numberOfCells_[0]-1))
            {
                massNwDerivative = std::abs(((*persistentMassNwContainer_)[globalColumnIdx+1] - (*persistentMassNwContainer_)[globalColumnIdx-1])/(2.0*deltaX_));
            }
            else if(globalColumnIdx==0)
            {
                massNwDerivative = std::abs(((*persistentMassNwContainer_)[globalColumnIdx+1] - (*persistentMassNwContainer_)[globalColumnIdx])/(deltaX_));
            }
            else if(globalColumnIdx == (numberOfCells_[0]-1))
            {
                massNwDerivative = std::abs(((*persistentMassNwContainer_)[globalColumnIdx] - (*persistentMassNwContainer_)[globalColumnIdx-1])/(deltaX_));
            }
        }
        else if constexpr(dim==3)
        {
            //TODO: fix this
            std::cout << "!!!!computeCentralDerivativeMassNw was not implemented for 3D yet!!!!" << std::endl;
        }
        

        return massNwDerivative;
    }


    TimeMeasuring& getTimeMeasurer() const
    {
        return timeMeasurer_;
    }


    //call on fine scale problem
    void updateFineSol()
    {
        VolumeVariablesVE volVarsVE;

        auto gridGeometryVEFine = this->gridGeometry();
        auto numElements = gridGeometryVEFine.numScv(); //only true for CCTPFA

        for(int localVEIndex=0; localVEIndex<numElements; localVEIndex++)
        {
            auto localVEFineElement = gridGeometryVEFine.element(localVEIndex);
            auto fvGeometryVEFine = localView(gridGeometryVEFine);
            fvGeometryVEFine.bind(localVEFineElement);
            const auto elemSolVEFine = elementSolution(localVEFineElement, (*fineSolution_), gridGeometryVEFine);

            for (const auto& scv : scvs(fvGeometryVEFine))
            {
                volVarsVE.update(elemSolVEFine, *this, localVEFineElement, scv);
                (*fineSolution_)[localVEIndex][0] = volVarsVE.pressure(0);
                (*fineSolution_)[localVEIndex][1] = volVarsVE.saturation(1);
            }
        }
    }


    //return localized fine-scale volume variable
    VolumeVariablesVE returnFineVolVar(const Element& element) const
    {
        // using VolumeVariablesVE = typename GetPropType<VerticalEquilibriumTypeTag, Properties::GridVolumeVariables>::VolumeVariables;
        VolumeVariablesVE volVarsVEFine;

        auto gridGeometryVEFine = this->gridGeometry();


        auto fvGeometryVEFine = localView(gridGeometryVEFine);
        fvGeometryVEFine.bind(element);
        const auto elemSolVEFine = elementSolution(element, (*fineSolution_), gridGeometryVEFine);

        for (const auto& scv : scvs(fvGeometryVEFine))
        {
            volVarsVEFine.update(elemSolVEFine, *this, element, scv);
        }

        return volVarsVEFine;
    }


private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    // only used in 3D case
    bool onFrontBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    // only used in 3D case
    bool onBackBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - eps_; }

    /*!
     * \brief Exports the multimap mapColumns_ to the spatialParams
     *
     * \param mapColumns mutltimap with column indices as keys and elements within respective column as values
     */
    void setSpatialParamsMapColumns_(std::shared_ptr<std::multimap<int, Element>> mapColumns)
    {
        this->spatialParams().setMapColumns(mapColumns);
    }

    /*!
     * \brief Calculates the coarse permeability in spatial params once and stores the values in a container
     */
    void calcPermeabilityAndPorosityCoarseInSpatialParams_()
    {
        this->spatialParams().calcSpatialParamsPermeabilityAndPorosityCoarse();
    }

    Scalar eps_;
    std::string problemName_;
    bool isCoarseVEModel_;
    std::shared_ptr<CouplingManager> couplingManager_;

    CellArray numberOfCells_, upperRightDomain_, lowerLeftDomain_;
    Scalar deltaX_, deltaY_, deltaZ_;
    std::shared_ptr<std::multimap<int, Element>> mapColumns_;
    std::shared_ptr<std::map<int, Element>> fineScaleMap_;

    std::shared_ptr<QuantityReconstructor> quantityReconstructor_;
    std::shared_ptr<QuantityStorage> quantityStorage_;

    std::shared_ptr<Problem> otherProblem_;

    std::shared_ptr<SolutionVector> fineSolution_, coarseSolution_;

    std::shared_ptr<typename MultiDomainTraits::SolutionVector> totalSolution_;

    TimeLoopPtr timeLoop_;

    //! Spatially varying parameters
    std::shared_ptr<SpatialParams> spatialParams_;

    std::shared_ptr<BoundaryInitialConditionsManager> boundAndInitManager_;

    std::shared_ptr<std::vector<double>> persistentSwContainer_;
    std::shared_ptr<std::vector<double>> persistentMassNwContainer_;

    std::shared_ptr<ProblemFullD> problemFullD_;

    std::vector<std::vector<size_t>> currentModelDistribution_;

    TimeMeasuring &timeMeasurer_;
};

} // end namespace Dumux

#endif
