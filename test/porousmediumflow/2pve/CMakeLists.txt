dune_symlink_to_source_files(FILES "params.input")
dune_symlink_to_source_files(FILES "adaptivity_paper_generate_results_pure_ve.py")
dune_symlink_to_source_files(FILES "adaptivity_paper_generate_results_pure_ve_3D.py")

# set(CMAKE_BUILD_TYPE Debug)

dumux_add_test(NAME test_2p_ve_compressible_tpfa
              SOURCES main.cc
              LABELS porousmediumflow 2pve)