// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_TEST_TWOPVE_PROBLEM_HH
#define DUMUX_TEST_TWOPVE_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/common/fvproblem.hh>
#include <dumux/verticalequilibriumcommon/quantitystorage.hh>
#include <dumux/timemeasurer/timemeasurements.hh>

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux {

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPVETestProblem : public FVProblem<TypeTag>
{
    //Here, the spatialparams take a parameter "paramGroup" which is not handled by the spatialParams constructor used in
    //FVProblemWithSpatialParams. Therefore init spatialParams here and instead of inheriting from FVProblemWithSpatialParams, inherit from FVProblem directly.
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using QuantityReconstructor = TwoPVEQuantityReconst<TypeTag>;

    using CellArray = std::array<unsigned int, dimWorld>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    using QuantityStorage = TwoPVEQuantityStorage<TypeTag>;

    using WettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::WettingPhase;
    using NonwettingPhase = typename GetProp<TypeTag, Properties::FluidSystem>::NonwettingPhase;

    using MyVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;

public:
    using SpatialParams = GetPropType<TypeTag, Properties::SpatialParams>;


    //constructor for the fine-scale problem
    TwoPVETestProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                      std::shared_ptr<const Problem> problemCoarse)
        : ParentType(gridGeometry, problemCoarse->paramGroup()),
          isCoarseVEModel_(false),
          numberOfCells_(problemCoarse->getNumberOfCells()),
          upperRightDomain_(problemCoarse->getUpperRightDomain()),
          lowerLeftDomain_(problemCoarse->getLowerLeftDomain()),
          deltaX_(problemCoarse->getDeltaX()),
          deltaY_(problemCoarse->getDeltaY()),
          deltaZ_(problemCoarse->getDeltaZ()),
          quantityReconstructor_(problemCoarse->getQuantityReconstructor()),
          quantityStorage_(problemCoarse->getQuantityStorage()),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometry, deltaZ_, isCoarseVEModel_)),
          timeMeasurer_(problemCoarse->getTimeMeasurer()),
          injectionRate_(getParam<double>("BoundaryConditions.InjectionRate"))
    {
        problemName_  =  getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name") + "_fine";
    }

    //constructor for the coarse-scale problem which also takes the gridGeometry of the fine grid
    TwoPVETestProblem(std::shared_ptr<const GridGeometry> gridGeometryCoarse,
                      std::shared_ptr<const GridGeometry> gridGeometryFine,
                      std::shared_ptr<QuantityReconstructor> quantityReconstructor,
                      TimeMeasuring& timeMeasurer,
                      const std::string& modelParamGroup = "")
        : ParentType(gridGeometryCoarse, modelParamGroup),
          problemName_(getParamFromGroup<std::string>(modelParamGroup, "Problem.Name")),
          isCoarseVEModel_(true),
          numberOfCells_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.FineCells")),
          upperRightDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.UpperRight")),
          lowerLeftDomain_(getParamFromGroup<CellArray>(modelParamGroup, "Grid.LowerLeft")),
          deltaX_(((double)upperRightDomain_[0] - (double)lowerLeftDomain_[0])/(double)numberOfCells_[0]),
          deltaY_(1.0),
          deltaZ_(((double)upperRightDomain_[dim-1] - (double)lowerLeftDomain_[dim-1])/(double)numberOfCells_[dim-1]),
          quantityReconstructor_(quantityReconstructor),
          spatialParams_(std::make_shared<SpatialParams>(gridGeometryCoarse, deltaZ_, isCoarseVEModel_, problemName_)),
          timeMeasurer_(timeMeasurer),
          injectionRate_(getParamFromGroup<double>(modelParamGroup, "BoundaryConditions.InjectionRate"))
    {
        if constexpr(dim==3)
        {
            deltaY_ = ((double)upperRightDomain_[1] - (double)lowerLeftDomain_[1])/(double)numberOfCells_[1];
        }

        std::cout << "in problem: problemName_: " << problemName_ << std::endl;
        quantityStorage_ = std::make_shared<QuantityStorage>(gridGeometryCoarse, gridGeometryFine);
        //create and store a multimap which includes the columnIdx as the key and maps it to all the fine-scale elements belonging to this column as a shared_ptr
        mapColumns_ = std::make_shared<std::multimap<int, Element>>(VerticalEquilibriumModelUtil::createVEMultiMapCoarseToFine<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse)); //key: coarse element idx, values: fine elements
        fineScaleMap_ = std::make_shared<std::map<int, Element>>(VerticalEquilibriumModelUtil::createVEMultiMapFineToCoarse<Element, GridGeometry>(gridGeometryFine, gridGeometryCoarse)); //key: fine element idx, values: coarse element
        setSpatialParamsMapColumns_(mapColumns_);
        calcPermeabilityAndPorosityCoarseInSpatialParams_();

        // VerticalEquilibriumModelUtil::printMultimap<Element>(*mapColumns_);
        // VerticalEquilibriumModelUtil::printMap<Element>(*fineScaleMap_);
    }


    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    const bool& isCoarseVEModel() const
    {
        return isCoarseVEModel_;
    }

//----------------------------------------------------------initial and boundary conditions start here----------------------------------------------------------

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

//-----------------------------------DIRICHLET CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const //without volvars caching, Dirichlet and Neumann function are only called on the coarse scale
    {
        PrimaryVariables values(0.0);

        values = dirichletCoarse(element, scvf);

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment of the coarse-scale grid. Intended for the coarse-scale grid.
     *
     * \param coarseElement The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     */
    PrimaryVariables dirichletCoarse(const Element &coarseElement, const SubControlVolumeFace &scvfCoarse) const
    {
        if(this->isCoarseVEModel() == true)
        {
            PrimaryVariables values(0.0);

            const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0

//             std::cout << "before map with columnIdx: " << columnIdx << std::endl;

            typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
            auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

//             std::cout << "after map" << std::endl;

            //evaluate pressure at bottom of column
            GlobalPosition globalPosFineElementBottom = (it->second).geometry().center();
            globalPosFineElementBottom[dim-1] -= 0.5*deltaZ_; //shift height by 0.5*cellHeight downwards, to evaluate p at the bottom of column and not in fineCell center
            values[pressureH2OIdx] = dirichletFineAtPos(globalPosFineElementBottom)[pressureH2OIdx]; //take pressure at bottom of domain as coarse-scale pressure

            for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
            {
                GlobalPosition globalPosFineElement = (it->second).geometry().center();
                globalPosFineElement[0] = scvfCoarse.center()[0];

                values[saturationGasIdx] += (dirichletFineAtPos(globalPosFineElement)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_; //integration of the saturation, see modelDescription
            }

            values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement); //average saturation by coarse-scale porosity

            return values;
        }
        else
        {
            PrimaryVariables values(0.0);
            GlobalPosition globalPosFineElement = coarseElement.geometry().center(); //in this case coarseElement is a fine element
            values[pressureH2OIdx] = dirichletFineAtPos(globalPosFineElement)[pressureH2OIdx];
            values[saturationGasIdx] = dirichletFineAtPos(globalPosFineElement)[saturationGasIdx];
//             values = dirichletFineAtPos(globalPos);

            return values;
        }

    }


    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment on the fine-scale grid. Intended for the fine-scale grid. Dirichlet conditions only need to be changed here.
     *
     * \param globalPos The global position of an element belonging to the fine scale.
     */
    PrimaryVariables dirichletFineAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;

        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = this->returnDensityLiquidAndViscosity(temperature, pressureWInit)[0];

        bool enableGravity = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }

        values[saturationGasIdx] = 0.0;

        return values;
    }

//-----------------------------------NEUMANN CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        values = neumannCoarse(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);

        return values;
    }


     /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumannCoarse(const Element& coarseElement,
                              const FVElementGeometry& fvGeometry,
                              const ElementVolumeVariables& elemVolVars,
                              const ElementFluxVariablesCache& elemFluxVarsCache,
                              const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

//         GlobalPosition globalPosCoarseScvf = scvf.geometry().center();
        GlobalPosition globalPosCoarseScvf = scvf.center();

        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

        if(onLeftBoundary_(globalPosCoarseScvf))
        {
           for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
           {
               GlobalPosition globalPosFineElement = (it->second).geometry().center();
//                globalPosFineElement[0] = scvf.geometry().center()[0];
               globalPosFineElement[0] = scvf.center()[0];
               values += neumannFineAtPos(globalPosFineElement)*deltaZ_;
           }
        }

        return values;
    }


    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment on the fine-scale grid. Intended for the fine-scale grid. Neumann conditions only need to be changed here.
     *
     * \param globalPos The position of the integration point of the boundary segment belonging to the fine-scale grid.
     */
    NumEqVector neumannFineAtPos(const GlobalPosition& globalPos) const
    {
        // NumEqVector values(0.0);
        // if (onLeftBoundary_(globalPos)) //TODO: ? simulation is 1s faster if kept in? But the check is already done in the function neumann(), should not be necessary here anymore
        // {
        //     values[contiGasEqIdx] = getParam<double>("BoundaryConditions.InjectionRate"); // kg / (m * s)
        // }


        if constexpr(dim==2)
        {
            NumEqVector values(0.0);
            if (onLeftBoundary_(globalPos))
                values[contiGasEqIdx] = injectionRate_; // kg / (m * s)
            return values;
        }
        else if constexpr(dim==3)
        {
            NumEqVector values(0.0);
            // put well at middle of y-direction boundary
            Scalar halfDepthBox = (this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1])/2.0;

            if(onLeftBoundary_(globalPos) && halfDepthBox<=(globalPos[1]+deltaY_/2.0) && halfDepthBox>(globalPos[1]-deltaY_/2.0) )
            {
                // std::cout << "injection in cell @: " << globalPos << std::endl;
                values[contiGasEqIdx] = injectionRate_/(deltaY_);
                // values[contiGasEqIdx] = injectionRate_;
            }

            return values;
        }
    }

//-----------------------------------INITIAL CONDITIONS-----------------------------------

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param element The element for which the initial condition is set
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);
        GlobalPosition globalPos = element.geometry().center();

        if(this->isCoarseVEModel() == true)
        {
            values = initialCoarse(element);
        }
        else
        {
            values = initialFineAtPos(globalPos);
        }

        return values;
    }


        /*!
     * \brief Evaluates the initial values for a control volume of the coarse-scale grid. Intended for the fine-scale grid.
     *
     * \param coarseElement The coarse-scale element
     */
    PrimaryVariables initialCoarse(const Element& coarseElement) const
    {
        PrimaryVariables values(0.0);

        const int columnIdx = this->gridGeometry().elementMapper().index(coarseElement); //needs to start at 0
        typename std::map<int, Element>::const_iterator it = (*mapColumns_).lower_bound(columnIdx);
        auto upperBoundIterator = (*mapColumns_).upper_bound(columnIdx);

        GlobalPosition columnBottomCoordinate = coarseElement.geometry().center(); //create a coordinate at the bottom of the column for the pressure calculation
        columnBottomCoordinate[dim-1] = lowerLeftDomain_[dim-1]; //coordinate at the bottom of the column

        values[pressureH2OIdx] = initialFineAtPos(columnBottomCoordinate)[pressureH2OIdx]; //coarse-scale pressure is supposed to be the fine-scale pressure at the bottom of the column

        for (; it != upperBoundIterator; ++it) //traverse over all fine-scale elements belonging to this one column
        {
            GlobalPosition globalPosFineElement = (it->second).geometry().center();
            values[saturationGasIdx] += (initialFineAtPos(globalPosFineElement)[saturationGasIdx] * this->spatialParams().porosityFineAtElement(it->second))*deltaZ_;
        }

        values[saturationGasIdx] /= this->spatialParams().porosityCoarseAtElement(coarseElement);

        return values;
    }


    /*!
     * \brief Evaluates the initial values for a control volume of the fine-scale grid. Intended for the fine-scale grid. Initial conditions only need to be changed here.
     *
     * \param globalPos The global position of the scv belonging to the fine-scale grid.
     */
    PrimaryVariables initialFineAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        Scalar temperature = 326.0;
        Scalar pressureWInit = 1.0e7;

        Scalar densityW = this->returnDensityLiquidAndViscosity(temperature, pressureWInit)[0];

        bool enableGravity = getParamFromGroup<bool>(this->paramGroup(), "Problem.EnableGravity");

        if(enableGravity==true)
        {
            values[pressureH2OIdx] = pressureWInit - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
        }
        else
        {
            values[pressureH2OIdx] = pressureWInit;
        }
        values[saturationGasIdx] = 0.0;

        return values;
    }


//----------------------------------------------------------initial and boundary conditions end here----------------------------------------------------------


    /*!
     * \brief Return the shared pointer to the quantityReconstructor given during initialization of problem
     */
    const std::shared_ptr<QuantityReconstructor> getQuantityReconstructor() const
    {
        return quantityReconstructor_;
    }


    /*!
     * \brief Return the number of cells of the fine-scale domain
     */
    const CellArray getNumberOfCells() const
    {
        return numberOfCells_;
    }


    // /*!
    //  * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
    //  */
    // int getNumberOfCellsX() const
    // {
    //     return numberOfCells_[0];
    // }

    // /*!
    //  * \brief Return the number of cells in x direction which is also the number of elements in the coarse grid
    //  */
    // int getNumberOfCellsY() const
    // {
    //     return numberOfCells_[1];
    // }

    /*!
     * \brief Return a shared pointer to the map containing the coarse element and their respective fine-scale elements
     */
    const std::shared_ptr<std::multimap<int, Element>> getMapColumns() const
    {
        return mapColumns_;
    }

    /*!
     * \brief Return the discretization width in x-direction
     */
    const Scalar getDeltaX() const
    {
        return deltaX_;
    }

    /*!
     * \brief Return the discretization width in y-direction
     */
    const Scalar getDeltaY() const
    {
        return deltaY_;
    }

    /*!
     * \brief Return the discretization width in z-direction
     */
    const Scalar getDeltaZ() const
    {
        return deltaZ_;
    }



    /*!
     * \brief Store a shared pointer to the fine-scale problem within the coarse-scale problem and vice versa
     */
    void setOtherProblem(std::shared_ptr<Problem> otherProblem)
    {
        otherProblem_ = otherProblem;
    }

    /*!
     * \brief Return the shared pointer of the fine-scale problem within the coarse-scale problem and vice versa
     */
    std::shared_ptr<Problem> getOtherProblem() const
    {
        return otherProblem_;
    }


    /*!
     * \brief Set the shared pointer to the fine-scale solution vector
     */
    void setFineSolution(std::shared_ptr<SolutionVector> fineSolution)
    {
        fineSolution_ = fineSolution;
    }

    /*!
     * \brief Return the shared pointer to the fine-scale solution vector
     */
    std::shared_ptr<SolutionVector> getFineSolution() const
    {
        return fineSolution_;
    }


    /*!
     * \brief Set the shared pointer to the coarse-scale solution vector
     */
    void setCoarseSolution(std::shared_ptr<SolutionVector> coarseSolution)
    {
        coarseSolution_ = coarseSolution;
    }

    /*!
     * \brief Return the shared pointer to the coarse-scale solution vector
     */
    SolutionVector getCoarseSolution() const
    {
        return *coarseSolution_;
    }


    /*!
     * \brief Return the coordinates of the lower left corner of the domain
     */
    const CellArray getUpperRightDomain() const
    {
        return upperRightDomain_;
    }

    /*!
     * \brief Return the coordinates of the lower left corner of the domain
     */
    const CellArray getLowerLeftDomain() const
    {
        return lowerLeftDomain_;
    }



    /*!
     * \brief Sets the time loop pointer.
     */
    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    /*!
     * \brief Sets the time loop pointer.
     */
    const TimeLoopPtr getTimeLoop() const
    { return timeLoop_; }

    /*!
     * \brief Returns the time.
     */
    Scalar time() const
    { return timeLoop_->time(); }


    const std::shared_ptr<QuantityStorage> getQuantityStorage() const
    {
        return quantityStorage_;
    }


    //! Return a reference to the underlying spatial parameters
    const std::shared_ptr<SpatialParams> getSpatialParams() const
    { return spatialParams_; }

    //! Return a reference to the underlying spatial parameters
    const SpatialParams& spatialParams() const
    { return *spatialParams_; }

    //! Return a reference to the underlying spatial parameters
    SpatialParams& spatialParams()
    { return *spatialParams_; }


    std::vector<Scalar> returnDensityLiquidAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {WettingPhase::density(temperature,pressure), WettingPhase::viscosity(temperature,pressure)};
    }

    std::vector<Scalar> returnDensityGasAndViscosity(const Scalar& temperature, const Scalar& pressure) const
    {
        return {NonwettingPhase::density(temperature,pressure), NonwettingPhase::viscosity(temperature,pressure)};
    }


    /*!
     * \brief Compute the wetting-phase saturation derivative on the coarse scale for all inner elements (left and right boundary element are excluded) via central difference quotient
     *
     * \param elementIdx index of the element for which the capillary pressure gradient should be computed
     */
    Scalar computeCentralDerivativeSw(const GlobalPosition& coarsePos) const
    {
        if constexpr(dim==2)
        {
            size_t elementIdx = (coarsePos[0]-0.5*deltaX_)/(deltaX_);
            Scalar swDerivative = 0.0;
            if(elementIdx > 0 && elementIdx < numberOfCells_[0]-2) //TODO: why -2 and not -1?
            {
                //left
                Scalar leftSw = 1.0 - (*coarseSolution_)[elementIdx-1][1];
                //right
                Scalar rightSw = 1.0 - (*coarseSolution_)[elementIdx+1][1];
                swDerivative = std::abs((rightSw - leftSw)/(2*deltaX_));
            }

            return swDerivative;
        }
        else if constexpr(dim==3)
        {
            size_t xIdx = (coarsePos[0]-0.5*deltaX_)/(deltaX_);
            size_t yIdx = (coarsePos[1]-0.5*deltaY_)/(deltaY_);
            size_t xCells = numberOfCells_[0];
            size_t yCells = numberOfCells_[1];
            size_t elementIdx = xIdx + yIdx*xCells;

            Scalar swDerivative = 0.0;
            if(xIdx > 0 && xIdx < xCells-1 && yIdx > 0 && yIdx < yCells-1)
            {
                //left
                Scalar leftSw = 1.0 - (*coarseSolution_)[elementIdx-1][1];
                //right
                Scalar rightSw = 1.0 - (*coarseSolution_)[elementIdx+1][1];
                //bottom
                Scalar bottomSw = 1.0 - (*coarseSolution_)[elementIdx-xCells][1];
                //top
                Scalar topSw = 1.0 - (*coarseSolution_)[elementIdx+xCells][1];

                // swDerivative = std::abs((rightSw - leftSw)/(2*deltaX_));
                swDerivative = -1.0; //TODO: in 2D one needs to compute derivative in x AND y direction, how to handle this?
            }

            return swDerivative;
        }
        
    }


    /*!
     * \brief Store information on the compressibility coefficients introduced in Bo Guo's "A vertically integrated model with vertical dynamics
for CO 2 storage".
     */
    void infoOnCompressibilityCoeff()
    {
        Scalar waterPressure_lowerBound = 9.76716e+06;
        Scalar waterPressure_upperBound = 1.02679e+07;

        Scalar gasPressure_lowerBound = 9.86716e+06;
        Scalar gasPressure_upperBound = 1.03766e+07;

        int numberOfEvalPoints = 100;
        Scalar waterDeltaP = (waterPressure_upperBound-waterPressure_lowerBound)/(double)numberOfEvalPoints;
        Scalar gasDeltaP = (gasPressure_upperBound-gasPressure_lowerBound)/(double)numberOfEvalPoints;

        Scalar varEps = 1e-8;
        Scalar temp = 326.0;


        std::vector<std::string> labelList = {"c_w", "c_g"};
        std::string fileName = "compr_coeffs.txt";

        VerticalEquilibriumModelUtil::deleteFile(fileName);


        for(int i=0; i<=numberOfEvalPoints; i++)
        {
            //water
            Scalar currentWaterPressure = waterPressure_lowerBound + i*waterDeltaP;
            Scalar currentWaterDensity = returnDensityLiquidAndViscosity(temp, currentWaterPressure)[0];
            Scalar currentWaterDerivative = computeDerivativeWater(currentWaterPressure, varEps, temp);

            Scalar waterDensity_coeff = (1/currentWaterDensity) * currentWaterDerivative;


            //gas
            Scalar currentGasPressure = gasPressure_lowerBound + i*gasDeltaP;
            Scalar currentGasDensity = returnDensityGasAndViscosity(temp, currentGasPressure)[0];
            Scalar currentGasDerivative = computeDerivativeGas(currentGasPressure, varEps, temp);

            Scalar gasDensity_coeff = (1/currentGasDensity) * currentGasDerivative;

            std::vector<Scalar> coeffs = {waterDensity_coeff, gasDensity_coeff};
            VerticalEquilibriumModelUtil::writeToFile<double>(coeffs, labelList, fileName);
        }
    }

    /*!
     * \brief Compute the derivative of gas density with regard to gas density
     *
     * \param pressureG gas pressure
     * \param varEps deflection coefficient for derivative
     * \param temp temperature
     */
    Scalar computeDerivativeGas(Scalar pressureG,
                                Scalar varEps,
                                Scalar temp)
    {
        Scalar leftDensityGas = returnDensityGasAndViscosity(temp, pressureG)[0];
        Scalar rightDensityGas = returnDensityGasAndViscosity(temp, pressureG+varEps*pressureG)[0];
        Scalar localDerivative = (rightDensityGas-leftDensityGas)/(varEps*pressureG);

        return localDerivative;
    }

    /*!
     * \brief Compute the derivative of water density with regard to gas density
     *
     * \param pressureW water pressure
     * \param varEps deflection coefficient for derivative
     * \param temp temperature
     */
    Scalar computeDerivativeWater(Scalar pressureW,
                                  Scalar varEps,
                                  Scalar temp)
    {
        Scalar leftDensityWater = returnDensityLiquidAndViscosity(temp, pressureW)[0];
        Scalar rightDensityWater = returnDensityLiquidAndViscosity(temp, pressureW+varEps*pressureW)[0];
        Scalar localDerivative = (rightDensityWater-leftDensityWater)/(varEps*pressureW);

        return localDerivative;
    }


    TimeMeasuring& getTimeMeasurer() const
    {
        return timeMeasurer_;
    }


    /*!
     * \brief Return the coarse-scale element to which a given fine-scale element (index) belongs
     * \param fineElementIdx index of the fine-scale element
     */
    const Element& returnCorrespondingCoarseElement(int fineElementIdx) const
    {
        return fineScaleMap_->find(fineElementIdx)->second;
    }


    //call on fine scale problem
    void updateFineSol()
    {
        MyVolumeVariables volVarsVE;

        auto gridGeometryVEFine = this->gridGeometry();
        auto numElements = gridGeometryVEFine.numScv(); //only true for CCTPFA

        for(int localVEIndex=0; localVEIndex<numElements; localVEIndex++)
        {
            auto localVEFineElement = gridGeometryVEFine.element(localVEIndex);
            auto fvGeometryVEFine = localView(gridGeometryVEFine);
            fvGeometryVEFine.bind(localVEFineElement);
            const auto elemSolVEFine = elementSolution(localVEFineElement, (*fineSolution_), gridGeometryVEFine);
//             const auto elemSolVE = {0.0, 0.0}; //dummy values

            for (const auto& scv : scvs(fvGeometryVEFine))
            {
                volVarsVE.update(elemSolVEFine, *this, localVEFineElement, scv);
                (*fineSolution_)[localVEIndex][0] = volVarsVE.pressure(0);
                (*fineSolution_)[localVEIndex][1] = volVarsVE.saturation(1);
            }
        }
    }





private:

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    // only used in 3D case
    bool onFrontBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    // only used in 3D case
    bool onBackBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - eps_;
    }


    /*!
     * \brief Exports the multimap mapColumns_ to the spatialParams
     *
     * \param mapColumns mutltimap with column indices as keys and elements within respective column as values
     */
    void setSpatialParamsMapColumns_(std::shared_ptr<std::multimap<int, Element>> mapColumns)
    {
        this->spatialParams().setMapColumns(mapColumns);
    }

    /*!
     * \brief Calculates the coarse permeability in spatial params once and stores the values in a container
     */
    void calcPermeabilityAndPorosityCoarseInSpatialParams_()
    {
        this->spatialParams().calcSpatialParamsPermeabilityAndPorosityCoarse();
    }



    static constexpr Scalar eps_ = 1e-6;

    std::string problemName_;
    bool isCoarseVEModel_;

    CellArray numberOfCells_;
    CellArray upperRightDomain_;
    CellArray lowerLeftDomain_;
    Scalar deltaX_;
    Scalar deltaY_;
    Scalar deltaZ_;
    std::shared_ptr<std::multimap<int, Element>> mapColumns_;
    std::shared_ptr<std::map<int, Element>> fineScaleMap_;
    std::shared_ptr<QuantityReconstructor> quantityReconstructor_;
    std::shared_ptr<QuantityStorage> quantityStorage_;

    std::shared_ptr<Problem> otherProblem_;

    std::shared_ptr<SolutionVector> fineSolution_, coarseSolution_;

    TimeLoopPtr timeLoop_;

    //! Spatially varying parameters
    std::shared_ptr<SpatialParams> spatialParams_;

    TimeMeasuring &timeMeasurer_;

    Scalar injectionRate_;
};

} // end namespace Dumux

#endif
