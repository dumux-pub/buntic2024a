// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTests
 * \brief Test for the two-phase porousmedium flow model.
 */
#include <config.h>

#include <iostream>
#include <sstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/loadsolution.hh>

#include <dumux/verticalequilibriumcommon/quantityreconstruction.hh>
#include <dumux/verticalequilibriumcommon/util.hh>

#include "properties.hh"

#include <dumux/timemeasurer/timemeasurements.hh>

#include <chrono>

#ifndef DIFFMETHOD
#define DIFFMETHOD DiffMethod::numeric
#endif


int main(int argc, char** argv)
{
    std::chrono::steady_clock::time_point mainStart = std::chrono::steady_clock::now();

    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TwoPVECompressibleTpfa;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    //initialize machine learning manager
    TimeMeasuring timeMeasurer{};


    //--------------------------------------------RESTRICTIONS CHECK--------------------------------------------
    VerticalEquilibriumModelUtil::checkForExceptionsOnlyVE();
    //----------------------------------------------------------------------------------------------------------

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    VerticalEquilibriumModelUtil::initializeFineGrid<TypeTag>(gridManager); //from dumux/porousmediumflow/2pve/util.hh
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManagerCoarse;
    VerticalEquilibriumModelUtil::initializeCoarseGrid<TypeTag>(gridManagerCoarse); //from dumux/porousmediumflow/2pve/util.hh

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    bool isMinimalRun = getParam<bool>("VerticalEquilibrium.MinimalRun");

    //--------------------------------------------GRIDVIEWS--------------------------------------------
    // we compute on the leaf grid view
    const auto& leafGridViewCoarse = gridManagerCoarse.grid().leafGridView();
    const auto& leafGridViewFine = gridManager.grid().leafGridView();

    //--------------------------------------------GRIDGEOMETRIES--------------------------------------------
    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometryCoarse = std::make_shared<GridGeometry>(leafGridViewCoarse);
    auto gridGeometryFine = std::make_shared<GridGeometry>(leafGridViewFine);

    //--------------------------------------------QUANTITYRECONSTRUCTOR--------------------------------------------
    // create an object of the QuantityReconstructor that contains functions for quantity reconstruction to the fine scale
    using QuantityReconstructor = TwoPVEQuantityReconst<TypeTag>;
    QuantityReconstructor quantityReconstructorFine{timeMeasurer};
    auto quantityReconstructorFineShared = std::make_shared<QuantityReconstructor>(quantityReconstructorFine);

    //--------------------------------------------PROBLEMS--------------------------------------------
    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problemCoarse = std::make_shared<Problem>(gridGeometryCoarse, gridGeometryFine, quantityReconstructorFineShared, timeMeasurer);
    auto problemFine = std::make_shared<Problem>(gridGeometryFine, problemCoarse);
    problemCoarse->setOtherProblem(problemFine);
    problemFine->setOtherProblem(problemCoarse);

    //--------------------------------------------TIMELOOP--------------------------------------------
    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoopCoarse = std::make_shared<TimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoopCoarse->setMaxTimeStepSize(maxDt);
    problemCoarse->setTimeLoop(timeLoopCoarse); //TODO: delete
    problemFine->setTimeLoop(timeLoopCoarse); //TODO: delete

    //--------------------------------------------SOLUTION VECTOR--------------------------------------------
    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    auto xCoarse = std::make_shared<SolutionVector>(gridGeometryCoarse->numDofs());
    auto xFine = std::make_shared<SolutionVector>(gridGeometryFine->numDofs());

    //--------------------------------------------APPLY INITIAL SOLUTIONS--------------------------------------------
    problemCoarse->applyInitialSolution(*xCoarse);
    problemFine->applyInitialSolution(*xFine);

    auto xOldCoarse = *xCoarse;
    auto xOldFine = *xFine;

    // required for fine-scale volume variables and fine-scale volume variables caching.
    problemCoarse->setFineSolution(xFine);
    problemCoarse->setCoarseSolution(xCoarse);
    problemFine->setFineSolution(xFine);
    problemFine->setCoarseSolution(xCoarse);

    //--------------------------------------------GRIDVARIABLES--------------------------------------------
    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariablesCoarse = std::make_shared<GridVariables>(problemCoarse, gridGeometryCoarse);
    gridVariablesCoarse->init(*xCoarse);
    auto gridVariablesFine = std::make_shared<GridVariables>(problemFine, gridGeometryFine);
    gridVariablesFine->init(*xFine);

    //--------------------------------------------VTKOUTPUTMODULES--------------------------------------------
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    constexpr int dim = GridView::dimension;
    bool enableLenses = getParam<bool>("SpatialParams.EnableLenses", false); //one time read in

    using CellArray = std::array<unsigned int, GridView::dimensionworld>;
    const auto numberCellsFine = getParam<CellArray>("Grid.FineCells"); //one time read in
    size_t numCellsDim1 = numberCellsFine[0];
    size_t numCellsDim2 = 0;
    if constexpr(dim==3)
        numCellsDim2 = numberCellsFine[1];
    size_t numCellsDim3 = numberCellsFine[dim-1];


    std::string VECoarseOutputName{};
    VECoarseOutputName = problemCoarse->name() + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariables, SolutionVector> vtkWriterCoarse(*gridVariablesCoarse, *xCoarse, VECoarseOutputName);
    using IOFieldsVECoarse = GetPropType<TypeTag, Properties::IOFields>;
    using VelocityOutputVE = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriterCoarse.addVelocityOutput(std::make_shared<VelocityOutputVE>(*gridVariablesCoarse));
    IOFieldsVECoarse::initOutputModule(vtkWriterCoarse, 1);
    vtkWriterCoarse.write(0.0);

    // initialize the darcyVEFine vtk output module
    std::string VEFineOutputName{};
    VEFineOutputName = problemFine->name() + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariables, SolutionVector> vtkWriterReconstructedSolution(*gridVariablesFine, *xFine, VEFineOutputName);
    IOFieldsVECoarse::initOutputModule(vtkWriterReconstructedSolution, 0); // Add model specific output fields
    vtkWriterReconstructedSolution.write(0.0);

    //--------------------------------------------ASSEMBLER--------------------------------------------
    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DIFFMETHOD>;
    auto assemblerCoarse = std::make_shared<Assembler>(problemCoarse, gridGeometryCoarse, gridVariablesCoarse, timeLoopCoarse, xOldCoarse);

    //--------------------------------------------SOLVER--------------------------------------------
    // the linear solver
    using LinearSolver = ILUBiCGSTABIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolverCoarse = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolverCoarse(assemblerCoarse, linearSolverCoarse);

    //--------------------------------------------HELPERS FOR PRINT MASS BALANCE--------------------------------------------
    auto totalInjectedGasMass = 0.0;
    //============================================FINISH HELPERS FOR PRINT MASS BALANCE============================================

    //Write meta data file
    std::vector<std::string> metaDataNameContainer = {"timeStep", "simulatedTime", "timeStepSize", "wallTime", "numberElementsVE", "numberElementsVEFine", "numElemDim1", "numElemDim2", "numElemDim3", "enableLenses"};
    std::string fileName = "onlyVE_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3)+ "_Lenses" + std::to_string(enableLenses) + "_metaData.txt";
    //delete existing files
    VerticalEquilibriumModelUtil::deleteFile(fileName);

    //assemble and write meta data
    std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorOnlyVEModel<GridGeometry, TimeLoop<Scalar>>
        (*gridGeometryCoarse, *gridGeometryFine, *timeLoopCoarse, {numCellsDim1,numCellsDim2,numCellsDim3}, enableLenses);
    VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);

    //store compressibility coefficients for checking
    problemCoarse->infoOnCompressibilityCoeff();


//     using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
//     using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;

    // time loop coarse
    timeLoopCoarse->start(); do
    {
//         std::cout << "-----START PART 1 plume spread computation-----" << std::endl;
//         auto oldPlumeSpread = VerticalEquilibriumModelUtil::findPlumeDistance<Scalar, GridGeometry, SolutionVector>(gridGeometryCoarse, *xCoarse, 1e-6);
//         auto maxVelocityX = VerticalEquilibriumModelUtil::findMaxVelocity<Scalar, GridVariables, GridGeometry, SolutionVector, GridView, VelocityOutput>(*gridVariablesCoarse, gridGeometryCoarse, *xCoarse, leafGridViewCoarse);
//         auto oldTime = timeLoopCoarse->time();
//         std::cout << "------END PART 1 plume spread computation------" << std::endl;


        // solve the non-linear system with time step control
        nonLinearSolverCoarse.solve(*xCoarse, *timeLoopCoarse);

        // necessary when using volvars caching option
        problemFine->updateFineSol(); //the only place fineSol is potentially used in is in util.hh for printing the mass balance. Its volvars depend only on the coarse-scale solution
        gridVariablesFine->update(*xFine);

        if(!isMinimalRun)
        {
            //--------------------------------------------HELPER FUNCTION FOR PRINT MASS BALANCE--------------------------------------------
            //helper function to print out wetting-phase and non-wetting-phase masses in the respective domains during the simulation
            VerticalEquilibriumModelUtil::printMassBalanceVE<GridGeometry, SolutionVector, Problem, TimeLoop<Scalar>, Scalar>(totalInjectedGasMass, gridGeometryCoarse, gridGeometryFine, xCoarse, problemCoarse, timeLoopCoarse);
        }


//         //print time derivative of pressure for estimating magnitude of compressibility impact
//         for(int i=0; i<(*xCoarse).size(); i++)
//         {
//             Scalar pDiff = (*xCoarse)[i][0] - xOldCoarse[i][0];
//             Scalar tDiff = timeLoopCoarse->timeStepSize();
//
//             std::cout << "For index i: " << i << ", the time derivative of the wetting-phase pressure is: " << pDiff/tDiff << std::endl;
//         }


        // make the new solution the old solution
        xOldCoarse = *xCoarse;
        xOldFine = *xFine; //TODO: not necessary
        gridVariablesCoarse->advanceTimeStep();
        gridVariablesFine->advanceTimeStep(); //TODO: not necessary

        // advance to the time loop to the next step
        timeLoopCoarse->advanceTimeStep();

        if(!isMinimalRun)
        {
            vtkWriterCoarse.write(timeLoopCoarse->time());
            vtkWriterReconstructedSolution.write(timeLoopCoarse->time());
        }



//         std::cout << "-----START PART 2 plume spread computation-----" << std::endl;
//         auto maxVelocityX_new = VerticalEquilibriumModelUtil::findMaxVelocity<Scalar, GridVariables, GridGeometry, SolutionVector, GridView, VelocityOutput>(*gridVariablesCoarse, gridGeometryCoarse, *xCoarse, leafGridViewCoarse);
//         auto timeDiff = timeLoopCoarse->time()-oldTime;
//         auto newPlumeSpread = VerticalEquilibriumModelUtil::findPlumeDistance<Scalar, GridGeometry, SolutionVector>(gridGeometryCoarse, *xCoarse, 1e-6);
//         Scalar trueSpread = newPlumeSpread-oldPlumeSpread;
//         Scalar predictedSpread = maxVelocityX[0]*timeDiff;
//         Scalar predictedSpreadPorosityCleaned = (1.0/problemCoarse->spatialParams().porosityFineAtElement({}))*predictedSpread;
//         std::vector<Scalar> plumeSpreadsToStore = {(Scalar)timeLoopCoarse->timeStepIndex(), timeLoopCoarse->time(), trueSpread, predictedSpread, predictedSpreadPorosityCleaned};
//         std::vector<std::string> nameContainer = {"timeStep", "time", "trueSpread", "predictedSpread", "predictedSpreadPorosityCleaned"};
//         VerticalEquilibriumModelUtil::writeToFile<Scalar>(plumeSpreadsToStore, nameContainer, "VECoarse_plumeSpread_plotData.txt");
//         std::cout << "------END PART 2 plume spread computation------" << std::endl;


        // report statistics of this time step
        timeLoopCoarse->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoopCoarse->setTimeStepSize(nonLinearSolverCoarse.suggestTimeStepSize(timeLoopCoarse->timeStepSize()));

        std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorOnlyVEModel<GridGeometry, TimeLoop<Scalar>>
            (*gridGeometryCoarse, *gridGeometryFine, *timeLoopCoarse, {numCellsDim1,numCellsDim2,numCellsDim3}, enableLenses);
        VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);

    } while (!timeLoopCoarse->finished());

    vtkWriterCoarse.write(timeLoopCoarse->time());
    vtkWriterReconstructedSolution.write(timeLoopCoarse->time());

    // output some Newton statistics
    nonLinearSolverCoarse.report();

    timeLoopCoarse->finalize(leafGridViewCoarse.comm());


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    std::chrono::steady_clock::time_point mainEnd = std::chrono::steady_clock::now();
    const double mainTotalTime = std::chrono::duration_cast<std::chrono::microseconds>(mainEnd - mainStart).count()/1000000.0;

    timeMeasurer.printAllTimesForPureVE(quantityReconstructorFineShared->getAverageNumberOfLocalNewtonIterations());
    std::cout << "- Total simulation time:                                                                           " << mainTotalTime << " seconds." << std::endl;
    std::cout << "- Total simulation time MINUS total VE time:                                                       " << mainTotalTime - timeMeasurer.getTotalVETime() << " seconds." << std::endl;
    std::cout << "- Total VE time:                                                                                   " << timeMeasurer.getTotalVETime() << " seconds." << std::endl;

    return 0;
} // end main
