// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_TEST_TWOPVE_PROPERTIES_HH
#define DUMUX_TEST_TWOPVE_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/air.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/porousmediumflow/2pve/model.hh>
#include <dumux/porousmediumflow/immiscible/localresidual.hh>

#include "problem.hh"
#include "spatialparams.hh"

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct TwoPVECompressible { using InheritsFrom = std::tuple<TwoPVE>; };
struct TwoPVECompressibleTpfa { using InheritsFrom = std::tuple<TwoPVECompressible, CCTpfaModel>; };
struct TwoPVECompressibleMpfa { using InheritsFrom = std::tuple<TwoPVECompressible, CCMpfaModel>; };
struct TwoPVECompressibleBox { using InheritsFrom = std::tuple<TwoPVECompressible, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
// struct Grid<TypeTag, TTag::TwoPVECompressible> { using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double, 2>>; };
struct Grid<TypeTag, TTag::TwoPVECompressible> { using type = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPVECompressible> { using type = TwoPVETestProblem<TypeTag>; };

// the local residual containing the analytic derivative methods
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::TwoPVECompressible> { using type = ImmiscibleLocalResidual<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPVECompressible>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::H2O<Scalar> >;
    using NonwettingPhase = FluidSystems::OnePGas<Scalar, Components::CH4<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPVECompressible>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = TwoPTestSpatialParams<GridGeometry, Scalar>;
};

// Enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPVECompressible> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPVECompressible> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::TwoPVECompressible> { static constexpr bool value = false; };

// Maybe enable the box-interface solver, required for Newton solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::TwoPVECompressible> { static constexpr bool value = ENABLEINTERFACESOLVER; };
} // end namespace Dumux::Properties

#endif
