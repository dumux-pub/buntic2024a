#Copy this file into the proper test folder within the build-cmake folder and run it with "python3"
#First build/make all necessary tests
import subprocess
import os

# Change the working directory to the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__)) #this python script is meant to be executed in the build-cmake folder and not in the test folder
os.chdir(script_dir)

#GLOBAL VARIABLES
currentNumberOfDataSets = 1

#brief: Runs simulations if necessary and return stored data set. If a folder for a requested set of parameters is already present, that execution of that respective simulation is skipped.
# \param parameter_tuple: contains run-time parameters for a simulation
def execute_single_run(parameter_tuple):
    numHorizontalCells = parameter_tuple[0]
    numVerticalCells = parameter_tuple[1]
    enableLenses = parameter_tuple[2]
    minimalRun = parameter_tuple[3]
    folderName = f"pureFD_{numHorizontalCells}x0x{numVerticalCells}_Lenses{enableLenses}"
    # crucialNamePart = f"{numHorizontalCells}x{numVerticalCells}_Lenses{enableLenses}"
    crucialNamePart2 = f"{numHorizontalCells}x0x{numVerticalCells}_Lenses{enableLenses}"

    global currentNumberOfDataSets
    print(f"Currently at simulation {currentNumberOfDataSets} ({crucialNamePart2})... ")

    #skip next steps if folder already exists
    if not os.path.isdir(folderName):
        #run the simulation
        subprocess.run([f"make test_2p_compressible_tpfa"], shell=True)
        subprocess.run([f"./test_2p_compressible_tpfa -LinearSolver.Verbosity 2 -TimeLoop.TEnd 4.32e5 -Grid.Cells \"{numHorizontalCells} {numVerticalCells}\" -Grid.UpperRight \"250 30\" -Grid.LowerLeft \"0 0\" -BoundaryConditions.InjectionRate \"-5.835e-4\" -SpatialParams.EnableLenses {enableLenses} -Vtk.MinimalRun {minimalRun} > {folderName}_log.txt"], shell=True)

        #move all data to respective folder
        subprocess.run([f"mkdir {folderName}"], shell=True)
        subprocess.run([f"mv *{crucialNamePart2}*.vtu *{crucialNamePart2}*.pvd *{crucialNamePart2}*.txt {folderName}"], shell=True)
    else:
        print(f"Folder for {folderName} already exists. Skipping simulation run.")

    currentNumberOfDataSets+=1


#START 'MAIN' -------------------------------------------------------------------------------------------------------------------------------------------
print(f"Running simulations for pure fullD model.")

parameter_tuple0 = (125, 15, 1, 1)
execute_single_run(parameter_tuple0);

parameter_tuple1 = (125, 30, 1, 1)
execute_single_run(parameter_tuple1);

parameter_tuple2 = (250, 15, 1, 1)
execute_single_run(parameter_tuple2);

parameter_tuple3 = (150, 25, 1, 1)
execute_single_run(parameter_tuple3);

parameter_tuple4 = (250, 30, 1, 1)
execute_single_run(parameter_tuple4);

parameter_tuple5 = (250, 60, 1, 1)
execute_single_run(parameter_tuple5);

parameter_tuple6 = (500, 30, 1, 1)
execute_single_run(parameter_tuple6);

parameter_tuple7 = (500, 60, 1, 1)
execute_single_run(parameter_tuple7);

parameter_tuple8 = (1000, 30, 1, 1)
execute_single_run(parameter_tuple8);

parameter_tuple9 = (1000, 60, 1, 1)
execute_single_run(parameter_tuple9);

parameter_tuple10 = (250, 120, 1, 1)
execute_single_run(parameter_tuple10);

parameter_tuple11 = (500, 30, 0, 1)
execute_single_run(parameter_tuple11);

parameter_tuple12 = (500, 60, 0, 1)
execute_single_run(parameter_tuple12);

parameter_tuple13 = (1000, 60, 0, 1)
execute_single_run(parameter_tuple13);

print(f"---------------------------------Finished running simulations for pure fullD model.---------------------------------")