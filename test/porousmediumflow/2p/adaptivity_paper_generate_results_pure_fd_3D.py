#Copy this file into the proper test folder within the build-cmake folder and run it with "python3"
#First build/make all necessary tests
import subprocess
import os

# Change the working directory to the script's directory
script_dir = os.path.dirname(os.path.abspath(__file__)) #this python script is meant to be executed in the build-cmake folder and not in the test folder
os.chdir(script_dir)

#GLOBAL VARIABLES
currentNumberOfDataSets = 1

#brief: Runs simulations if necessary and return stored data set. If a folder for a requested set of parameters is already present, that execution of that respective simulation is skipped.
# \param parameter_tuple: contains run-time parameters for a simulation
def execute_single_run(parameter_tuple):
    numHorizontalCells = parameter_tuple[0]
    numYCells = parameter_tuple[1]
    numVerticalCells = parameter_tuple[2]
    enableLenses = parameter_tuple[3]
    minimalRun = parameter_tuple[4]
    folderName = f"pureFD_{numHorizontalCells}x{numYCells}x{numVerticalCells}_Lenses{enableLenses}"
    # crucialNamePart = f"{numHorizontalCells}x{numVerticalCells}_Lenses{enableLenses}"
    crucialNamePart2 = f"{numHorizontalCells}x{numYCells}x{numVerticalCells}_Lenses{enableLenses}"

    global currentNumberOfDataSets
    print(f"Currently at simulation {currentNumberOfDataSets} ({crucialNamePart2})... ")

    #skip next steps if folder already exists
    if not os.path.isdir(folderName):
        #run the simulation
        subprocess.run([f"make test_2p_compressible_tpfa"], shell=True)
        subprocess.run([f"./test_2p_compressible_tpfa -LinearSolver.Verbosity 2 -TimeLoop.TEnd 5e6 -TimeLoop.DtInitial 0.125 -Grid.Cells \"{numHorizontalCells} {numYCells} {numVerticalCells}\" -Grid.UpperRight \"250 100 30\" -Grid.LowerLeft \"0 0 0\" -BoundaryConditions.InjectionRate \"-1.45875e-3\" -SpatialParams.EnableLenses {enableLenses} -Vtk.MinimalRun {minimalRun} > {folderName}_log.txt"], shell=True)

        #move all data to respective folder
        subprocess.run([f"mkdir {folderName}"], shell=True)
        subprocess.run([f"mv *{crucialNamePart2}*.vtu *{crucialNamePart2}*.pvd *{crucialNamePart2}*.txt {folderName}"], shell=True)
    else:
        print(f"Folder for {folderName} already exists. Skipping simulation run.")

    currentNumberOfDataSets+=1


#START 'MAIN' -------------------------------------------------------------------------------------------------------------------------------------------
print(f"Running simulations for pure fullD model.")

parameter_tuple0 = (80, 40, 10, 1, 0)
execute_single_run(parameter_tuple0);

parameter_tuple1 = (160, 20, 20, 1, 0)
execute_single_run(parameter_tuple1);

parameter_tuple2 = (160, 20, 10, 1, 0)
execute_single_run(parameter_tuple2);

parameter_tuple3 = (160, 10, 20, 1, 0)
execute_single_run(parameter_tuple3);

parameter_tuple4 = (160, 40, 10, 1, 0)
execute_single_run(parameter_tuple4);

print(f"---------------------------------Finished running simulations for pure fullD model.---------------------------------")