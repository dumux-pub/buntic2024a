dune_symlink_to_source_files(FILES "params.input")
dune_symlink_to_source_files(FILES "adaptivity_paper_generate_results_pure_fd.py")
dune_symlink_to_source_files(FILES "adaptivity_paper_generate_results_pure_fd_3D.py")

# set(CMAKE_BUILD_TYPE Debug)

dumux_add_test(NAME test_2p_compressible_tpfa
              SOURCES main.cc
              LABELS porousmediumflow 2p)