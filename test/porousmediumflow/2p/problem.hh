// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH
#define DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/problem.hh>

#include <dumux/verticalequilibriumcommon/util.hh>

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux {

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        pressureH2OIdx = Indices::pressureIdx,
        saturationGasIdx = Indices::saturationIdx,
        contiGasEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        gasPhaseIdx = FluidSystem::phase1Idx
    };

    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using MyVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;

public:
    TwoPTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        problemName_ = getParam<std::string>("Problem.Name");

        const Scalar yWidth = getParam<std::vector<Scalar>>("Grid.UpperRight")[1] - getParam<std::vector<Scalar>>("Grid.LowerLeft")[1];
        auto yCells = getParam<std::vector<Scalar>>("Grid.Cells")[1];
        deltaY_ = yWidth/yCells;

        const Scalar zWidth = getParam<std::vector<Scalar>>("Grid.UpperRight")[dim-1] - getParam<std::vector<Scalar>>("Grid.LowerLeft")[dim-1];
        auto zCells = getParam<std::vector<Scalar>>("Grid.Cells")[dim-1];
        deltaZ_ = zWidth/zCells;
        injectionRate_ = getParam<double>("BoundaryConditions.InjectionRate");
        mapColumnsFullD_ = std::make_shared<std::multimap<int, Element>>(VerticalEquilibriumModelUtil::createFullDMultiMapCoarseToFine<Element, GridGeometry, Scalar>(*gridGeometry, "", false));

        // //iterate over fullD elements via
        // for(int columnIdx = 0; columnIdx<xCells_; columnIdx++)
        // {
        //     const auto range = mapColumnsFullD_->equal_range(columnIdx);
        //     for(auto iterator = range.first; iterator!=range.second; iterator++)
        //     {
        //         const auto eIdx = gridGeometry->elementMapper().index(iterator->second);
        //     }
        // }
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(this->spatialParams().temperatureAtPos(globalPos));
        fluidState.setPressure(waterPhaseIdx, /*pressure=*/1.0e7);
        fluidState.setPressure(gasPhaseIdx, /*pressure=*/1.0e7);
        Scalar densityW = FluidSystem::density(fluidState, waterPhaseIdx);
        
        if(onRightBoundary_(globalPos))
        {
            if(getParam<bool>("Problem.EnableGravity"))
                values[pressureH2OIdx] = 1.0e7 - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
            else
                values[pressureH2OIdx] = 1.0e7;
            values[saturationGasIdx] = 0.0;
        }
        else
        {
            values[pressureH2OIdx] = 1.0e7;
            values[saturationGasIdx] = 1.0;
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        if constexpr(dim==2)
        {
            NumEqVector values(0.0);
            if (onLeftBoundary_(globalPos))
                values[contiGasEqIdx] = injectionRate_; // kg / (m * s)
            return values;
        }
        else if constexpr(dim==3)
        {
            NumEqVector values(0.0);
            // put well at middle of y-direction boundary
            Scalar halfDepthBox = (this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1])/2.0;

            if(onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos) || onFrontBoundary_(globalPos) || onBackBoundary_(globalPos))
            {
                values[contiGasEqIdx] = 0.0;
            }
            else if(onLeftBoundary_(globalPos) && halfDepthBox<=(globalPos[1]+deltaY_/2.0) && halfDepthBox>(globalPos[1]-deltaY_/2.0) )
            {
                // std::cout << "injection in cell @: " << globalPos << std::endl;
                // values[contiGasEqIdx] = injectionRate_;
                values[contiGasEqIdx] = injectionRate_/(deltaY_);
            }

            return values;
        }
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        GetPropType<TypeTag, Properties::FluidState> fluidState;
        fluidState.setTemperature(this->spatialParams().temperatureAtPos(globalPos));
        fluidState.setPressure(waterPhaseIdx, /*pressure=*/1e7);
        fluidState.setPressure(gasPhaseIdx, /*pressure=*/1e7);
        Scalar densityW = FluidSystem::density(fluidState, waterPhaseIdx);

        // hydrostatic pressure
        values[pressureH2OIdx] = 1.0e7 - densityW * this->spatialParams().gravity(globalPos).two_norm() * globalPos[dim-1];
        values[saturationGasIdx] = 0.0;
        
        return values;
    }


private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }

    // only used in 3D case
    bool onFrontBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    // only used in 3D case
    bool onBackBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < this->gridGeometry().bBoxMin()[dim-1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] > this->gridGeometry().bBoxMax()[dim-1] - eps_;
    }

    static constexpr Scalar eps_ = 1e-6;
    std::string problemName_;

    Scalar deltaY_, deltaZ_;
    Scalar injectionRate_;
    std::shared_ptr<std::multimap<int, Element>> mapColumnsFullD_;
};

} // end namespace Dumux

#endif
