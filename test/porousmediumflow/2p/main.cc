// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTests
 * \brief Test for the two-phase porousmedium flow model.
 */
#include <config.h>

#include <iostream>
#include <sstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/loadsolution.hh>

#include "properties.hh"

#include <dumux/verticalequilibriumcommon/util.hh>

#ifndef DIFFMETHOD
#define DIFFMETHOD DiffMethod::numeric
#endif

int main(int argc, char** argv)
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TwoPCompressibleTpfa;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    bool isMinimalRun = getParam<bool>("Vtk.MinimalRun");

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = getParam<Scalar>("Restart.Time", 0);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(gridGeometry->numDofs());
    if (restartTime > 0)
    {
        using IOFields = GetPropType<TypeTag, Properties::IOFields>;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        const auto fileName = getParam<std::string>("Restart.File");
        loadSolution(x, fileName, createPVNameFunction<IOFields, PrimaryVariables, ModelTraits, FluidSystem>(), *gridGeometry);
    }
    else
        problem->applyInitialSolution(x);
    auto xOld = x;

//     // maybe update the interface parameters
//     if (ENABLEINTERFACESOLVER)
//         problem->spatialParams().updateMaterialInterfaces(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);


    //necessary for util functions (find max velocity)
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    constexpr int dim = GridView::dimension;

    using CellArray = std::array<unsigned int, GridView::dimensionworld>;
    const auto numberCellsFullD = getParam<CellArray>("Grid.Cells"); //one time read in
    size_t numCellsDim1 = numberCellsFullD[0];
    size_t numCellsDim2 = 0;
    if constexpr(dim==3)
        numCellsDim2 = numberCellsFullD[1];
    size_t numCellsDim3 = numberCellsFullD[dim-1];

    bool enableLenses = getParam<bool>("SpatialParams.EnableLenses", false); //one time read in

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    std::string fineDOutputName{};
    // if constexpr(dim==2)
    //     fineDOutputName = problem->name() + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim3) + "_Lenses" + std::to_string(enableLenses);
    // else if constexpr(dim==3)
    //     fineDOutputName = problem->name() + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3)+ "_Lenses" + std::to_string(enableLenses);
    fineDOutputName = problem->name() + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3)+ "_Lenses" + std::to_string(enableLenses);
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, fineDOutputName);
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVariables));
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(restartTime);

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DIFFMETHOD>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = ILUBiCGSTABIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();
    
    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);


    //--------------------------------------------HELPERS FOR PRINT MASS BALANCE--------------------------------------------
    auto totalInjectedGasMass = 0.0;
    using VolumeVariablesFullD = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::VolumeVariables;
    //============================================FINISH HELPERS FOR PRINT MASS BALANCE============================================





    //Write meta data file
    std::vector<std::string> metaDataNameContainer = {"timeStep", "simulatedTime", "timeStepSize", "wallTime", "numberElementsFullD", "numElemDim1", "numElemDim2", "numElemDim3", "enableLenses"};
    std::string fileName = "pureFD_" + std::to_string(numCellsDim1) + "x" + std::to_string(numCellsDim2) + "x" + std::to_string(numCellsDim3)+ "_Lenses" + std::to_string(enableLenses) + "_metaData.txt";
    //delete existing files
    VerticalEquilibriumModelUtil::deleteFile(fileName);

    //assemble and write meta data
    std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorOnlyFullDModel<GridGeometry, TimeLoop<Scalar>>
        (*gridGeometry, *timeLoop, {numCellsDim1, numCellsDim2, numCellsDim3}, enableLenses);
    VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);

    // time loop
    timeLoop->start(); do
    {
//         std::cout << "-----START plume spread computation-----" << std::endl;
//         auto oldPlumeSpread = VerticalEquilibriumModelUtil::findPlumeDistance<Scalar, GridGeometry, SolutionVector>(gridGeometry, x, 1e-6);
//         auto maxVelocityX = VerticalEquilibriumModelUtil::findMaxVelocity<Scalar, GridVariables, GridGeometry, SolutionVector, GridView, VelocityOutput>(*gridVariables, gridGeometry, x, leafGridView);
//         auto oldTime = timeLoop->time();
//         std::cout << "------END plume spread computation------" << std::endl;

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        if(!isMinimalRun)
        {
            //--------------------------------------------HELPER FUNCTION FOR PRINT MASS BALANCE--------------------------------------------
            //helper function to print out wetting-phase and non-wetting-phase masses in the respective domains during the simulation
            VerticalEquilibriumModelUtil::printMassBalanceFullD<GridGeometry, SolutionVector, Problem, TimeLoop<Scalar>, Scalar, VolumeVariablesFullD>(totalInjectedGasMass, gridGeometry, x, problem, timeLoop);
        }

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        if(!isMinimalRun)
        {
            // write vtk output
            vtkWriter.write(timeLoop->time());
        }



//         std::cout << "-----START plume spread computation-----" << std::endl;
//         auto timeDiff = timeLoop->time()-oldTime;
//         auto newPlumeSpread = VerticalEquilibriumModelUtil::findPlumeDistance<Scalar, GridGeometry, SolutionVector>(gridGeometry, x, 1e-6);
//         auto maxVelocityX_new = VerticalEquilibriumModelUtil::findMaxVelocity<Scalar, GridVariables, GridGeometry, SolutionVector, GridView, VelocityOutput>(*gridVariables, gridGeometry, x, leafGridView);
//         // uncomment to write file for plume spread distances
//         Scalar trueSpread = newPlumeSpread-oldPlumeSpread;
//         Scalar predictedSpread = maxVelocityX[0]*timeDiff;
//         Scalar predictedSpreadPorosityCleaned = (1.0/problem->spatialParams().porosityAtPos({}))*predictedSpread;
//         std::vector<std::string> nameContainer = {"timeStep", "time", "trueSpread", "predictedSpread", "predictedSpreadPorosityCleaned"};
//         std::vector<Scalar> plumeSpreadsToStore = {(Scalar)timeLoop->timeStepIndex(), timeLoop->time(), trueSpread, predictedSpread, predictedSpreadPorosityCleaned};
//         VerticalEquilibriumModelUtil::writeToFile<Scalar>(plumeSpreadsToStore, nameContainer, "fullD_plumeSpread_plotData.txt");
//         std::cout << "------END plume spread computation------" << std::endl;


        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));


        std::vector<double> metaDataVector = VerticalEquilibriumModelUtil::assembleMetaDataVectorOnlyFullDModel<GridGeometry, TimeLoop<Scalar>>
            (*gridGeometry, *timeLoop, {numCellsDim1, numCellsDim2, numCellsDim3}, enableLenses);
        VerticalEquilibriumModelUtil::writeToFile<double>(metaDataVector, metaDataNameContainer, fileName);
    } while (!timeLoop->finished());


    //output last time step
    vtkWriter.write(timeLoop->time());

    // output some Newton statistics
    nonLinearSolver.report();

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
